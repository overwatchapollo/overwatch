# install back end
mkdir -p /bin/overwatch
cp package/overwatch.jar /bin/overwatch
cp package/overwatch /root/Documents/overwatch
cp package/startService /root/Desktop
cp package/stopService /root/Desktop

chmod u+x /root/Documents/overwatch/overwatch
chmod u+x /root/Desktop/startService
chmod u+x /root/Desktop/stopService

#install front end
yarn global add serve

mkdir -p /etc/overwatchFront
cp -r client/build/* /etc/overwatchFront
cp package/overwatchFront.service /etc/systemd/system/overwatchFront.service
cp package/serve.json /root/Documents/overwatch
cp package/overwatchFront /root/Documents/overwatch

chmod u+x /root/Documents/overwatch/overwatchFront

# restart services
systemctl daemon-reload
/root/Desktop/stopService
/root/Desktop/startService