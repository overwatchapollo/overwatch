export class User {
    id: number;
    username: string;
    password: string;
    email: string;
    roleNames: Array<string>;

    constructor() {
        this.id = -1;
        this.username = "";
        this.password = "";
        this.email = "";
        this.roleNames = new Array(0);
    }
}