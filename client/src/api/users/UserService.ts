import { User } from './User';

export function getUsersDetailed(): Promise<Array<User>> {
    return fetch('/api/users/detailed')
    .then(response => {
        if (response.status === 200) {
            return response.json();
        } else {
            return Promise.reject(`Unable to get group information`)
        }
    })
    .then(data => data as Array<User>)
};

export function deleteUser(user: User): Promise<Response> {
    return fetch(`/api/users/user/${user.id}`, {
        method: 'DELETE'
    })
}

export function userExists(user: string): Promise<boolean> {
    return fetch(`/api/users/exist/${user}`)
        .then(x => x.status === 200)
        .catch(() => false);
}

export function updateEmail(userId: number, email: string):Promise<Response> {
    const passwordUpdate = new User();
    passwordUpdate.id = userId;
    passwordUpdate.email = email;
    return fetch(`/api/users/user/${userId}`, {
        method: 'PUT',
        body: JSON.stringify(passwordUpdate),
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        }
    })
}

export function updatePassword(user: User, password: string):Promise<Response> {
    const passwordUpdate = new User();
    passwordUpdate.id = user.id;
    passwordUpdate.password = password;
    return fetch(`/api/users/user/${user.id}`, {
        method: 'PUT',
        body: JSON.stringify(passwordUpdate),
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        }
    })
}