import { TopicMessage } from './TopicMessage';
import { Observable } from 'rxjs';
export type ICallbacks = (message: MessageEvent) => void;
export type ITopicCallback = (message: TopicMessage) => void;

export type MqttMessage = {topic: string, payload: unknown};

export interface IMessageStream {
    connect(username: string, password: string): void;
    disconnect(): void;
    messageObservable(): Observable<MqttMessage>;
    observeTopic(topic: string): Observable<MqttMessage>
    observeTopicRegex(topic: string | RegExp): Observable<MqttMessage>;
}