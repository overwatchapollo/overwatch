class TopicMessage {
    topic: string;
    payload: any;

    constructor() {
        this.topic = "";
        this.payload = undefined;
    }
}
export {TopicMessage};