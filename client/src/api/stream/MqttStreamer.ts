import { IMessageStream, MqttMessage } from './IMessageStream';
import { connect, MqttClient } from 'mqtt';
import { Observable, Subject, ReplaySubject } from 'rxjs';
import { filter } from 'rxjs/operators';

export class MqttStreamer implements IMessageStream {
    
    private subject: Subject<MqttMessage>;
    private topicSubscriptions: ReplaySubject<string>;
    private client?: MqttClient;
    
    constructor() {
        this.subject = new Subject();
        this.topicSubscriptions = new ReplaySubject();
        this.client = undefined;
    }

    connect(username: string, password: string): void {
        const protocolPrefix = window.location.protocol === 'https:' ? 'wss:' : 'ws:';
        const suffix = window.location.protocol === 'https:' ? ":8884" : ":15675/ws";
        const addr = protocolPrefix + "//" + window.location.hostname + suffix;
        const newClient = connect(addr, {username: username, password: password});
        this.client = newClient;

        this.client.on('connect', () => {
            this.topicSubscriptions.subscribe(x => newClient.subscribe(x, {qos:1}))
        })
        this.client.on('message', 
            (topic, message) => {
                this.handleMessage(topic, message);
            }
        )
    }

    disconnect(): void {
        const client = this.client;
        if (client) {
            client.end();
        }
    }

    observeTopicRegex(topic: string | RegExp): Observable<MqttMessage> {
        throw new Error("Method not implemented.");
    }

    messageObservable(): Observable<MqttMessage> {
        return this.subject;
    }

    observeTopic(topic: string): Observable<MqttMessage> {
        this.topicSubscriptions.next(topic);
        return this.subject.pipe(filter(msg => msg.topic === topic));
    }

    private handleMessage(topicString: string, message:Buffer) {
        const value = JSON.parse(message.toString());
        this.subject.next({topic: topicString, payload: value});
    }
}