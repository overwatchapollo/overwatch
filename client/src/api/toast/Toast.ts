import { IMessageStream } from 'api/stream';
// import { HelpRequest } from 'src/liveSystem/alerts/HelpRequest';
// import { ServerStatusUpdate } from '../server/ServerStatusUpdate';

export class Toast {

    stream: IMessageStream;
    transformMap : Map<string, (obj: any) => string>;
    messageConsumer: Array<(message: string) => void>;

    constructor(stream: IMessageStream) {
        this.stream = stream;
        this.transformMap = new Map();
        this.messageConsumer = new Array(0);
        this.stream.observeTopic("/player/kicked").subscribe(msg => "kicked");
        // this.register("/request/help", (obj:HelpRequest) => obj.renderMessage());
        // this.register("/server/update", (obj:ServerStatusUpdate) => obj.renderUpdateMessage());
    }

    registerConsumer(consumer: (message: string) => void) {
        this.messageConsumer.push(consumer);
    }

    register(topic: string, transform: (obj: any) => string): void {
        this.transformMap.set(topic, transform);
    }

    unregister(topic: string) {
        this.transformMap.delete(topic);
    }
}