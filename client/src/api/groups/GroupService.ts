import { PlayerGroup } from './PlayerGroup';
import { User } from 'api/users/User';
import { ServerConfiguration } from 'api/server/ServerConfiguration';
import { PermissionWrapper } from 'api/PermissionWrapper';
import { UserRole } from 'api/roles/UserRole';
import { RoleMember } from 'api/roles/RoleMember';

export interface FullGroupDetails {
    id: number;
    name: string;
    roles: Array<PermissionWrapper<UserRole>>;
    members: Array<RoleMember>;
    servers: Array<ServerConfiguration>;
}

export function createGroup(group: PlayerGroup): Promise<Response> {
    return fetch('/api/groups/group', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(group)
    })
}

export function deleteGroup(id: number): Promise<Response> {
    return fetch(`/api/groups/group/${id}`, {
        method:'DELETE',
    })
}

export function addRoleToGroup(groupId: number, roleName: string): Promise<Response> {
    return fetch(`/api/groups/group/${groupId}/role`, {
        method:'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({name: roleName})
    })
}

export function removeRoleFromGroup(groupId: number, roleId: string | number): Promise<Response> {
    return fetch(`/api/groups/group/${groupId}/role/${roleId}`, {
        method:'DELETE',
    })
}

export function getGroups() : Promise<Array<FullGroupDetails>> {
    return fetch(`/api/groups`)
        .then(x => {
            if (x.status === 200) {
                return x.json()
            } else {
                return Promise.reject(x.status)
            }
            
        })
        .then(x => x as Array<FullGroupDetails>);
};

export function getMembers(id: number): Promise<Array<User>> {
    return fetch(`/api/groups/group/${id}/members`)
        .then(x => x.json())
        .then(x => x as Array<User>);
}

export function addUserToGroup(groupId: number, userId: number): Promise<Response> {
    return fetch(`/api/groups/group/${groupId}/members/${userId}`, {
        method: 'POST'
    })
};

export function removeMemberFromGroup(groupId: number, userId: number): Promise<Response> {
    return fetch(`/api/groups/group/${groupId}/members/${userId}`, {
        method: 'DELETE'
    })
}

export function getServersOfGroup(groupId: number): Promise<Array<ServerConfiguration>> {
    return fetch(`/api/groups/group/${groupId}/assets`)
        .then(x => x.json())
        .then(x => x as Array<ServerConfiguration>);
}