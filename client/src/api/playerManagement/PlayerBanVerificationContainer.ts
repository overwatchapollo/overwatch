export class PlayerBanVerificationContainer {
    alias: string;
    aliasList: Array<string>;
    guid: string;

    constructor() {
        this.alias = "";
        this.aliasList = new Array(0);
        this.guid = "";
    }
}