import { BanRecord } from './BanRecord';

export interface ServerBanList {
    serverMrid: number;
    banRecords: Array<BanRecord>;
    totalBans: number;
}