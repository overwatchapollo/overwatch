export class BanRecord {
    id: number;
    guid: string;
    duration: string;
    message: string;

    constructor() {
        this.id = -1;
        this.guid = "";
        this.duration = "";
        this.message = "";
    }
}