export interface Note {
    id: number;
    note: string;
    createdBy: string;
    createdOn: string;
}