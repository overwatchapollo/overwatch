export class SteamProfileReport {
    steamid: string;
    personaname: string;
    profileurl: string;
    avatar: string;

    constructor() {
        this.steamid = "";
        this.personaname = "";
        this.profileurl = "";
        this.avatar = "";
    }
}