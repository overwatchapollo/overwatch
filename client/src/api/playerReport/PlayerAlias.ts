export class PlayerAlias {
    alias: string;
    used: number;
    serverId: number;
    firstUsed: Date;
    lastUsed: Date;

    constructor(json: PlayerAlias) {
        this.alias = json.alias;
        this.used = json.used;
        this.serverId = json.serverId;
        this.firstUsed = new Date(json.firstUsed);
        this.lastUsed = new Date(json.lastUsed);
    }
}