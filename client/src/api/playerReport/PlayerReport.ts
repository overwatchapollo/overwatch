import { PlayerAlias } from './PlayerAlias';
import { KickHistoryAudit } from 'api/management/kickaudit/KickHistoryAudit';
import { Note } from 'api/playerManagement/Note';

export class PlayerReport {
    guid: string;
    name: string;
    aliasRecords: Array<PlayerAlias>;
    firstSeen: Date;
    lastSeen: Date;
    recordCount: number;
    kickHistory: Array<KickHistoryAudit>;
    notes: Array<Note>

    constructor(json: PlayerReport) {
        this.name = json.name;
        this.guid = json.guid;
        this.aliasRecords = json.aliasRecords.map(value => new PlayerAlias(value));
        this.firstSeen = new Date(json.firstSeen);
        this.lastSeen = new Date(json.lastSeen);
        this.recordCount = json.recordCount;
        this.kickHistory = json.kickHistory;
        this.notes = json.notes;
    }

    getLastName(): string {
        const aliasList = this.aliasRecords;
        const namealias = aliasList.sort((a: PlayerAlias, b: PlayerAlias) => {
            const alast = new Date(a.lastUsed);
            const blast = new Date(b.lastUsed);
            return alast>blast 
                ? -1 
                : alast<blast 
                    ? 1 : 0;
        })[aliasList.length - 1];
        const alias = namealias === undefined ? "unknown" : namealias.alias;
        return alias;
    }
}