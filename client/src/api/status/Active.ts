export class ServerActiveRest {
    getServerActive(serverMrid: string) : Promise<string | undefined> {
        return fetch('/api/status/rcon/active/' + serverMrid)
        .then(response => response.json())
        .catch(error => undefined);
    }
}