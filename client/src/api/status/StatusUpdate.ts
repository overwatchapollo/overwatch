export class StatusUpdate {
    serverId: number;
    serverName: string;
    version: string;
    authenticated: boolean;
    active: boolean;
    maxPlayers: number;
    currentPlayers: number;

    constructor() {
        this.serverId = -1;
        this.serverName = "";
        this.version = "";
        this.authenticated = false;
        this.active = false;
        this.maxPlayers = 0;
        this.currentPlayers = 0;
    }
}