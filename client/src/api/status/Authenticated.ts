export class ServerAuthenticatedRest {
    getServerAuthenticated(serverMrid: string) : Promise<string | undefined> {
        return fetch('/api/status/rcon/loggedin/' + serverMrid)
        .then(response => response.json())
        .catch(error => undefined);
    }
}