import { StatusUpdate } from './StatusUpdate';

export function getStatus(serverId: number) : Promise<StatusUpdate> {
    return fetch(`/api/server/v1/status/${serverId}`)
        .then(x => x.json())
        .catch(error => getDefaultUpdate(serverId));
};

function getDefaultUpdate(serverId: number) : StatusUpdate {
    const update = new StatusUpdate();
    update.active = false;
    update.authenticated = false;
    update.serverId = serverId;
    update.serverName = "Unknown";
    update.version = "Unknown";
    return update;
}