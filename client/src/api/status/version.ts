export class ServerRconVersionRest {
    getServerRconVersion(serverMrid: string) : Promise<string | undefined> {
        return fetch('/api/status/rcon/version/' + serverMrid)
        .then(response => response.json())
        .catch(error => undefined);
    }
}