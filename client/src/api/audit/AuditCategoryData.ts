import { AuditCategory } from './AuditCategory';
import { AuditRecord } from './AuditRecord';

export interface AuditCategoryData extends AuditCategory {
    info: Array<AuditRecord>;
}