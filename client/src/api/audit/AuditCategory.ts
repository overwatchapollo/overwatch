export interface AuditCategory {
    categoryId: number;
    categoryName: string;
}