import { AuditCategory } from './AuditCategory';
import { AuditCategoryData } from './AuditCategoryData';

const endpoint = '/api/audit/v1'

export function getAuditLogCategory(id: number): Promise<AuditCategoryData> {
    return fetch(`${endpoint}/logs/${id}`)
        .then(x => x.json().then(y => y as AuditCategoryData));
}

export function getAuditCategories(): Promise<Array<AuditCategory>> {
    return fetch(`${endpoint}/categories`)
        .then(x => x.json())
        .then(x => x as Array<AuditCategory>);
}