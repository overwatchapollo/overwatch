export interface AuditRecord {
    invoker: any;
    timestamp: string;
    message: string;
    category: string;
}