import { ServerConfiguration } from './ServerConfiguration';
import { IMessageStream } from 'api/stream/IMessageStream';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ServerRegisterRequest } from './ServerRegisterRequest';
import { ServerModificationRequest } from './ServerModificationRequest';
import { ServerStatusUpdate } from './ServerStatusUpdate';
import { ServerWrapper } from './ServerWrapper';
export class ServerConfigurationService {

    /**
     * Message streamer to get async server updates.
     */
    private stream: IMessageStream;

    constructor(stream: IMessageStream) {
        this.stream = stream;
    }

    getServerUpdateObservable(serverId: number): Observable<ServerStatusUpdate> {
        return this.stream.observeTopic(`server/${serverId}/status`)
            .pipe(map(x => x.payload as ServerStatusUpdate));
    }
}

export function getServerPermissionConfiguration(): Promise<Array<ServerWrapper>> {
    return fetch('/api/server/v1/connectionPermissions')
        .then(response => response.status === 200 ? response.json() : Array<ServerWrapper>(0))
        .catch(error => Array<ServerWrapper>(0))
        .then((x: Array<ServerWrapper>) => x.sort((a, b) => a.data.serverId < b.data.serverId ? -1 : 1));
}

export function getConfigurationList(): Promise<Array<ServerConfiguration>> {
    return fetch('/api/server/v1/connection')
        .then(response => response.status === 200 ? response.json() : Array<ServerConfiguration>(0))
        .catch(error => Array<ServerConfiguration>(0))
        .then((x: Array<ServerConfiguration>) => x.sort((a, b) => a.serverId < b.serverId ? -1 : 1));
}

export function softRestart(serverId: number): Promise<Response> {
    return fetch(`/api/rcon/restart/${serverId}`, {
        method: 'POST'
    })
}

export function updateConfiguration(server:ServerModificationRequest): Promise<Response> {
    const output = server as any;
    if (server.password === undefined) {
        output.password = null;
    }
    
    return fetch(`api/server/v1/connection/${server.serverId}`, {
        method:'put',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(output),
    })
}

export function submitConfiguration(server: ServerRegisterRequest): Promise<Response> {
    return fetch('api/server/v1/connection', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(server)
    })
}

export function deleteConfiguration(serverId: number): Promise<Response> {
    return fetch(`api/server/v1/connection/${serverId}`, {
        method: 'DELETE',
    })
}

export function setNewServer(serverId: number): Promise<Response> {
    return fetch(`api/rcon/v1/runner/${serverId}`, {
        method: 'POST'
    })
}
