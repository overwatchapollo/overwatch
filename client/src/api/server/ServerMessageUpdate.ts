export interface ServerMessageUpdate {
    serverId: number;
    connected: boolean;
}