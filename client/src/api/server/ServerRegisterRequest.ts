export interface ServerRegisterRequest {
    serverName: string;
    hostName: string;
    port: number;
    password: string;
    players: number;
}