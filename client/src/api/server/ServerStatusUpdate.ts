export interface ServerStatusUpdate {
    serverId: number;
    connected: boolean;
    authenticated: boolean;
    state: string;
}