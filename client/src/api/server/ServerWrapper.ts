import { ServerConfiguration } from './ServerConfiguration';
import { PermissionWrapper } from 'api/PermissionWrapper';

export interface ServerWrapper extends PermissionWrapper<ServerConfiguration> {
    status: string;
}