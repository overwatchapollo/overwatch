import { ServerStatusUpdate } from './ServerStatusUpdate';

export interface ServerConfiguration {
    serverName: string;
    serverId: number;
    ownerId: number;
    ipAddress: string;
    port: number;
    password: string;
    maxPlayers: number;
    connected: boolean;
    state: ServerStatusUpdate;
}