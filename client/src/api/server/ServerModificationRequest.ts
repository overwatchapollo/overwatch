export interface ServerModificationRequest {
    serverId: number;
    serverName: string;
    hostName: string;
    port: number;
    password?: string;
    players: number;
}