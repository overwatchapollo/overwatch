export interface PermissionWrapper<T> {
    actions: Array<string>;
    data: T;
}