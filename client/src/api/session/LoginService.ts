import { User } from '../users/User';
import { RestResponse } from 'api/RestResponse';

const endpoint = '/api/session'
const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
}

const postHeaders = {
    method: 'POST',
    headers: headers
}

export function loginRequest(user: string, pass: string): Promise<User> {
    const postMessage = { username: user, password: pass }
    return fetch(`${endpoint}/v2/login`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(postMessage)
    })
        .then(x => {
            if (x.status !== 200 ) {
                return Promise.reject("err")
            }
            return x.json()
        })
        .then(x => x as RestResponse<User>)
        .then(x => x.data === undefined || x.data === null
            ? Promise.reject(x.error) 
            : Promise.resolve(x.data));
}

export function logoutRequest() : Promise<Response> {
    return fetch(`${endpoint}/v1/logout`, postHeaders);
}

export function getMyDetails(): Promise<User> {
    return fetch(`${endpoint}/v2/me`, {
        method: 'GET',
        headers: headers
    })
    .then(x => x.json()
        .then(y => y as RestResponse<User>)
        .then(y => y.data === undefined || y.data === null
            ? Promise.reject(y.error) 
            : Promise.resolve(y.data))
    );
}

export function registerUser(user: string, pass: string, email: string): Promise<Response> {
    const postMessage = {
        "username": user,
        "password": pass,
        "email": email,
    }
    return fetch(`${endpoint}/v1/register`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(postMessage)
    });
}