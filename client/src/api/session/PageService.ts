
export function getPageList(): Promise<Array<string>> {
    return fetch('/api/session/v1/pages')
        .then(response => response.json())
        .then(data => data as Array<string>)
        .catch(error => Array<string>(0));
}