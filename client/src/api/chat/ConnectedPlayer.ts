export class Player {
    serverId: number;
    sessionId: number;
    ipAddress: string;
    ping: number;
    guid: string;
    name: string;
    connected: boolean;
    kicked: boolean;
    banned: boolean;
    disconnectionMessage: string;

    constructor() {
        this.serverId = -1;
        this.sessionId = -1;
        this.ipAddress = "";
        this.ping = -1;
        this.guid = "";
        this.name = "";
        this.connected = false;
        this.kicked = false;
        this.banned = false;
        this.disconnectionMessage = "";
    }
}
