import {Player} from './ConnectedPlayer';

export class ChatMessage {
    messageId: number;
    serverId: number;
    rcon: boolean;
    timeStamp: string;
    sender: string;
    channel: string;
    message: string;
    senderObject: Player;

    constructor() {
        this.messageId = -1;
        this.serverId = -1;
        this.rcon = false;
        this.timeStamp = "";
        this.sender = "";
        this.channel = "";
        this.message = "";
        this.senderObject = new Player();
    }
}