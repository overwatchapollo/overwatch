import { ChatMessage } from './ChatMessage';
import { Observable } from 'rxjs';
import { IMessageStream } from 'api/stream/IMessageStream';
import { map } from 'rxjs/operators';

export class ChatService {
    /**
     * Message streamer to get async chat updates.
     */
    stream: IMessageStream;

    constructor(stream: IMessageStream) {
        this.stream = stream;
    }

    getChatUpdatesObservable(serverId: number) : Observable<ChatMessage> {
        return this.stream.observeTopic(`server/${serverId}/update/chat/message`)
            .pipe(map(x => x.payload as ChatMessage));
    }
}

export function sendAll(serverMrid: number, postMessage: string): Promise<Response> {
    return fetch(`/api/chat/sendAll/${serverMrid}`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(postMessage)
    });
}

export function whisper(serverMrid: string | number, playerId: string | number, postMessage: string): Promise<Response> {
    return fetch(`/api/chat/send/${serverMrid}/${playerId}`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(postMessage)
    });
}

export function getChatLog(serverMrid: number, lim: number) : Promise<Array<ChatMessage>> {
    const fetchString = `/api/chat/log/${serverMrid}?${new URLSearchParams({limit: lim.toString()}).toString()}`;
    return fetch(fetchString)
        .then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                return Promise.reject();
            }
        })
        .then(data => data as Array<ChatMessage>);
}