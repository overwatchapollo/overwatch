import { ChatMessage } from 'api/chat/ChatMessage';

export class HelpRequest {
    helpRequest: string;
    originalMessage: ChatMessage;

    constructor() {
        this.helpRequest = "";
        this.originalMessage = new ChatMessage();
    }

    renderMessage(): string {
        return "Help has been requested from " + this.originalMessage.sender + ": " + this.helpRequest;
    }
}