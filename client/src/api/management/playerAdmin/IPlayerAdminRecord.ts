export interface IPlayerAdminRecord {
    serverId: number;
    username: string;
    guid: string;
    steamId: string;
    level: number;
}