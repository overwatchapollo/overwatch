import { PlayerAdministrationRecord } from './PlayerAdministrationRecord';

export interface AggregatePlayerAdminRecord {
    readonly username: string;
    readonly guid: string;
    readonly steamId: string;
    records: Array<PlayerAdministrationRecord>;
}