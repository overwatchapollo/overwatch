import { PlayerAdministrationRecord } from './PlayerAdministrationRecord';
import { IPlayerAdminRecord } from './IPlayerAdminRecord';

export function getAdmins(serverId: number): Promise<Array<IPlayerAdminRecord>> {
    return fetch(`/api/admin/ingame/list/${serverId}`)
        .then(response => response.status === 200 ? response.json() : Array<IPlayerAdminRecord>(0))
        .then((x: Array<IPlayerAdminRecord>) => x.map(y=> {
            y.serverId = serverId;
            return y;
        }))
        .catch(() => Array<IPlayerAdminRecord>(0));
}

export function addAdmin(serverId: number, newRecord: PlayerAdministrationRecord): Promise<Response> {
    return fetch(`/api/admin/ingame/players/${serverId}`, {
        method: "post",
        headers: {
            'Accept':'application/json',
            'Content-Type':'application/json'
        },
        body: JSON.stringify(newRecord)
    })
}

export function removeAdmin(serverId: number, guid: string): Promise<Response> {
    return fetch(`/api/admin/ingame/players/${serverId}`, {
        method: "delete",
        headers: {
            'Accept':'application/json',
            'Content-Type':'application/json'
        },
        body: JSON.stringify({guid})
    })
}