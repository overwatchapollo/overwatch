export interface PlayerAdministrationRecord {
    server: number;
    serverName: string;
    guid: string;
    level: number;
}