import { KickHistoryAudit } from 'api/management/kickaudit/KickHistoryAudit';

export class KickAuditService {
    getKickAuditHistory(serverMrid: string): Promise<Array<KickHistoryAudit>> {
        return fetch('/api/rcon/kickHistory/' + serverMrid)
            .then(response => response.json())
            .then(data => data as Array<KickHistoryAudit>)
            .catch(error => Array<KickHistoryAudit>(0));
    }
}

export function getKickAuditHistory(serverMrid: string): Promise<Array<KickHistoryAudit>> {
    return fetch(`/api/rcon/kickHistory/${serverMrid}`)
        .then(response => response.json())
        .then(data => data as Array<KickHistoryAudit>)
        .catch(error => Array<KickHistoryAudit>(0));
};