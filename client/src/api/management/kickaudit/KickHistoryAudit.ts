export class KickHistoryAudit {
    requester: string;
    serverLocation: number;
    kickedUser: string;
    message: string;
    timeStamp: string;
    successful: boolean;
    ipAddress: string;

    constructor() {
        this.requester = "";
        this.serverLocation = -1;
        this.kickedUser = "";
        this.message = "";
        this.timeStamp = "";
        this.ipAddress = "";
        this.successful = false;
    }

    renderMessage(): string {
        return this.kickedUser + " has been kicked by " + this.requester;
    }
}