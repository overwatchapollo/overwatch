export interface FeatureToggle {
    featureName: string;
    enabled: boolean;
}