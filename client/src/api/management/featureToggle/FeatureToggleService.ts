import { FeatureToggle } from './FeatureToggle';

export function getFeatureToggles(): Promise<Array<FeatureToggle>> {
    return fetch('/api/features')
        .then(x => {
            if (x.status === 200) {
                return x.json();
            }
            throw new Error("hi")})
        .then(x => x as Array<FeatureToggle>)
}

export function setFeatureToggle(toggle: FeatureToggle, value: boolean): Promise<Response> {
    return fetch('/api/features', {
        method:"post",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({featureName: toggle.featureName, enabled: value})
    })
}