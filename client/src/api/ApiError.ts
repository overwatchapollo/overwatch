/**
 * Encapsulation of API Errors.
 */
export interface ApiError {
    status: string;
    message: string;
    errors: Array<string>;
}