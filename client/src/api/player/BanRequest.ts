export class BanRequest {
    playerGuid: string;
    duration: number;
    reason: string;
    serverLocation: string;
    sessionId?: number;

    constructor(playerGuid: string, duration: number, reason: string, serverLocation: string) {
        this.playerGuid = playerGuid;
        this.duration = duration;
        this.reason = reason;
        this.serverLocation = serverLocation;
    }
}