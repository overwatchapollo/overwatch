import { ConversionData } from './ConversionData';

export class GuidConversion {
    error: boolean;
    data: ConversionData;

    constructor() {
        this.error = true;
        this.data = new ConversionData();
    }
}