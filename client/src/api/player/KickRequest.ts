export class KickRequest {
    serverLocation: number;
    sessionId: string;
    playerName: string;
    message: string;
    constructor() {
        this.serverLocation = -1;
        this.sessionId = "";
        this.playerName = "";
        this.message = "";
    }
}
