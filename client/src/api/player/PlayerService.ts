import {KickRequest} from "./KickRequest"
import { Player } from 'api/chat/ConnectedPlayer';
import { BanRequest } from 'api/player/BanRequest';
import { IMessageStream } from 'api/stream';
import { ServerBanList } from 'api/playerManagement/ServerBanList';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export class PlayerService {

    /**
     * Message streamer to get async player updates.
     */
    stream: IMessageStream;

    constructor(stream: IMessageStream) {
        this.stream = stream;
    }

    getSteamProfile() {
        return Promise.reject();
    }

    getPlayerUpdatesObservable(serverId: number):Observable<Player> {
        return this.stream.observeTopic(`server/${serverId}/update/player`).pipe(map(x => x.payload as Player));
    }

    getStalePlayerListObservable(serverId: number): Observable<unknown> {
        return this.stream.observeTopic(`server/${serverId}/update/player/stale`);
    }
}

export function kickPlayer(serverLocation: number, sessionId: string, playerName: string, message: string): Promise<Response> {
    const request = new KickRequest();
    request.sessionId = sessionId;
    request.playerName = playerName;
    request.serverLocation = serverLocation;
    request.message = message;
    return kickPlayerRequest(request);
};

export function kickPlayerRequest(kickBody: KickRequest):Promise<Response> {
    return fetch('/api/rcon/kick', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(kickBody)
    });
}

export function getPlayerList(serverMrid: number) : Promise<Array<Player>> {
    return fetch(`/api/rcon/players/${serverMrid}`)
        .then(response => {
            if (response.status === 200) {
                return response.json();
            } else {
                return Promise.reject();
            }
            
        })
        .then(data => data as Array<Player>);
}

export function getArchivePlayerList(serverMrid: number) : Promise<Array<Player>> {
    return fetch(`/api/rcon/archive/players/${serverMrid}`)
        .then(response => response.json())
        .then(data => data as Array<Player>)
        .catch(error => Array<Player>(0));
}

export function getBanList(serverMrid: number, limit?: number, skip?: number): Promise<ServerBanList> {
    let url = `/api/rcon/banList/${serverMrid}`
    let params = 0;
    if (limit) {
        url = `${url}${params === 0 ? "?" : "&"}limit=${limit}`;
        params += 1;
    }

    if (skip) {
        url = `${url}${params === 0 ? "?" : "&"}skip=${skip}`;
        params += 1;
    }
    return fetch(url)
        .then(response => response.status === 200 
            ? response.json() 
            : new Promise(() => ({serverMrid, banRecords: [], error: true})))
        .then(data => data as ServerBanList);
}

export function forceBanListRefresh(serverMrid: number): Promise<Response> {
    return fetch(`/api/rcon/banRefresh/${serverMrid}`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });
}

export function getBanListPlayer(serverMrid: number, guid: string): Promise<ServerBanList> {
    const params = new URLSearchParams({guid});
    const fetchString = `/api/rcon/banList/${serverMrid}?${params}`;
    return fetch(fetchString)
        .then(response => response.status === 200 ? response.json() : Promise.reject())
        .then(data => data as ServerBanList);
}

export function unbanPlayer(serverLocation: number, banId: number) {
    return fetch('/api/rcon/banRemove', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({serverLocation, banId})
    });
};

export function refreshPlayerList(serverMrid: number) : Promise<Response> {
    return fetch(`/api/rcon/players/refresh/${serverMrid}`, {
        method: 'POST'
    })
}

export function submitBan(request: BanRequest): Promise<Response> {
    return fetch('/api/rcon/ban', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(request)
    });
}

export function submitRconCommand(serverId: number, data: string): Promise<Response> {
    return fetch(`/api/rconLog/server/${serverId}`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({data})
    });
}