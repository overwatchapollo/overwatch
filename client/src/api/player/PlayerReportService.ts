import { PlayerReport } from 'api/playerReport/PlayerReport';
import { Note } from 'api/playerManagement/Note';

export function getPlayerReport(guid: string) : Promise<PlayerReport> {
    const fetchString = `/api/player/by-guid/${guid}`;
    return fetch(fetchString)
        .then(response => {
            if (response.status === 200) {
                return response.json()
            }
            return Promise.reject();
        })
        .then(data => {
            const player = new PlayerReport(data as PlayerReport);
            return player;
        });
}

export function getPlayerReportName(playerName: string) : Promise<Array<PlayerReport>> {
    const fetchString = `/api/player/by-name?${new URLSearchParams({name: playerName}).toString()}`;
    return fetch(fetchString)
        .then(response => response.json())
        .then(data => (data as Array<PlayerReport>).map(value => new PlayerReport(value)))
}

export function getPlayerNotes(guid: string): Promise<Array<Note>> {
    const fetchString = `/api/player/notes/${guid}`;
    return fetch(fetchString)
        .then(response => response.json())
        .then(data => (data as Array<Note>))
};

export function deletePlayerNote(id: number) : Promise<Response> {
    const fetchString = `/api/player/note/${id}`;
    return fetch(fetchString, { method: "delete" });
};

export function modifyPlayerNote(id: number, note: string): Promise<Response> {
    const fetchString = `/api/player/notes/${id}`;
    return fetch(fetchString, {
            method: "put",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({note})
        })
}

export function addPlayerNote(guid: string, note: string): Promise<Response> {
    const fetchString = `/api/player/notes/${guid}`;
    return fetch(fetchString, {
        method:"post",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({note})
    })
}