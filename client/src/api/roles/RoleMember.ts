export interface RoleMember {
    id: number;
    userName: string;
}