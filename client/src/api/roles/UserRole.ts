import { RoleMember } from './RoleMember';

export interface Permission {
    permission: string;
    name: string;
    description: string;
}

export interface UserRole {
    id: string;
    name: string;
    permissions: Array<Permission>;
    members?: Array<RoleMember>;
}