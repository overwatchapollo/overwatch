import { UserRole } from './UserRole';
import { PermissionWrapper } from 'api/PermissionWrapper';

export interface Permission {
    id: number;
    name: string;
}

export function getRoles(): Promise<Array<UserRole>> {
    return fetch('/api/roles')
    .then(response => response.json())
    .then(data => data as Array<UserRole>)
    .catch(error => Array<UserRole>(0));
};

export function getVisibleRoles(): Promise<Array<PermissionWrapper<UserRole>>> {
    return fetch (`/api/roles/visible`)
        .then(response => response.json())
        .then(data => data as any);
}

export function createRole(roleName: string): Promise<UserRole> {
    return fetch('/api/roles', {
        method:'post',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({roleName: roleName})
    }) 
        .then(response => response.json())
        .then(data => data as UserRole);
}

export function getPermissionsOfRole(roleId: number): Promise<Array<string>> {
    return fetch(`/api/roles/${roleId}/permissions`)
        .then(response => response.json())
        .then(data => data as Array<string>)
        .catch(error => Array<string>(0));
}

export function addPermissionToRole(roleId: string | number, permission: string): Promise<Response> {
    return fetch(`/api/roles/${roleId}/permissions`, {
        method:'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            permission: permission
        })
    })
}

export function removePermissionFromRole(roleId: string, permission: string): Promise<Response> {
    return fetch(`/api/roles/${roleId}/permissions`, {
        method:'delete',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            permission: permission
        })
    })
}

export function addRoleToUserName(username: string, roleId: number):Promise<Response> {
    return fetch(`/api/roles/v1/${roleId}/members/${username}`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    });
}

export function addRoleToUser(userId: number, roleId: number):Promise<Response> {
    return fetch(`/api/roles/${roleId}/members`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "userId": userId
        })
    });
}

export function removeRoleFromUser(userId: number, roleId: number): Promise<Response> {
    return fetch(`/api/roles/${roleId}/members`, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "userId": userId
        })
    });
}

export function deleteRole(roleId: number | string): Promise<Response> {
    return fetch(`/api/roles/${roleId}`, {
        method: 'DELETE'
    });
}