import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker, { unregister } from './registerServiceWorker';
import store from 'store';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';
unregister();

ReactDOM.render(<AppContainer>
  <Provider store={store}>
        <App />
  </Provider>
</AppContainer>, document.getElementById("root"));
registerServiceWorker();
