import React from 'react';
import { Typography, Grid, Link } from '@material-ui/core';
import { PageContainer } from 'components/pagecontainer/PageContainer';

import { Link as RouterLink } from 'react-router-dom';
import { mapSessionStateToProps, mapSessionDispatchToProps } from 'container/SessionContainer';
import { connect } from 'react-redux';
import { SessionState } from 'reducers/SessionReducer';

interface IHome {
    sessionState: SessionState;
}

export const BasicHome: (props: IHome) => JSX.Element = (props:IHome) => {
        const user = props.sessionState.activeUser;
        return <PageContainer>
            <Grid container={true} direction="column" style={{padding: 8}} spacing={1}>
                <Grid item={true}>
                    <Typography variant="h5">Welcome {user?.username ?? ""}</Typography>
                </Grid>
                <Grid item={true}>
                    <Typography variant="body1">
                        Overwatch-Apollo is a tool for managing game servers through an RCON connection.
                    </Typography>

                    <Typography variant="body1">
                        Why not <Link component={RouterLink} to='/serverManagement' variant="body1">register</Link> a server with us?
                    </Typography>
                </Grid>
                <Grid item={true}>
                    <Typography variant="body1">
                        If you notice an issue, please <a href="https://gitlab.com/overwatchapollo/overwatch/issues">report it!</a>
                    </Typography>
                </Grid>
            </Grid>
        </PageContainer>
}

const Home = connect(mapSessionStateToProps, mapSessionDispatchToProps)(BasicHome);
export {Home};