import * as React from 'react';
import { Divider, Drawer, List, ListSubheader, ListItem, ListItemText, Button, useTheme } from '@material-ui/core';
import styles from './styles';
import { Link } from "react-router-dom";
import { getPageList } from './api/session/PageService';
import { useEffect, useState } from 'react';

interface ISiteNav {
    openSidebar: boolean;
}

interface IPageButton {
    label: string;
    path: string;
}

const pageMap: Map<string, IPageButton> = new Map();

const SiteNavigation: (props: ISiteNav) => JSX.Element = props => {

    const [viewableCategories, setViewableCategories] = useState(new Array<string>(0));
    const [buttonList] = useState(new Map<string, string>());
    
    function populateMap() {
        pageMap.set("SERVER_DASHBOARD", {label: "Server Dashboard", path: "/"});
        pageMap.set("SERVER_STATUS", {label: "Server Status", path: "/"});

        pageMap.set("PLAYER_LOOKUP", {label: "Player Lookup", path: "/playerLookup"});
        pageMap.set("MULTI_BAN", {label: "Multi User Ban", path: "/administration/multiban"});

        pageMap.set("SERVER_MANAGEMENT", {label: "Server Management", path: "/serverManagement"});
        pageMap.set("LOGIN", {label: "Login", path: "/login"});
        pageMap.set("LOGOUT", {label: "Logout", path: "/logout"});
        pageMap.set("REGISTER", {label: "Register", path: "/register"});
    }

    function updatePageList() {
        getPageList()
            .then(pageName => setViewableCategories(pageName));
    }

    function hasAccess(accessLevel: string): boolean {
        return viewableCategories.filter(x => x.localeCompare(accessLevel) === 0).length !== 0;
    }
    useEffect(() => {
        populateMap();
        updatePageList();
    }, []);

    const classes = styles(useTheme());

    const sessionRefs = new Array<INavRef>(0);
    const currentStatusRefs = new Array<INavRef>(0);
    const serverAdminRefs = new Array<INavRef>(0);
    const experimentalRefs = new Array<INavRef>(0);
    
    sessionRefs.push(buildRef('Home', '/'));
    if (viewableCategories.length === 0) {
        sessionRefs.push(buildRef('Login', '/login'));
        sessionRefs.push(buildRef('Register', '/register'));
    } else {
        sessionRefs.push(buildRef('Logout', '/logout'));
        sessionRefs.push(buildRef('Manage Account', '/account'));
    }
    
    if (hasAccess("status")) {
        currentStatusRefs.push(buildRef('Server Status', '/status'));
        currentStatusRefs.push(buildRef('Player List', '/playerList'));
        currentStatusRefs.push(buildRef('Chat Log', '/messageTracker'));
        currentStatusRefs.push(buildRef('Server Overview', '/overview'));
    }
    
    serverAdminRefs.push(buildRef("Access Management", "/accessManagement"));
    serverAdminRefs.push(buildRef("Server Management", "/serverManagement"));
    
    

    if (hasAccess("investigate")) {
        serverAdminRefs.push(buildRef("Player Investigation", "/playerInvestigate"));
    }

    if (hasAccess("userManager")) {
        serverAdminRefs.push(buildRef("User List", "/administration/users"));
        serverAdminRefs.push(buildRef("Role List", "/administration/roles"));
    }
    
    if (hasAccess("siteAdmin")) {
        experimentalRefs.push(buildRef("Player Lookup", "/playerLookup"));
        experimentalRefs.push(buildRef("Multi User Ban", "/administration/multiban"));
    }

    
    if (hasAccess("experimental")) {
        experimentalRefs.push(buildRef("Group Management", "/administration/groups"));
        experimentalRefs.push(buildRef("Kick Audit History", "/exp/kickHistory"));
        experimentalRefs.push(buildRef("Full Ban List", "/administration/banList"));
        experimentalRefs.push(buildRef("Ban Search", "/administration/banSearch"));
        experimentalRefs.push(buildRef("Audit Log", "/administration/audit/log"));
        experimentalRefs.push(buildRef("Admin List", "/administration/playerAdmins"));
        experimentalRefs.push(buildRef("Role Manage", "/administration/roleManage"));
        experimentalRefs.push(buildRef("Feature Toggles", "/administration/featureToggle"));
    }
    
    const buttons: Array<[string, (props: any) => JSX.Element]> 
        = new Array<[string, (props: any) => JSX.Element]>(0);
    buttonList.forEach((path:string, label: string) => {
        buttons.push(generateButton(label, path));
    });
    
    return <nav className={classes.drawer}>
        <Drawer
            variant="persistent"
            anchor={'left'}
            className={classes.drawer}
            classes={{
                paper: classes.drawerPaper,
            }}
            open={props.openSidebar}
            ModalProps={{
                keepMounted: true,
            }}
            >
        <div className={classes.toolbar}>
            <Button color="primary" style={{width: "100%", backgroundColor:"#f96161"}} onClick={event => {
                window.location.href = 'https://gitlab.com/overwatchapollo/overwatch/issues'; 
                }}>Report Issue</Button>
            <Divider />
            <RenderNavList label="Current Server Status" refs={sessionRefs}/>
            <Divider />
            <RenderNavList label="Current Server Status" refs={currentStatusRefs}/>
            <Divider />
            <RenderNavList label="Current Server Status" refs={serverAdminRefs}/>
            <Divider />
            <RenderNavList label="EXPERIMENTAL" refs={experimentalRefs}/>
            <Divider />
        </div>
        </Drawer>
    </nav>   
}

interface INavList {
    label: string;
    refs: Array<INavRef>;
}

const RenderNavList:(props:INavList) => JSX.Element = (props: INavList) => {
    if (props.refs.length === 0) {
        return <div/>
    }
    
    return <List subheader={<ListSubheader>{props.label}</ListSubheader>}>
        {props.refs.map(x => 
            <ListItem button={true} key={x.label} component={Link} to={x.ref}>
                <ListItemText primary={x.label}/>
            </ListItem>
        )}
    </List>
}

interface INavRef {
    label: string;
    ref: string;
}

const buildRef:(label: string, ref: string) => INavRef = (label, ref) => 
{
    const reference:INavRef = {label, ref};
    return reference;
}

function generateButton(title: string, reference: string):[string, ((props: any) => JSX.Element)] {
    return [title,    ((props: any) => <Link to={reference} {...props}/>)];
}


export default SiteNavigation;