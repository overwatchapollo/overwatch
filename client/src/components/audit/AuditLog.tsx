import React from 'react';
import { Paper, Table, TableBody, TableRow, TableCell, Typography, TableHead, Button, Grid } from '@material-ui/core';
import { AuditCategory } from 'api/audit/AuditCategory';
import { AuditCategoryData } from 'api/audit/AuditCategoryData';
import { AuditFilter } from './AuditFilter';
import { getAuditCategories, getAuditLogCategory } from 'api/audit/AuditService';

interface IAuditState {
    auditMap: Map<number, AuditCategoryData>;
    auditCategories: Array<AuditCategory>;
    selectedCategories: Array<number>;
    limit: number;
}

class BasicAuditLog extends React.Component<{}, IAuditState> {
    constructor(props: {}) {
        super(props);
        this.state = {
            auditMap: new Map(),
            auditCategories: new Array(0),
            selectedCategories: new Array(0),
            limit: 100,
        }
    }

    componentDidMount() {
        getAuditCategories()
            .then(x => {
                const newMap = new Map<number, AuditCategoryData>();
                x.forEach(y => newMap.set(y.categoryId, {categoryId: y.categoryId, categoryName: y.categoryName, info: new Array(0)}))
                this.setState({auditCategories: x, auditMap: newMap});
            });
    }

    render() {
        return (<Paper>
            <Grid container={true} direction="column" spacing={2} style={{padding: 8}}>
                <Grid item={true}>
                    <Grid container={true}>
                        <Grid item={true}>
                            <Typography variant="h5">Audit Record</Typography>
                        </Grid>
                        <Grid item={true}>
                            <Button variant="contained" color="primary" onClick={() => this.setState({limit: (this.state.limit + 100)})}>Get next 100</Button>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item={true}>
                    <Typography variant="h6">Categories</Typography>
                </Grid>
                <Grid item={true}>
                    <AuditFilter 
                        auditCategories={this.state.auditCategories} 
                        onCheck={(id, checked) => {
                            getAuditLogCategory(id).then(x => {
                                let selectedArray = this.state.selectedCategories;
                                if (checked) {
                                    selectedArray.push(id);
                                } else {
                                    selectedArray = selectedArray.filter(y => y !== id);
                                }
                                
                                this.setState({selectedCategories: selectedArray, 
                                    auditMap: this.state.auditMap.set(id, x)})
                            })
                        }}/>
                </Grid>
                <Grid item={true}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Invoker</TableCell>
                                    <TableCell>Category</TableCell>
                                    <TableCell>Timestamp</TableCell>
                                    <TableCell>Message</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                        {this.state.selectedCategories
                            .map(x => this.state.auditMap.get(x))
                            .filter(x => x !== undefined)
                            .map(x => {
                                const data = x as AuditCategoryData;
                                data.info?.forEach(y => y.category = x?.categoryName ?? "unknown")
                                return data.info;
                            })
                            .flat()
                            .filter(x => x !== null && x !== undefined)
                            .sort((a,b) => a.timestamp.localeCompare(b.timestamp))
                            .reverse()
                            .slice(0, this.state.limit)
                            .map((y, i) => {
                                const date = new Date(y.timestamp);
                                return <TableRow key={i}>
                                    <TableCell>{y.invoker}</TableCell>
                                    <TableCell>{date.toLocaleString()}</TableCell>
                                    <TableCell>{y.category}</TableCell>
                                    <TableCell>{y.message}</TableCell>
                                </TableRow>
                            })
                        }
                        </TableBody>
                        </Table>
                </Grid>
            </Grid>
        </Paper>);
    }
}

export { BasicAuditLog as AuditLog }