import React, { useState } from "react";
import { Grid, FormControlLabel, Checkbox } from '@material-ui/core';
import { AuditCategory } from 'api/audit/AuditCategory';

interface AuditFilterProps {
    auditCategories: Array<AuditCategory>;
    onCheck:(id: number, checked: boolean) => void;
}

const AuditFilter: (props: AuditFilterProps) => JSX.Element = (props: AuditFilterProps) => {
    const [selectedState, setSelectedState] = useState(new Array<number>(0))
    const { auditCategories, onCheck } = props;

    return <Grid container={true} spacing={2}>
        {auditCategories.sort((a,b) => a.categoryName.localeCompare(b.categoryName)).map(x => <Grid item={true} key={x.categoryId}>
                <FormControlLabel 
                    value={"false"}
                    control={<Checkbox onChange={(event, checked) => {
                        if (checked) {
                            setSelectedState([...selectedState, x.categoryId]);
                        } else {
                            const newArray = selectedState.filter(y => y !== x.categoryId);
                            setSelectedState(newArray)
                        }
                        
                        onCheck(x.categoryId, checked);
                    }}/>}
                    checked={selectedState.includes(x.categoryId)}
                    label={x.categoryName}/>
            </Grid>)}
    </Grid>
}

export { AuditFilter }