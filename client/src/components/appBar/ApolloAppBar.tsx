import React from "react";
import { AppBar, Grid, IconButton, Typography, useTheme } from '@material-ui/core'

import clsx from 'clsx'
import { LoginBar } from 'components/authentication/LoginBar'
import ExitToApp from "@material-ui/icons/ExitToApp"
import AccountButton from 'components/accountButton/AccountButton';
import MenuIcon from '@material-ui/icons/Menu';
import styles from 'styles'
import Home from '@material-ui/icons/Home'
import MenuBar from './MenuBar';
import { RootState } from 'reducers';
import { useSelector } from 'react-redux';

interface IAppBar {
    openSideBar: boolean;
    toggleSidebar: (status: boolean) => void;
}

const ApolloAppBar: (props: IAppBar) => JSX.Element = (props: IAppBar) => {
    const classes = styles(useTheme());
    const appClass = clsx(classes.appBar, {
        [classes.appBarShift]: props.openSideBar,
    });
    const activeUser = useSelector((state: RootState) => state.session.activeUser)
    const loggedIn = activeUser !== undefined;

    const pages = useSelector((state:RootState) => state.pageState.pages);

    return <AppBar position="sticky" className={appClass}>
        <Grid container={true} direction="column" justify="center">
            <Grid item={true}>
                <Grid style={{padding:8}} wrap="nowrap" container={true} spacing={8} direction="row" alignItems="center" justify="center">
                    <Grid item={true}>
                        <Grid container={true} alignItems="center" spacing={2}>
                            <Grid item={true}>
                                <IconButton
                                    color="inherit"
                                    aria-label="Open drawer"
                                    onClick={() => props.toggleSidebar(!props.openSideBar)}>
                                    <MenuIcon/>
                                </IconButton>
                            </Grid>
                            <Grid item={true}>
                                <Typography variant="h3" style={{alignSelf: 'center'}}>Overwatch-Apollo</Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    
                    <Grid item={true}>
                        {!loggedIn && <LoginBar /> }
                        {loggedIn && <Grid container={true} alignItems="center" direction="row" spacing={2}>
                            <Grid item={true}>
                                <AccountButton label="Home" reference="/">
                                    <Home className={classes.accountButton}/>
                                </AccountButton>
                            </Grid>
                            <Grid item={true}>
                                <AccountButton label="Logout" reference="/logout">
                                    <ExitToApp className={classes.accountButton}/>
                                </AccountButton>
                            </Grid>
                        </Grid>}
                    </Grid>
                </Grid>
            </Grid>
            <Grid item={true} className={classes.menuBarContainer} style={{paddingTop:8}}>
                <MenuBar categories={pages}/>
            </Grid>
        </Grid>
    </AppBar>
}

export default ApolloAppBar;