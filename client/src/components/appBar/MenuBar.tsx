import { Grid, Button, Menu, Fade, MenuItem, Switch, FormControlLabel, Divider, Box, Typography, useTheme } from '@material-ui/core'
import styles from 'styles';
import React from 'react';
import ArrowDropDown from '@material-ui/icons/ArrowDropDown'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { mapThemeStateToProps, mapThemeDispatchToProps } from 'container/ThemeContainer';
import { ThemeState } from 'reducers/ThemeReducer';

interface IMenuBar {
    categories: Array<string>;
    themeState: ThemeState;
    SetDarkMode: (val: boolean) => void;
}

const MenuBar: (props: IMenuBar) => JSX.Element = (props: IMenuBar) => {
    const classes = styles(useTheme());
    return <Grid direction="row" className={classes.menuBar} container={true} justify="flex-start" alignItems="stretch" spacing={1} style={{paddingLeft: 4}}>
        <Grid item={true}>
            <MenuDropDown label="Server Management" items={ServerMenuLinks(props.categories)}/>
        </Grid>
        <Grid item={true}>
            <MenuDropDown label="Community Management" items={CommunityManagementLinks(props.categories)}/>
        </Grid>
        <Grid item={true}>
            <MenuDropDown label="System Administration" items={SystemAdminLinks(props.categories)}/>
        </Grid>
        <Grid item={true}>
            <MenuDropDown label="Account" items={AccountLinks(props.categories)}/>
        </Grid>
        <Grid item={true}>
            <FormControlLabel
                control={<Switch onChange={(event, checked) => props.SetDarkMode(checked)} checked={props.themeState.darkMode}/>}
                label={props.themeState.darkMode ? "Dark Mode" : "Light Mode"}
            />
            
        </Grid>
    </Grid>
}

export default (connect(mapThemeStateToProps, mapThemeDispatchToProps)(MenuBar));

interface IMenuDropDown {
    label: string;
    items: Array<LinkReference>;
}

const ServerMenuLinks: (categories: Array<string>) => Array<LinkReference> = (categories: Array<string>) => {
    const arr = new Array<LinkReference>();
    if (hasAccess(categories, "status")) {
        arr.push(MakeLinkReference("/accessManagement", "Access Management"));
    }

    if (hasAccess(categories, "investigate")) {
        arr.push(MakeLinkReference("/banList", "Ban List"));
    }

    arr.push(MakeLinkReference("/serverManagement", "Configuration Management"));

    if (hasAccess(categories, "status")) {
        arr.push(MakeLinkReference("/overview", "Overview"));
    }
    
    if (hasRegexAccess(categories, "rcon")) {
        arr.push(MakeLinkReference("/administration/rcon", "RCON Console"));
    }

    
    
    
    return arr;
}

function hasAccess(categories: Array<string>, accessLevel: string): boolean {
    return categories.filter(x => x.localeCompare(accessLevel) === 0).length !== 0;
}

function hasRegexAccess(categories: Array<string>, accessLevel: string): boolean {
    const pattern = new RegExp(accessLevel);
    return categories.filter(x => pattern.test(accessLevel)).length !== 0;
}

const CommunityManagementLinks: (categories: Array<string>) => Array<LinkReference> = (categories: Array<string>) => {
    const arr = new Array<LinkReference>();
    if (hasAccess(categories, "investigate")) {
        arr.push(MakeLinkReference("/playerInvestigate", "Player Lookup"));
    }

    if (hasAccess(categories, "experimental")) {
        arr.push(MakeLinkReference("/administration/groups", "Group Management"));
        arr.push(MakeLinkReference("/administration/privileged", "Privileged User Management"));
    }
    
    if (hasAccess(categories, "userManager")) { 
        arr.push(MakeLinkReference("/administration/playerAdmins", "Admin List"));
        arr.push(MakeLinkReference("/administration/users", "User List"));
        arr.push(MakeLinkReference("/administration/roles", "Role List"));
    }
    
    return arr;
}

const SystemAdminLinks: (categories: Array<string>) => Array<LinkReference> = (categories: Array<string>) => {
    const arr = new Array<LinkReference>();
    if (hasAccess(categories, "experimental")) {
        arr.push(MakeLinkReference("/administration/audit/log", "Audit Log"));
        arr.push(MakeLinkReference("/administration/roleManage", "Role Manage"));
    }
    
    if (hasAccess(categories, "featureToggle")) {
        arr.push(MakeLinkReference("/administration/featureToggle", "Feature Toggle"));
    }
    
    return arr;
}

const AccountLinks: (categories: Array<string>) => Array<LinkReference> = (categories: Array<string>) => {
    const arr = new Array<LinkReference>();
    if (categories.length === 0) {
        arr.push(MakeLinkReference("/login", "Login"));
        arr.push(MakeLinkReference("/register", "Register"));
    } else {
        arr.push(MakeLinkReference("/account", "Account"));
        arr.push(MakeLinkReference("/logout", "Logout"));
    }
    
    return arr;
}

const MakeLinkReference: (to: string, label: string) => LinkReference = (to, label) => {
    return {to, label};
}

const MenuDropDown: (props: IMenuDropDown) => JSX.Element = (props: IMenuDropDown) => {
    const classes = styles(useTheme());
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    if (props.items.length === 0) {
        return <div/>
    }

    return <>
        <Button onClick={handleClick} variant="text" className={classes.menuDropDown}>
            {props.label} <ArrowDropDown/>
        </Button>
        <Menu anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
            transformOrigin={{ vertical: "top", horizontal: "right" }}
            getContentAnchorEl={null}
            anchorEl={anchorEl} 
            open={open} 
            onClose={handleClose} 
            TransitionComponent={Fade}>
                <Box border={2}>
            {Interleave(props.items.map((x:LinkReference) => 
                <MenuItem key={x.to} component={Link} to={x.to} onClick={handleClose}>
                    <Typography variant="subtitle1">{x.label}</Typography>
                </MenuItem>), <Divider/>)}
                </Box>
        </Menu>
    </>
}

function Interleave(elements: Array<JSX.Element>, interleave: JSX.Element): Array<JSX.Element> {
    const maxIndex = elements.length - 1;
    const out = new Array<JSX.Element>(0)
    elements.forEach((x, i) => {
        out.push(x);
        if (i < maxIndex) {
            const newValue = Object.assign({}, interleave, {key: i})
            out.push(newValue);
        }
    })
    return out;
}

interface LinkReference {
    to: string;
    label: string;
}