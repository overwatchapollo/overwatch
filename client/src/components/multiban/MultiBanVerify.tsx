import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import { PlayerBanVerificationContainer } from '../../api/playerManagement/PlayerBanVerificationContainer';

interface IBanVerify {
    userBanList: () => Array<PlayerBanVerificationContainer>;
    open: () => boolean;
    onClose: () => void;
}

export class MultiBanVerify extends React.Component<IBanVerify, IBanVerify> {
    constructor(props: IBanVerify) {
        super(props);
        this.state = {
            userBanList: this.props.userBanList,
            open: this.props.open,
            onClose: this.props.onClose
        }
    }

    performMultiBan() {
        console.info("ban!");
    }

    render() {
        const banList = this.state.userBanList();
        return (
            <Dialog
                open={this.state.open()}
                onClose={(event: any) => this.state.onClose()}
                onBackdropClick={(event: any) => this.state.onClose()}>
                <DialogTitle>
                    Verify Ban List
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Verify that the following users should be banned.
                    </DialogContentText>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Alias</TableCell> 
                                <TableCell>GUID</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                banList.map((player: PlayerBanVerificationContainer, i: number) => {
                                    return (
                                        <TableRow key={i}>
                                            <TableCell>{player.alias}</TableCell>
                                            <TableCell>{player.guid}</TableCell>
                                        </TableRow>
                                    )})
                            }
                        </TableBody>
                    </Table>
                </DialogContent>
                <DialogActions>
                    <Button onClick={(event: any) => this.performMultiBan()} color="secondary">
                        Ban
                    </Button>
                    <Button onClick={(event: any) => this.state.onClose()} color="primary" autoFocus={true}>
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}