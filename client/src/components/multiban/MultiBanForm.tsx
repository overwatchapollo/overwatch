import React from 'react';
import { Typography, Toolbar, TextField, Button } from '@material-ui/core';
import { MultiBanVerify } from './MultiBanVerify';
import { PlayerBanVerificationContainer } from '../../api/playerManagement/PlayerBanVerificationContainer';
import { PageContainer } from 'components/pagecontainer/PageContainer';

interface IBanList {
    rawBanList: string;
    verify: boolean;
    playerList: Array<PlayerBanVerificationContainer>;
}

export class MultiBanForm extends React.Component<{}, IBanList> {
    constructor(props: any) {
        super(props);
        this.state = {
            rawBanList: "",
            verify: false,
            playerList: []
        }
    }

    handleReasonChange(event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement | HTMLSelectElement>) {
        const msg = event.target.value;
        this.setState({rawBanList: msg});
    }

    handleSubmit(event: React.MouseEvent<HTMLElement>) {
        fetch('/api/player/by-guid-list')
            .then(response => response.json())
            .then(data => {
                const arr = data as Array<{guid: string, name: string}>;
                const list = arr.map((x: {guid: string, name: string}, i: number) => {
                    const v = new PlayerBanVerificationContainer();
                    v.alias = x.name
                    v.guid = x.guid
                    return v;
                })
                this.setState({playerList: list, verify: true});
            })
    }

    render() {
        return <PageContainer>
                <Toolbar>
                    <Typography variant="h6" color="inherit">
                        Multiple User Ban Form
                    </Typography>
                </Toolbar>
                <TextField
                    multiline={true} 
                    autoFocus={true}
                    margin="dense"
                    id="guids"
                    label="GUIDs to ban"
                    type="text"
                    fullWidth={true}
                    rows={10}
                    onChange={(event: any) => this.handleReasonChange(event)}/>
                <Button color="primary" onClick={(event: any) => this.handleSubmit(event)}>
                    Verify Ban List
                </Button>
                <MultiBanVerify 
                    userBanList={() => this.state.playerList}
                    open={() => this.state.verify} 
                    onClose={() => this.setState({verify: false})}/>
            </PageContainer>
    }
}