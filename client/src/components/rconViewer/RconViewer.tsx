import React from "react";
import { Button, Grid, Table, TableRow, TableCell, TableBody, TableHead, TextField, Typography } from '@material-ui/core';
import { darken } from '@material-ui/core/styles';
import { submitRconCommand } from 'api/player/PlayerService';
import { Subscription } from 'rxjs';
import { asyncScheduler } from 'rxjs';
import { observeOn } from 'rxjs/operators';
import { mqttStream } from 'reducers/ServiceReducer';

interface RconLog {
    timeStamp: number;
    message: string;
}
interface RconState {
    messages: Array<RconLog>;
    command?: string;
}

interface RconViewerProps {
    serverId: number;
    serverName: string;
}

class BasicRconViewer extends React.Component<RconViewerProps, RconState> {
    private sub?: Subscription;
    constructor(props: RconViewerProps) {
        super(props);
        this.state ={
            messages: new Array(0),
        }
    }

    componentDidMount() {
        fetch(`/api/rconLog/server/${this.props.serverId}`)
            .then(resp => resp.ok ? resp.json() : new Array(0))
            .then(data => data as Array<RconLog>)
            .then(x => this.setState({messages: x}))
        this.sub = mqttStream.observeTopic(`server/${this.props.serverId}/update/rcon`)
            .pipe(observeOn(asyncScheduler))
            .subscribe(x => this.setState({messages: [...this.state.messages, x.payload as any]}))
    }

    componentWillUnmount() {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }

    render() {
        return <Grid container={true} direction="column" alignItems="center">
            <Grid item={true}>
                <Typography variant="h4">{this.props.serverName}</Typography>
            </Grid>
            <Grid item={true}>
                <Grid container={true} alignItems="flex-end">
                    <Grid item={true}>
                        <TextField label="Command" value={this.state.command} onChange={event => this.setState({command: event.target.value})}/>
                    </Grid>
                    <Grid item={true}>
                        <Button onClick={() => {
                            const msg = this.state.command;
                            if (msg) {
                                submitRconCommand(1, msg).then(() => this.setState({command: undefined}))
                            }
                        }}>Send</Button>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item={true}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Timestamp</TableCell>
                            <TableCell>Message</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            this.state.messages.reverse().map((x, i) => 
                                <TableRow key={i} style={{backgroundColor: i % 2 ? darken("#dddddd", 0.2) : "#dddddd"}}>
                                    <TableCell>{new Date(x.timeStamp).toLocaleString()}</TableCell>
                                    <TableCell>{x.message}</TableCell>
                                </TableRow>)
                        }
                    </TableBody>
                </Table>
            </Grid>
        </Grid>
    }
}

export { BasicRconViewer as RconViewer }