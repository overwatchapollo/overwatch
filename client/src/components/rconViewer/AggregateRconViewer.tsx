import React from "react";
import { PageContainer } from 'components/pagecontainer/PageContainer';
import { ServerConfigurationState } from 'reducers/ServerConfigurationReducer';
import { Grid } from '@material-ui/core';
import { RconViewer } from './RconViewer';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from 'container/ServerConfigurationContainer';

interface AggregateRconProps {
    servers: ServerConfigurationState;
}

const BasicAggregateRconViewer: (props: AggregateRconProps) => JSX.Element = props => {
    return <PageContainer>
        <Grid container={true} direction="row" spacing={2} wrap="nowrap" style={{padding: 8}}>
            {props.servers.servers.map(x => <Grid item={true} key={x.data.serverId} xs={12}>
                <RconViewer serverId={x.data.serverId} serverName={x.data.serverName}/>
            </Grid>)}
        </Grid>
    </PageContainer>
}

const AggregateRconViewer = connect(mapStateToProps, mapDispatchToProps)(BasicAggregateRconViewer);
export { AggregateRconViewer }