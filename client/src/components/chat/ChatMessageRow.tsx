import React, { useState } from 'react';
import { ChatMessage } from 'api/chat/chat';
import { Tooltip, TableRow, TableBody, TableCell, Grid, WithStyles, withStyles, Theme, List, ListItem, ListItemText, ListItemSecondaryAction, IconButton, ClickAwayListener, Divider } from '@material-ui/core';
import { Redirect } from 'react-router';
import { CSSProperties } from '@material-ui/styles';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import SearchIcon from '@material-ui/icons/Search';

import EjectIcon from '@material-ui/icons/Eject';
import BlockIcon from '@material-ui/icons/Block';
import ChatIcon from '@material-ui/icons/Chat';
import { useDispatch } from 'react-redux';
import { BanDispatcher } from 'container/BanDialogContainer';
import { KickDispatcher } from 'container/KickDialogContainer';
import { WhisperDispatcher } from 'container/WhisperDialogContainer';

interface IChat extends WithStyles {
    actions: Array<string>;
    chat: ChatMessage;
}
const ChatMessageRow: (props: IChat) => JSX.Element = (props:IChat) => {
    
    const dispatch = useDispatch();
	const banDispatcher = new BanDispatcher(dispatch);
	const kickDispatcher = new KickDispatcher(dispatch);
    const whisperDispatcher = new WhisperDispatcher(dispatch);
    
    const d = new Date(props.chat.timeStamp);
    const {classes} = props;
    const style = getStyle(props.chat.channel, classes);
    
    const [hideDetails, setHideDetails] = useState(false);
    const [lookupRedirect, setRedirect] = useState(false);
    const [showCopyTooltip, setShowCopyTooltip] = useState(false);

    const canKick = props.actions.includes("kick");
    const canBan = (props.actions.includes("ban") || props.actions.includes("ban-2w") || props.actions.includes("ban-1w"));
    
    const canWhisper = props.actions.includes("whisper");
    const canLookup = props.actions.includes("investigate");

    return <TableBody>
        <TableRow onClick={() => setHideDetails(!hideDetails)} className={style}>
            <TableCell className={style}>{d.toLocaleTimeString()}</TableCell>
            <TableCell className={style}>{props.chat.channel}</TableCell>
            <TableCell className={style}>{props.chat.sender}</TableCell>
            <TableCell className={style} style={{hyphens:"auto", overflowWrap:"break-word", whiteSpace: "normal", wordWrap: "break-word"}}>{props.chat.message}</TableCell>
        </TableRow>
        {(hideDetails && props.chat.senderObject !== null) && <TableRow>
        <TableCell colSpan={4}>
            <List>
                <ListItem>
                    <ListItemText>GUID: {props.chat.senderObject.guid}</ListItemText>
                    <ListItemSecondaryAction>
                        <ClickAwayListener onClickAway={() => setShowCopyTooltip(false)}>
                            <Tooltip title="Copied to clipboard"
                                open={showCopyTooltip}
                                disableFocusListener={true}
                                disableHoverListener={true}
                                disableTouchListener={true}
                            >
                                <IconButton onClick={() => {
                                    navigator.clipboard.writeText(props.chat.senderObject.guid);
                                    setShowCopyTooltip(true)
                                    }}>
                                    <FileCopyIcon/>
                                </IconButton>
                            </Tooltip>
                        </ClickAwayListener>
                    </ListItemSecondaryAction>
                </ListItem>
                <Divider/>
                <ListItem>
                    <ListItemText>Interact</ListItemText>
                    <ListItemSecondaryAction>
                        <Grid container={true} spacing={2}>
                            {canWhisper && <Grid item={true}>
                                <Tooltip title="Message">
                                    <IconButton onClick={() => whisperDispatcher.setWhisperPlayerInfo(props.chat.senderObject, props.chat.senderObject.serverId)}>
                                        <ChatIcon color="action"/>
                                    </IconButton>
                                </Tooltip>
                            </Grid>}
                            {canLookup && <Grid item={true}>
                                <Tooltip title="Lookup">
                                    <IconButton onClick={() => setRedirect(true)}>
                                    {lookupRedirect && <Redirect to={`/playerInvestigate/${props.chat.senderObject.guid}`}/>}
                                        <SearchIcon color="action"/>
                                    </IconButton>
                                </Tooltip>
                            </Grid>}
                        </Grid>
                        
                    </ListItemSecondaryAction>
                </ListItem>
                <Divider/>
                {(canKick || canBan) && <ListItem>
                    <ListItemText>Punish</ListItemText>
                    <ListItemSecondaryAction>
                        <Grid container={true} spacing={2}>
                            {canKick && <Grid item={true}>
                                <Tooltip title="Kick">
                                    <IconButton onClick={() => kickDispatcher.setKickPlayerInfo(props.chat.senderObject, props.chat.senderObject.serverId)}>
                                        <EjectIcon color="secondary"/>
                                    </IconButton>
                                </Tooltip>
                            </Grid>}
                            {canBan && <Grid item={true}>
                            <Tooltip title="Ban">
                                <IconButton onClick={() => banDispatcher.setBanPlayerInfo(props.chat.senderObject, props.chat.senderObject.serverId)}>
                                    <BlockIcon color="secondary"/>
                                </IconButton>
                                </Tooltip>
                            </Grid>}
                        </Grid>
                    </ListItemSecondaryAction>
                </ListItem>}
            </List>
        </TableCell>
    </TableRow>}
    </TableBody>
}

function getStyle(channel: string, classes: Record<string,string>) {
    switch (channel) {
        case "Group":
            return classes.groupMessage;
        case "Side":
            return classes.sideMessage;
        case "Command":
            return classes.commandMessage;
        case "Global":
            return classes.globalMessage;
        case "Vehicle":
            return classes.vehicleMessage;
        case "Unknown":
            return classes.unknownMessage;
        default:
            return classes.defaultMessage;
    }
}

const chatRowStyles: (theme: Theme) => Record<string,CSSProperties> = (theme: Theme) => ({
    sideMessage: {
        backgroundColor: "#0071b2",
        color:'white'
    },
    commandMessage: {
        backgroundColor: "#e69d00"
    },
    groupMessage: {
        backgroundColor: "#009e73",
        color:'white'
    },
    globalMessage: {

    },
    defaultMessage: {
        backgroundColor: theme.palette.type === "dark" ? theme.palette.grey[600] : theme.palette.grey[200],
    },
    vehicleMessage: {
        backgroundColor: "#d55c00",
        color:'white'
    },
    unknownChannel: {
        backgroundColor: "#d55c00"
    }
});

export default withStyles(chatRowStyles)(ChatMessageRow)