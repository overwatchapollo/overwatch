import React, { useState } from 'react';
import { ServerConfiguration } from 'api/server/ServerConfiguration';
import { Button, TextField, MenuItem, Select, FormControl, Grid, Typography, IconButton, Paper } from '@material-ui/core';
import ViewHeadline from '@material-ui/icons/ViewHeadline'
import { sendAll } from 'api/chat/ChatService';

interface IAggregateMessageForm {
    serverList: Array<ServerConfiguration>;
}

const AggregateMessageForm: (props: IAggregateMessageForm) => JSX.Element = (props: IAggregateMessageForm) => {
    const [message, setMessage] = useState("");
    const [show, setShow] = useState(false);
    const servers = props.serverList;
    const [serverId, setServerId] = useState(servers.map(x => x.serverId).reduce((a, b) => a > b ? -1 : 1, -1));
    if (servers.length === 0) {
        return <div/>
    }

    return <Grid container={true} spacing={1} direction="column" justify="center" alignItems="flex-end">
        <Grid item={true}>
            <Grid container={true} alignItems="center">
                <Grid item={true}>
                    <Typography variant="h5">Broadcast Message</Typography>
                </Grid>
                <Grid item={true}>
                    <IconButton onClick={() => setShow(!show)}>
                        <ViewHeadline/>
                    </IconButton>
                </Grid>
            </Grid>
        </Grid>
        
        {show && <Grid item={true}>
            <Paper>
                <Grid container={true} direction="column" spacing={2} justify="center" alignItems="flex-end" style={{padding: 8}}>
                    <Grid item={true}>
                        <Grid container={true} spacing={1}direction="row" alignItems="center">
                            <Grid item={true}><Typography variant="body1">Select Server</Typography></Grid>
                            <Grid item={true}>
                                <FormControl>
                                    <Select
                                        autoWidth={true}
                                        value={serverId}
                                        onChange={event => setServerId(event.target.value as number)}>
                                        {servers.map(x => 
                                            <MenuItem key={x.serverId} value={x.serverId}>
                                                {x.serverName}
                                            </MenuItem>)}
                                    </Select>
                                </FormControl>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item={true}>
                        <Grid container={true} direction="row" spacing={2} justify="center" alignItems="center">
                            <Grid item={true}>
                                <Typography variant="body1">Message to send:</Typography>
                            </Grid>
                            <Grid item={true}>
                                <TextField
                                    multiline={true}
                                    id="reason"
                                    type="text"
                                    value={message}
                                    onChange={event => setMessage(event.target.value)}/>
                            </Grid>
                            
                        </Grid>
                    </Grid>
                    <Grid item={true}>
                        <Button 
                            variant="contained" 
                            color="secondary" 
                            type="submit" 
                            onClick={event => handleSubmit(serverId, message) }>
                            Send
                        </Button>
                    </Grid>
                </Grid>
            </Paper>
        </Grid>}
    </Grid>
}

function handleSubmit(serverId: number, message: string) {
    if (serverId !== -1 && message !== "") {
        sendAll(serverId, message);
    }
}

export default AggregateMessageForm;