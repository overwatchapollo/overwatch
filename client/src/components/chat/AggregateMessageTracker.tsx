import React from 'react';

import { Typography, Grid, Divider } from '@material-ui/core';
import { ServerConfiguration } from 'api/server/ServerConfiguration';
import AggregateMessageForm from './AggregateMessageForm';
import { PlayerFilter, ServerFilter } from 'components/filters';
import { PermissionWrapper } from 'api/PermissionWrapper';
import { mapStateToProps, mapDispatchToProps } from 'container/ServerConfigurationContainer';
import { connect } from 'react-redux';
import { ServerConfigurationState } from 'reducers/ServerConfigurationReducer';
import { PageContainer } from 'components/pagecontainer/PageContainer';
import { ChatViewer } from './ChatViewer';

interface IMessageStreamer {
    servers: ServerConfigurationState;
}

interface IAggregateMessageTracker {
    servers: Map<number, boolean>;
    nameFilter: string;
}

class BasicAggregateMessageTracker extends React.Component<IMessageStreamer, IAggregateMessageTracker> {
    constructor(props: IMessageStreamer) {
        super(props);
        this.state = {
            servers: new Map(),
            nameFilter: "",
        }
    }

    render() {
        const servers = this.props.servers.servers;
        const canChat = servers
            .map(x => x.actions.includes("broadcast") || x.actions.includes("whisper"))
            .reduce((a,b) => a === false ? false : b, true);
        return <PageContainer>
                <Grid spacing={2} container={true} justify="center" alignItems="center" direction="column" style={{padding: 8, width:"100%"}}>
                    <Grid item={true} style={{width:'100%'}}>
                        <Grid container={true} justify="space-between" alignItems="flex-start" spacing={2}>
                            <Grid item={true}>
                                <Typography variant="h4" color="inherit">
                                    Chat Log
                                </Typography>
                            </Grid>
                            {canChat && <Grid item={true}>
                                <AggregateMessageForm 
                                    serverList={servers
                                        .filter(x => x.actions.includes("broadcast") || x.actions.includes("whisper"))
                                        .map(x => x.data)} />
                            </Grid>}
                        </Grid>
                    </Grid>

                    <Grid item={true}>
                        <Grid container={true} direction="row" spacing={2} justify="center" alignItems="center">
                            <Grid item={true}>
                            <ServerFilter 
                                allServers={servers.map(x => x.data)} 
                                selectedServers={(server) => {
                                    const viewMap = new Map<number, boolean>();
                                    server.forEach(x => viewMap.set(x.serverId, true));
                                    this.setState({servers: viewMap});
                                }}/>
                            </Grid>
                            <PlayerFilter pushFilter={(name) => this.setState({nameFilter: name})}/>
                        </Grid>
                    </Grid>
                    
                    <Divider/>
                    <Grid item={true}>
                        <Grid container={true}
                            direction="row"
                            justify="center"
                            alignItems="flex-start"
                            wrap="nowrap"
                            spacing={2}>

                            {servers
                                .filter(x => this.state.servers.get(x.data.serverId) === true)
                                .map((x: PermissionWrapper<ServerConfiguration>, i: number) => {
                                return <Grid 
                                        item={true}
                                        key={x.data.serverId} 
                                        className="ChatContainer">
                                    <Grid container={true} direction="column" justify="flex-start" alignItems="flex-start" style={{padding:8}}>
                                        <Grid item={true}>
                                            <Typography variant="h6" color="inherit" noWrap={true}>
                                                {x.data.serverName}
                                            </Typography>
                                        </Grid>
                                        <Grid item={true}>
                                            <ChatViewer server={x} nameFilter={this.state.nameFilter}/>
                                        </Grid>
                                            
                                    </Grid>
                                </Grid>
                            })}
                        </Grid>
                    </Grid>
                </Grid>
            </PageContainer>
    }
}

const AggregateMessageTracker = connect(mapStateToProps, mapDispatchToProps)(BasicAggregateMessageTracker);
export {AggregateMessageTracker};