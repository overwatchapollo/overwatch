import React from "react";
import { Grid, Table, TableHead, TableRow, TableCell } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { ChatMessage } from 'api/chat/chat';
import { PermissionWrapper } from 'api/PermissionWrapper';
import { ServerConfiguration } from 'api/server/ServerConfiguration';
import ChatMessageRow from './ChatMessageRow';
import { RootState } from 'reducers';

interface ChatViewerProps {
    server: PermissionWrapper<ServerConfiguration>;
    nameFilter?: string;
}

const defaultChatMessages = new Map<number,ChatMessage>();

const ChatViewer: (props: ChatViewerProps) => JSX.Element = props => {    
    return <Grid container={true} direction="column" className={"ChatLog"} style={{padding:8}} spacing={2}>
        <Grid item={true}>
            <ChatViewerTable
                nameFilter={props.nameFilter ?? ""}
                server={props.server}/>
        </Grid>
    </Grid>
}

interface IChatViewerTable {
    server: PermissionWrapper<ServerConfiguration>;
    nameFilter?: string;
}

const ChatViewerTable: (props: IChatViewerTable) => JSX.Element = props => {
    const { actions } = props.server;
    const serverId = props.server.data.serverId;
    const chatCache = useSelector((state: RootState) => state.chatCacheState.serverMap.get(serverId));
    const chatMessages = chatCache ?? defaultChatMessages;

    return <Table style={{tableLayout: "fixed"}}>
        <TableHead>
            <TableRow>
                <TableCell>TimeStamp</TableCell>
                <TableCell>Channel</TableCell>
                <TableCell>Sender</TableCell>
                <TableCell>Message</TableCell>
            </TableRow>
        </TableHead>
        {Array.from(chatMessages.values())
            .sort((a,b) => a.messageId > b.messageId ? -1 : 1)
            .map(item => {
                const filter = props.nameFilter;
                if (filter) {
                    const filterString = filter.toLocaleLowerCase();
                    if (filterString !== "") {
                        const name = item.sender.toLocaleLowerCase();
                        const match = name.localeCompare(filterString) === 0 || name.lastIndexOf(filterString) !== -1;
                        if (!match) {
                            return false;
                        }
                    }
                }
                return <ChatMessageRow 
                    key={item.messageId} 
                    chat={item}
                    actions={actions} />
            })
        }
    </Table>
}

export { ChatViewer }