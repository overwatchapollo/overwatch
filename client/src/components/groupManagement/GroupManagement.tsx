import React, { useEffect, useState } from 'react';
import { PageContainer } from 'components/pagecontainer/PageContainer';
import { createGroup, getGroups, FullGroupDetails, addRoleToGroup, removeRoleFromGroup, deleteGroup } from 'api/groups/GroupService';
import { Collapse, Divider, Grid, List, ListItem, Typography, IconButton, ListItemText, ListItemSecondaryAction, Tabs, Tab, Dialog, DialogTitle, DialogContent, DialogActions, Button, TextField } from '@material-ui/core';

import AddBoxIcon from '@material-ui/icons/AddBox';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { PermissionWrapper } from 'api/PermissionWrapper';
import { UserRole } from 'api/roles/UserRole';
import Delete from "@material-ui/icons/Delete";
import { RoleMember } from 'api/roles/RoleMember';
import { darken } from '@material-ui/core/styles';
import { ServerConfiguration } from 'api/server/ServerConfiguration';
import { ConfirmationDialog } from 'components/confirmation/ConfirmationDialog';
import { RoleAccessManagement } from 'components/accessManagement/RoleAccessManagement';

const CreateGroupDialog: (props: {open: boolean, onClose: (success: boolean) => void}) => JSX.Element = props => {
    const [groupName, setGroupName] = useState("");
    const addAction = () => createGroup({name: groupName}).then(() => props.onClose(true))
    const cancelAction = () => props.onClose(false);
    return <Dialog open={props.open} onBackdropClick={() => props.onClose(false)}>
        <DialogTitle>Create new group</DialogTitle>
        <DialogContent>
            <TextField 
                value={groupName} 
                onChange={event => setGroupName(event.target.value)} 
                label="Group Name"
                variant="outlined"/>
        </DialogContent>
        <DialogActions>
            <Button variant="contained" color="primary" onClick={addAction}>
                Add
            </Button>
            <Button variant="contained" color="secondary" onClick={cancelAction}>
                Cancel
            </Button>
        </DialogActions>
    </Dialog>
}

const GroupManagement: () => JSX.Element = () => {
    const [openGroupDialog, setOpenGroupDialog] = useState(false);
    const [groups, setGroups] = useState<Array<FullGroupDetails>>(new Array(0));
    const refresh = () => getGroups().then(x => setGroups(x));
    useEffect(() => {
        refresh();
    }, [])
    return <PageContainer>
        <CreateGroupDialog open={openGroupDialog} onClose={success => {
            setOpenGroupDialog(false)
            if (success) {
                refresh();
            }
        }}/>
        <Grid container={true} direction="column" spacing={2} style={{padding: 8}}>
            <Grid item={true}>
                <Grid container={true} alignItems="stretch" spacing={2}>
                    <Grid item={true}>
                        <Typography variant="h4">Group Management</Typography>
                    </Grid>
                    <Grid item={true}>
                        <IconButton onClick={() => setOpenGroupDialog(!openGroupDialog)}>
                            <AddBoxIcon/>
                        </IconButton>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item={true}>
                <List>
                    {groups.map(x => <GroupView key={x.id} group={x} update={() => refresh()}/>)}
                </List>
            </Grid>
        </Grid>
    </PageContainer>
}

const GroupView: (props: {group: FullGroupDetails, update:() => void}) => JSX.Element = props => {
    const [showGroup, setShowGroup] = useState(true);
    const [tabPage, setTabPage] = React.useState(0);
    const [deleteClicked, setDeleteClicked] = useState(false);
    return <ListItem>
        <ConfirmationDialog open={deleteClicked}
                title="Delete Group"
                question="Are you sure you want to delete this group?"
                confirmationButtonLabel="Delete"
                cancelButtonLabel="No"
                result={res => {
                    if (res) {
                        deleteGroup(props.group.id).then(() => props.update());
                    }
                    setDeleteClicked(false);
                }}
            />
        <List style={{width: '100%', borderWidth: 2, borderColor: '#b20071', borderStyle: 'solid'}}>
            <ListItem onClick={() => setShowGroup(!showGroup)}>
                <ListItemText primaryTypographyProps={{variant: "h5"}} style={{textAlign:'center'}}>
                    {props.group.name}
                </ListItemText>
                <ListItemSecondaryAction>
                    <Grid container={true} spacing={2} alignItems="center">
                        <Grid item={true}>
                            <IconButton onClick={() => setDeleteClicked(true)}>
                                <Delete edgeMode="end" color="secondary"/>
                            </IconButton>
                        </Grid>
                        <Grid item={true} onClick={() => setShowGroup(!showGroup)}>
                            {showGroup ? <ExpandLessIcon/> : <ExpandMoreIcon/>}
                        </Grid>
                    </Grid>
                </ListItemSecondaryAction>
            </ListItem>
            <Collapse in={showGroup} timeout="auto">
                <Divider/>
                <Grid container={true} direction="column">
                    <Grid item={true}>
                        <Tabs value={tabPage} onChange={(_, value) => setTabPage(value as number)} variant="fullWidth">
                            <Tab label="Overview" {...a11yProps(0, tabPage)}/>
                            <Tab label="Members" {...a11yProps(1, tabPage)}/>
                            <Tab label="Servers" {...a11yProps(2, tabPage)}/>
                            <Tab label="Roles" {...a11yProps(3, tabPage)}/>
                        </Tabs>
                    </Grid>
                    <TabPanel value={tabPage} index={0}>
                        overview
                    </TabPanel>
                    <TabPanel value={tabPage} index={1}>
                        <MembersTab members={props.group.members}/>
                    </TabPanel>
                    <TabPanel value={tabPage} index={2}>
                        <ServerTab servers={props.group.servers}/>
                    </TabPanel>
                    <TabPanel value={tabPage} index={3}>
                        <RoleTab roles={props.group.roles} groupId={props.group.id} roleUpdate={() => props.update()}/>
                    </TabPanel>
                </Grid>
            </Collapse>
        </List>        
    </ListItem>
}

const RoleAddDialog: (props: {groupId: number, open: boolean, onClose: (change: boolean) => void}) => JSX.Element = props => {
    const [roleName, setRoleName] = useState("");
    return <Dialog open={props.open} onBackdropClick={() => props.onClose(false)}>
        <DialogTitle>Add role to group</DialogTitle>
        <DialogContent>
            <TextField value={roleName} onChange={event => setRoleName(event.target.value)} label="Role Name"/>
        </DialogContent>
        <DialogActions>
            <Button variant="contained" color="primary" onClick={() => {
                addRoleToGroup(props.groupId, roleName).then(() => props.onClose(true))
            }}>
                Add
            </Button>
            <Button variant="contained" color="secondary" onClick={() => props.onClose(false)}>Cancel</Button>
        </DialogActions>
    </Dialog>
}

const RoleTab: (props: {roles: Array<PermissionWrapper<UserRole>>, groupId: number, roleUpdate: () => void}) => JSX.Element = props => {
    const [showAdd, setShowAdd] = useState(false);
    return <List>
        <ListItem>
            <RoleAddDialog groupId={props.groupId} open={showAdd} onClose={change => {
                setShowAdd(false);
                if (change) {
                    props.roleUpdate();
                }
            }}/>
            <ListItemText primaryTypographyProps={{variant: "h6"}}>Roles</ListItemText>
            <ListItemSecondaryAction>
                <Grid container={true} spacing={2} alignItems="center">
                    <Grid item={true}>
                        <IconButton onClick={() => setShowAdd(true)}><AddBoxIcon/></IconButton>
                    </Grid>
                </Grid>
            </ListItemSecondaryAction>
        </ListItem>
        {props.roles.map((x, i) => <ListItem key={i}>
            <RoleAccessManagement data={x} update={() => props.roleUpdate()} overrideDeleteRole={async () => {
                await removeRoleFromGroup(props.groupId, x.data.id);
                props.roleUpdate();
            }}/>
        </ListItem>)}
    </List>
}

const ServerTab: (props: {servers: Array<ServerConfiguration>}) => JSX.Element = props => {
    return <List>
        <ListItem>
            <ListItemText primaryTypographyProps={{variant: "h6"}}>Servers</ListItemText>
            <ListItemSecondaryAction>
                <Grid container={true} spacing={2} alignItems="center">
                    <Grid item={true}>
                        <IconButton><AddBoxIcon/></IconButton>
                    </Grid>
                </Grid>
            </ListItemSecondaryAction>
        </ListItem>

        {props.servers.map((x,i) => <ListItem key={i}>
            <ListItemText primaryTypographyProps={{variant: "subtitle1"}}>
                {x.serverName}
            </ListItemText>
            <ListItemSecondaryAction>
                <IconButton>
                    <Delete edgeMode="end" color="secondary"/>
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>)}
    </List>
}

const MembersTab: (props: {members: Array<RoleMember>}) => JSX.Element = props => {
    return <List style={{paddingLeft: 8}}>
        <ListItem>
            <ListItemText primaryTypographyProps={{variant: "h6"}}>Members</ListItemText>
            <ListItemSecondaryAction>
                <Grid container={true} spacing={2} alignItems="center">
                    <Grid item={true}>
                        <IconButton><AddBoxIcon/></IconButton>
                    </Grid>
                </Grid>
            </ListItemSecondaryAction>
        </ListItem>
        
        {props.members.map((x,i) => <ListItem key={i}>
            <ListItemText primaryTypographyProps={{variant: "subtitle1"}}>
                {x.userName}
            </ListItemText>
            <ListItemSecondaryAction>
                <IconButton>
                    <Delete edgeMode="end" color="secondary"/>
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>)}
    </List>
}

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}
function a11yProps(index: number, selected: number) {
return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
    style:{backgroundColor: selected === index ? '#b20071' : darken('#b20071', 0.2)}
    }
};

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;
    const hidden = value !== index;
    return <Grid 
        role="tabpanel"
        hidden={hidden} 
        item={true}
        id={`simple-tabpanel-${index}`}
        {...other}>
            {children}
        </Grid>
  }

const ExpGroup = GroupManagement;

export {ExpGroup}