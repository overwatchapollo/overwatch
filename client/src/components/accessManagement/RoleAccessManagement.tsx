import { Grid, Dialog, DialogTitle, DialogContent, TextField, DialogActions, Button, List, ListItem, ListItemAvatar, Avatar, ListItemText, ListItemSecondaryAction, IconButton, Divider, Collapse, useTheme } from '@material-ui/core';
import styles from 'styles';
import React from "react";
import { PermissionWrapper } from 'api/PermissionWrapper';
import { UserRole } from 'api/roles/UserRole';
import { useState } from 'react';
import { addPermissionToRole, deleteRole, removeRoleFromUser, removePermissionFromRole } from 'api/roles/RoleService';
import { ConfirmationDialog } from 'components/confirmation/ConfirmationDialog';

import Delete from "@material-ui/icons/Delete";
import Remove from "@material-ui/icons/Remove";
import GroupIcon from '@material-ui/icons/Group';

import AddBoxIcon from '@material-ui/icons/AddBox';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { AddUserToGroup } from './AddUserToGroup';

interface SingleProps {
    data: PermissionWrapper<UserRole>;
    update: () => void;
    overrideDeleteRole?: (roleId: number | string) => void;
}

type removeUser = {id: number, username: string};

const RoleAccessManagement: (props: SingleProps) => JSX.Element = props => {
    const classes = styles(useTheme());
    const userRole = props.data.data;
    const actions = props.data.actions;
    const canDelete = actions.includes("delete")
    const canAddAction = actions.includes("addAction")
    const canDeleteAction = actions.includes("removeAction")
    const canAddUser = actions.includes("addUser")
    const canRemoveUser = actions.includes("removeUser")

    const [show, setShow] = useState(true);
    const [showPermissions, setShowPermissions] = useState(false);
    const [permissionsFilter, setPermissionsFilter] = useState("");
    const [showMembers, setShowMembers] = useState(false);
    const members = userRole.members ?? [];
    const [membersFilter, setMembersFilter] = useState("");

    const [deleteClicked, setDeleteClicked] = useState(false);
    const [removeUser, setRemoveUser] = useState<removeUser | undefined>(undefined)

    const [removeAction, setRemoveAction] = useState<string | undefined>(undefined)

    const [openAddMember, setAddMember] = useState(false);
    const [addPermission, setAddPermission] = useState(false);

    const [tempPermission, setTempPermission] = useState("");

    return <Grid container={true} direction="column" style={{borderWidth: 2, borderColor: '#0071b2', borderStyle: 'solid'}}>
        <Grid item={true}>
            <AddUserToGroup 
                open={openAddMember} 
                groupId={userRole.id} 
                groupName={userRole.name}
                onClose={res => {
                    if (res) {
                        props.update();
                    }
                    
                    setAddMember(false);
                }}/>

            <Dialog open={addPermission} onBackdropClick={() => setAddPermission(false)}>
                <DialogTitle>Add Permission to {userRole.name}</DialogTitle>
                <DialogContent>
                    <TextField value={tempPermission} onChange={event => setTempPermission(event.target.value)}/>
                </DialogContent>
                <DialogActions>
                    <Button 
                        variant="contained" 
                        color="primary" 
                        onClick={() => addPermissionToRole(userRole.id, tempPermission)
                            .then(() => props.update())
                            .then(() => setAddPermission(false))}
                    >
                        Add
                    </Button>
                    <Button 
                        variant="contained" 
                        color="secondary" 
                        onClick={() => setAddPermission(false)}
                    >
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>

            <ConfirmationDialog open={deleteClicked}
                title="Delete Group"
                question="Are you sure you want to delete this group?"
                confirmationButtonLabel="Delete"
                cancelButtonLabel="No"
                result={res => {
                    if (res) {
                        if (props.overrideDeleteRole) {
                            props.overrideDeleteRole(userRole.id);
                        } else {
                            deleteRole(userRole.id).then(() => props.update());
                        }
                    }
                    setDeleteClicked(false);
                }}
            />

            <ConfirmationDialog open={removeUser !== undefined}
                title="Remove User"
                question={`Are you sure you want to remove ${removeUser?.username}?`}
                confirmationButtonLabel="Remove"
                cancelButtonLabel="No"
                result={async res => {
                    if (res) {
                        if (removeUser) {
                            await removeRoleFromUser(removeUser.id, Number.parseInt(userRole.id, 10))
                                .then(() => props.update())
                                .then(() => setRemoveUser(undefined));
                        }
                    } else {
                        setRemoveUser(undefined);
                    }
                }}
            />

            <ConfirmationDialog open={removeAction !== undefined}
                title="Remove Permission"
                question={`Are you sure you want to remove ${removeAction} from group ${props.data.data.name}?`}
                confirmationButtonLabel="Remove"
                cancelButtonLabel="No"
                result={res => {
                    if (res) {
                        if (removeAction) {
                            removePermissionFromRole(userRole.id, removeAction).then(() => props.update())
                        }
                    }
                    setRemoveAction(undefined);
                }}
            />

            <List>
                <ListItem button={true} onClick={() => setShow(!show)}>
                    <ListItemAvatar>
                        <Avatar className={classes.roleGroupIcon}>
                            <GroupIcon />
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText primary={userRole.name} primaryTypographyProps={{variant:"h5"}}/>
                    <ListItemSecondaryAction>
                        <Grid container={true} spacing={2} alignItems="center">
                            {canDelete && <Grid item={true}>
                                <IconButton onClick={() => setDeleteClicked(true)}>
                                    <Delete edgeMode="end" color="secondary"/>
                                </IconButton>
                            </Grid>}

                            <Grid item={true} onClick={() => setShow(!show)}>
                                {show ? <ExpandLessIcon/> : <ExpandMoreIcon/>}
                            </Grid>
                        </Grid>
                    </ListItemSecondaryAction>
                </ListItem>

                {show && <Divider/>}

                {show && <ListItem button={true} onClick={() => setShowPermissions(!showPermissions)}>
                    <ListItemText primary="Permissions" inset={true} primaryTypographyProps={{variant:"h6"}}/>
                    <ListItemSecondaryAction>
                        <Grid container={true} spacing={2} alignItems="center">
                            <Grid item={true}>
                                {showPermissions && <TextField label="Filter" value={permissionsFilter} onChange={event => setPermissionsFilter(event.target.value)}/>}
                            </Grid>
                            {canAddAction && <Grid item={true}>
                                <IconButton onClick={() => setAddPermission(!addPermission)}>
                                    <AddBoxIcon/>
                                </IconButton>
                            </Grid>}
                            <Grid item={true} onClick={() => setShowPermissions(!showPermissions)}>
                                {showPermissions ? <ExpandLessIcon/> : <ExpandMoreIcon/>}
                            </Grid>
                        </Grid>
                    </ListItemSecondaryAction>
                </ListItem>}

                {show && <Collapse in={showPermissions} timeout="auto">
                    <List>
                        {userRole.permissions
                            .filter(x => x.permission.toLocaleLowerCase().includes(permissionsFilter.toLocaleLowerCase()))
                            .sort((a,b) => a.name.localeCompare(b.name))
                            .map((x, i) => <ListItem divider={true} key={i}>
                                <ListItemText inset={true}>
                                    {x.name}
                                </ListItemText>
                                
                                {canDeleteAction && <ListItemSecondaryAction>
                                    <IconButton onClick={() => setRemoveAction(x.permission)}>
                                        <Delete edgeMode="end" color="secondary"/>
                                    </IconButton>
                                </ListItemSecondaryAction>}
                            </ListItem>)
                        }
                    </List>
                </Collapse>}

                {show && <Divider/>}

                {show && <ListItem button={true} onClick={() => setShowMembers(!showMembers)}>
                    <ListItemText primary="Members" inset={true} primaryTypographyProps={{variant:"h6"}}/>
                    <ListItemSecondaryAction>
                        <Grid container={true} spacing={2} alignItems="center">
                            <Grid item={true}>
                                {showMembers && <TextField label="Filter" value={membersFilter} onChange={event => setMembersFilter(event.target.value)}/>}
                            </Grid>
                            {canAddUser && <Grid item={true}>
                                <IconButton onClick={() => setAddMember(true)}>
                                    <AddBoxIcon/>
                                </IconButton>
                            </Grid>}
                            <Grid item={true} onClick={() => setShowMembers(!showMembers)}>
                                {showMembers ? <ExpandLessIcon /> : <ExpandMoreIcon/>}
                            </Grid>
                        </Grid>
                    </ListItemSecondaryAction>
                </ListItem>}

                {show && <Collapse in={showMembers} timeout="auto">
                    <List>
                        {members.filter(x => x.userName.toLocaleLowerCase().includes(membersFilter.toLocaleLowerCase()))
                            .sort((a,b) => a.userName.localeCompare(b.userName))
                            .map((x, i) => <ListItem divider={true} key={i}>
                                <ListItemText inset={true}>
                                    {x.userName}
                                </ListItemText>
                                
                                {canRemoveUser && <ListItemSecondaryAction>
                                    <IconButton onClick={() => setRemoveUser({id: x.id, username: x.userName})}>
                                        <Remove edgeMode="end" color="secondary"/>
                                    </IconButton>
                                </ListItemSecondaryAction>}
                            </ListItem>)
                        }
                    </List>
                </Collapse>}
            </List>
        </Grid>
    </Grid>
}

export { RoleAccessManagement }
