import { useState } from 'react';
import clsx from 'clsx';
import React from "react";
import { addRoleToUserName } from 'api/roles/RoleService';
import { Dialog, DialogTitle, DialogContent, Grid, TextField, IconButton, Fab, CircularProgress, DialogActions, Button } from '@material-ui/core';
import { userExists } from 'api/users/UserService';
import { useStyles } from '.';
import { Done, Search } from "@material-ui/icons";

interface AddUserProps {
    open: boolean;
    groupId: string;
    groupName: string;
    onClose: (refresh?:boolean) => void;
}

const AddUserToGroup: (props:AddUserProps) => JSX.Element = props => {
    const [username, setUsername] = useState("");
    const [checkUsername, setCheckUsername] = useState<boolean | "progress" | undefined>(undefined);
    const classes = useStyles();
    const buttonClassname = clsx({
        [classes.buttonSuccess]: checkUsername === true,
        [classes.buttonFail]: checkUsername === false,
    });

    const addAction = () => {
        addRoleToUserName(username, Number.parseInt(props.groupId, 10))
            .then(() => props.onClose(true),
                () => setCheckUsername(false));
    };

    // do search bar like the interactive integration button here https://material-ui.com/components/progress/
    return <Dialog open={props.open} onBackdropClick={() => props.onClose()}>
        <DialogTitle>Add User to {props.groupName}</DialogTitle>
        <DialogContent>
            <Grid container={true} direction="column" spacing={2}>
                <Grid item={true}>
                    <Grid container={true} spacing={2} alignItems="center">
                        <Grid item={true}>
                            <TextField 
                                label="Username" 
                                value={username} 
                                onChange={event => {
                                    setUsername(event.target.value);
                                    setCheckUsername(undefined);
                                }}/>
                        </Grid>
                        <Grid item={true}>
                            <IconButton >
                                <Fab aria-label="save"
                                    color="primary"
                                    className={buttonClassname} 
                                    onClick={() => {
                                        setCheckUsername("progress");
                                        userExists(username).then(x => setCheckUsername(x))
                                    }}>
                                    
                                    {(checkUsername !== true) && <Search className={buttonClassname}/>}
                                    {checkUsername === true && <Done/>}
                                </Fab>
                                {checkUsername === "progress" && <CircularProgress size={68} className={classes.fabProgress}/>}
                            </IconButton>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            
        </DialogContent>
        <DialogActions>
            <Button variant="contained" color="primary" onClick={addAction}>
                Add
            </Button>
            <Button variant="contained" color="secondary" onClick={() => props.onClose()}>
                Cancel
            </Button>
        </DialogActions>
    </Dialog>
}

export { AddUserToGroup }