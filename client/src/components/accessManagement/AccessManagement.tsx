import React, { useEffect, useState } from "react";
import { Grid, Typography } from '@material-ui/core'
import { getVisibleRoles } from 'api/roles/RoleService';
import { PermissionWrapper } from 'api/PermissionWrapper';
import { UserRole } from 'api/roles/UserRole';

import { PageContainer } from 'components/pagecontainer/PageContainer';
import { RoleAccessManagement } from './RoleAccessManagement';

const AccessManagement: () => JSX.Element = () => {

    const [roles, setRoles] = useState<Array<PermissionWrapper<UserRole>>>(new Array(0));

    const roleUpdate = async () => {
        getVisibleRoles().then(x => setRoles(x.sort((a,b) => a.data.name.localeCompare(b.data.name))));
    }
    useEffect(() => {
        roleUpdate()
    }, []);
    return <PageContainer>
        <Grid container={true} style={{padding: 8}} spacing={2} direction="column">
            <Grid item={true}>
                <Typography variant="h4">Access Management</Typography>
            </Grid>
            {roles.map(x => <Grid item={true} key={x.data.id}>
                    <RoleAccessManagement data={x} update={() => roleUpdate()}/>
                </Grid>)}
        </Grid>
    </PageContainer>
}

export { AccessManagement }

