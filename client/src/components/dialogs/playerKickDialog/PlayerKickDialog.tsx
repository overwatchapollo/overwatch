import React, { useState } from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button, TextField, DialogContentText, Typography } from '@material-ui/core'
import { connect } from 'react-redux';
import { mapKickDialogState, mapKickDialogDispatch } from 'container/KickDialogContainer';
import { KickDialogState } from 'reducers/KickDialogReducer';
import { kickPlayer } from 'api/player/PlayerService';
import { Player } from 'api/chat/ConnectedPlayer';

interface PlayerKickDialogProps {
    kickDialogState: KickDialogState;
    clearPlayerKickInfo: () => void;
}

function doKick(message: string, player?: Player, serverId?: number): Promise<Response> {
    if (!player) {
        return Promise.reject("No player selected.");
    }

    if (!serverId) {
        return Promise.reject("No server selected.");
    }

    if (message.length === 0) {
        return Promise.reject("No message supplied.")
    }

    return kickPlayer(serverId, player.sessionId.toString(), player.name, message);
}

const BasicPlayerKickDialog: (props: PlayerKickDialogProps) => JSX.Element = props => {
    const { kickDialogState } = props;
    const [kickMessage, setKickMessage] = useState("");
    const [errorMessage, setErrorMessage] = useState("");
    return <Dialog 
        open={kickDialogState.player !== undefined && kickDialogState.serverId !== undefined}
        onClose={() => props.clearPlayerKickInfo()}
        onBackdropClick={() => props.clearPlayerKickInfo()}
    >
        <DialogTitle>
            Kick Player {kickDialogState.player?.name}
        </DialogTitle>
        <DialogContent>
            {errorMessage.length !== 0 && 
                <Typography variant="h6" style={{color: 'red'}}>
                    {errorMessage}
                </Typography>}
            <DialogContentText>
                Kick player to remove them from the server.
            </DialogContentText>
            <TextField variant="outlined" autoFocus={true} margin="dense" label="Kick Message" type="text"fullWidth={true}
                value={kickMessage}
                onChange={event => setKickMessage(event.target.value)}/>
        </DialogContent>

        <DialogActions>
            <Button variant="contained" color="primary" onClick={() => 
                doKick(kickMessage, kickDialogState?.player, kickDialogState?.serverId)
                .then(() => props.clearPlayerKickInfo(), error => setErrorMessage(error))
            }>
                Kick
            </Button>
            <Button variant="contained" color="secondary" onClick={() => props.clearPlayerKickInfo()}>
                Cancel
            </Button>
        </DialogActions>
    </Dialog>
}

const PlayerKickDialog = connect(mapKickDialogState, mapKickDialogDispatch)(BasicPlayerKickDialog);

export { PlayerKickDialog }