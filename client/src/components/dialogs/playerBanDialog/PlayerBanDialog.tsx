import React, { useState } from "react";
import { CircularProgress, Dialog, DialogTitle, DialogContent, List, Typography, TextField, DialogActions, Button, Grid, Select, MenuItem, Checkbox, FormControlLabel, ListItem, ListItemText, ListItemAvatar } from '@material-ui/core';
import { submitBan } from 'api/player/PlayerService';
import { useSelector, useDispatch } from 'react-redux';
import { Player } from 'api/chat/ConnectedPlayer';
import { BanRequest } from 'api/player/BanRequest';
import { RootState } from 'reducers';
import { BanDispatcher } from 'container/BanDialogContainer';
import Done from '@material-ui/icons/Done'
import CloseIcon from '@material-ui/icons/Close';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import { setTimeout } from 'timers';


function doBan(duration: number | 'perm', message: string, player?: Player, server?: number): Promise<Response> {

    if (!player) {
        return Promise.reject("Player not present");
    }

    if (!server) {
        return Promise.reject("Server id not present.");
    }

    const ban: BanRequest = {
        duration: duration === 'perm' ? -1 : duration,
        playerGuid: player.guid,
        reason: message,
        serverLocation: server.toString(),
        sessionId: player.sessionId
    }
    return submitBan(ban);
}

type BanState = 'form' | 'issueBan' | 'awaitConfirmation' | 'success' | 'failure';

const BasicPlayerBanDialog: () => JSX.Element = () => {
    const [multiplier, setMultiplier] = useState(60);
    const [banMessage, setBanMessage] = useState("");
    const [issueBanErrorMessage, setIssueBanErrorMessage] = useState<string | undefined>(undefined);
    const [duration, setDuration] = useState(0);
    const [permament, setPermanent] = useState(false);

    const [banState, setBanState] = useState<BanState>('form');

    const banDialogState = useSelector((state: RootState) => state.banState)
    const servId = banDialogState.serverId ?? 0;
    const servers = useSelector((state: RootState) => state.servers.servers.find(x => x.data.serverId === servId));

    const dispatch = useDispatch();
    const banDispatch = new BanDispatcher(dispatch);
    
    const canBanPerm = (servers?.actions.includes("ban")) ?? false;

    const targetPlayer = useSelector((state: RootState) => state.playerCacheState.serverMap.get(servId)?.get(banDialogState.player?.sessionId ?? -1));
    const targetRemoved = targetPlayer !== undefined && targetPlayer.connected === false && targetPlayer.banned === true;

    return <Dialog 
        open={banDialogState.player !== undefined && banDialogState.serverId !== undefined}
        onClose={() => banDispatch.clearPlayerBanInfo()}
        onBackdropClick={() => banDispatch.clearPlayerBanInfo()}
    >
        <DialogTitle>
            Ban Player {banDialogState.player?.name}
        </DialogTitle>
        <DialogContent>
            <Grid container={true} direction="column">
                <Grid item={true}>
                    {issueBanErrorMessage?.length !== 0 && 
                    <Typography variant="h6" color="error">
                        {issueBanErrorMessage}
                    </Typography>}
                </Grid>
                <Grid item={true}>
                    <Typography variant="body1">Ban player to remove them from the server.</Typography>
                </Grid>
                <Grid item={true}>
                    <TextField label="Ban Duration" type="number" value={duration} onChange={event => setDuration(Number.parseInt(event.target.value, 10))}/>
                </Grid>
                {canBanPerm && <Grid item={true}>
                    <FormControlLabel 
                        label="Pernament?"
                        control={
                            <Checkbox 
                                checked={permament} 
                                onChange={(_: any, ch: boolean) => setPermanent(ch)} 
                                color="primary"/>}/>
                </Grid>}
                <Grid item={true}>
                    <Select title="Ban Multiplier" 
                        value={multiplier}
                        onChange={event => setMultiplier(event.target.value as number)}
                        style={{width:"100%"}}>
                        <MenuItem value={60}>Hours</MenuItem>
                        <MenuItem value={1440}>Days</MenuItem>
                        <MenuItem value={10080}>Weeks</MenuItem>
                    </Select>
                </Grid>
                <Grid item={true}>
                    <TextField variant="outlined" autoFocus={true} margin="dense" label="Ban Message" type="text"fullWidth={true}
                        value={banMessage}
                        onChange={event => setBanMessage(event.target.value)}/>
                </Grid>
                {banState !== 'form' && <Grid item={true}>
                    <List>
                        <ListItem>
                            <ListItemText>Ban Progress</ListItemText>
                        </ListItem>
                        <ListItem>
                            <ListItemAvatar>
                                {banState === 'issueBan' && <CircularProgress/>}
                                {banState !== 'issueBan' && issueBanErrorMessage === undefined && <Done/>}
                                {banState !== 'issueBan' && issueBanErrorMessage !== '' && <CloseIcon/>}
                            </ListItemAvatar>
                            <ListItemText>Issuing Ban to Overwatch</ListItemText>
                        </ListItem>
                        {banState !== 'issueBan' && issueBanErrorMessage && <ListItem>
                            <ListItemText inset={true}>Server refused to allow the ban. {issueBanErrorMessage}</ListItemText>
                        </ListItem>}
                        <ListItem>
                            <ListItemAvatar>
                                {banState === 'issueBan' && <MoreHorizIcon/>}
                                {banState === 'awaitConfirmation' && <CircularProgress/>}
                                {banState === 'success' && <Done/>}
                                {banState === 'failure' && <CloseIcon/>}
                            </ListItemAvatar>
                            <ListItemText>Awaiting confirmation of player removal</ListItemText>
                        </ListItem>
                        {banState === 'success' && <ListItem>
                            <ListItemText inset={true}>{banDialogState.player?.name} was banned from {servers?.data.serverName}.</ListItemText>
                        </ListItem>}
                        {banState === 'failure' && <ListItem>
                            <ListItemText inset={true}>{banDialogState.player?.name} failed to be banned from {servers?.data.serverName}.</ListItemText>
                        </ListItem>}
                    </List>
                </Grid>}
            </Grid>
        </DialogContent>

        <DialogActions>
            <Button variant="contained" color="primary" disabled={banState === 'awaitConfirmation' || banState === 'success' || banState === 'issueBan'} onClick={() => {
                setIssueBanErrorMessage(undefined);
                setBanState('issueBan');
                doBan(permament ? 'perm' : (duration * multiplier), banMessage, banDialogState?.player, banDialogState?.serverId)
                    .then(
                        () => {
                            setTimeout(() => {
                                if (!targetRemoved) {
                                    setBanState('failure')
                                } else {
                                    setBanState('success')
                                }
                            }, 2000);
                        }, 
                        error => {
                            setIssueBanErrorMessage(error);
                            setBanState('failure')
                        })
            }}>
                Ban
            </Button>
            {banState !== 'success' && <Button variant="contained" color="secondary" disabled={banState === 'awaitConfirmation' || banState === 'issueBan'} onClick={() => banDispatch.clearPlayerBanInfo()}>
                Cancel
            </Button>}

            {banState === 'success' && <Button variant="contained" color="secondary" onClick={() => banDispatch.clearPlayerBanInfo()}>
                Close
            </Button>}
        </DialogActions>
    </Dialog>
}

export { BasicPlayerBanDialog as PlayerBanDialog }