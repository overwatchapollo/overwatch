import React, { useState } from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button, TextField, DialogContentText, Typography } from '@material-ui/core'
import { Player } from 'api/chat/ConnectedPlayer';
import { whisper } from 'api/chat/ChatService';
import { useSelector } from 'react-redux';
import { RootState } from 'reducers';
import { useDispatch } from 'react-redux';
import { WhisperDispatcher } from 'container/WhisperDialogContainer';

function doWhisper(message: string, player?: Player, serverId?: number): Promise<Response> {
    if (!player) {
        return Promise.reject("No player selected.");
    }

    if (!serverId) {
        return Promise.reject("No server selected.");
    }

    if (message.length === 0) {
        return Promise.reject("No message supplied.")
    }

    return whisper(serverId, player.sessionId, message);
}

const BasicPlayerWhisperDialog: () => JSX.Element = () => {
    const whisperDialogState = useSelector((state: RootState) => state.whisperState);
    const dispatch = useDispatch();
    const whisperDispatcher = new WhisperDispatcher(dispatch);
    const [whisperMessage, setWhisperMessage] = useState("");
    const [errorMessage, setErrorMessage] = useState("");
    return <Dialog 
        open={whisperDialogState.player !== undefined && whisperDialogState.serverId !== undefined}
        onClose={() => whisperDispatcher.clearPlayerWhisperInfo()}
        onBackdropClick={() => whisperDispatcher.clearPlayerWhisperInfo()}
    >
        <DialogTitle>
            Whisper Player {whisperDialogState.player?.name}
        </DialogTitle>
        <DialogContent>
            {errorMessage.length !== 0 && 
                <Typography variant="h6" style={{color: 'red'}}>
                    {errorMessage}
                </Typography>}
            <DialogContentText>
                Whisper private message to player.
            </DialogContentText>
            <TextField variant="outlined" autoFocus={true} margin="dense" label="Whisper Message" type="text"fullWidth={true}
                value={whisperMessage}
                onChange={event => setWhisperMessage(event.target.value)}/>
        </DialogContent>

        <DialogActions>
            <Button variant="contained" color="primary" onClick={() => 
                doWhisper(whisperMessage, whisperDialogState?.player, whisperDialogState?.serverId)
                    .then(
                        () => whisperDispatcher.clearPlayerWhisperInfo(), 
                        error => setErrorMessage(error))
            }>
                Whisper
            </Button>
            <Button variant="contained" color="secondary" onClick={() => whisperDispatcher.clearPlayerWhisperInfo()}>
                Cancel
            </Button>
        </DialogActions>
    </Dialog>
}

export { BasicPlayerWhisperDialog as PlayerWhisperDialog }