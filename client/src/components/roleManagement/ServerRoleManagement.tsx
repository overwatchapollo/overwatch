import React from 'react';

import { connect } from 'react-redux';
import { PageContainer } from 'components/pagecontainer/PageContainer';
import { ServerConfigurationState } from 'reducers/ServerConfigurationReducer';
import { mapStateToProps, mapDispatchToProps } from 'container/ServerConfigurationContainer';
import { Grid, Typography } from '@material-ui/core';
import { SpecificServerRoleManager } from './SpecificServerRoleManager';

interface ServerRoleProps {
    servers: ServerConfigurationState;
}

const BasicServerRoleManagement: (props: ServerRoleProps) => JSX.Element = props => {
    const matchingServers = props.servers.servers.filter(x => x.actions.includes("owner"));
    return <PageContainer>
        
        <Grid container={true} direction="column" style={{padding: 8, width: "100%"}} spacing={2} alignItems="stretch" justify="flex-start">
            <Grid item={true}>
                <Typography variant="h4">Server Access Management</Typography>
            </Grid>
            {matchingServers
                .map(x => <Grid item={true} key={x.data.serverId}>
                    <SpecificServerRoleManager server={x}/>
            </Grid>)}
        </Grid>
    </PageContainer>
}

const ServerRoleManagement = connect(mapStateToProps, mapDispatchToProps)(BasicServerRoleManagement);
export { ServerRoleManagement }