import React, { useState, useEffect } from "react";
import { Typography, Grid, TableHead, TableRow, Table, TableCell, TableBody, Button, TextField, IconButton, Divider } from '@material-ui/core';
import { PermissionWrapper } from 'api/PermissionWrapper';
import { ServerConfiguration } from 'api/server/ServerConfiguration';
import { UserRole, Permission } from 'api/roles/UserRole';
import { getRoles, createRole } from 'api/roles/RoleService';

import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Done from '@material-ui/icons/Done';
import Close from '@material-ui/icons/Close';

interface RoleProps {
    server: PermissionWrapper<ServerConfiguration>;
}

const OkPermission: () => JSX.Element = () => {
    return <Done style={{color: 'green'}}/>
}

const NoPermission: () => JSX.Element = () => {
    return <Close style={{color: 'red'}}/>
}

function hasPermission(permissions: Array<Permission>, permission: string): boolean {
    return permissions.map(x => x.permission).includes(permission);
}

interface PermissionCheck {
    permissions: Array<Permission>;
    permission: string;
}

const CheckPermission: (props: PermissionCheck) => JSX.Element = props => {
    return <TableCell align="center">{hasPermission(props.permissions, props.permission) ? <OkPermission/> : <NoPermission/>}</TableCell>
}

interface CreateRowProps {
    onCreate: (role: UserRole) => void;
}

const CreateRoleRow:(props: CreateRowProps) => JSX.Element = props => {
    const [roleName, setRoleName] = useState("");
    return <TableRow>
        <TableCell colSpan={6}>
            <Grid container={true} spacing={2}>
                <Grid item={true}>
                    <TextField value={roleName} onChange={event => setRoleName(event.target.value)}/>
                </Grid>
                <Grid item={true}>
                    <Button onClick={() => createRole(roleName).then(x => props.onCreate(x))}>
                        Create
                    </Button>
                    <Button onClick={() => setRoleName("")}>Clear</Button>
                </Grid>
            </Grid>
        </TableCell>
    </TableRow>
}

const SpecificServerRoleManager: (props: RoleProps) => JSX.Element = props => {
    const [show, setShow] = useState(false);
    const [showConfiguration, setShowConfiguration] = useState(false);
    const [showAdministration, setShowAdministration] = useState(false);
    const [roles, setRows] = useState(new Array<UserRole>(0));

    const roleUpdate = async () => {
        getRoles().then(x => setRows(x));
    }

    useEffect(() => {roleUpdate()}, []);
    return <Grid container={true} direction="column" spacing={1}>
        <Grid item={true}>
            <Grid container={true} alignItems="center" direction="row">
                <Grid item={true}>
                    <Typography variant="h6">{props.server.data.serverName}</Typography>
                </Grid>
                <Grid item={true}>
                    <IconButton onClick={() => setShow(!show)}>{show ? <ArrowDropUpIcon/> : <ArrowDropDownIcon/>}</IconButton>
                </Grid>
            </Grid>
        </Grid>
        <Grid item={true}>
            <Divider/>
        </Grid>
        {show && <Grid item={true}>
            <Grid container={true} direction="column">
                <Grid item={true}>
                    <Grid container={true} alignItems="center" spacing={2}>
                        <Grid item={true}>
                            <Typography variant="subtitle1">Configuration Permissions</Typography>
                        </Grid>
                        <Grid item={true}>
                            <IconButton onClick={() => setShowConfiguration(!showConfiguration)}>{showConfiguration ? <ArrowDropUpIcon/> : <ArrowDropDownIcon/>}</IconButton>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item={true}>
                    <Divider/>
                </Grid>

                <Grid item={true}>
                    {showConfiguration && <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Role</TableCell>
                                <TableCell align="center">View</TableCell>
                                <TableCell align="center">Modify</TableCell>
                                <TableCell align="center">Assign Permissions</TableCell>
                                <TableCell align="center">Ownership</TableCell>
                                <TableCell align="center">Ban Refresh</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {roles.map(x => <TableRow key={x.id}>
                                <TableCell>{x.name}</TableCell>
                                <CheckPermission permissions={x.permissions} permission={`server:${props.server.data.serverId}:view`}/>
                                <CheckPermission permissions={x.permissions} permission={`server:${props.server.data.serverId}:modify`}/>
                                <CheckPermission permissions={x.permissions} permission={`server:${props.server.data.serverId}:assignPermissions`}/>
                                <CheckPermission permissions={x.permissions} permission={`server:${props.server.data.serverId}:owner`}/>
                                <CheckPermission permissions={x.permissions} permission={`server:${props.server.data.serverId}:ban-refresh`}/>
                            </TableRow>)}
                            <CreateRoleRow onCreate={role => setRows([...roles, role])}/>
                        </TableBody>
                    </Table>}
                </Grid>

                <Grid item={true}>
                    <Divider/>
                </Grid>

                <Grid item={true}>
                    <Grid container={true} spacing={2} alignItems="center">
                        <Grid item={true}>
                            <Typography variant="subtitle1">Administrative Permissions</Typography>
                        </Grid>
                        <Grid item={true}>
                            <IconButton onClick={() => setShowAdministration(!showAdministration)}>{showAdministration ? <ArrowDropUpIcon/> : <ArrowDropDownIcon/>}</IconButton>
                        </Grid>
                    </Grid>
                    
                    {showAdministration && <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Role</TableCell>
                                <TableCell>View</TableCell>
                                <TableCell>Kick</TableCell>
                                <TableCell>Ban (1Week)</TableCell>
                                <TableCell>Ban (2Week)</TableCell>
                                <TableCell>Ban (Pernament)</TableCell>
                                <TableCell>Ban Remove</TableCell>
                                <TableCell>Restart</TableCell>
                                <TableCell>Whisper</TableCell>
                                <TableCell>Broadcast</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {roles.map(x => <TableRow key={x.id}>
                                <TableCell>{x.name}</TableCell>
                                <CheckPermission permissions={x.permissions} permission={`server:${props.server.data.serverId}:view`}/>
                                <CheckPermission permissions={x.permissions} permission={`server:${props.server.data.serverId}:kick`}/>
                                <CheckPermission permissions={x.permissions} permission={`server:${props.server.data.serverId}:ban-1w`}/>
                                <CheckPermission permissions={x.permissions} permission={`server:${props.server.data.serverId}:ban-2w`}/>
                                <CheckPermission permissions={x.permissions} permission={`server:${props.server.data.serverId}:ban`}/>
                                <CheckPermission permissions={x.permissions} permission={`server:${props.server.data.serverId}:ban-remove`}/>
                                <CheckPermission permissions={x.permissions} permission={`server:${props.server.data.serverId}:restart`}/>
                                <CheckPermission permissions={x.permissions} permission={`server:${props.server.data.serverId}:whisper`}/>
                                <CheckPermission permissions={x.permissions} permission={`server:${props.server.data.serverId}:broadcast`}/>
                            </TableRow>)}
                            <CreateRoleRow onCreate={role => setRows([...roles, role])}/>
                        </TableBody>
                    </Table>}
                </Grid>

                <Grid item={true}>
                    <Divider/>
                </Grid>
            </Grid>
        </Grid>}
    </Grid>
}

export { SpecificServerRoleManager }