import React from "react";
import { useSelector } from 'react-redux';
import { RootState } from 'reducers';
import { ChatSubscriptionManager } from './ChatSubscriptionManager';

const ChatSubscriberAggregate: () => JSX.Element = () => {
    const servers = useSelector((state: RootState) => state.servers.servers.map(x => x.data.serverId))
    return <>
        {servers.map((x, i) => <ChatSubscriptionManager serverId={x} key={i}/>)}
    </>
}

export { ChatSubscriberAggregate }