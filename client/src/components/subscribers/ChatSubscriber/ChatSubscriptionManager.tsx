import React, { useEffect,  } from "react"
import { useDispatch } from 'react-redux';
import { ChatDispatcher } from 'container/ChatCacheContainer';
import { getChatLog } from 'api/chat/ChatService';
import { asyncScheduler } from 'rxjs';
import { observeOn } from 'rxjs/operators';
import { chatService } from 'reducers/ServiceReducer';

const ChatSubscriptionManager: (props: {serverId: number}) => JSX.Element = props => {
    const { serverId } = props;
    const dispatch = useDispatch();
    
    useEffect(() => {
        const chatDispatcher = new ChatDispatcher(dispatch);
        const sub = chatService.getChatUpdatesObservable(serverId).pipe(observeOn(asyncScheduler)).subscribe(x => chatDispatcher.updateChatLog(serverId, x));
        getChatLog(serverId, 100)
            .then(x => chatDispatcher.setChatLog(serverId, x))
            .catch(() => console.error(`Failed to get chat log`))
        return () => {
            sub.unsubscribe();
        }
    }, [serverId, dispatch])
    
    return <>{false}</>
}

export { ChatSubscriptionManager }