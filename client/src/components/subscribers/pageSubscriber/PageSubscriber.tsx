import React, { useEffect } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from 'reducers';
import { getPageList } from 'api/session/PageService';
import { SetPages, ClearPages } from 'actions/PageActions';

const PageSubscriber: () => JSX.Element | null = () => {
    const userSelect = useSelector((state:RootState) => state.session.activeUser);
    const dispatch = useDispatch();
    useEffect(() => {
        if (userSelect) {
            getPageList().then(x => dispatch(SetPages(x)));
        } else {
            dispatch(ClearPages());
        }
    }, [userSelect, dispatch])
    return <div/>
}

export { PageSubscriber }