import React from 'react';
import { useDispatch } from 'react-redux'
import { useEffect } from 'react';
import { asyncScheduler } from 'rxjs';
import { observeOn } from 'rxjs/operators';
import { PlayerDispatcher } from 'container/PlayerContainer';
import { getPlayerList } from 'api/player/PlayerService';
import { playerService } from 'reducers/ServiceReducer';

const PlayerSubscription: (props: { serverId: number}) => JSX.Element = props => {
    const dispatch = useDispatch();
    useEffect(() => {
        const playerDispatcher = new PlayerDispatcher(dispatch);
        function refresh() {
            getPlayerList(props.serverId).then(x => playerDispatcher.setPlayerLog(props.serverId, x));
        }
        refresh();
        const playerUpdateSub = playerService.getPlayerUpdatesObservable(props.serverId)
            .pipe(observeOn(asyncScheduler))
            .subscribe(x => playerDispatcher.updatePlayerLog(props.serverId, x))
        const playerStaleSub = playerService.getStalePlayerListObservable(props.serverId)
            .pipe(observeOn(asyncScheduler))
            .subscribe(() => refresh());
        return function cleanup() {
            playerUpdateSub.unsubscribe();
            playerStaleSub.unsubscribe();
        }
    }, [props.serverId, dispatch])
    return <div/>
}

export { PlayerSubscription }