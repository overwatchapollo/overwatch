import React from "react";
import { useSelector } from 'react-redux';
import { RootState } from 'reducers';
import { PlayerSubscription } from './PlayerSubscriber';

const PlayerSubscriberAggregate: () => JSX.Element = () => {
    const servers = useSelector((state: RootState) => state.servers.servers.map(x => x.data.serverId))
    return <>
        {servers.map(x => <PlayerSubscription serverId={x} />)}
    </>
}

export { PlayerSubscriberAggregate }