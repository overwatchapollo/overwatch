import React, { useEffect } from "react";
import { useSelector } from 'react-redux';
import { RootState } from 'reducers';
import { mqttStream } from 'reducers/ServiceReducer';
const MqttSubscriber: () => JSX.Element | null = () => {
    const userSelect = useSelector((state:RootState) => state.session.activeUser, (a,b) => a?.id === b?.id);
    useEffect(() => {
        if (userSelect) {
            console.log(`mqtt login`)
            const username = localStorage.getItem("username") ?? "";
            const password = localStorage.getItem("password") ?? "";
            mqttStream.connect(username, password)
        } else {
            console.log(`no user`)
            mqttStream.disconnect();
        }
    }, [userSelect])
    return <div/>
}

export { MqttSubscriber }