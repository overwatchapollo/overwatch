import React, { useState } from 'react';
import { Grid, TextField, Button, useTheme } from '@material-ui/core';
import styles from 'styles';
import { useDispatch } from 'react-redux';
import { doLogin } from 'App';

const LoginBar: () => JSX.Element = () => {
    const dispatch = useDispatch();
    const classes = styles(useTheme());
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    return <form onSubmit={() => doLogin(dispatch, username, password)}>
        <Grid className={classes.loginBox} container={true} direction="row" spacing={1} alignItems="flex-end">
            <Grid item={true}>
                <TextField 
                    id="username"
                    type="text" 
                    autoComplete='username'
                    placeholder="Username"
                    value={username}
                    onChange={event => setUsername(event.target.value)}/>
            </Grid>
            <Grid item={true}>
                <TextField 
                id="password"
                type="password"
                autoComplete='current-password'
                placeholder="Password"
                value={password}
                onChange={event => setPassword(event.target.value)}/>
                
            </Grid>
            <Grid item={true}>
                <Button className={classes.loginButton}
                    type="submit" 
                    variant="outlined">
                    Login
                </Button>
            </Grid>
        </Grid>
    </form>
}

export { LoginBar }