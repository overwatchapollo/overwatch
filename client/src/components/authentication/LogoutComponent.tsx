import React, { useState, useEffect } from 'react';
import { logoutRequest } from 'api/session/LoginService';
import { Redirect } from 'react-router';
import { useDispatch } from 'react-redux';
import { ClearSession } from 'actions/SessionActions';

const PureLogoutComponent: () => JSX.Element = () => {
    const dispatch = useDispatch();
    const [success, setSuccess] = useState(false);
    useEffect(() => {
        logoutRequest()
            .then(() => setSuccess(false))
            .then(() => dispatch(ClearSession()));
    })
    return <div>
        {success && <Redirect to="/login"/>}
    </div>
}

export { PureLogoutComponent as LogoutComponent }