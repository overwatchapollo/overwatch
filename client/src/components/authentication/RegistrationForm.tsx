import React, { FormEvent, useState } from 'react';
import { registerUser } from 'api/session/LoginService';
import { Card, CardContent, TextField, Button, Typography, Grid, Box, List, ListItem, useTheme } from '@material-ui/core';
import { Redirect } from 'react-router';
import { ApiError } from 'api/ApiError';
import styles from 'styles';

import { PageContainer } from 'components/pagecontainer/PageContainer';


const RegistrationFormComponent: () => JSX.Element = () => {
    
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [email, setEmail] = useState("");
    const [registerSuccessful, setRegisterSuccessful] = useState(false);
    const [errors, setErrors] = useState(new Array<string>());

    function handleSubmit(event: FormEvent<HTMLFormElement> | undefined, username: string, password: string, email: string): Promise<Response> {
        event?.preventDefault();
        return registerUser(username, password, email);
    }

    const classes = styles(useTheme());
    return <PageContainer>
        {registerSuccessful && <Redirect to="/"/>}
        <Box>
            <Grid container={true} direction="column" alignItems="center" style={{paddingTop: 16, paddingBottom: 16}}>
                <Grid item={true}>
                    <Typography variant="h5">Registration</Typography>
                </Grid>
                <Grid item={true}>
                    <List>
                        <ListItem>
                            <TextField 
                                variant="outlined"
                                id="username" 
                                autoFocus={true} 
                                margin="dense" 
                                label="Username" 
                                type="text" 
                                required={true}
                                fullWidth={true}
                                value={username}
                                onChange={event => setUsername(event.target.value)}/>
                        </ListItem>
                        <ListItem>
                            <TextField 
                                variant="outlined"
                                id="password"
                                margin="dense"
                                label="Password"
                                type="password"
                                fullWidth={true}
                                required={true}
                                value={password}
                                onChange={event => setPassword(event.target.value)}
                                />
                        </ListItem>
                        <ListItem>
                            <TextField 
                                variant="outlined"
                                id="email"
                                margin="dense"
                                label="Email"
                                type="text"
                                fullWidth={true}
                                value={email}
                                onChange={event => setEmail(event.target.value)}
                                />
                        </ListItem>
                    </List>
                </Grid>
                <Grid item={true}>
                    <Button variant="contained" color="primary" type="submit" onClick={() => 
                        handleSubmit(undefined, username, password, email)
                        .then(x => {
                            if (x.status === 200) {
                                setRegisterSuccessful(true)
                            } else {
                                x.json().then(y => {
                                    const error = y as ApiError;
                                    setErrors(error.errors)
                                });
                            }
                        })}>
                        Register
                    </Button>
                </Grid>
                {errors.length > 0 && <Grid item={true}>
                    <Card className={classes.errorbar}>
                        <CardContent>
                            <Grid container={true} direction="column">
                                <Grid item={true}><Typography color="error" variant="h6">Errors</Typography></Grid>
                                {errors?.map((x, i) => <Typography key={x} color="error" variant="body1">{i + 1}. {x}</Typography>)}
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>}
            </Grid>
        </Box>
    </PageContainer>
}

const RegistrationForm = RegistrationFormComponent;
export { RegistrationForm }