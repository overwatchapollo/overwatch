import React, { useState } from 'react';
import { TextField, Button, Grid, Typography } from '@material-ui/core';
import { Redirect } from 'react-router';
import { PageContainer } from 'components/pagecontainer/PageContainer';
import { useDispatch } from 'react-redux';
import { doLogin } from 'App';

const LoginForm: () => JSX.Element = () => {
    const dispatch = useDispatch();
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [redirect, setRedirect] = useState(false);

    return <PageContainer>
        {redirect && <Redirect to="/register"/>}

        <Grid container={true} direction="column" spacing={2} style={{padding: 8}}>
            <Grid item={true}>
                <Typography variant="h4">Login</Typography>
            </Grid>
            <Grid item={true}>
                <TextField 
                    id="username" 
                    autoFocus={true} 
                    label="Username" 
                    type="text" 
                    fullWidth={true}
                    value={username}
                    onChange={event => setUsername(event.target.value)}/>
            </Grid>
            <Grid item={true}>
                <TextField 
                    id="password"
                    label="Password"
                    type="password"
                    fullWidth={true}
                    value={password}
                    onChange={event => setPassword(event.target.value)}
                    />
            </Grid>
            <Grid item={true}>
                <Button variant="contained" onClick={() => doLogin(dispatch, username, password)}>Login</Button>
            </Grid>
            <Grid item={true}>
                <Button variant="contained" onClick={() => setRedirect(true)}>
                Register
                </Button>
            </Grid>
        </Grid>
    </PageContainer>
}

export { LoginForm }