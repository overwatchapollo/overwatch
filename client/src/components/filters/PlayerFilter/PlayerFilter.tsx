import React, {useState} from 'react';
import { TextField, Button, Grid } from '@material-ui/core';

interface IPlayerFilterProps {
    pushFilter: (players: string) => void;
}

const PlayerFilter: (props: IPlayerFilterProps) => JSX.Element = (props: IPlayerFilterProps) => {
    const [name, setName] = useState("");
    return <Grid item={true}>
        <TextField
                autoFocus={true}
                margin="dense"
                id="reason"
                label="Name"
                type="text"
                fullWidth={true}
                value={name}
                onChange={(event:any) => setName(event.target.value)}/>
            <Button onClick={() => props.pushFilter(name)}>Apply</Button>
            <Button onClick={() => {setName(""); props.pushFilter("");}}>Clear</Button>
    </Grid>
}

export default PlayerFilter;