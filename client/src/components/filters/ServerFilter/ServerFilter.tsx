import React from 'react';
import { FormControlLabel, Checkbox, Grid } from '@material-ui/core';
import { ServerConfiguration } from 'api/server/ServerConfiguration';


interface IServerFilterProps {
    allServers: Array<ServerConfiguration>;
    selectedServers: (server: Array<ServerConfiguration>) => void;
}

interface IServerFilterState {
    servers: Map<ServerConfiguration, boolean>;
    viewAll: boolean;
}

export class ServerFilter extends React.Component <IServerFilterProps, IServerFilterState> {
    constructor (props: IServerFilterProps) {
        super(props);
        this.state = {
            servers: new Map(this.props.allServers.map(x => [x, false])),
            viewAll: false,
        }
    }

    render() {
        const servers = this.props.allServers;
        return (
                <Grid container={true} direction="row" justify="center" spacing={2}>
                    {this.props.allServers.length !== 0 && <Grid item={true}>
                        <FormControlLabel 
                            value={this.state.viewAll ? "true" : "false"}
                            control={<Checkbox onChange={(event, checked) => {
                                servers.forEach(x => this.setVisibility(x, checked))
                                this.updateVisibility();
                                this.setState({viewAll: checked})
                            }}/>} 
                            checked={this.state.viewAll}
                            label={"All"}/>
                    </Grid>}
                    {servers.map((x, i) => {
                        const conf = this.getServerConfiguration(servers, x.serverId);
                        const visible = this.state.servers.get(x);
                        const value = visible === true;
                        return <Grid item={true} key={x.serverId}>
                            <FormControlLabel 
                                value={value ? "true" : "false"}
                                control={<Checkbox onChange={(event, checked) => {
                                    this.setVisibility(x, checked);
                                    if (this.state.viewAll && !checked) {
                                        this.setState({viewAll: false})
                                    }

                                    if (this.props.allServers.map(x => this.state.servers.get(x) === true).reduce((a,b) => b === false ? false : a)) {
                                        this.setState({viewAll: true})
                                    }
                                    this.updateVisibility();
                                }}/>} 
                                checked={value}
                                label={conf.serverName}/>
                                
                        </Grid>;
                    })}
                </Grid>
        )
    }

    private getServerConfiguration(servers: Array<ServerConfiguration>, serverId: number): ServerConfiguration {
        const conf = servers.find(x => x.serverId === serverId);
        if (conf === undefined) {
            throw new Error("state error");
        }

        return conf;
    }

    private setVisibility(server: ServerConfiguration, value: boolean) {
        this.state.servers.set(server, value);
    }

    private updateVisibility() {
        this.setState({servers: this.state.servers});
        const visibleServers = new Array<ServerConfiguration>(0);
        this.state.servers.forEach((value, key) => {
            if (value) {
                visibleServers.push(key);
            }
        });
        this.props.selectedServers(visibleServers);
    }
}