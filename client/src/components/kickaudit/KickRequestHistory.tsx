import React from 'react';
import { Table, TableRow, Typography, TableHead, TableCell } from '@material-ui/core';
import { KickHistoryAudit } from '../../api/management/kickaudit/KickHistoryAudit';
import { getKickAuditHistory } from 'api/management/kickaudit/KickAuditService';
import { Toast } from 'api/toast/Toast';
import { IMessageStream } from 'api/stream';
import { Subscription } from 'rxjs';
import { PageContainer } from 'components/pagecontainer/PageContainer';

interface IKickRequestState {
    kickHistory: Array<KickHistoryAudit>;
    subscription: Subscription;
}

interface IKickRequestProps {
    stream: IMessageStream;
    serverGuid: string;
    toast: Toast;
}

export class KickRequestHistory extends React.Component<IKickRequestProps, IKickRequestState> {
    constructor(props: IKickRequestProps) {
        super(props);
        this.state = {
            kickHistory: new Array(0),
            subscription: this.props.stream.observeTopic("/player/kicked").subscribe(x => this.onMessage(x.topic, x.payload))
        }

        getKickAuditHistory(this.props.serverGuid)
            .then(x => this.setState({kickHistory: x}))
            .catch(error => console.log("could not get kick history"));
        props.toast.register("/player/kicked", (obj:KickHistoryAudit) => obj.kickedUser + " has been kicked by " + obj.requester);
    }

    componentWillUnmount() {
        this.state.subscription.unsubscribe();
    }

    onMessage(topic: string, message: any) {
        const newKick = message as KickHistoryAudit;
        const value = this.state.kickHistory;
        value.push(newKick);
        this.setState({kickHistory: value});
    }

    render() {
        return <PageContainer>
            <Typography variant="h6" color="inherit" noWrap={true}>
                Server {this.props.serverGuid}
            </Typography>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Time</TableCell>
                        <TableCell>Kicker</TableCell>
                        <TableCell>Kicked User</TableCell>
                        <TableCell>Message</TableCell>
                    </TableRow>
                </TableHead>

                {this.state.kickHistory.map((kickAudit: KickHistoryAudit, index: number) => {
                    const d = new Date(kickAudit.timeStamp);
                    return(
                    <TableRow key={index}>
                        <TableCell>{d.toLocaleString()}</TableCell>
                        <TableCell>{kickAudit.requester}</TableCell>
                        <TableCell>{kickAudit.kickedUser}</TableCell>
                        <TableCell>{kickAudit.message}</TableCell>
                    </TableRow>)
                })}
            </Table>
        </PageContainer>
    }
}