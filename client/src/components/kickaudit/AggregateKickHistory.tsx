import React from 'react';
import { PermissionWrapper } from 'api/PermissionWrapper';
import { ServerConfiguration } from 'api/server/ServerConfiguration';
import { getKickAuditHistory } from 'api/management/kickaudit/KickAuditService';
import { Grid, Typography, TableHead, TableBody, Table, TableRow, TableCell } from '@material-ui/core';
import { KickHistoryAudit } from 'api/management/kickaudit/KickHistoryAudit';
import { ServerFilter } from 'components/filters/filters';
import { PageContainer } from 'components/pagecontainer/PageContainer';

interface IAggregateKickHistory {
    servers: Array<PermissionWrapper<ServerConfiguration>>;
}

interface IAggregateHistoryState {
    selectedServers: Array<number>;
    historyMap: Map<number, Array<KickHistoryAudit>>;
}

class basicAggregateKickHistory extends React.Component<IAggregateKickHistory, IAggregateHistoryState> {
    constructor(props: IAggregateKickHistory) {
        super(props);
        this.state = {
            selectedServers: new Array(0),
            historyMap: new Map()
        }
    }

    render() {
        return <PageContainer>
            <Grid container={true} direction="column" alignItems="center" style={{padding:8}} wrap='nowrap'>
                <Grid item={true}>
                    <Typography variant="h4">Aggregate Kick History</Typography>
                </Grid>
                <Grid item={true}>
                    <ServerFilter
                        allServers={this.props.servers.map(x => x.data)}
                        selectedServers={servers => {
                                const existing = this.state.selectedServers;
                                const newServers = servers.filter(x => !existing.includes(x.serverId));                                
                                newServers.map(x => {
                                    const history = getKickAuditHistory(x.serverId.toString())
                                    return {x, history};
                                }).forEach(x => x.history.then(y => {
                                        this.state.historyMap.set(x.x.serverId, y)
                                        this.setState({historyMap: this.state.historyMap})
                                    }));
                                this.setState({selectedServers: servers.map(x => x.serverId)})
                            }
                        }/>
                    </Grid>
                </Grid>
                <Grid item={true}>
                    <Grid container={true} direction="row" spacing={2} alignItems="center">
                        {this.state.selectedServers.map(x => {
                        const history = this.state.historyMap.get(x);
                        const conf = this.props.servers.filter(y => y.data.serverId === x)[0];
                        return <Grid item={true} key={x}>
                            <KickHistory name={conf.data.serverName} history={history ? history : new Array(0)}/>
                        </Grid>
                    })}
                </Grid>
            </Grid>
        </PageContainer>
    }
}

export { basicAggregateKickHistory as AggregateKickHistory }

interface IKickHistory {
    name: string;
    history: Array<KickHistoryAudit>;
}

const KickHistory: (props: IKickHistory) => JSX.Element = (props: IKickHistory) => {
    return <Grid container={true} direction="column" alignItems="center" spacing={2} style={{padding:8}}>
        <Grid item={true}>
            <Typography variant="h6">{props.name}</Typography>
        </Grid>
        <Grid item={true}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Timestamp</TableCell>
                        <TableCell>Requester</TableCell>
                        <TableCell>Kicked User</TableCell>
                        <TableCell>Message</TableCell>
                        <TableCell>Success</TableCell>
                    </TableRow>
                </TableHead>
                {props.history.reverse().map((x, i) => 
                {
                    const timestamp = new Date(x.timeStamp);
                    return <TableBody key={i}>
                    <TableRow>
                        <TableCell>{timestamp.toLocaleString()}</TableCell>
                        <TableCell>{x.requester}</TableCell>
                        <TableCell>{x.kickedUser}</TableCell>
                        <TableCell>{x.message}</TableCell>
                        <TableCell>{x.successful + ""}</TableCell>
                    </TableRow>
                </TableBody>})}
            </Table>
        </Grid>
    </Grid>
}