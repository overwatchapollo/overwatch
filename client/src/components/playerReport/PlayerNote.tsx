import React from 'react';
import { Card, CardContent, CardActions, Button, Grid, Divider } from '@material-ui/core';
import { Note } from 'api/playerManagement/Note';

interface INoteProps {
    note: Note;
    onModify: (id: number) => void;
    onDelete: (id: number) => void;
}

const PlayerNote = (props: INoteProps) => {
    const num = Number.parseInt(props.note.createdOn);
    return <Card>
            <CardContent>
                <Grid container={true}>
                    <Grid xs={12} item={true}>
                        {props.note.note}
                        <Divider/>
                    </Grid>
                    <Grid xs={6} item={true}>
                        Author {props.note.createdBy}
                    </Grid>
                    <Grid xs={6} item={true}>
                        Date {(new Date(num)).toLocaleString()}
                    </Grid>
                </Grid>
            </CardContent>
            <CardActions>
                <Button onClick={() => props.onModify(props.note.id)}>Modify</Button>
                <Button onClick={() => props.onDelete(props.note.id)}>Delete</Button>
            </CardActions>
        </Card>
}

export default PlayerNote;