import React, { useState } from 'react';
import { Dialog, DialogContent, DialogActions, Button, TextField, DialogTitle, Grid, Typography } from '@material-ui/core';

interface INoteProps {
    onOpen:() => boolean;
    onClose:() => void;
    name: () => string;
    onSubmit: (note: string) => void;
}

const PlayerNoteDialog = (props: INoteProps) => {
    const [note, setNote] = useState("");
    const maxSize = 1024;
    return <Dialog 
            open={props.onOpen()}
            aria-labelledby="form-dialog-title"
            onBackdropClick={() => {handleClose(props.onClose); setNote("")}}>
        <DialogTitle>Add note to {props.name()}</DialogTitle>
        <DialogContent>
            <Grid container={true} direction="column">
                <Grid item={true}>
                    <TextField
                        label="Note message"
                        type="string"
                        value={note}
                        onChange={(event:any) => {
                            if (note.length < maxSize) {
                                setNote(event.target.value);
                            }
                        }}/>
                </Grid>
                <Grid item={true}>
                    <Typography variant="caption">{maxSize - note.length} of  {maxSize}</Typography>
                </Grid>
            </Grid>
            

        </DialogContent>
        <DialogActions>
            <Button onClick={event => handleSubmit(note, props.onSubmit, props.onClose)}>Submit</Button>
            <Button onClick={event => {handleClose(props.onClose); setNote("")}}>Cancel</Button>
        </DialogActions>
    </Dialog>
}

export default PlayerNoteDialog;

function handleSubmit(note: string, onSubmit: (note: string) => void, onClose:() => void) {
    onSubmit(note);
    handleClose(onClose);
}

function handleClose(onClose:() => void) {
    onClose();
}