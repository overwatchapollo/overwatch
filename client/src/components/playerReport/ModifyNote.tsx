import React, { useState } from 'react';
import { Dialog, DialogContent, Button, DialogActions, TextareaAutosize, DialogTitle } from '@material-ui/core'

interface ModNotesProps {
    id: number;
    note: string;
    setNote: (note: string) => void;
    onClose: () => void;
}

const ModifyNotes: (props: ModNotesProps) => JSX.Element = (props: ModNotesProps) => {
    const [updatedNote, setUpdatedNote] = useState(props.note);
    const [forcedUpdate, setForcedUpdate] = useState(false);
    const close = () => {
        setUpdatedNote("");
        setForcedUpdate(false);
        props.onClose();
    };
    if (updatedNote === "" && props.note !== "" && !forcedUpdate) {
        setUpdatedNote(props.note);
        setForcedUpdate(true);
    }
    return <Dialog open={props.id !== 0} onBackdropClick={close}>
        <DialogTitle>
            Modify note
        </DialogTitle>
        <DialogContent>
            <TextareaAutosize value={updatedNote} onChange={event => setUpdatedNote(event.target.value)}>
                {props.note}
            </TextareaAutosize>
        </DialogContent>
        <DialogActions>
            <Button onClick={() => {
                    props.setNote(updatedNote);
                    close();
                }}>
                Submit
            </Button>
            <Button onClick={close}>Close</Button>
        </DialogActions>
    </Dialog>
}

export {ModifyNotes}