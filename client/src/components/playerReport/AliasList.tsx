import React from 'react';
import { Table, TableHead, TableRow, TableCell, TableBody, Card, CardContent, CardHeader } from '@material-ui/core';
import { PlayerAlias } from '../../api/playerReport/PlayerAlias';

interface IAliasList {
    getList : () => Array<PlayerAlias>;
}

const AliasList: (props: IAliasList) => JSX.Element = (props: IAliasList) => {
    const aliasList = props.getList();
    return (
        <Card>
            <CardHeader title="Known Aliases"/>                
            <CardContent>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Alias</TableCell>
                            <TableCell>Times Used</TableCell>
                            <TableCell>First Used</TableCell>
                            <TableCell>Last Used</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {aliasList.map((alias: PlayerAlias, i: number) => {
                            return (
                                <TableRow key={i}>
                                    <TableCell>{alias.alias}</TableCell>
                                    <TableCell>{alias.used}</TableCell>
                                    <TableCell>{alias.firstUsed.toLocaleString()}</TableCell>
                                    <TableCell>{alias.lastUsed.toLocaleString()}</TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            </CardContent>
        </Card>
    )
}

export default AliasList;