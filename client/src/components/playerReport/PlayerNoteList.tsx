import React from 'react';
import { Note } from 'api/playerManagement/Note';
import { Grid } from '@material-ui/core';
import PlayerNote from './PlayerNote';

interface IPlayerNoteList {
    notes: Array<Note>;
    onModify: (id: number) => void;
    onDelete: (id: number) => void;
}

const PlayerNoteList = (props:IPlayerNoteList) => {
    return <Grid container={true}>
            {props.notes.map(x => <Grid item={true} key={x.id} xs={12}>
                <PlayerNote note={x} onModify={props.onModify} onDelete={props.onDelete}/>
            </Grid>)}
        </Grid>
}

export default PlayerNoteList;