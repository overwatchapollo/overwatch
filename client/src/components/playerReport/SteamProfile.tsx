import React from 'react';
import { Paper, Card, CardContent } from '@material-ui/core';
import { ConversionData } from 'api/player/ConversionData';
import { SteamProfileReport } from '../../api/playerReport/SteamProfileReport';
import { playerService } from 'reducers/ServiceReducer';

interface ISteamProps {
}
interface ISteamProfile {
    conv: ConversionData | undefined;
    steamProfile: SteamProfileReport | undefined;   
}

class basicSteamProfile extends React.Component<ISteamProps, ISteamProfile> {
    /*
    private static steamKey = "76561197969814893";
*/
    constructor(props: any) {
        super(props);
        this.state = {
            conv: undefined,
            steamProfile: undefined
        }
        
        
    }

    componentDidMount() {
        /*
        this.props.playerService.getSteamIdFromGuid(basicSteamProfile.steamKey)
            .then(x => {
                const data = x.data[0]
                this.setState({conv: data});
                
            }).then(x => this.getSteamProfileInfo());
            */
    }

    

    renderConv() {
        if (this.state.conv === undefined) {
            return <div/>
        }
        
        return (<Card>
            <CardContent>
            Steam ID: {this.state.conv.steamid} <br/>
            Arma GUID: {this.state.conv.src}
            </CardContent>
        </Card>);
    }


    renderProfile() {
        if (this.state.steamProfile === undefined) {
            return <div/>
        }
        return (<Card>
            Steam Name: {this.state.steamProfile.personaname}
        </Card>)
    }

    getSteamProfileInfo() {
        playerService.getSteamProfile()
            .then(x => {
                this.setState({steamProfile: x as SteamProfileReport});
            })
    }


    render() {
        return (<Paper>
            Test good?
            {this.renderConv()}
            {this.renderProfile()}
        </Paper>);
    }
}

export { basicSteamProfile as SteamProfile }