import React from 'react';
import { PlayerReport } from '../../api/playerReport/PlayerReport';
import { Paper, CardHeader, Card, CardContent, TextField, CardActions, Button, Table, TableBody, TableRow, TableCell, TableHead } from '@material-ui/core';
import { getPlayerReportName, getPlayerReport } from 'api/player/PlayerReportService';
import { PlayerReportView } from './PlayerReportView';

interface IFetchPlayer {
    searchDone: boolean;
    playersNameResults: Array<PlayerReport>;
    playerToReport: PlayerReport | undefined;
    guid: string;
    name: string;
}

class basicPlayerReportComponent extends React.Component<{}, IFetchPlayer> {

    constructor(props: {}) {
        super(props);
        this.state = {
            searchDone: false,
            playersNameResults: new Array(0),
            playerToReport: undefined,
            guid: "",
            name: "",
        }
    }

    handleSubmit(event: any) {
        event.preventDefault();
        getPlayerReport(this.state.guid)
            .then(x => this.setState({playerToReport: x}))
            .catch(error => this.setState({playerToReport: undefined}))
    }

    handleNameSearch(event: any) {
        event.preventDefault();
        getPlayerReportName(this.state.name)
            .then(x => this.setState({playersNameResults: x, searchDone: true}))
    }

    selectPlayer(player: PlayerReport) {
        this.setState({playerToReport: player});
    }

    renderResult(results: Array<PlayerReport>): JSX.Element {
        return(<Card>
            <CardHeader title="Search Results"/>
            <CardContent>
                <CardActions>
                    <Button onClick={(event: any) => this.setState({playersNameResults: new Array(0), playerToReport: undefined, searchDone: false})}>
                        Clear Results
                    </Button>
                </CardActions>
                <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell>GUID</TableCell>
                        <TableCell>Visit Count</TableCell>
                        <TableCell>First Seen</TableCell>
                        <TableCell>Last Seen</TableCell>
                        <TableCell>Load</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {this.state.playersNameResults.map((value, index) => {
                        return <TableRow key={value.guid}>
                            <TableCell>{value.getLastName()}</TableCell>
                            <TableCell>{value.guid}</TableCell>
                            <TableCell>{value.recordCount}</TableCell>
                            <TableCell>{value.firstSeen.toLocaleString()}</TableCell>
                            <TableCell>{value.lastSeen.toLocaleString()}</TableCell>
                            <TableCell><Button onClick={() => this.selectPlayer(value)}>Select</Button></TableCell>
                        </TableRow>
                    })}
                </TableBody>
            </Table>
            </CardContent>
        </Card>);
    }

    render() {
        return (
            <Paper>
                <Card>
                    <CardHeader title="Search for Player"/>
                    <CardContent>
                        <TextField 
                            margin="dense"
                            id="name"
                            label="Player Name"
                            type="text"
                            fullWidth={true}
                            defaultValue=""
                            value={this.state.name}
                            onChange={(event: any) => this.setState({name: event.target.value})}/>
                        <TextField
                            autoFocus={true}
                            margin="dense"
                            id="reason"
                            label="GUID"
                            type="text"
                            fullWidth={true}
                            defaultValue=""
                            value={this.state.guid}
                            onChange={(event: any) => this.setState({guid: event.target.value})}/>
                        
                    </CardContent>
                    <CardActions>
                            <Button
                                onClick={(event: any) => this.handleNameSearch(event)}>
                                Search By Name
                            </Button>
                            <Button 
                                variant="contained" 
                                color="secondary" 
                                type="submit"
                                onClick={(event: any) => this.handleSubmit(event)}>Search By GUID</Button>
                        </CardActions>
                </Card>

                {this.state.searchDone ? this.renderResult(this.state.playersNameResults) : <div/>}
                {this.state.playerToReport === undefined 
                    ? <div/> 
                    : <PlayerReportView playerToReport={this.state.playerToReport}/>}
            </Paper>
        )
    }
}

export { basicPlayerReportComponent as PlayerReportComponent }