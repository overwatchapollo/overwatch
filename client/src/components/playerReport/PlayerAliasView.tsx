import React from 'react';
import { PlayerReport } from '../../api/playerReport/PlayerReport';
import { Card, CardContent } from '@material-ui/core';
import AliasList from './AliasList';

interface IPlayer {
    player: () => (PlayerReport | undefined);
}

export class PlayerAliasView extends React.Component<IPlayer, IPlayer> {
    constructor(props: IPlayer) {
        super(props);
        this.state = {
            player: props.player
        }
    }

    render() {
        const player = this.state.player();
        if (player === undefined) {
            return <div/>
        }

        return (
            <Card>
                <CardContent>
                <AliasList getList={() => {
                    const pl = this.state.player();
                    return pl === undefined
                        ? [] 
                        : pl.aliasRecords;
                }}/>
                </CardContent>
            </Card>
        );
    }
}