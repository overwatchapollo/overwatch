import React from 'react';
import { CardContent, Card, CardActions, Button } from '@material-ui/core';
import { KickHistoryView } from './KickHistoryView';
import { PlayerReport } from '../../api/playerReport/PlayerReport';
import { PlayerAliasView } from './PlayerAliasView';
import { addPlayerNote, deletePlayerNote } from 'api/player/PlayerReportService';
import PlayerNoteList from './PlayerNoteList';
import PlayerNoteDialog from './PlayerNoteDialog';
import { PageContainer } from 'components/pagecontainer/PageContainer';

interface IPlayerReportProps {
    playerToReport: PlayerReport;
}

interface IReportState {
    addNote: boolean;
}

export class PlayerReportView extends React.Component<IPlayerReportProps, IReportState> {
    constructor(props: IPlayerReportProps) {
        super(props);
        this.state = {
            addNote: false
        }
    }
    render() {
        return <PageContainer>
            <PlayerAliasView player={() => this.props.playerToReport}/>
            <KickHistoryView history={() => this.props.playerToReport.kickHistory}/>

            <PlayerNoteDialog 
                onOpen={() => this.state.addNote}
                onClose={() => this.setState({addNote: false})}
                name={() => this.props.playerToReport.name}
                onSubmit={note => addPlayerNote(this.props.playerToReport.guid, note)}/>
                
            <Card>
                <CardActions>
                    <Button onClick={() => this.setState({addNote: true})}>Add note</Button>
                </CardActions>
                <CardContent>                    
                    <PlayerNoteList 
                        notes={this.props.playerToReport.notes} 
                        onModify={val => console.log(val)}
                        onDelete={id => deletePlayerNote(id)}/>
                </CardContent>
            </Card>
        </PageContainer>
    }
}