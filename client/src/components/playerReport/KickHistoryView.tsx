import React from 'react';
import { KickHistoryAudit } from 'api/management/kickaudit/KickHistoryAudit';
import { Card, CardHeader, CardContent, TableHead, Table, TableRow, TableCell, TableBody } from '@material-ui/core';

interface IKickAudit {
    history: () => Array<KickHistoryAudit>;
}

export class KickHistoryView extends React.Component<IKickAudit, {}> {
    render() {
        return (<Card>
            <CardHeader title="Kick History"/>
            <CardContent>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Admin</TableCell>
                            <TableCell>Server</TableCell>
                            <TableCell>Timestamp</TableCell>
                            <TableCell>Message</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.history().map((value, index) => {
                            return (<TableRow key={index}>
                                <TableCell>{value.requester}</TableCell>
                                <TableCell>{value.serverLocation}</TableCell>
                                <TableCell>{value.timeStamp}</TableCell>
                                <TableCell>{value.message}</TableCell>
                            </TableRow>)
                        })}
                    </TableBody>
                </Table>
            </CardContent>
        </Card>);
    }
}