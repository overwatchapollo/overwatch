import React, { ReactElement } from "react";
import { IconButton, Avatar, useTheme } from '@material-ui/core';
import styles from 'styles';
import { Link } from 'react-router-dom';

interface IButton {
    label: string;
    reference: string;
    children: ReactElement;
}

const AccountButton: (props: IButton) => JSX.Element = (props: IButton) => {
    const classes = styles(useTheme());
    return <Avatar>
        <Link to={props.reference}>
        <IconButton className={classes.appBarButton}>
            {props.children}            
        </IconButton>
        </Link>
    </Avatar>
}

export default AccountButton;