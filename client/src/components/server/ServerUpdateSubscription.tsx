import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { ConfigurationDispatcher } from 'container/ServerConfigurationContainer';
import { ServerStatusUpdate } from 'api/server/ServerStatusUpdate';
import { asyncScheduler } from 'rxjs';
import { observeOn } from 'rxjs/operators';
import { serverConfigService } from 'reducers/ServiceReducer';
import { RootState } from 'reducers';
import { PermissionWrapper } from 'api/PermissionWrapper';
import { ServerConfiguration } from 'api/server/ServerConfiguration';

const HandleAllServerSubscriptions: () => JSX.Element = () => {
    const servers = useSelector((state: RootState) => state.servers.servers);
    return <>
        {servers.map(x => <HandleServerSubscription server={x} key={x.data.serverId}/>)}
    </>
}

const HandleServerSubscription: (props: { server: PermissionWrapper<ServerConfiguration> }) => JSX.Element = props => {
    const dispatch = useDispatch();
    const dis = new ConfigurationDispatcher(dispatch);
    useEffect(() => {
        const sub = getSub(props.server.data.serverId, (id, state) => dis.connectionUpdate(id, state))
        return () => sub.unsubscribe();
    }, [dis, props.server]);
    return <div/>
}

function getSub(serverId: number, connectionUpdate: (serverId: number, state: ServerStatusUpdate) => void) {
    const sub = serverConfigService.getServerUpdateObservable(serverId)
        .pipe(observeOn(asyncScheduler))
        .subscribe(y => connectionUpdate(y.serverId, y));
    return sub;
}

const ServerUpdateSubscription = HandleAllServerSubscriptions;
export { ServerUpdateSubscription }