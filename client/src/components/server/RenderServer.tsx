import React from 'react';
import { ServerConfiguration } from 'api/server/ServerConfiguration';
import { TableRow, TableCell } from '@material-ui/core';

interface IConf {
    config: ServerConfiguration
    onClick?: ((event: React.MouseEvent<HTMLTableRowElement, MouseEvent>) => void) | undefined;
}

const RenderServer: (conf: IConf) => JSX.Element = (conf: IConf) => {
    return (<TableRow onClick={conf.onClick}>
                <TableCell>{conf.config.serverId}</TableCell>
                <TableCell>{conf.config.serverName}</TableCell>
                <TableCell>{conf.config.state?.state ?? "unknown"}</TableCell>
                <TableCell>{conf.config.ipAddress + ":" + conf.config.port}</TableCell>
            </TableRow>);
}

export default RenderServer;