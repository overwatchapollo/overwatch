import React, { useEffect } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from 'reducers';
import { getServerPermissionConfiguration } from 'api/server/ServerConfigurationService';
import { AddServer } from 'actions/ServerConfigurationActions';

const ServerDataRegister: () => JSX.Element | null = () => {
    const userSelect = useSelector((state:RootState) => state.session.activeUser);
    const dispatch = useDispatch();
    useEffect(() => {
        if (userSelect) {
            getServerPermissionConfiguration()
                .then(x => x.forEach(y => dispatch(AddServer(y))));
        }
    })
    return <div/>
}

export { ServerDataRegister }