
import React, { useState } from 'react';
import { TableHead, Table, TableRow, TableCell, TableBody, Grid, Button } from '@material-ui/core';
import { ServerConfiguration } from 'api/server/ServerConfiguration';
import { setNewServer, deleteConfiguration, updateConfiguration, softRestart } from 'api/server/ServerConfigurationService';
import { ConfirmationDialog } from 'components/confirmation/ConfirmationDialog';
import RenderServer from './RenderServer';
import ServerActions from './ServerActions';
import { ServerConfigurationState } from 'reducers/ServerConfigurationReducer';
import { mapStateToProps, mapDispatchToProps } from 'container/ServerConfigurationContainer';
import { connect } from 'react-redux';
import { ModifyServer } from './ModifyServer';
import { PermissionWrapper } from 'api/PermissionWrapper';
import { ServerWrapper } from 'api/server/ServerWrapper';

interface IViewServerProps {
    servers: ServerConfigurationState;
    removeConfig: (id: number) => void;
    addConfig: (conf: ServerWrapper) => void;
    refresh: () => void;
}

interface IServerIds {
    configToMod?: ServerConfiguration;
    restartId: number;
    serverRemoveId?: number;
}

interface IServers {
    servers: ServerConfigurationState;
    modify: (conf: ServerConfiguration) => void;
    handleRestart: (id: number) => void;
    removeRegistration: (id: number) => void;
}

const ServerListRender: (props: IServers) => JSX.Element = props => {
    return <Table>
        <TableHead>
            <TableRow>
                <TableCell>ID</TableCell>
                <TableCell>Name</TableCell>
                <TableCell>Status</TableCell>
                <TableCell>URI</TableCell>
            </TableRow>
        </TableHead>
        {props.servers.servers.sort((a,b) => a.data.serverId > b.data.serverId ? 1 : -1)
            .map(x => <SingleServerRender server={x} key={x.data.serverId}
                remove={() => props.removeRegistration(x.data.serverId)}
                modify={(conf: ServerConfiguration) => props.modify(conf)} 
                handleRestart={() => props.handleRestart(x.data.serverId)}/>)}
    </Table>
}

const ReduxServerListRender = connect(mapStateToProps, mapDispatchToProps)(ServerListRender);

interface IServer {
    server: PermissionWrapper<ServerConfiguration>;
    modify: (conf: ServerConfiguration) => void;
    handleRestart: () => void;
    remove: () => void;
}

const BasicSingleServerRender: (props: IServer) => JSX.Element = (props: IServer) => {
    const [visible, setVisible] = useState(false);
    const canSeeCon = props.server.actions.includes('owner');
    return <TableBody>
        <RenderServer 
            config={props.server.data}
            onClick={() => setVisible(!visible)}/>
        {visible && <ServerActions config={props.server} 
                delete={() => props.remove()}
                modify={conf => props.modify(conf)} 
                handleRestart={() => props.handleRestart()}/>}
        <TableRow>
            {(visible && canSeeCon) && <TableCell colSpan={4}>
                <Grid container={true}>
                    <Grid item={true}>
                        <Button onClick={() => setNewServer(props.server.data.serverId)}>Rebuild Connection</Button>
                    </Grid>
                </Grid>
            </TableCell>}
        </TableRow>
    </TableBody>
}

const SingleServerRender = BasicSingleServerRender;

class ViewServers extends React.Component<IViewServerProps, IServerIds> {
    constructor(props: IViewServerProps) {
        super(props);
        this.state = {
            configToMod: undefined,
            serverRemoveId: undefined,
            restartId: -1,
        }
    }
    render() {
        return <Grid container={true} direction="column" style={{padding: 8}}>
            <Grid item={true}>
                <ConfirmationDialog 
                    open={this.state.restartId !== -1} 
                    result={val => this.handleRestartConfirmation(val)}
                    title={`Restart Server ${this.state.restartId}`}
                    question={"Are you sure you want to restart the server?"}/>
                <ModifyServer
                    serverConfiguration={this.state.configToMod}
                    close={() => this.setState({configToMod: undefined})}
                    onSubmit={conf => updateConfiguration(conf).then(x => {
                        if (x.status === 200) {
                            this.setState({configToMod: undefined})
                            this.props.refresh();
                        }
                    })}/>
            </Grid>
            <Grid item={true}>
                <ConfirmationDialog
                    open={this.state.serverRemoveId !== undefined}
                    result={val => this.handleDelete(val)}
                    title={`Delete server configuration ${this.state.serverRemoveId}`}
                    question={`Are you sure you want to delete the server configuration? This is irreversible.`}/>
                <ReduxServerListRender 
                    modify={(conf:ServerConfiguration) => this.modify(conf)} 
                    removeRegistration={(id: number) => this.setState({serverRemoveId: id})}
                    handleRestart={(id:number) => this.handleRestart(id)} />
            </Grid>
        </Grid>
    }

    handleDelete(success:boolean) {
        const serverId = this.state.serverRemoveId;
        if (success && serverId) {
            deleteConfiguration(serverId)
                .then(x => this.props.removeConfig(serverId))
        }
        this.setState({serverRemoveId: undefined})
    }

    modify(config: ServerConfiguration) {
        this.setState({configToMod: config})
    }

    handleRestart(id: number) {
        this.setState({restartId: id})
    }

    handleRestartConfirmation(val: boolean) {
        if (val) {
            softRestart(this.state.restartId);
            this.setState({restartId: -1});
        }
    }
}

const ReduxViewServers = connect(mapStateToProps, mapDispatchToProps)(ViewServers);
export {ReduxViewServers};