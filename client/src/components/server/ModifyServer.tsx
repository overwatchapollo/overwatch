import React, { useState } from 'react';
import { Button, Dialog, DialogTitle, DialogContent, DialogContentText, TextField, DialogActions } from '@material-ui/core';
import { ServerConfiguration } from 'api/server/ServerConfiguration';
import { ServerModificationRequest } from 'api/server/ServerModificationRequest';

interface ModifyServerProps {
    serverConfiguration?: ServerConfiguration;
    onSubmit: (config: ServerModificationRequest) => void;
    close: () => void;
}

function register(serverId: number, serverName: string, hostname: string, port: number, password: string, maxPlayers: number): ServerModificationRequest {
    const errors = new Array<string>(0);
    const registration: ServerModificationRequest = {
        serverId,
        serverName,
        password,
        port,
        hostName: hostname,
        players: maxPlayers
    }

    if (registration.serverId <= 0) {
        errors.push("Invalid server configuration. Report this error!")
    }

    if (registration.serverName === "") {
        errors.push("Server name is required.")
    }

    if (registration.hostName === "") {
        errors.push("Host name or IP address required.")
    }

    if (registration.players === 0) {
        errors.push("Server can not have zero players.")
    }

    if (registration.password === "") {
        registration.password = undefined;
    }

    if (registration.port === 0) {
        errors.push("Server port is required.")
    }
    
    if (errors.length > 0) {
        const message = errors.join(" ");
        throw new Error(message);
    }

    return registration;
}

const BasicModifyServer: (props: ModifyServerProps) => JSX.Element = props => {

    const [configuration, setConfiguration] = useState(props.serverConfiguration);

    const [serverId, setServerId] = useState(props.serverConfiguration?.serverId);
    const [serverName, setServerName] = useState(props.serverConfiguration?.serverName);
    const [hostName, setHostName] = useState(props?.serverConfiguration?.ipAddress);
    const [port, setPort] = useState(props.serverConfiguration?.port);
    const [password, setPassword] = useState(props.serverConfiguration?.password);
    const [maxPlayers, setMaxPlayers] = useState(props.serverConfiguration?.maxPlayers);
    const [errors, setErrors] = useState("");

    if (props.serverConfiguration !== configuration) {
        setConfiguration(props.serverConfiguration)
        setServerName(props.serverConfiguration?.serverName);
        setHostName(props.serverConfiguration?.ipAddress);
        setPort(props.serverConfiguration?.port);
        setPassword(props.serverConfiguration?.password);
        setMaxPlayers(props.serverConfiguration?.maxPlayers);
        setServerId(props.serverConfiguration?.serverId);
        setErrors("");
    }

    return <Dialog open={props.serverConfiguration !== undefined} onBackdropClick={() => props.close()}>
        <DialogTitle>Modify Server</DialogTitle>
        <DialogContent>
            <DialogContentText>
                Modify server details.
            </DialogContentText>
            <DialogContentText style={{color: 'red'}}>
                {errors}
            </DialogContentText>
            <TextField 
                label="Server Name"
                type="text"
                fullWidth={true}
                value={serverName}
                onChange={event => setServerName(event.target?.value)}/>
            <TextField 
                    label="Server Address"
                    type="text"
                    fullWidth={true}
                    value={hostName}
                    onChange={event => setHostName(event.target?.value)}/>
                <TextField 
                    label="Port"
                    type="number"
                    fullWidth={true}
                    value={port}
                    onChange={event => setPort(parseInt(event.target.value, 10))}/>
                <TextField 
                    label="Password"
                    type="password"
                    fullWidth={true}
                    value={password ?? ""}
                    onChange={event => setPassword(event.target?.value)}/>
                <TextField 
                    label="Max Players"
                    type="number"
                    fullWidth={true}
                    value={maxPlayers}
                    onChange={event => setMaxPlayers(parseInt(event.target.value, 10))}/>
        </DialogContent>
        <DialogActions>
            <Button variant='contained' color="primary" onClick={() => {
                try {
                    const registration = register(serverId ?? 0, serverName ?? "", hostName ?? "", port ?? 0, password ?? "", maxPlayers ?? 0);
                    props.onSubmit(registration);
                } catch (err) {
                    const ex: Error = err;
                    setErrors(ex.message)
                }
            }}>Submit Modification</Button>
            <Button variant='contained' color='secondary' onClick={() => props.close()}>Cancel</Button>
        </DialogActions>
    </Dialog>
}

const ModifyServer = BasicModifyServer;

export { ModifyServer }