import React from 'react';
import { ServerConfiguration } from 'api/server/ServerConfiguration';
import { TableRow, TableCell, Button, Grid, Typography } from '@material-ui/core';
import { PermissionWrapper } from 'api/PermissionWrapper';

interface IConf {
    config: PermissionWrapper<ServerConfiguration>;
    modify: (conf: ServerConfiguration) => void;
    handleRestart: (id: number) => void;
    delete: () => void;
}

const ServerActions: (conf: IConf) => JSX.Element = (conf: IConf) => {
    const canModify = conf.config.actions.includes("modify");
    const canRestart = conf.config.actions.includes("restart");
    const canDelete = conf.config.actions.includes("delete");
    const noActions = !canModify && !canRestart && !canDelete;
    return (<TableRow>
        <TableCell colSpan={4}>
            <Grid container={true}>
                {canModify && <Grid item={true}>
                    <Button onClick={() => conf.modify(conf.config.data)}>Modify</Button>
                </Grid>}
                {canRestart && <Grid item={true}>
                    <Button onClick={() => conf.handleRestart(conf.config.data.serverId)}>Soft Restart</Button>
                </Grid>}
                {canDelete && <Grid item={true}>
                    <Button onClick={() => conf.delete()}>Remove</Button>
                </Grid>}
                {noActions && <Grid item={true}>
                    <Typography variant="body1">You can not take any administrative action on this server.</Typography>
                </Grid>}
            </Grid>
        </TableCell>
    </TableRow>)
}
export default ServerActions;