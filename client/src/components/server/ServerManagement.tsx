import { ReduxViewServers } from './ViewServers';
import React, { useState } from 'react';
import { Button, Grid, Typography } from '@material-ui/core';
import { getServerPermissionConfiguration } from 'api/server/ServerConfigurationService';
import { PageContainer } from 'components/pagecontainer/PageContainer';

import { useDispatch } from 'react-redux';
import { RegisterServer } from './RegisterServer';
import { ConfigurationDispatcher } from 'container/ServerConfigurationContainer';
import { ServerWrapper } from 'api/server/ServerWrapper';

const PureServerManagement: () => JSX.Element = () => {
    const [registerServer, setRegisterServer] = useState(false);

    const confDispatcher = new ConfigurationDispatcher(useDispatch());
    const addConfig = confDispatcher.addConfig;
    return <PageContainer>
        <RegisterServer 
            open={registerServer} 
            close={() => setRegisterServer(false)}/>
        <Grid container={true} direction="column" spacing={2} style={{padding: 8}}>
            <Grid item={true}>
                <Grid container={true} spacing={4}>
                    <Grid item={true}>
                        <Typography variant="h5" color="inherit" noWrap={true}>
                            Configuration Management
                        </Typography>
                    </Grid>
                    <Grid item={true}>
                        <Button variant='contained' color="primary" onClick={() => setRegisterServer(true)}>Register Server</Button>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item={true}>
                <ReduxViewServers refresh={() => refresh(addConfig)}/>
            </Grid>
        </Grid>
    </PageContainer>
}

async function refresh(addConfig: (conf: ServerWrapper) => void): Promise<void> {
    return getServerPermissionConfiguration()
        .then(x => x.forEach(value => addConfig(value)))
}

export { PureServerManagement as ServerManagement }