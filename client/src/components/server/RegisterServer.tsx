import React, { useState } from 'react';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button, TextField, Grid } from '@material-ui/core';
import { ServerRegisterRequest } from 'api/server/ServerRegisterRequest';
import { ConfigurationDispatcher } from 'container/ServerConfigurationContainer';
import { useDispatch } from 'react-redux';
import { getServerPermissionConfiguration, submitConfiguration } from 'api/server/ServerConfigurationService';
import { ServerWrapper } from 'api/server/ServerWrapper';

interface RegisterInterface {
    open: boolean;
    close: () => void;
}

const BasicRegisterServer: (props: RegisterInterface) => JSX.Element = props => {
    const [serverName, setServerName] = useState("");
    const [hostName, setHostName] = useState("");
    const [port, setPort] = useState(2307);
    const [password, setPassword] = useState("");
    const [maxPlayers, setMaxPlayers] = useState(0);
    const [errors, setErrors] = useState("");

    const confDispatcher = new ConfigurationDispatcher(useDispatch());

    return <Dialog open={props.open}
            aria-labelledby="form-dialog-title"
            onBackdropClick={() => props.close()}>
        <DialogTitle>Register Server</DialogTitle>
        <DialogContent>
            <form autoComplete="off">
                <Grid container={true} direction="column" spacing={2} style={{padding: 16, width: '100%'}}>
                    <DialogContentText>
                        Register server to be managed.
                    </DialogContentText>
                    <DialogContentText style={{color: 'red'}}>
                        {errors}
                    </DialogContentText>
                    <Grid item={true} style={{width: '100%'}}>
                        <TextField 
                                label="Server Name"
                                type="text"
                                variant="outlined"
                                fullWidth={true}
                                value={serverName}
                                onChange={event => setServerName(event.target?.value)}/>
                    </Grid>
                    <Grid item={true}>
                        <TextField 
                            label="Server Address"
                            type="text"
                            variant="outlined"
                            fullWidth={true}
                            value={hostName}
                            onChange={event => setHostName(event.target?.value)}/>
                    </Grid>
                    <Grid item={true}>
                        <TextField 
                            label="RCON Port"
                            type="number"
                            variant="outlined"
                            fullWidth={true}
                            value={port}
                            onChange={event => setPort(parseInt(event.target.value, 10))}/>
                    </Grid>
                    <Grid item={true}>
                        <TextField 
                            label="RCON Password"
                            type="password"
                            variant="outlined"
                            fullWidth={true}
                            value={password}
                            onChange={event => setPassword(event.target?.value)}/>
                    </Grid>
                    <Grid item={true}>
                        <TextField 
                            label="Max Players"
                            type="number"
                            variant="outlined"
                            fullWidth={true}
                            value={maxPlayers}
                            onChange={event => setMaxPlayers(parseInt(event.target.value, 10))}/>
                    </Grid>
                </Grid>
            </form>
        </DialogContent>
        <DialogActions>
            <Button variant='contained' color="primary" onClick={async () => {
                try {
                    const registration = register(serverName, hostName, port, password, maxPlayers);
                    await submitConfiguration(registration);
                    refresh(confDispatcher.addConfig);
                    props.close();
                } catch (err) {
                    const ex: Error = err;
                    setErrors(ex.message)
                }
            }}>Register</Button>
            <Button variant='contained' color='secondary' onClick={() => props.close()}>Cancel</Button>
        </DialogActions>
    </Dialog>
}

async function refresh(addConfig: (conf: ServerWrapper) => void): Promise<void> {
    return getServerPermissionConfiguration()
        .then(x => x.forEach(value => addConfig(value)))
}

function register(serverName: string, hostname: string, port: number, password: string, maxPlayers: number): ServerRegisterRequest {
    const errors = new Array<string>(0);
    const registration: ServerRegisterRequest = {
        serverName: serverName,
        hostName: hostname,
        password: password,
        players: maxPlayers,
        port: port
    }

    if (registration.serverName === "") {
        errors.push("Server name is required.")
    }

    if (registration.hostName === "") {
        errors.push("Host name or IP address required.")
    }

    if (registration.players === 0) {
        errors.push("Server can not have zero players.")
    }

    if (registration.password === "") {
        errors.push("RCON requires a password.")
    }

    if (registration.port === 0) {
        errors.push("Server port is required.")
    }
    
    if (errors.length > 0) {
        const message = errors.join(" ");
        throw new Error(message);
    }

    return registration;
}

const RegisterServer = BasicRegisterServer;
export { RegisterServer }