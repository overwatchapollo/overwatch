import React from 'react';
import { Paper, Grid, Typography } from '@material-ui/core';

interface AuthStore {
    auths: Array<string>;
}

export class Auths extends React.Component<{}, AuthStore> {

    constructor(props: any) {
        super(props);
        this.state = {
            auths: new Array(0)
        }
    }

    componentDidMount() {
        fetch('/api/session/v1/auths')
            .then(x => x.json())
            .then(x => x as Array<string>)
            .then(x => this.setState({auths: x}))
    }

    render() {
        return <Paper>
            <Grid container={true} direction="column">
                <Grid item={true}>
                    <Typography variant="h4">Auths on session</Typography>
                </Grid>
                {this.state.auths.map(x => <Grid item={true} key={x}>
                    <Typography variant="h6">{x}</Typography>
                </Grid>)}
                
            </Grid>
        </Paper>
    }
}