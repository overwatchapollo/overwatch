import React from 'react';
import { Typography, Paper, Grid, Button } from '@material-ui/core';
import { Redirect } from 'react-router-dom';
import { RouteComponentProps, withRouter } from 'react-router';

type BoundaryProps = RouteComponentProps;

interface IErrorState {
    hasError: boolean;
    redirect: boolean;
    error: Error | undefined;
    errorRoute: string | undefined;
}

class ErrorBoundary extends React.Component<BoundaryProps, IErrorState> {
    constructor(props: BoundaryProps) {
      super(props);
      this.state = { 
          hasError: false, 
          redirect: false,
          error: undefined,
          errorRoute: undefined
        };
    }
  
    static getDerivedStateFromError(error: any) {
      // Update state so the next render will show the fallback UI.
      return { hasError: true, error: error };
    }
  
    componentDidCatch(error:any, errorInfo:any) {
      // You can also log the error to an error reporting service
      this.setState({errorRoute: this.props.location.pathname})
      console.error(`Error ${error}`);
    }
  
    render() {
        if (this.state.errorRoute !== undefined && this.state.errorRoute !== this.props.location.pathname) {
            this.setState({hasError: false, error:undefined, errorRoute: undefined, redirect: false})
        }
        
        if (this.state.hasError) {
            // You can render any custom fallback UI
            const errormsg = this.state.error === undefined ? "" : this.state.error.message;
            return <Paper>
                <Grid container={true} direction="column" style={{padding: 8}} spacing={2}>
                    <Grid item={true}>
                        <Typography variant="h5">Something went wrong with the user interface.</Typography>
                    </Grid>
                    <Grid item={true}>
                        <Typography variant="body1">Error message: {errormsg}</Typography>
                    </Grid>
                    <Grid item={true}>
                        <Button variant="contained" color="primary" onClick={() => this.setState({hasError: false, error: undefined})}>Retry Action</Button>
                    </Grid>
                    <Grid item={true}>
                        <Button variant="contained" color="secondary" onClick={() => this.setState({redirect: true, hasError: false, error:undefined})}>Go to home</Button>
                    </Grid>
                </Grid>
            </Paper>;
      }

      if (this.state.redirect) {
        this.setState({redirect: false})
        return <Redirect to="/"/>
      }
  
      return this.props.children; 
    }
  }

  export default withRouter(ErrorBoundary);