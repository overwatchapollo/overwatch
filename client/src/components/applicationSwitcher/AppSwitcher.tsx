import React from "react";
import { Route, Switch } from 'react-router';
import { ManageUser } from 'components/users/ManageUser';
import { RconStatus } from 'components/overview/Status';
import { AccessManagement } from 'components/accessManagement/AccessManagement';
import { AggregateMessageTracker } from 'components/chat/chat';
import { ReduxAggregatePlayerList } from 'components/player/AggregatePlayerList';
import { PlayerReportComponent } from 'components/playerReport/PlayerReportComponent';
import { Investigation } from 'components/playerInvestigation/PlayerInvestigation';
import { MultiBanForm } from 'components/multiban/MultiBanForm';
import { ExpGroup } from 'components/groupManagement/GroupManagement';
import { ServerManagement } from 'components/server/ServerManagement';
import { LoginForm } from 'components/authentication/LoginForm';
import { LogoutComponent } from 'components/authentication/LogoutComponent';
import { Auths } from 'components/testComponents/Auths';
import { RegistrationForm } from 'components/authentication/RegistrationForm';
import { ReduxOverviewAggregate } from 'components/overview/OverviewAggregate';
import { UserList } from 'components/users/UserList';
import { SteamProfile } from 'components/playerReport/SteamProfile';
import { GroupManagement } from 'components/groups/GroupManagement';
import { RoleList } from 'components/users/RoleList';
import { PrivilegedList } from 'components/privilegedGuids/PrivilegedList';
import { ReduxAggregateBanList } from 'components/player/bans/AggregateServerBanList';
import { AuditLog } from 'components/audit/AuditLog';
import { PlayerAdministration } from 'components/player/PlayerAdministration';
import { RoleManage } from 'components/users/RoleManage';
import { FeatureToggles } from 'components/featureToggle/FeatureToggles';
import { AggregateRconViewer } from 'components/rconViewer/AggregateRconViewer';
import { ServerRoleManagement } from 'components/roleManagement/ServerRoleManagement';
import { Home } from 'Home';

const AppSwitcher: () => JSX.Element = () => {
	return <Switch>
		<Route path="/external/gitlab" component={() => {
			window.location.href = 'https://gitlab.com/overwatchapollo/overwatch/issues';
			return null;
		}} />
		<Route path="/account">
			<ManageUser />
		</Route>
		<Route path="/status">
			<RconStatus />
		</Route>
		<Route path="/accessManagement">
			<AccessManagement />
		</Route>
		<Route path="/messageTracker">
			<AggregateMessageTracker />
		</Route>
		<Route path="/playerList">
			<ReduxAggregatePlayerList />
		</Route>
		<Route path="/playerLookup">
			<PlayerReportComponent />
		</Route>
		<Route path="/playerInvestigate/:guid?">
			<Investigation />
		</Route>
		<Route path="/administration/multiban">
			<MultiBanForm />
		</Route>
		<Route path="/exp/group">
			<ExpGroup />
		</Route>
		<Route path="/serverManagement">
			<ServerManagement />
		</Route>
		<Route path="/login">
			<LoginForm />
		</Route>
		<Route path="/logout">
			<LogoutComponent />
		</Route>
		<Route path="/test/auths">
			<Auths />
		</Route>
		<Route path="/register">
			<RegistrationForm />
		</Route>
		<Route path="/overview">
			<ReduxOverviewAggregate />
		</Route>
		<Route path="/administration/users">
			<UserList />
		</Route>
		<Route path="/exp/steam">
			<SteamProfile />
		</Route>
		<Route path="/administration/groups">
			<GroupManagement />
		</Route>
		<Route path="/administration/roles">
			<RoleList />
		</Route>
		<Route path="/administration/privileged">
			<PrivilegedList />
		</Route>
		<Route path="/banList">
			<ReduxAggregateBanList />
		</Route>
		<Route path="/administration/audit/log">
			<AuditLog />
		</Route>
		<Route path="/administration/playerAdmins">
			<PlayerAdministration />
		</Route>
		<Route path="/administration/roleManage">
			<RoleManage />
		</Route>
		<Route path="/administration/featureToggle">
			<FeatureToggles />
		</Route>
		<Route path="/administration/rcon">
			<AggregateRconViewer />
		</Route>
		<Route path="/administration/serverAccess">
			<ServerRoleManagement />
		</Route>
		<Route path="/" exact={true}>
			<Home />
		</Route>
	</Switch>
}

export { AppSwitcher }