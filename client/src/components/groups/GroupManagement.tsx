import React from 'react';
import { Button, Typography, Grid } from '@material-ui/core';
import { getGroups, deleteGroup, createGroup } from 'api/groups/GroupService';
import { PlayerGroup } from 'api/groups/PlayerGroup';
import PlayerGroups from './PlayerGroups';
import PlayerGroupCreate from './PlayerGroupCreate';
import { PageContainer } from 'components/pagecontainer/PageContainer';
import { User } from 'api/users/User';
import { getUsersDetailed } from 'api/users/UserService';

interface IGroupState {
    groups: Array<PlayerGroup>;
    createDialog: boolean;
    allUsers: Array<User>;
    error: string;
}

class BasicGroupManagement extends React.Component<{}, IGroupState> {
    constructor(props: {}) {
        super(props);
        this.state = {
            groups: new Array(0),
            createDialog: false,
            allUsers: new Array(0),
            error: ""
        }
    }

    componentDidMount() {
        this.refreshGroups();
        getUsersDetailed().then(x => this.setState({allUsers: x}), x => console.error(x));
    }

    render() {
        return <PageContainer>
            <PlayerGroupCreate 
                open={this.state.createDialog}
                onClose={() => this.setState({createDialog: false})}
                onSubmit={async (name: string) => {
                    const group: PlayerGroup = {name: name}
                    return createGroup(group)
                        .then(x => {
                            this.refreshGroups();
                            return x;
                        })
                }}/>
            <Grid container={true} direction="column" spacing={2} style={{padding:8}}>
                <Grid item={true}>
                    <Typography variant="h5">Group Management</Typography>
                </Grid>
                <Grid item={true}>
                    <Typography variant="subtitle1" color="error">{this.state.error}</Typography>
                </Grid>
                <Grid item={true}>
                    <Button variant="contained" color="primary" onClick={() => this.setState({createDialog: true})}>Create Group</Button>
                </Grid>
                <Grid item={true}>
                    <PlayerGroups 
                        allUsers={this.state.allUsers}
                        groups={this.state.groups} 
                        delete={id => deleteGroup(id).then(x => this.refreshGroups)}/>
                </Grid>
            </Grid>
        </PageContainer>
    }

    private refreshGroups() {
        getGroups()
            .then(x => this.setState({groups: x}), err => this.setState({error: groupError(err)}))
    }
}

function groupError(code: number): string {
    switch (code) {
        case 500:
            return "An internal server error has ocurred."
        default:
            return `An unknown error has ocurred, code: ${code}`
    }
}

export { BasicGroupManagement as GroupManagement };