import React from "react";
import { Dialog, DialogTitle, DialogContent, DialogActions, Button, Grid } from '@material-ui/core';
import { PlayerGroup } from 'api/groups/PlayerGroup';
import { User } from 'api/users/User';
 
interface IAddUserDialog {
    group: PlayerGroup | undefined;
    members: Array<User>;
    allUsers: Array<User>;
    onClose: () => void;
    addUser: (user: User) => void;
}

const AddUserDialogue: (props: IAddUserDialog) => JSX.Element = (props: IAddUserDialog) => {
    return <Dialog
        open={props.group !== undefined}
        onBackdropClick={event => props.onClose()}>
        <DialogTitle>Add user to group</DialogTitle>
        <DialogContent>
            <Grid container={true} direction="column">
                {props.allUsers
                    .filter(x => !props.members.map(y => y.id).includes(x.id))
                    .map(x => <Grid item={true} key={x.username}>
                    {x.username}<Button onClick={event => props.addUser(x)}>Add</Button>
                </Grid>)}
            </Grid>
        </DialogContent>
        <DialogActions>
            <Button onClick={() => props.onClose()}>Close</Button>
        </DialogActions>
    </Dialog>
}

export default AddUserDialogue;