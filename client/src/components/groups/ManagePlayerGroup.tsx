import React, { useState } from "react";
import { PlayerGroup } from 'api/groups/PlayerGroup';
import { Grid, Typography, Button } from '@material-ui/core';
import { User } from 'api/users/User';
import AddUserDialogue from './AddUserDialogue';
import { ServerConfiguration } from 'api/server/ServerConfiguration';

interface GroupManagement {
    group: PlayerGroup;
    members: Array<User>;
    allUsers: Array<User>;
    servers: Array<ServerConfiguration>;
    addUserToGroup: (user: User) => void;
    removeUserFromGroup: (user: User) => void;
    close: () => void;
}

const ManagePlayerGroup: (props: GroupManagement) => JSX.Element = (props: GroupManagement) => {
    const [addUser, setAddUser] = useState(false);
    return <Grid container={true} direction="column">
        <Grid item={true}>
            <Grid container={true}>
                <Grid item={true}>
                    <Typography variant="h5">Manage {props.group.name}</Typography>
                </Grid>
                <Grid item={true}>
                    <Button onClick={() => props.close()}>Back</Button>
                </Grid>
            </Grid>
        </Grid>
        
        <Grid item={true}>
            <Grid container={true} direction="column">
                <Grid item={true}>
                    <Typography variant="h6">Servers</Typography> <Button>Add Server</Button>
                </Grid>
                {props.servers.map(x => <Grid item={true}>
                    {x.serverName}
                </Grid>)}
            </Grid>
        </Grid>

        <Grid item={true}>
            <Grid container={true} direction="column">
                <Grid item={true}>
                    <AddUserDialogue 
                        addUser={user => props.addUserToGroup(user)}
                        group={addUser ? props.group : undefined}
                        onClose={() => setAddUser(false)}
                        members={props.members}
                        allUsers={props.allUsers}/>
                    <Button onClick={event => setAddUser(true)}>Add user</Button>
                </Grid>
                {props.members
                    .map(x => <Grid item={true}>
                    <Typography variant="body1">{x.username}</Typography> <Button onClick={event => props.removeUserFromGroup(x)}>remove</Button>
                </Grid>)}
            </Grid>
        </Grid>
    </Grid>
    
}

export default ManagePlayerGroup;