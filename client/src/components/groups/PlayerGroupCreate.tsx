import React, { useState } from 'react';
import { Dialog, TextField, DialogActions, Button, DialogContent, DialogTitle, Typography } from '@material-ui/core';

interface ICreateProps {
    open: boolean;
    onSubmit: (name: string) => Promise<Response>;
    onClose: () => void;
}

const PlayerGroupCreate: (props: ICreateProps) => JSX.Element = (props: ICreateProps) => {
    const [name, setName] = useState("");
    const [error, setError] = useState("");
    return <Dialog 
        open={props.open} 
        onBackdropClick={() => props.onClose()} title="Create Group">
            <DialogTitle>Create Group</DialogTitle>
            <DialogContent>
                <TextField variant="outlined" value={name} onChange={event => setName(event.target.value)} label="Name"/>

                {error !== "" && <Typography variant="body1">
                    An error has ocurred: {error}
                </Typography>}
            </DialogContent>
        
        <DialogActions>
            <Button variant="contained" color="primary" onClick={() => {
                setError("");
                props.onSubmit(name).then(x => {
                    switch (x.status) {
                        case 200:
                            props.onClose();
                            break;
                        case 403:
                            setError("You do not have permission to create a group.")
                            break;
                        case 500:
                            setError("An internal server error has ocurred. Please try again later.")
                            break;
                        default:
                            setError(`${x.status}`)
                    }
                })
            }}>Submit</Button>
            <Button variant="contained" color="secondary" onClick={() => props.onClose()}>Cancel</Button>
        </DialogActions>
    </Dialog>
}

export default PlayerGroupCreate;