import React, { useState } from 'react';
import { Grid, Button } from '@material-ui/core';
import { PlayerGroup } from 'api/groups/PlayerGroup';
import { getServersOfGroup, removeMemberFromGroup, addUserToGroup, getMembers } from 'api/groups/GroupService';
import { User } from 'api/users/User';
import ManagePlayerGroup from './ManagePlayerGroup';
import { ServerConfiguration } from 'api/server/ServerConfiguration';

interface IGroups {
    groups: Array<PlayerGroup>;
    delete: (id: number) => void;
    allUsers: Array<User>;
}

const PlayerGroups: (props: IGroups) => JSX.Element = (props: IGroups) => {
    const [selectedGroup, setSelectedGroup] = useState(new Array<PlayerGroup>(0))
    const [selectedMembers, setSelectedMembers] = useState(new Array<User>(0));
    const [selectedServers, setSelectedServers] = useState(new Array<ServerConfiguration>(0));
    return <Grid container={true} direction="column">
        {selectedGroup.length === 0 && props.groups.map(x => <PlayerGroupView 
            selectGroup={group => 
                {
                    const id = group.id;
                    if (!id) {
                        return;
                    }
                    getMembers(id)
                        .then(x => {
                            setSelectedMembers(x);
                            setSelectedGroup(new Array(group));
                        });
                    getServersOfGroup(id).then(x => setSelectedServers(x));
                }}
            group={x}
            delete={props.delete}/>)}
        {selectedGroup.length > 0 && 
            <ManagePlayerGroup 
                addUserToGroup={user => {
                    const id = selectedGroup[0].id;
                    if (!id) {
                        return;
                    }
                    addUserToGroup(selectedGroup[0].id === undefined ? -1 : selectedGroup[0].id, user.id)
                    .then(x => {
                        getMembers(id)
                        .then(members => {
                            setSelectedMembers(members);
                        });
                    })
                    
                }}
                servers={selectedServers}
                removeUserFromGroup={user => {
                    const id = selectedGroup[0].id;
                    if (!id) {
                        return;
                    }
                    removeMemberFromGroup(id, user.id)
                    .then(x => {
                        getMembers(id)
                        .then(members => {
                            setSelectedMembers(members);
                        });
                    })
                }}
                close={() => setSelectedGroup(new Array(0))}
                allUsers={props.allUsers}
                group={selectedGroup[0]} 
                members={selectedMembers}/>}
    </Grid>
}

interface IGroup {
    group: PlayerGroup;
    delete: (id: number) => void;
    selectGroup: (group: PlayerGroup) => void;
}

const PlayerGroupView: (props: IGroup) => JSX.Element = (props: IGroup) => {
    const [members, setMembers] = useState(new Array<User>(0));
    const id = props.group.id;
    if (!id) {
        return <Grid item={true}></Grid>
    }
    return <Grid item={true}>
        {props.group.id} | {props.group.name} | <Button onClick={event => props.delete(id)}>X</Button>
        <Button onClick={() => props.selectGroup(props.group)}>Manage</Button>
        <Button onClick={() => getMembers(id).then(x => setMembers(x))}>Members</Button>
        <Grid container={true} direction="column">
            {members.map(x => <Grid item={true}>
                    Username: {x.username} <Button onClick={() => removeMemberFromGroup(id, x.id)}>x</Button>
                </Grid>)}
        </Grid>
    </Grid>
}

export default PlayerGroups;