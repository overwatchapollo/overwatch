import React, {useState} from 'react'
import { Grid, Typography, TextField, Button } from '@material-ui/core';

interface ILookup {
    search: (name: string) => void;
}

const NameLookup: (props: ILookup) => JSX.Element = (props: ILookup) => {
    const [name, setName] = useState("");
    return <Grid container={true} direction="column">
        <Grid item={true}>
            <Typography variant="h6">Alias Lookup</Typography>
        </Grid>
        <Grid item={true}>    
            <Grid container={true} direction="row" alignItems="center">
                <Grid item={true}>
                    <TextField id="nameLookup" value={name} onChange={event => setName(event.target.value)}/>
                </Grid>
                <Grid item={true}>
                    <Button onClick={event => props.search(name)}>Search</Button>
                </Grid>
            </Grid>
        </Grid>
    </Grid>
}

export {NameLookup};