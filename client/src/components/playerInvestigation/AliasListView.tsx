import React from "react";
import { PlayerAlias } from 'api/playerReport/PlayerAlias';
import { Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';

interface IAliasList {
    aliases: Array<PlayerAlias>;
}

const AliasListView:(props: IAliasList) => JSX.Element = (props: IAliasList) => {
    return <Table>
        <TableHead>
            <TableRow>
                <TableCell>Alias</TableCell>
                <TableCell>Server</TableCell>
                <TableCell>Times Used</TableCell>
                <TableCell>First Used</TableCell>
                <TableCell>Last Used</TableCell>
            </TableRow>
        </TableHead>
        <TableBody>
            {props.aliases.map(alias => {
                return <TableRow key={`${alias.alias}|${alias.serverId}`}>
                    <TableCell>{alias.alias}</TableCell>
                    <TableCell>{alias.serverId}</TableCell>
                    <TableCell>{alias.used}</TableCell>
                    <TableCell>{alias.firstUsed.toLocaleString()}</TableCell>
                    <TableCell>{alias.lastUsed.toLocaleString()}</TableCell>
                </TableRow>
            })}
        </TableBody>
    </Table>
}

export { AliasListView }