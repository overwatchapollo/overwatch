
import { PlayerReport } from 'api/playerReport/PlayerReport';
import { Grid, Tabs, Tab, Typography } from '@material-ui/core';
import React from 'react';
import { AliasListView } from './AliasListView';
import { ReduxPlayerHistory } from './PlayerKickHistory';
import { PlayerReportDetails } from './PlayerReportDetails';
import { PlayerNotes } from './PlayerNotes';
import WarningIcon from '@material-ui/icons/Warning';
import { connect } from 'react-redux';
import { mapThemeStateToProps, mapThemeDispatchToProps } from 'container/ThemeContainer';
import { ThemeState } from 'reducers/ThemeReducer';

interface IResults {
    results: Array<PlayerReport>;
    refresh:(guid: string) => void;
}

const PlayerResults:(props: IResults) => JSX.Element = (props: IResults) => {
    return <Grid container={true} direction="column" justify="center" alignItems="flex-start">
        {props.results.map(x => <StyledReport 
            key={x.guid} 
            report={x} 
            refresh={() => props.refresh(x.guid)}/>)}
    </Grid>
}

export {PlayerResults}

interface IReportResult {
    themeState: ThemeState;
    report: PlayerReport;
    refresh: () => void;
}

const PlayerReportView:(props: IReportResult) => JSX.Element = (props:IReportResult) => {
    const [tabPage, setTabPage] = React.useState(0);
    const useNoteIcon = props.report.notes.length !== 0;
    return <Grid item={true}>
        <Grid container={true} direction="column">
            <Grid item={true}>
                <Tabs value={tabPage} onChange={(event,value) => setTabPage(value as number)}>
                    <Tab label="General Details" {...a11yProps(0)}/>
                    <Tab label="Aliases" {...a11yProps(1)}/>
                    <Tab label="Player Notes" {...a11yProps(2)} 
                        icon={useNoteIcon 
                            ? <WarningIcon style={{color: props.themeState.darkMode ? '#FFA500' : '#ff0000'}}/> 
                            : undefined}/>
                    <Tab label="Kick and Bans" {...a11yProps(3)}/>
                </Tabs>
            </Grid>
            <Grid item={true}>
                <Grid container={true}>
                    <TabPanel value={tabPage} index={0}>
                        <PlayerReportDetails report={props.report} refresh={() => {}}/>
                    </TabPanel>
                    <TabPanel value={tabPage} index={1}>
                        <AliasListView aliases={props.report.aliasRecords}/>
                    </TabPanel>
                    <TabPanel value={tabPage} index={2}>
                        <PlayerNotes report={props.report} refresh={() => props.refresh()}/>
                    </TabPanel>
                    <TabPanel value={tabPage} index={3}>
                        <ReduxPlayerHistory report={props.report} refresh={() => {}}/>
                    </TabPanel>
                    <TabPanel value={tabPage} index={4}>
                        <Typography variant="h6">Coming soon!</Typography>
                    </TabPanel>
                </Grid>
            </Grid>
        </Grid>
    </Grid>
}

const StyledReport = connect(mapThemeStateToProps, mapThemeDispatchToProps)(PlayerReportView);

interface TabPanelProps {
    children?: React.ReactNode;
    index: number;
    value: number;
}
function a11yProps(index: number) {
return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
};
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, ...other } = props;
    const hidden = value !== index;
    return <Grid 
        role="tabpanel"
        hidden={hidden} 
        item={true}
        id={`simple-tabpanel-${index}`}
        {...other}>
            {children}
        </Grid>
  }