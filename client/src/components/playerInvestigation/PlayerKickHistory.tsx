import { PlayerReport } from 'api/playerReport/PlayerReport';
import { submitBan } from 'api/player/PlayerService';
import { ServerConfigurationState } from 'reducers/ServerConfigurationReducer';
import React, { useState } from 'react';
import { Grid, Typography, Button, Divider, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import BanRequestDialog from 'components/player/bans/BanRequestDialog';
import { mapStateToProps, mapDispatchToProps } from 'container/ServerConfigurationContainer';
import { connect } from 'react-redux';

interface IServerRepo {
    report: PlayerReport;
    refresh: () => void;
    servers:ServerConfigurationState;
}

const PlayerKickHistory:(props: IServerRepo) => JSX.Element = (props: IServerRepo) => {
    const [openBan, setOpenBan] = useState(false);
    const actions = props.servers.servers[0] === undefined ? new Array<string>(0) : props.servers.servers[0].actions;
    return <Grid container={true} direction="column" style={{padding: 8}}>
        <BanRequestDialog 
            open={openBan} 
            guid={props.report.guid} 
            name={props.report.name} 
            onSubmit={req => submitBan(req)}
            onClose={() => setOpenBan(false)}
            servers={props.servers.servers.map(x => x.data)}
            actions={actions}/>
        <Grid item={true}>
            <Grid container={true} direction="column">
                <Grid item={true}>
                    <Typography variant="h6">Administrative Actions</Typography>
                </Grid>
                <Grid item={true}>
                    <Button onClick={() => setOpenBan(true)}>Issue Ban</Button>
                </Grid>
            </Grid>
        </Grid>
        <Grid item={true}>
            <Divider/>
        </Grid>
        <Grid item={true}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Kicked By</TableCell>
                        <TableCell>Server</TableCell>
                        <TableCell>IP Address</TableCell>
                        <TableCell>Timestamp</TableCell>
                        <TableCell>Message</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.report.kickHistory.map(x => <TableRow key={x.timeStamp}>
                        <TableCell>{x.requester}</TableCell>
                        <TableCell>{x.serverLocation}</TableCell>
                        <TableCell>{x.ipAddress}</TableCell>
                        <TableCell>{x.timeStamp}</TableCell>
                        <TableCell>{x.message}</TableCell>
                    </TableRow>)}
                </TableBody>
            </Table>
        </Grid>
    </Grid>
}

const ReduxPlayerHistory = connect(mapStateToProps, mapDispatchToProps)(PlayerKickHistory);
export { ReduxPlayerHistory }