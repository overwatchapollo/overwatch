import React, { useState } from "react";
import { PlayerReport } from 'api/playerReport/PlayerReport';
import { addPlayerNote, modifyPlayerNote, deletePlayerNote } from 'api/player/PlayerReportService';
import { Grid, Button } from '@material-ui/core';
import PlayerNoteDialog from 'components/playerReport/PlayerNoteDialog';
import { ModifyNotes } from 'components/playerReport/ModifyNote';
import PlayerNoteList from 'components/playerReport/PlayerNoteList';

interface PlayerNotesProps {
    report: PlayerReport;
    refresh: () => void;
}

const PlayerNotes:(props: PlayerNotesProps) => JSX.Element = (props: PlayerNotesProps) => {
    const [noteDialog, setNoteDialog] = useState(false);
    const [noteModifyId, setNoteModifyId] = useState(0);
    return <Grid container={true}>
        <PlayerNoteDialog 
                onOpen={() => noteDialog}
                onClose={() => setNoteDialog(false)}
                name={() => props.report.guid}
                onSubmit={note => {
                    addPlayerNote(props.report.guid, note).then(() => props.refresh());
                }}/>
        <ModifyNotes 
            id={noteModifyId}
            note={props.report.notes.find(x => x.id === noteModifyId)?.note ?? ""}
            setNote={note => 
                modifyPlayerNote(noteModifyId, note)
                    .then(() => setNoteModifyId(0))
                    .then(() => props.refresh())
            }
            onClose={() => setNoteModifyId(0)}/>
        <Button onClick={() => setNoteDialog(true)}>Add Note</Button>
        <PlayerNoteList 
            notes={props.report.notes} 
            onModify={id => setNoteModifyId(id)}
            onDelete={id => deletePlayerNote(id).then(() => props.refresh())}/>
    </Grid>
}

export { PlayerNotes }