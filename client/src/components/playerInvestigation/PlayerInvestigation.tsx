import React, { useState } from 'react';
import { Grid, Typography, Select, FormControl, TextField, Button, Divider, Box } from '@material-ui/core';
import { getPlayerReport, getPlayerReportName } from 'api/player/PlayerReportService';
import { PlayerReport } from 'api/playerReport/PlayerReport';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { PlayerResults } from './PlayerResults';
import { PageContainer } from 'components/pagecontainer/PageContainer';

export interface InvestigationMatch {
    guid?: string;
}

interface IInvestigation extends RouteComponentProps<InvestigationMatch>{
    guid?: string;
}

type InvestigationProps = IInvestigation ;

const PlayerInvestigation: (props: InvestigationProps) => JSX.Element = (props: InvestigationProps) => {
    const [searchValue, setSearchValue] = useState("");
    const [results, setResults] = useState(new Array<PlayerReport>(0));
    const guid = props.match.params.guid;

    const [hasSearched, setHasSearched] = useState(false);
    if (results.length === 0 && guid !== undefined) {
        getPlayerReport(guid).then(x => {
            if (x) {
                setResults(new Array(x))
            }
        }).then(() => setHasSearched(true))
    }
    return <PageContainer>
        <Grid 
            style={{padding:8}}
            container={true} 
            direction="column" 
            justify="center" 
            alignItems="flex-start" spacing={2}>
            <Grid item={true}>
                <Typography variant="h4">Player Investigation</Typography>
            </Grid>

            <Grid item={true}>
                <Typography variant="body1">Search the database for player names or GUIDs to check their record.</Typography>
            </Grid>

            <Grid item={true}>
                <Divider/>
            </Grid>

            <Grid item={true}>
                <Box border={2}>
                    <Grid container={true} alignItems={"center"} spacing={2} style={{padding: 16}}>
                        <Grid item={true}>
                            <FormControl>
                                <TextField variant="outlined" label="Search" value={searchValue} onChange={event => setSearchValue(event.target.value)}/>
                            </FormControl>
                        </Grid>

                        <Grid item={true}>
                            <Grid container={true} direction="column" spacing={1} alignItems="center" alignContent="center">
                                <Grid item={true}>
                                    <Typography variant="subtitle1">Search as</Typography>
                                </Grid>
                                <Grid item={true}>
                                    <Grid container={true} spacing={2}>
                                        <Grid item={true}>
                                            <Button 
                                                variant="contained" 
                                                color="primary" 
                                                onClick={() => getPlayerReport(searchValue).then(x => setResults(new Array(x))).then(() => setHasSearched(true))}>
                                                GUID
                                            </Button>
                                        </Grid>
                                        <Grid item={true}>
                                            <Button 
                                                variant="contained" 
                                                color="primary" 
                                                onClick={() => getPlayerReportName(searchValue).then(x => setResults(x)).then(() => setHasSearched(true))}>
                                                Name
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Box>
            </Grid>
            
            {hasSearched && <Grid item={true}>
                <Grid container={true} spacing={2} alignItems="center">
                    <Grid item={true}>Results:{results.length}</Grid>
                    <Grid item={true}>
                        <FormControl>
                            Sort
                            <Select native={true}>
                                <option>Most Recently Played</option>
                            </Select>
                        </FormControl>
                    </Grid>

                    {results.length !== 0 && <Grid item={true}><PlayerResults 
                        results={results} 
                        refresh={guid => getPlayerReport(guid)
                        .then(x => {
                            if (x) {
                                setResults(new Array(x));
                            }
                        })}/>
                    </Grid>}
                </Grid>
            </Grid>}
        </Grid>
    </PageContainer>
}

const Investigation = withRouter(PlayerInvestigation)

export {Investigation};
