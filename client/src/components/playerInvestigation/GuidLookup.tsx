import React, { useState } from 'react';
import { Grid, TextField, Button, Typography } from '@material-ui/core';

interface ILookup {
    search: (guid: string) => void;
}

const GuidLookup: (props: ILookup) => JSX.Element = (props: ILookup) => {
    const [guid, setGuid] = useState("");
    return <Grid container={true} direction="column" spacing={2} justify="center" alignItems="flex-start">
        <Grid item={true}>
            <Typography variant="h6">GUID Lookup</Typography>
        </Grid>
        <Grid item={true} container={true} direction="row" alignItems="center">
            <Grid item={true}>
                <TextField id="guidLookup" value={guid} onChange={event => setGuid(event.target.value)}/>
            </Grid>
            <Grid item={true}>
                <Button onClick={event => props.search(guid)}>Search</Button>
            </Grid>
        </Grid>
    </Grid>
}

export default GuidLookup;