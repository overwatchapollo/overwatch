import React from "react";
import { PlayerReport } from 'api/playerReport/PlayerReport'
import { Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core'

interface PlayerReportProps {
    report: PlayerReport;
    refresh: () => void;
}

const PlayerReportDetails:(props:PlayerReportProps) => JSX.Element = (props:PlayerReportProps) => {
    return <Table>
        <TableHead>
            <TableRow>
                <TableCell>Detail</TableCell>
                <TableCell>Value</TableCell>
            </TableRow>
        </TableHead>
        <TableBody>
            <TableRow>
                <TableCell>
                    Last name
                </TableCell>
                <TableCell>
                    {props.report.getLastName()}
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell>
                    GUID
                </TableCell>
                <TableCell>
                    {props.report.guid}
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell>
                    Times seen
                </TableCell>
                <TableCell>
                    {props.report.recordCount}
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell>
                    First Seen
                </TableCell>
                <TableCell>
                    {props.report.firstSeen.toLocaleString()}
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell>
                    Last Seen
                </TableCell>
                <TableCell>
                    {props.report.lastSeen.toLocaleString()}
                </TableCell>
            </TableRow>
        </TableBody>
    </Table>
}

export { PlayerReportDetails }