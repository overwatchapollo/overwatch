import React from 'react';
import { Paper } from '@material-ui/core';
import { addRoleToUser, getRoles } from 'api/roles/RoleService';
import { getUsersDetailed } from 'api/users/UserService';
import { UserRole } from 'api/roles/UserRole';
import { RoleListView, IRoleAllocation } from './RoleListView';
import { AddUserDialogue } from './AddUserDialogue';
import { User } from 'api/users/User';

interface IRoleListState {
    allocations: Array<IRoleAllocation>;
    roleMap: Map<string, UserRole>;
    roles: Array<UserRole>;
    selectedRole: number;
    users:Array<User>;
}

class BasicRoleList extends React.Component<{}, IRoleListState> {
    constructor(props: {}) {
        super(props);
        this.state = {
            allocations: new Array(0),
            roleMap: new Map(),
            roles: new Array(0),
            users: new Array(0),
            selectedRole: -1,
        }
    }

    componentDidMount() {
        getRoles()
            .then(x => this.setState({roles: x}))
            .then(x => this.grabUserInfo());
    }

    grabUserInfo() {
        getUsersDetailed().then(userArray => {
            const allocation = new Array<IRoleAllocation>(0);
            this.state.roles.forEach(x => {
                allocation.push({id: x.id, roleName:x.name, members: new Array(0)});
            });
            userArray.forEach(user => {
                user.roleNames.forEach(role => {
                    const maybeRole = allocation.find(x => x.roleName.localeCompare(role) === 0);
                    if (maybeRole !== undefined) {
                        maybeRole.members.push(user);
                    }
                })
            });
            this.setState({allocations: allocation, users: userArray});
        })
    }

    render() {
        return (<Paper>         
            <AddUserDialogue 
                onClose={() => this.setState({selectedRole: -1})}
                getUsers={() => this.state.users}
                getSelectedRole={() => this.state.selectedRole}
                getRoleList={() => this.state.roles}
                onSubmit={(user: number, role: number) => {
                    addRoleToUser(user, role);
                    this.grabUserInfo();
                }}/>
            <RoleListView 
                roles={() => this.state.allocations} 
                onChange={() => {this.grabUserInfo();}} 
                addUser={roleId => this.addUser(roleId)}/>
        </Paper>)
    }

    private addUser(roleId: number) {
        this.setState({selectedRole: roleId});
    }
}

export { BasicRoleList as RoleList }