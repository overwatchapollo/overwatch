import React from 'react';
import { TextField, Dialog, Button, DialogContent, DialogActions, DialogTitle } from '@material-ui/core';

interface ICreateRoleProps {
    shouldOpen: () => boolean;
    onSubmit: (rolename: string) => void;
    onClose: () => void;
}

interface ICreateRoleState {
    roleName: string;
}

export class CreateRoleDialogue extends React.Component<ICreateRoleProps, ICreateRoleState> {
    constructor(props: ICreateRoleProps) {
        super(props);
        this.state = {
            roleName: ""
        }
    }
    render() {
        return (<Dialog
                    open={this.props.shouldOpen()}
                    aria-labelledby="form-dialog-title"
                    onBackdropClick={() => this.props.onClose()}>
                <DialogTitle>
                    Add role to system.
                </DialogTitle>
                <DialogContent>
                    <TextField 
                        label="RoleName" 
                        value={this.state.roleName}
                        onChange={event => this.setState({roleName: event.target.value})}/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.onSubmit(this.state.roleName)}>Submit</Button>
                    <Button onClick={() => this.props.onClose()}>Cancel</Button>
                </DialogActions>
        </Dialog>)
    }
}