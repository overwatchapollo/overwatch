import React from 'react';
import { UserRole } from 'api/roles/UserRole';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button, Select, MenuItem } from '@material-ui/core';
import { addRoleToUser } from 'api/roles/RoleService';

interface IAddRoleDialogue {
    selectedRoleId?: number;
}

interface IAddRoleProps {
    shouldOpen: boolean;
    close: () => void;
    playerId?: number;
    roles: Array<UserRole>;
}

export class AddRoleDialogue extends React.Component<IAddRoleProps, IAddRoleDialogue> {
    constructor(props: IAddRoleProps) {
        super(props);
        this.state = {
            selectedRoleId: undefined
        }
    }

    changeSelection(event: React.ChangeEvent<HTMLSelectElement>) {
        const id = event.target.value;
        if (id === undefined) {
            this.setState({selectedRoleId: id});
        } else {
            const selected = this.props.roles.find(x => x.id === id);
            if (selected !== undefined) {
                this.setState({selectedRoleId: Number.parseInt(selected.id, undefined)});
            }
        }
    }

    handleSubmit() {
        if (this.props.playerId && this.state.selectedRoleId) {
            addRoleToUser(this.props.playerId, this.state.selectedRoleId)
                .then(() => this.props.close())
        } else {
            this.props.close();
        }
    }

    render() {
        return (
            <Dialog
                open={this.props.shouldOpen}
                aria-labelledby="form-dialog-title"
                onBackdropClick={() => this.props.close()}>
                <DialogTitle>
                    Add role
                </DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Add role to user.
                    </DialogContentText>
                    <Select
                        value={this.state.selectedRoleId}
                        onChange={(event: any,comp: any) => this.changeSelection(event)}>
                        <MenuItem value={undefined}>-</MenuItem>
                        {this.props.roles.filter(x => x.name !== null).map(x => <MenuItem key={x.id} value={x.id}>{x.name}</MenuItem>)}
                    </Select>
                </DialogContent>
                <DialogActions>
                    <Button onClick={(event) => this.handleSubmit()} color="primary" autoFocus={true}>
                        Add
                    </Button>
                    <Button onClick={() => this.props.close()} color="secondary">
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}