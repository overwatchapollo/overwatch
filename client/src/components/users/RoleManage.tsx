import React from 'react';
import { Paper, Button, List, ListItem, CardContent, Card, CardActions, CardHeader } from '@material-ui/core';
import { CreateRoleDialogue } from './CreateRoleDialogue';
import { deleteRole, getPermissionsOfRole, createRole, getRoles } from 'api/roles/RoleService';
import { UserRole } from 'api/roles/UserRole';
import { RoleModify } from './RoleModify';
import { ConfirmationDialog } from 'components/confirmation/ConfirmationDialog';

interface IRoleManageState {
    createRole: boolean;
    roles: Array<UserRole>;
    selectedRole: UserRole | undefined;
    rolePermission: Array<string>;
    modifyRole: boolean;
    roleToDelete: UserRole | undefined;
}

class BasicRoleManage extends React.Component<{}, IRoleManageState> {
    constructor(props: {}) {
        super(props);
        this.state = {
            createRole: false,
            roles: new Array(0),
            selectedRole: undefined,
            rolePermission: new Array(0),
            modifyRole: false,
            roleToDelete: undefined
        }
    }

    componentDidMount() {
        getRoles().then(x => this.setState({roles: x}))
    }

    render() {
        return <Paper>
                <CreateRoleDialogue
                    shouldOpen={() => this.state.createRole}
                    onClose={() => this.setState({createRole: false})}
                    onSubmit={(roleName: string) => {
                        createRole(roleName)
                            .then(() => this.setState({createRole: false}))
                    }}/>
                <RoleModify 
                    shouldOpen={() => this.state.modifyRole}
                    roleData={() => ({
                        role: this.state.selectedRole,
                        permissions: this.state.rolePermission})
                    }
                    onClose={() => this.setState({modifyRole: false})}/>
                <ConfirmationDialog 
                    open={this.state.roleToDelete !== undefined} 
                    result={res => {
                        if (res && this.state.roleToDelete) {
                            deleteRole(Number.parseInt(this.state.roleToDelete.id, 10))
                        }
                        this.setState({roleToDelete: undefined})
                    }} 
                    title={this.state.roleToDelete !== undefined ? `Remove role ${this.state.roleToDelete.name}` : "Remove role"}
                    question={"Do you want to remove role?"}/>
                <Card>
                    <CardContent>
                        Add new role
                    </CardContent>
                    <CardActions>
                        <Button onClick={() => this.setState({createRole: true})}>Create Role</Button>
                    </CardActions>
                </Card>
                

                <Card>
                    <CardHeader title="Modify Roles"/>
                    <CardContent>
                        <List>
                            {this.state.roles.map(x => 
                                <ListItem key={x.id}>
                                    {x.name}
                                    <Button onClick={() => this.setRoleToManager(x)}>Change</Button>
                                    <Button onClick={() => this.setState({roleToDelete: x})}>Remove</Button>
                                </ListItem>)}
                        </List>
                    </CardContent>
                </Card>
                
            </Paper>
    }

    private setRoleToManager(role: UserRole) {
        const numRole = parseInt(role.id, 10);
        getPermissionsOfRole(numRole)
            .then(x => this.setState({modifyRole: true, selectedRole:role, rolePermission: x}))
    }
}

export { BasicRoleManage as RoleManage }