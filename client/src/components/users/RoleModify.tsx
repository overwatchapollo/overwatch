import React from 'react';
import { Dialog, DialogActions, Button, DialogTitle, DialogContent, Table, TableBody, TableRow, TableCell, TextField } from '@material-ui/core';
import { UserRole } from 'api/roles/UserRole';
import { removePermissionFromRole, addPermissionToRole } from 'api/roles/RoleService';

interface IRoleModifyProps {
    shouldOpen:() => boolean;
    roleData: () => {role: UserRole | undefined, permissions: Array<string>};
    onClose:() => void;
}

interface IRoleModState {
    permission:string;
}

export class RoleModify extends React.Component<IRoleModifyProps, IRoleModState> {
    
    constructor(props: IRoleModifyProps) {
        super(props);
        this.state = {
            permission: ""
        }
    }

    render() {
        const renderRole = this.props.roleData();
        if (renderRole.role === undefined) {
            renderRole.role = {id:"-1", name: "", permissions: new Array(0)};
        }
        return <Dialog open={this.props.shouldOpen()}
            aria-labelledby="form-dialog-title"
            onBackdropClick={() => this.props.onClose()}>
                
                <DialogTitle>
                    Modify role {renderRole.role.name}
                </DialogTitle>

                <DialogContent>
                    <Table>
                        <TableBody>
                            {renderRole.permissions.sort((a,b) => a.localeCompare(b)).map(x => <TableRow>
                                <TableCell>{x} <Button onClick={event => this.deletePermission(renderRole.role as UserRole, x)}>Remove</Button></TableCell>
                            </TableRow>)}
                        </TableBody>
                    </Table>
                    <TextField 
                        label="Permission" 
                        value={this.state.permission}
                        onChange={event => this.setState({permission: event.target.value})}/>

                    <Button onClick={event => this.submitPermission(renderRole.role as UserRole)}>Push</Button>
                    
                </DialogContent>

                <DialogActions>
                    <Button onClick={() => this.props.onClose()} color="secondary">
                        Close
                    </Button>
                </DialogActions>
        </Dialog>
    }

    private submitPermission(role: UserRole) {
        addPermissionToRole(role.id, this.state.permission)
            .then(x => this.setState({permission: ""}))
    }

    private deletePermission(role:UserRole, permission: string) {
        removePermissionFromRole(role.id, permission);
    }
}