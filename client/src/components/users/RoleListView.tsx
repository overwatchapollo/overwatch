import React, {useState} from 'react';
import { User } from 'api/users/User';
import { ListItem, IconButton, Grid, List, Button, Typography } from '@material-ui/core';
import { removeRoleFromUser } from 'api/roles/RoleService';
import ArrowDropDown from '@material-ui/icons/ArrowDropDown'

export interface IRoleList {
    roles: () => Array<IRoleAllocation>;
    onChange: () => void;
    addUser: (roleId: number) => void;
}

export interface IRoleAllocation {
    id: string;
    roleName: string;
    members: Array<User>
}

export class RoleListView extends React.Component<IRoleList, {}> {

    deleteRole(user: number, role: number) {
        removeRoleFromUser(user, role);
        this.props.onChange();
    }

    render() {
        return <Grid container={true} direction="column" spacing={1} style={{padding:8}}>
                {this.props.roles()
                    .sort((a,b) => a.roleName.localeCompare(b.roleName))
                    .map((x, i) => {
                    return <RoleSingle key={i} 
                        role={x} 
                        addUser={roleId => this.props.addUser(roleId)}
                        removeFromRole={(user, role) => this.deleteRole(user, role)}/>
                })}
            </Grid>
    }
}

interface RoleProp {
    role: IRoleAllocation;
    addUser: (roleId: number) => void;
    removeFromRole: (userId: number, roleId: number) => void;
}

const RoleSingle: (prop: RoleProp) => JSX.Element = (prop: RoleProp) => {
    const roleId = Number.parseInt(prop.role.id);
    const [visible, setVisible] = useState(false);
    return <Grid item={true}>
        <Grid container={true} alignItems="center">
            <Grid item={true}>
                <Typography variant="h6">{prop.role.roleName}</Typography>
            </Grid>
            <Grid item={true}>
                <IconButton onClick={event => setVisible(!visible)}>
                    <ArrowDropDown/>
                </IconButton>
            </Grid>
        </Grid>
        
        {visible && <Grid container={true} direction="column">
            <Grid item={true}>
                <Button onClick={event => prop.addUser(roleId)}>Add User</Button>
            </Grid>
            <Grid item={true}>
                <List>
                    {prop.role.members
                        .sort((a,b) => a.username.localeCompare(b.username))
                        .map((member, j) => <ListItem key={j}>{member.username} 
                            <Button onClick={event => prop.removeFromRole(member.id, roleId)}>X</Button>
                            </ListItem>)}
                </List>
            </Grid>
        </Grid>}
    </Grid>
}