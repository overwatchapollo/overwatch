import React, { useState, useEffect } from 'react';
import { Typography, Grid, TextField, Button, Table, TableRow, TableCell, TableBody, useTheme } from '@material-ui/core';
import { updatePassword, getUsersDetailed } from 'api/users/UserService';
import { User } from 'api/users/User';
import { UserView } from './UserView';
import { AddRoleDialogue } from './AddRoleDialogue';
import { getRoles } from 'api/roles/RoleService';
import { UserRole } from 'api/roles/UserRole';
import { ChangePasswordDialogue } from './ChangePasswordDialog';
import styles from 'styles';
import { PageContainer } from 'components/pagecontainer/PageContainer';

const UserList: () => JSX.Element = () => {
    const [nameFilter, setNameFilter] = useState("");
    const [selectedUser, setSelectedUser] = useState<User|undefined>(undefined);
    const [changePassword, setChangePassword] = useState(false);
    const [playerList, setPlayerList] = useState(new Array<string>());
    const [playerDetails, setPlayerDetails] = useState(new Map<string, User>());
    const [shouldAddRole, setShouldAddRole] = useState(false);
    const [roles, setRoles] = useState(new Array<UserRole>());
    const [selectedPlayerId, setSelectedPlayerId] = useState<number|undefined>(undefined);

    function populateList() {
        getUsersDetailed()
            .then(x => {
                const names = x.map(x => x.username);
                const details = new Map<string, User>();
                x.forEach(x => details.set(x.username, x));
                setPlayerList(names);
                setPlayerDetails(details);
            })
    }

    

    

    function changePasswordCall(user: User, password: string) {
        updatePassword(user, password);
        setChangePassword(false);
    }

    useEffect(() => {
        function refresh() {
            populateList();
            getRoles().then(x => setRoles(x));
        }
        refresh()
    }, [])

    const reg = new RegExp(nameFilter, 'i');
    return <PageContainer>
        <AddRoleDialogue 
            playerId={selectedPlayerId} 
            close={() => {populateList(); setShouldAddRole(false);}} 
            shouldOpen={shouldAddRole} 
            roles={roles} />
        <ChangePasswordDialogue 
            shouldOpen={changePassword}
            onClose={() => setChangePassword(false)}
            onSubmit={(user: User, password: string) => changePasswordCall(user, password)}
            user={selectedUser}/>
        
        <Grid container={true} direction="column" spacing={1} justify="center" style={{padding:8}}>
            <Grid item={true}>
                <Grid container={true} 
                    spacing={1}
                    direction="row" 
                    justify="center" 
                    alignItems="flex-end">
                    <Grid item={true}>
                        <Typography variant="h5">User List</Typography>
                    </Grid>
                    <Grid item={true}>
                        <TextField label="filter" value={nameFilter} onChange={event => setNameFilter(event.target.value)}/>
                    </Grid>
                    <Grid item={true}>
                        <Button onClick={() => setNameFilter("")}>Clear</Button>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item={true}>
                <Table>
                    {playerList.filter(x => reg.test(x))
                        .sort((a,b) => a.localeCompare(b))
                        .map((x,i) => {
                            const details = playerDetails.get(x);
                            if (details === undefined) {
                                return <TableBody key={x}/>
                            }
                            return <UserRow key={x}
                                username={x}
                                roles={roles}
                                details={details}
                                addRole={id => {
                                    setSelectedPlayerId(id);
                                    setShouldAddRole(true);
                                }}
                                passwordUpdate={(user: User) => {
                                    setChangePassword(true);
                                    setSelectedUser(user);
                                }}
                                populate={() => populateList()}/>
                        })}
                </Table>
            </Grid>
        </Grid>
    </PageContainer>
}

interface IRow{
    username: string;
    roles: Array<UserRole>;
    details: User;
    addRole:(id: number) => void;
    passwordUpdate:(user: User) => void;
    populate: () => void;
}

const UserRow = (props: IRow) => {
    const [show, setShow] = useState(false);
    const classes = styles(useTheme());
    return <TableBody key={props.username}>
        <TableRow onClick={() => setShow(!show)}>
            <TableCell>{props.username}</TableCell>
        </TableRow>
        {show && <TableRow>
            <TableCell>
                <UserView classes={classes}
                    userUpdated={() => {props.populate()}}
                    roleList={props.roles}
                    user={props.details}
                    openAddRole={() => props.addRole(props.details.id)}
                    selectPasswordUpdate={(user: User) => props.passwordUpdate(user)}/>
                </TableCell>
        </TableRow>}
    </TableBody>
}

export { UserList }