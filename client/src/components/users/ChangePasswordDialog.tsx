import React from 'react';
import { Dialog, DialogActions, Button, DialogContent, DialogTitle, TextField } from '@material-ui/core';
import { User } from 'api/users/User';

interface IPasswordUpdate {
    shouldOpen: boolean;
    onSubmit: (user: User, password: string) => void;
    onClose: () => void;
    user: User | undefined;
}

interface IPasswordState {
    value: string;
}

export class ChangePasswordDialogue extends React.Component<IPasswordUpdate, IPasswordState> {
    constructor(props:IPasswordUpdate) {
        super(props);
        this.state = {
            value: "",
        }
    }
    render () {
        const user = this.props.user;
        if (!user) {
            return <div/>;
        }

        return <Dialog
            open={this.props.shouldOpen}
            aria-labelledby="form-dialog-title"
            onBackdropClick={() => this.props.onClose()}>
            <DialogTitle>
                Change password for {user.username}
            </DialogTitle>
            <DialogContent>
                <TextField
                    autoFocus={true}
                    margin="dense"
                    label="Password"
                    type="password"
                    fullWidth={true}
                    defaultValue=""
                    onChange={event => this.setState({value: event.target.value})}/>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => this.props.onSubmit(user, this.state.value)}>Submit</Button>
                <Button onClick={() => this.props.onClose()}>Cancel</Button>
            </DialogActions>
        </Dialog>
    }
}