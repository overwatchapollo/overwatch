import React, { useState, useEffect } from 'react';
import { Button, DialogTitle, Typography, Grid, Divider, ListItem, ListItemText, ListItemSecondaryAction, List, ListSubheader, IconButton, Dialog, DialogContent, TextField, DialogActions } from '@material-ui/core';
import { updatePassword, updateEmail } from 'api/users/UserService';
import { User } from 'api/users/User';
import { getMyDetails } from 'api/session/LoginService';
import { ChangePasswordDialogue } from './ChangePasswordDialog';
import { PageContainer } from 'components/pagecontainer/PageContainer';
import EditIcon from '@material-ui/icons/Edit';

const AccountDetail: (props: {title: string, value?: string, children?: any}) => JSX.Element = props => {
    return <ListItem>
        <ListItemText>{props.title}</ListItemText>
        <ListItemSecondaryAction>
            <Grid container={true} spacing={2} alignItems="center">
                
                <Grid item={true}>
                    <Typography variant="body1">{props.value}</Typography>
                </Grid>
                <Grid item={true}>
                    {props.children}
                </Grid>
            </Grid>
        </ListItemSecondaryAction>
    </ListItem>
}

const ChangeEmailDialog: (props: {open: boolean, userId?: number, onClose: (email?: string) => void, email: string}) => JSX.Element = props => {
    const { userId } = props;
    console.log(userId)
    const [email, setEmail] = useState(props.email);
    return <Dialog open={props.open} onBackdropClick={() => props.onClose()}>
        <DialogTitle>Change Email</DialogTitle>
        <DialogContent>
            <TextField variant="outlined"  value={email} onChange={event => setEmail(event.target.value)}/>
        </DialogContent>
        <DialogActions>
            <Button onClick={() => {
                if (userId) {
                    updateEmail(userId, email).then(() => props.onClose(email))
                }
            }}>
                Update
            </Button>
            <Button onClick={() => {
                props.onClose()
            }}>Cancel</Button>
        </DialogActions>
    </Dialog>
}

const PureManageUser: () => JSX.Element = () => {
    const [passwordDialog, setPasswordDialog] = useState(false);
    const [target, setTarget] = useState<User|undefined>(undefined);
    const [emailDialog, setEmailDialog] = useState(false);

    useEffect(() => {
        getMyDetails().then(x => setTarget(x))
    }, []);

    if (target === undefined) {
        return <div/>
    }

    const changePassword:(password:string) => void = password =>{
        const user = target;
        if (user) {
            updatePassword(user, password).then(() => setPasswordDialog(false));
        }
    }

    return <PageContainer>
        <ChangePasswordDialogue 
            shouldOpen={passwordDialog}
            onClose={() => setPasswordDialog(false)}
            onSubmit={(_: User, password: string) => changePassword(password)}
            user={target}/>
        <ChangeEmailDialog 
            userId={target?.id} 
            open={emailDialog} 
            email={target.email}
            onClose={email => {
                if (email) {
                    setTarget(Object.assign({}, target, {email}))
                }
                setEmailDialog(false)
            }} 
        />
        <Grid container={true} direction="column" spacing={2} style={{padding: 8}}>
            <Grid item={true}>
                <Typography variant="h4" style={{paddingLeft: 16}}>Account Management</Typography>
            </Grid>
            <Grid item={true}>
                <List>
                    <ListSubheader>Account Details</ListSubheader>
                    <AccountDetail title="Username" value={target.username}>
                        <IconButton disabled={true}>
                            <EditIcon style={{visibility:'hidden'}}/>
                        </IconButton>
                        </AccountDetail>
                    <AccountDetail title="Email" value={target.email}>
                        <IconButton onClick={() => setEmailDialog(true)}>
                            <EditIcon/>
                        </IconButton>
                    </AccountDetail>
                    <AccountDetail title="Password">
                        <IconButton onClick={() => setPasswordDialog(true)}>
                            <EditIcon/>
                        </IconButton>
                    </AccountDetail>
                    <Divider/>

                    <ListSubheader>Roles</ListSubheader>
                    {target.roleNames.sort((a,b) => a.localeCompare(b)).map((role: string, index: number) => 
                        <ListItem key={index}>{role}</ListItem>)}
                </List>
            </Grid>
            <Grid item={true}>
                <Divider/>
            </Grid>
            {false && <Grid item={true}>
                <Grid container={true} direction="column" spacing={1} style={{paddingLeft: 16}}>
                    <Grid item={true}>
                        <Typography variant="h6">Account Actions</Typography>
                    </Grid>
                    <Grid item={true}>
                        <Button variant="contained">Delete Account</Button>
                    </Grid>
                </Grid>
            </Grid>}
        </Grid>
    </PageContainer>
}

const ManageUser = PureManageUser;
export { ManageUser }