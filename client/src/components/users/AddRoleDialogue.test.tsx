import { AddRoleDialogue } from './AddRoleDialogue';
import { shallow, configure } from 'enzyme';
import { assert } from 'chai';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
configure({ adapter: new Adapter() });

describe("add role tests", () => {
    it("must create", () => {
        const roles = new Array({id:"1", name:"test1"}, {id:"2", name:"test2"});
        const wrapper = shallow(<AddRoleDialogue
            shouldOpen={true}
            close={() => {}}
            playerId={1}
            roles={roles}/>);
        assert.notEqual(wrapper, undefined);
    })
})

export default undefined;