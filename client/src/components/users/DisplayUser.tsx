import React from 'react';
import { User } from 'api/users/User';
import { ListSubheader, Grid, Typography, List, ListItem, Divider, ListItemText, ListItemSecondaryAction, IconButton } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';

const AccountDetail: (props: {title: string, value: string, children?: any}) => JSX.Element = props => {
    return <ListItem>
        <ListItemText>{props.title}</ListItemText>
        <ListItemSecondaryAction>
            <Grid container={true} spacing={2} alignItems="center">
                <Grid item={true}>
                    {props.children}
                </Grid>
                <Grid item={true}>
                    <Typography variant="body1">{props.value}</Typography>
                </Grid>
            </Grid>
        </ListItemSecondaryAction>
    </ListItem>
}

export const DisplayUser: (props: {user: User}) => JSX.Element = props => {
    return <Grid container={true} direction="column" spacing={2}>
        <Grid item={true}>
            <Typography variant="h4">Account Management</Typography>
        </Grid>
        <Grid item={true}>
            <Divider/>
        </Grid>
        <Grid item={true}>
            <List>
                <ListSubheader>Account Details</ListSubheader>
                <AccountDetail title="Username" value={props.user.username}/>
                <AccountDetail title="Email" value={props.user.email}>
                    <IconButton>
                        <EditIcon/>
                    </IconButton>
                </AccountDetail>
                <Divider/>

                <ListSubheader>Roles</ListSubheader>
                {props.user.roleNames.map((role: string, index: number) => 
                    <ListItem key={index}>{role}</ListItem>)}
            </List>
        </Grid>
    </Grid>
}