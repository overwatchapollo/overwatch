import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button } from '@material-ui/core';

interface IAddPermissionToRoleProps {
    shouldOpen: () => boolean;
    onSubmit: (permission: string) => void;
    onClose: () => void;
}

interface IAddPermissionToRoleState {
    roleName: string;
}

export class AddPermissionToRole extends React.Component<IAddPermissionToRoleProps, IAddPermissionToRoleState> {
    render() {
        return <Dialog
                    open={this.props.shouldOpen()}
                    aria-labelledby="form-dialog-title"
                    onBackdropClick={() => this.props.onClose()}>
                <DialogTitle>Add permission to role</DialogTitle>
                <DialogContent>
                    Content
                </DialogContent>
                <DialogActions>
                    <Button>Submit</Button>
                    <Button>Cancel</Button>
                </DialogActions>
        </Dialog>
    }
}