import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogContentText, Select, MenuItem, DialogActions, Button } from '@material-ui/core';
import { User } from 'api/users/User';
import { UserRole } from 'api/roles/UserRole';

interface IAddUserProps {
    onClose: () => void;
    onSubmit:(user:number, role: number) => void;
    getUsers: () => Array<User>;
    getRoleList: () => Array<UserRole>;
    getSelectedRole: () => number;
}

interface IAddUserState {
    selectedId: number;
}

export class AddUserDialogue extends React.Component<IAddUserProps, IAddUserState> {
    constructor(props: IAddUserProps) {
        super(props);
        this.state = {
            selectedId: -1,
        }
    }

    render() {
        const selectedRole = this.props.getSelectedRole();
        return (<Dialog
                    open={selectedRole !== -1}
                    aria-labelledby="form-dialog-title"
                    onBackdropClick={() => this.props.onClose()}>
            <DialogTitle>Add user to role</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Select user to add to role.
                </DialogContentText>
                <Select value={this.state.selectedId}
                    onChange={(event,comp) => this.changeSelection(event.target.value)}>
                    {this.props.getUsers().map(x => <MenuItem key={x.id} value={x.id}>{x.username}</MenuItem>)}
                </Select>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => {
                    const sel = this.state.selectedId;
                    if (sel !== undefined) {
                        this.props.onSubmit(this.state.selectedId, selectedRole)};
                        this.props.onClose();
                    }}>
                    Add
                </Button>
                <Button onClick={() => this.props.onClose()}>
                    Cancel
                </Button>
            </DialogActions>
        </Dialog>);
    }

    private changeSelection(value: unknown) {
        const id = value as string;
        if (id !== undefined) {
            this.setState({selectedId: Number.parseInt(id, undefined)});
        }
    }
}