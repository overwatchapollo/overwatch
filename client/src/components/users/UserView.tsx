import React, {useState} from 'react';
import { User } from 'api/users/User';
import { Button, Grid, Tabs, Tab, Table, TableRow, TableCell, Typography, Divider, WithStyles } from '@material-ui/core';
import { deleteUser } from 'api/users/UserService';
import { removeRoleFromUser } from 'api/roles/RoleService';
import { UserRole } from 'api/roles/UserRole';
import clsx from 'clsx';

interface IUser extends WithStyles {
    user: User;
    openAddRole: () => void;
    roleList: Array<UserRole>;
    userUpdated: () => void;
    selectPasswordUpdate: (user: User) => void;
}

interface UserViewState {
    tabPage: number;
}

export class UserView extends React.Component <IUser, UserViewState> {
    
    constructor(props: IUser) {
        super(props);
        this.state = {
            tabPage: 0,
        }
    }

    deleteRole(userId: number, roleId: number) {
        removeRoleFromUser(userId, roleId);
        const rolename = this.props.roleList.find(x => roleId.toString().localeCompare(x.id) === 0);
        
        if (rolename !== undefined) {
            this.props.userUpdated();
        }
    }

    render() {
        const user = this.props.user;
        const {classes} = this.props;
        return <Grid container={true} alignItems="center" className={classes.userDetailsContainer} direction="column" spacing={1}>
            <Grid item={true}>
                <Grid alignItems="flex-start" container={true} direction="column" style={{padding:8}}>
                    <Grid item={true}>
                        <Grid container={true}>
                            <Tabs value={this.state.tabPage} onChange={(event,value) => this.setState({tabPage: (value as number)})}>
                                <Tab className={TabClassName(classes, this.state.tabPage, 0)} 
                                    label="Details" 
                                    {...a11yProps(0)}/>
                                <Tab className={TabClassName(classes, this.state.tabPage, 1)} 
                                    label="Roles" 
                                    {...a11yProps(1)}/>
                            </Tabs>
                        </Grid>
                    </Grid>
                    <Grid item={true} className={classes.userDetailsPanel}>
                        <TabPanel value={this.state.tabPage} index={0} >
                            <Grid container={true} direction="column" spacing={1}>
                                <Grid item={true}>
                                    <Table>
                                        <TableRow>
                                            <TableCell>Username</TableCell>
                                            <TableCell>{user.username}</TableCell>
                                        </TableRow>
                                    </Table>
                                </Grid>
                                <Grid item={true}>
                                    <UserActions user={user} passwordUpdate={u => this.props.selectPasswordUpdate(u)} deleteUser={u => deleteUser(u)}/>
                                </Grid>
                            </Grid>
                        </TabPanel>

                        <TabPanel value={this.state.tabPage} index={1}>
                            <Grid container={true} direction="column" spacing={1}>
                                <Grid item={true}>
                                    <Typography variant="h6">Actions</Typography>
                                </Grid>
                                <Grid item={true}>
                                    <Button onClick={() => {this.props.openAddRole()}}>Add Role</Button>
                                </Grid>
                                <Grid item={true}>
                                    <Divider/>
                                </Grid>
                                <Grid item={true}>
                                    <Roles userId={user.id} roleNames={user.roleNames} roleList={this.props.roleList} deleteRole={(userId, roleId) => this.deleteRole(userId, roleId)}/>
                                </Grid>
                            </Grid>
                        </TabPanel>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    }
}

const TabClassName:(classes: Record<string, string>, tabPage: number, tab: number) => string = (classes: Record<string, string>, tabPage: number, tab: number) => {
    return clsx(classes.userDetailsTab, {[classes.userDetailsTabSelected]: tabPage === tab,});
}

function a11yProps(index: number) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
    }

interface IUserActions {
    user: User;
    passwordUpdate: (user: User) => void;
    deleteUser: (user: User) => void;
}

const UserActions: (props: IUserActions) => JSX.Element = (props: IUserActions) => {
    const [deleted, setDeleted] = useState(false);
    return <Grid container={true}>
        <Grid item={true}>
            <Button onClick={() => props.passwordUpdate(props.user)}>Change Password</Button>
        </Grid>
        <Grid item={true}>
            <Button disabled={deleted} onClick={() => {setDeleted(true);props.deleteUser(props.user);}}>Delete Account</Button>
        </Grid>
    </Grid>
}

interface IRoles {
    userId: number;
    roleNames: Array<string>;
    roleList: Array<UserRole>;
    deleteRole:(userId: number, roleId: number) => void;
}

const Roles: (props: IRoles) => JSX.Element = (props: IRoles) => {
    return <Grid container={true} direction="column">
        {props.roleNames.map((value, index) => {
            const roleid = props.roleList.find(x => x.name === value);
            if (roleid === undefined) {
                return <div key={index}/>
            }
            const id = Number.parseInt(roleid.id, undefined);
            return <Grid item={true} key={value}>
                {value} <Button onClick={() => props.deleteRole(props.userId, id)}>X</Button>
            </Grid>
        })}
    </Grid>
}

interface TabPanelProps {
    children?: React.ReactNode;
    className?: string;
    index: number;
    value: number;
}

function TabPanel(props: TabPanelProps) {
    const { children, value, index, className, ...other } = props;
    return <Grid 
        role="tabpanel"
        hidden={value !== index} 
        id={`simple-tabpanel-${index}`}
        className={className}
        {...other}>
            {children}
        </Grid>
  }