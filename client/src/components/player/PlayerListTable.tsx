import React, { useState } from 'react';
import { Divider, Table, TableHead, TableRow, TableCell, TableBody, Grid, Button, Typography, Theme, ListItemText, List, ListItem, ListItemSecondaryAction, ClickAwayListener, Tooltip, IconButton, useTheme, makeStyles } from '@material-ui/core';
import { PermissionWrapper } from 'api/PermissionWrapper';
import { ServerConfiguration } from 'api/server/ServerConfiguration';
import { Player } from 'api/chat/chat';
import { Redirect } from 'react-router-dom';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import ChatIcon from '@material-ui/icons/Chat';
import SearchIcon from '@material-ui/icons/Search';
import EjectIcon from '@material-ui/icons/Eject';
import BlockIcon from '@material-ui/icons/Block';
import { BanDispatcher } from 'container/BanDialogContainer';
import { useDispatch } from 'react-redux';
import { KickDispatcher } from 'container/KickDialogContainer';
import { WhisperDispatcher } from 'container/WhisperDialogContainer';

interface IPlayerProps {
	server: PermissionWrapper<ServerConfiguration>;
	players: Array<Player>;
	overrideFilter: boolean;
	showOffline:boolean;
}

const PlayerListTable: (props: IPlayerProps) => JSX.Element = (props: IPlayerProps) => {
	const [sortName, setSortName] = useState(true);
	return <Table>
		<TableHead>
			<TableRow>
				<TableCell>
					<Button onClick={() => setSortName(false)}>Session ID</Button>
				</TableCell>
				<TableCell colSpan={2}>
					<Button onClick={() => setSortName(true)}>Player Name</Button>
				</TableCell>
				<TableCell>Ping</TableCell>
			</TableRow>
		</TableHead>
		{props.players
			.filter(x => props.overrideFilter || props.showOffline ? true : x.connected)
			.sort(sortName ? (a, b) => a.name.localeCompare(b.name) : (a, b) => a.sessionId - b.sessionId)
			.map(x => <StyledPlayerListRow
				key={x.sessionId}
				actions={props.server.actions}
				player={x} />)
		}
	</Table>
}

interface IPlayerRow {
	player: Player;
	actions: Array<string>;
}

const PlayerListRow: (props: IPlayerRow) => JSX.Element = (props: IPlayerRow) => {
	const dispatch = useDispatch();
	const banDispatcher = new BanDispatcher(dispatch);
	const kickDispatcher = new KickDispatcher(dispatch);
	const whisperDispatcher = new WhisperDispatcher(dispatch);
	
	const { actions, player } = props;
	const classes = playerRowStyles(useTheme());
	const [hideDetails, setHideDetails] = useState(false);
	const [lookupRedirect, setRedirect] = useState(false);
	const className = props.player.connected
		? classes.connectedPlayer
		: props.player.banned
			? classes.bannedPlayer
			: props.player.kicked
				? classes.kickedPlayer
				: classes.offlinePlayer;

	const canWhisper = actions.includes("whisper");
	const canLookup = actions.includes("investigate");
	const canKick = actions.includes("kick");
	const canBan = actions.includes("ban") || actions.includes("ban-2w") || actions.includes("ban-1w");

	const guid = (player.guid ?? "").replace("(?)", "")
	return <TableBody className={className}>
		<TableRow onClick={() => setHideDetails(!hideDetails)}>
			<TableCell>
				<Typography variant="body1">{player.sessionId}</Typography>
			</TableCell>
			<TableCell colSpan={2}>
				<Typography variant="body1">{player.name}</Typography>
			</TableCell>
			<TableCell>
				<Typography variant="body1">{player.ping}</Typography>
			</TableCell>
		</TableRow>
		{hideDetails && <TableRow>
			<TableCell colSpan={4}>
				<List>
					<PlayerConnection player={props.player} />
					<GuidItem guid={guid} />
					<Divider />
					{(canWhisper || canLookup) && <ListItem>
						<ListItemText inset={true}>Interact</ListItemText>
						<ListItemSecondaryAction>
							<Grid container={true} spacing={2}>
								{(player.connected && canWhisper) && <Grid item={true}>
									<Tooltip title="Message">
										<IconButton onClick={() => whisperDispatcher.setWhisperPlayerInfo(player, player.serverId)}>
											<ChatIcon color="action" />
										</IconButton>
									</Tooltip>
								</Grid>}
								{canLookup && <Grid item={true}>
									<Tooltip title="Lookup">
										<IconButton onClick={() => setRedirect(true)}>
											{lookupRedirect && <Redirect to={`/playerInvestigate/${player.guid}`} />}
											<SearchIcon color="action" />
										</IconButton>
									</Tooltip>
								</Grid>}
							</Grid>

						</ListItemSecondaryAction>
					</ListItem>}
					{(canKick || canBan) && <ListItem>
						<ListItemText inset={true}>Punish</ListItemText>
						<ListItemSecondaryAction>
							<Grid container={true} spacing={2}>
								{(player.connected && canKick) && <Grid item={true}>
									<Tooltip title="Kick">
										<IconButton onClick={() => kickDispatcher.setKickPlayerInfo(props.player, props.player.serverId)}>
											<EjectIcon color="secondary" />
										</IconButton>
									</Tooltip>
								</Grid>}
								{canBan && <Grid item={true}>
									<Tooltip title="Ban">
										<IconButton onClick={() => banDispatcher.setBanPlayerInfo(props.player, props.player.serverId)}>
											<BlockIcon color="secondary" />
										</IconButton>
									</Tooltip>
								</Grid>}
							</Grid>
						</ListItemSecondaryAction>
					</ListItem>}
				</List>
			</TableCell>
		</TableRow>}
	</TableBody>
}

const PlayerConnection: (props: { player: Player }) => JSX.Element = props => {
	if (props.player.connected) {
		return <>{false}</>
	}

	if (props.player.kicked) {
		return <ListItem>
			<ListItemText inset={true}>Player was kicked: {props.player.disconnectionMessage}</ListItemText>
		</ListItem>
	}

	if (props.player.banned) {
		return <ListItem>
			<ListItemText inset={true}>Player was banned: {props.player.disconnectionMessage}</ListItemText>
		</ListItem>
	}

	return <ListItem>
		<ListItemText inset={true}>Player has disconnected</ListItemText>
	</ListItem>
}

const GuidItem: (props: { guid: string }) => JSX.Element = props => {
	const [showCopyTooltip, setShowCopyTooltip] = useState(false);
	return <ListItem>
		<ListItemText inset={true} primaryTypographyProps={{noWrap: true}}>GUID {props.guid}</ListItemText>
		<ListItemSecondaryAction>
			<ClickAwayListener onClickAway={() => setShowCopyTooltip(false)}>
				<Tooltip title="Copied to clipboard"
					open={showCopyTooltip}
					disableFocusListener={true}
					disableHoverListener={true}
					disableTouchListener={true}
				>
					<IconButton onClick={() => {
						navigator.clipboard.writeText(props.guid);
						setShowCopyTooltip(true)
					}}>
						<FileCopyIcon />
					</IconButton>
				</Tooltip>
			</ClickAwayListener>
		</ListItemSecondaryAction>
	</ListItem>
}

const playerRowStyles = makeStyles((theme: Theme) => ({
	offlinePlayer: {
		backgroundColor: theme.palette.primary.dark,
		color: theme.palette.primary.contrastText,
	},
	connectedPlayer: {
		backgroundColor: theme.palette.primary.light,
		color: theme.palette.primary.contrastText,
	},
	kickedPlayer: {
		backgroundColor: theme.palette.secondary.light,
		color: theme.palette.secondary.contrastText,
	},
	bannedPlayer: {
		backgroundColor: theme.palette.secondary.dark,
		color: theme.palette.secondary.contrastText,
	}
}));

const StyledPlayerListRow = PlayerListRow;

export default PlayerListTable;