import React from 'react';
import { Button, TableRow, TableCell, Table, TableBody } from '@material-ui/core';
import { removeAdmin } from 'api/management/playerAdmin/PlayerAdminService';
import { AggregatePlayerAdminRecord } from 'api/management/playerAdmin/AggregatePlayerAdminRecord';


interface IAdminRecord {
    record: AggregatePlayerAdminRecord;
}

export const PlayerAdminDetailed: (props: IAdminRecord) => JSX.Element = props => {
    return <TableRow>
        <TableCell colSpan={4}>
            <Table>
                <TableBody>
                    {props.record.records.map(x => <TableRow key={`${x.guid}/${x.server}`}>
                            <TableCell>{x.serverName}</TableCell>
                            <TableCell>{x.level}</TableCell>
                            <TableCell>
                                <Button onClick={() => removeAdmin(x.server, x.guid)}>Remove</Button>
                            </TableCell>
                        </TableRow> 
                    )}                
                </TableBody>
            </Table>
        </TableCell>
    </TableRow>
}