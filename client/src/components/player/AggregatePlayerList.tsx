import React from 'react';
import { Grid, Divider, Toolbar } from '@material-ui/core';
import { Typography } from '@material-ui/core';
import { PlayerList } from './PlayerList';
import { PlayerFilter, ServerFilter } from 'components/filters';
import { mapStateToProps, mapDispatchToProps } from 'container/ServerConfigurationContainer';
import { connect } from 'react-redux';
import { ServerConfigurationState } from 'reducers/ServerConfigurationReducer';
import { PageContainer } from 'components/pagecontainer/PageContainer';
import { ServerWrapper } from 'api/server/ServerWrapper';

interface IMessageStreamer {
    servers: ServerConfigurationState;
}

interface IPlayerListState {
    selectedServers: Array<ServerWrapper>;
    servers: Map<number, boolean>;
    nameFilter: string;
}

class AggregatePlayerList extends React.Component<IMessageStreamer, IPlayerListState> {
    constructor(props: IMessageStreamer) {
        super(props);
        this.state = {
            selectedServers: new Array<ServerWrapper>(0),
            servers: new Map(),
            nameFilter: "",
        }
    }

    render() {
        return <PageContainer>
                <Grid container={true} spacing={2} direction="column" justify="space-between" alignItems="center" style={{padding: 8}}>
                    <Grid item={true}>
                        <Toolbar>
                            <Typography variant="h4">Global Player List</Typography>
                        </Toolbar>
                    </Grid>
                    <Grid item={true}>
                        <Grid container={true} alignItems="center">
                            <Grid item={true}>
                            <ServerFilter 
                                allServers={this.props.servers.servers.map(x => x.data)} 
                                selectedServers={servers => {
                                    const mappedServers = servers
                                        .map(x => this.props.servers.servers.find(y => y.data.serverId === x.serverId) as ServerWrapper)
                                    this.setState({selectedServers: mappedServers})
                                }}/>
                            </Grid>
                            <PlayerFilter pushFilter={(name) => this.setState({nameFilter: name})}/>
                        </Grid>                        
                    </Grid>

                    {this.state.selectedServers.length !== 0 && 
                        <Grid item={true}>
                            <Divider/>
                        </Grid>}

                    <Grid item={true}>
                        <ShowPlayerList 
                            nameFilter={this.state.nameFilter} 
                            servers={this.state.selectedServers}/>
                    </Grid>
                </Grid>
            </PageContainer>
    }
}

const ReduxAggregatePlayerList = connect(mapStateToProps, mapDispatchToProps)(AggregatePlayerList);
export {ReduxAggregatePlayerList};

interface IPlayerList {
    nameFilter: string;
    servers: Array<ServerWrapper>;
}

const ShowPlayerList:(props: IPlayerList) => JSX.Element = (props: IPlayerList) => {
    return <Grid container={true} spacing={1} direction="row" justify="center" alignItems="flex-start" wrap="nowrap">
    {props.servers.map((x: ServerWrapper) => 
        <Grid item={true} key={x.data.serverId}>
            <PlayerList 
                key={x.data.serverId}
                config={x}
                nameFilter={props.nameFilter}/>
        </Grid>
                
    )}
    </Grid>
}
