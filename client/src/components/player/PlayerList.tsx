import * as React from 'react';
import {Player} from 'api/chat/ConnectedPlayer';
import { Switch, Button, Grid, Typography, LinearProgress, IconButton, Dialog, DialogTitle, DialogContent, DialogActions, TextField, Menu, MenuItem, Fade, Box, FormControl, FormControlLabel, Divider  } from '@material-ui/core';
import { refreshPlayerList } from 'api/player/PlayerService';
import PlayerListTable from './PlayerListTable';
import ErrorIcon from '@material-ui/icons/Error';
import { useSelector } from 'react-redux';
import { ServerWrapper } from 'api/server/ServerWrapper';
import ChatIcon from '@material-ui/icons/Chat';
import { sendAll } from 'api/chat/ChatService';
import { useState } from 'react';
import { RootState } from 'reducers';
import { Done, ViewHeadline } from '@material-ui/icons';

interface IPlayerListProps {
    config: ServerWrapper;
    nameFilter: string;
}

const PurePlayerList: (props:IPlayerListProps) => JSX.Element = props => {
    const [archivePlayerList] = useState<Array<Player>>(new Array(0));
    const [showOffline, setShowOffline] = useState(false);
    const [viewArchive, setViewArchive] = useState(false);
    const [showOptions, setShowOptions] = useState(false);
    const [optionTarget, setOptionTarget] = useState<undefined | Element>(undefined);

    const { config } = props;
    const { serverId } = config.data;
    const playerCache = useSelector((state: RootState) => state.playerCacheState)
    const playerMap = playerCache.serverMap.get(serverId) ?? new Map<number, Player>();
    const players = Array.from(playerMap.values());
    
    const filter = props.nameFilter;

    const playerList = getPlayerList(filter, viewArchive, players, archivePlayerList);

    return <Grid container={true} direction="column" style={{padding:8}} alignItems="stretch" justify="center" spacing={2}>
        <Grid item={true}>
            <Grid container={true} alignContent="stretch" justify="center">
                <Grid item={true}>
                    <Typography variant="subtitle1">Players since restart: {players.length}</Typography>
                </Grid>
                <Grid item={true}>
                    <IconButton onClick={event => {
                        setShowOptions(!showOptions)
                        setOptionTarget(event.currentTarget)
                    }}>
                        <ViewHeadline/>
                    </IconButton>
                    <OptionMenu open={showOptions} onClose={() => setShowOptions(false)} anchor={optionTarget}
                        offline={[showOffline, (value) => setShowOffline(value)]}
                        archive={[viewArchive, (value) => setViewArchive(value)]}
                        canRefresh={[config.actions.includes(`refresh:${serverId}`), () => refreshPlayerList(serverId)]}
                        />
                </Grid>
            </Grid>
            
        </Grid>
        <Grid item={true}>
            <Divider/>
        </Grid>
        <Grid item={true}>
            <PlayerListTable
                server={config}
                players={playerList}
                overrideFilter={viewArchive}
                showOffline={showOffline}/>
        </Grid>
    </Grid>
}

interface ListingProps {
    connectedPlayers: number;
    maxPlayers: number;
}
const PlayerListing: (props: ListingProps) => JSX.Element = props => {
    const fullPercentage = (props.connectedPlayers / props.maxPlayers) * 100;

    return <Grid container={true} direction="column" alignItems="center" spacing={2}>
        <Grid item={true}>
            <Typography variant="body1">Capacity</Typography>
        </Grid>
        <Grid item={true}>
            <LinearProgress style={{width: 50, height: 5}} variant="determinate" value={fullPercentage}/>
        </Grid>
        <Grid item={true}>
            <Typography variant="body1">{props.connectedPlayers} of {props.maxPlayers}</Typography>
        </Grid>
    </Grid>
}

interface OptionProps {
    open: boolean;
    onClose: () => void;
    anchor?: Element;
    offline: [boolean, (value: boolean) => void];
    archive: [boolean, (value: boolean) => void];
    canRefresh: [boolean, () => void];
}

const OptionMenu: (props: OptionProps) => JSX.Element = props => {
    return <Menu 
            anchorOrigin={{ vertical: "bottom", horizontal: "right" }} 
            transformOrigin={{ vertical: "top", horizontal: "right" }} 
            getContentAnchorEl={null}
            open={props.open} 
            onClose={() => props.onClose()} 
            TransitionComponent={Fade}
            anchorEl={props.anchor}
            >
                <Box border={2}>
            <MenuItem>
                <FormControl>
                    <FormControlLabel  
                        label={props.offline[0] ? "Showing offline players" : "Hiding offline players"} 
                        control={<Switch checked={props.offline[0]} onChange={event => props.offline[1](event.target.checked)}/>}/>             
                </FormControl>
            </MenuItem>
            <Divider/>
            <MenuItem>
                <FormControl>
                    <FormControlLabel  
                        label={props.archive[0] ? "Showing archive players" : "Show current players"} 
                        control={<Switch checked={props.archive[0]} onChange={event => props.archive[1](event.target.checked)}/>}/>             
                </FormControl>
            </MenuItem>
            {props.canRefresh[0] && <MenuItem>
                <FormControl>
                    <Button onClick={props.canRefresh[1]}>Refresh Player List</Button>
                </FormControl>
            </MenuItem>}
        </Box>
    </Menu>
}

interface HeaderProps {
    config: ServerWrapper;
}

const PlayerListHeader: (props:HeaderProps) => JSX.Element = props => {
    const [showBroadcast, setShowBroadcast] = useState(false);
    const { serverId, serverName } = props.config.data;
    const playerCache = useSelector((state: RootState) => state.playerCacheState.serverMap.get(serverId) ?? new Map<number, Player>())
    return <Grid container={true} spacing={2} alignItems="stretch" justify="center" alignContent="stretch">
        <Grid item={true}>
            <Typography variant="h6" color="inherit">{serverName}</Typography>    
        </Grid>
        {props.config.actions.includes("broadcast") && <Grid item={true}>
            <BroadcastComponent
                show={showBroadcast}
                serverId={serverId}
                serverName={serverName}
                onClose={() => setShowBroadcast(false)}
            />
            <IconButton onClick={() => setShowBroadcast(true)}>
                <ChatIcon/>
            </IconButton>
        </Grid>}
        <Grid item={true}>
            <PlayerListing connectedPlayers={[...playerCache.entries()].filter(x => x[1].connected).length} maxPlayers={props.config.data.maxPlayers}/>
        </Grid>
        <Grid item={true}>
            <IconButton size='small' style={{backgroundColor: '#ededed'}}>
                {props.config.data.connected ? <Done style={{color: 'green'}}/> : <ErrorIcon style={{color: 'red'}}/>}
            </IconButton>
        </Grid>
    </Grid>
}
export { PlayerListHeader }
function getPlayerList(filter: string, viewArchive: boolean, full: Array<Player>, archive: Array<Player>): Array<Player> {
    const workingPlayerList = viewArchive ? archive : full;
    return filterPlayerList(filter, workingPlayerList);
}

function filterPlayerList(filter: string, players: Array<Player>): Array<Player> {
    const reg = new RegExp(filter, 'i');
    return players.filter(x => {
        return reg.test(x.name)
    });
}

interface BroadcastProps {
    serverId: number;
    serverName: string;
    show: boolean;
    onClose: () => void;
}

const BroadcastComponent: (props: BroadcastProps) => JSX.Element = props => {
    const [broadcastText, setBroadcastText] = useState("");
    return <Dialog open={props.show} onBackdropClick={() => props.onClose()}>
        <DialogTitle>Broadcast message to {props.serverName}</DialogTitle>
        <DialogContent>
            <TextField 
                label="Message" 
                variant="outlined" 
                value={broadcastText} 
                onChange={event => setBroadcastText(event.target.value)}
                multiline={true}
                style={{width: '100%'}}/>
        </DialogContent>
        <DialogActions>
            <Button variant="contained"
                color="primary"
                onClick={() => {
                if (broadcastText) {
                    sendAll(props.serverId, broadcastText)
                        .then(() => setBroadcastText(""))
                        .then(() => props.onClose())
                }
            }}>
                Send
            </Button>
            <Button 
                variant="contained" 
                color="secondary" 
                onClick={() => {
                    setBroadcastText("")
                    props.onClose()
                }}>
                    Cancel
            </Button>
        </DialogActions>
    </Dialog>
}

const PlayerList = PurePlayerList;


export {PlayerList};