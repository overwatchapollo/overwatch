import React, { ReactNode } from 'react';
import { Dialog, DialogTitle, DialogContent, DialogContentText, TextField, DialogActions, Button, Select, MenuItem, FormControlLabel, Checkbox, FormGroup } from '@material-ui/core';
import { BanRequest } from '../../../api/player/BanRequest';

interface IBanRequestForm {
    getName: () => string;
    getGuid: () => string;
    open: () => boolean;
    onClose: () => void;
    onSubmit: (request:BanRequest) => void;
    serverLocation: number;
}

interface IBanState {
    duration: number;
    reason: string;
    pernament: boolean;
    multiplier: number;
    isBanning: boolean;
}

export class BanRequestForm extends React.Component<IBanRequestForm, IBanState> {
    constructor(props: IBanRequestForm) {
        super(props);
        this.state = {
            duration: 3600,
            reason: "",
            pernament: false,
            multiplier: 1,
            isBanning: false,
        }
    }

    handleReasonChange(event: any) {
        const msg = event.target.value;
        this.setState({reason: msg});
    }

    handleSubmit(event: any) {
        event.preventDefault();
        const banBody = new BanRequest(
            this.props.getGuid(), 
            this.state.pernament ? 0 : this.state.duration * this.state.multiplier, 
            this.state.reason, 
            this.props.serverLocation.toString());
        this.props.onSubmit(banBody);
        this.props.onClose();
    }

    handleMultiplierChange (event: React.ChangeEvent<HTMLSelectElement>, comp: ReactNode) {
        this.setState({multiplier: parseInt(event.target.value, 10)});
    }

    handleChange (event: React.ChangeEvent<HTMLElement>, checked: boolean) {
        this.setState({pernament: checked});
    }

    render() {
        return (
            <Dialog 
                open={this.props.open()}
                aria-labelledby="form-dialog-title">
                <DialogTitle>Ban Player {this.props.getName()}</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Ban player to remove them from the server.
                    </DialogContentText>
                    <FormGroup>
                        <TextField
                            id="duration"
                            label="Ban Duration"
                            type="number"
                            value={this.state.duration}
                            onChange={(event:any) => this.setState({duration: parseInt(event.target.value, 1)})}/>
                        <Select title="Ban Multiplier" 
                            value={this.state.multiplier}
                            onChange={(event: any, comp:any) => this.handleMultiplierChange(event, comp)}>
                            <MenuItem value={60}>Hours</MenuItem>
                            <MenuItem value={1440}>Days</MenuItem>
                            <MenuItem value={10080}>Weeks</MenuItem>
                        </Select>
                        <FormControlLabel 
                            label="Pernament?"
                            control={<Checkbox 
                                checked={this.state.pernament} 
                                onChange={(event: any, ch: boolean) => this.setState({pernament: ch})} 
                                value="pernamentBan"
                                color="primary"/>}/>
                    </FormGroup>
                    <TextField
                        autoFocus={true}
                        margin="dense"
                        id="reason"
                        label="Ban Message"
                        type="text"
                        fullWidth={true}
                        onChange={(event:any) => this.handleReasonChange(event)}/>
                </DialogContent>
                <DialogActions>
                    <Button onClick={(event: any) => this.handleSubmit(event)} color="primary" autoFocus={true}>
                        Ban
                    </Button>
                    <Button onClick={(event: any) => this.props.onClose()} color="secondary">
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}