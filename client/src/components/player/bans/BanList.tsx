import React from 'react';
import { TableHead, Paper, TableCell, Table, TableBody, TableRow, Typography, Button, Grid, useTheme } from '@material-ui/core';
import { unbanPlayer, forceBanListRefresh, getBanList, getBanListPlayer } from 'api/player/PlayerService';
import { BanRecord } from 'api/playerManagement/BanRecord';
import { ServerBanList } from 'api/playerManagement/ServerBanList';
import { BanModifyDialog } from './BanModifyDialog';
import styles from 'styles';
import { ConfirmationDialog } from 'components/confirmation/ConfirmationDialog';
import { ServerConfiguration } from 'api/server/ServerConfiguration';
import { PermissionWrapper } from 'api/PermissionWrapper';


interface IBanListProps {
    server: PermissionWrapper<ServerConfiguration>;
    guidFilter?: string;
    limit?: number;
}

interface IBanListState {
    bans: ServerBanList | undefined;
    modify: boolean;
    limit?: number;
    guidFilter: string;
    banIdToRemove: number | undefined;
}

export class BanList extends React.Component<IBanListProps, IBanListState> {
    constructor(props: IBanListProps) {
        super(props);
        this.state = {
            bans: undefined,
            modify: false,
            limit: props.limit,
            guidFilter: props.guidFilter === undefined ? "" : props.guidFilter,
            banIdToRemove: undefined,
        }
    }

    componentDidMount() {
        this.queryBanList();
    }

    transformDuration(duration: string): string {
        
        if (duration.localeCompare("PT0S") === 0) {
            return "Perm";
        }

        return duration;
    }

    queryBanList() {
        if (this.state.guidFilter !== "") {
            getBanListPlayer(this.props.server.data.serverId, this.state.guidFilter)
            .then(x => this.setState({bans: x}))
        } else {
            getBanList(this.props.server.data.serverId, this.state.limit, undefined)
                .then(x => this.setState({bans: x}));
        }
    }

    render() {
        const banState = this.state.bans;

        if (this.state.limit !== this.props.limit) {
            getBanList(this.props.server.data.serverId, this.props.limit, undefined)
                .then(x => this.setState({limit: this.props.limit, bans: x}));
        }

        return <Paper>
            <BanModifyDialog open={this.state.modify} 
                banUpdate={record => {console.log(record)}} 
                close={() => this.setState({modify:false})}/>
            <ConfirmationDialog
                open={this.state.banIdToRemove !== undefined}
                title={"Ban Removal"}
                question={"Are you sure you want to remove this ban?"}
                result={result => {
                    const banid = this.state.banIdToRemove;
                    if (result && banid) {
                        unbanPlayer(this.props.server.data.serverId, banid)
                            .then(x => this.setState({banIdToRemove: undefined}));
                    }
                }}/>
            <Grid container={true} direction="column" spacing={2} style={{padding: 8}}>
                <Grid item={true}>
                    <Typography variant="h6">{this.props.server.data.serverName}</Typography>
                </Grid>
                <Grid item={true}>
                    <Grid container={true} spacing={2} alignItems="center">
                        <Grid item={true}>
                            <Typography variant="subtitle1">Showing {this.state.limit} of {banState?.totalBans}</Typography>
                        </Grid>
                        <Grid item={true}>
                            {this.props.server.actions.includes("ban-refresh") &&
                                <Button onClick={() => forceBanListRefresh(this.props.server.data.serverId).then(x => this.queryBanList())}>
                                    Force refresh
                                </Button>
                            }
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item={true}>
                    {banState && <StyledServerBans 
                        bans={banState} 
                        canRemoveBans={this.props.server.actions.includes("ban-remove")}
                        onRemove={(server, ban) => this.setState({banIdToRemove: ban})}
                        modify={(server, ban) => console.log(`ban modify ${server} ${ban}`)}/>}
                </Grid>
            </Grid>
        </Paper>
    }
}

interface ServerBans {
    bans: ServerBanList;
    canRemoveBans: boolean;
    modify:(serverId: number, banId: number) => void;
    onRemove: (serverId: number, banId: number) => void;
}

const ServerBanRender: (props:ServerBans) => JSX.Element = (props:ServerBans) => {
    const classes = styles(useTheme());
    return <Table>
        <TableHead>
            <TableRow>
                <TableCell>Ban ID</TableCell>
                <TableCell>GUID</TableCell>
                <TableCell>Duration</TableCell>
                <TableCell colSpan={2}>Message</TableCell>
            </TableRow>
        </TableHead>
        {props.bans.banRecords.map((x, i) => <StyledRecordRow 
            className={i % 2 ? classes.banRowOdd : classes.banRowEven}
            key={x.id} 
            record={x} 
            modify={props.modify}
            canRemoveBans={props.canRemoveBans}
            onRemove={banId => props.onRemove(props.bans.serverMrid, banId)}/>)}
    </Table>
}

interface IRecordRow {
    className?: string;
    record: BanRecord;
    canRemoveBans: boolean;
    modify:(serverId: number, banId: number) => void;
    onRemove: (banId: number) => void;
}

const BanRecordRow: (props:IRecordRow) => JSX.Element = (props:IRecordRow) => {
    return <TableBody className={props.className}>
        <TableRow>
            <TableCell>{props.record.id}</TableCell>
            <TableCell>{props.record.guid}</TableCell>
            <TableCell>{props.record.duration}</TableCell>
            <TableCell>{props.record.message}</TableCell>
            <TableCell>{props.canRemoveBans && <Button onClick={() => props.onRemove(props.record.id)}>Remove</Button>}</TableCell>
        </TableRow>
    </TableBody>
}


const StyledServerBans = ServerBanRender;
const StyledRecordRow = BanRecordRow;