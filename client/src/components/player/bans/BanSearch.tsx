import React from 'react';
import { Button, Grid, Paper, TextField, Card, CardContent, Table, TableBody, TableRow, TableCell, CardActions, Toolbar, Typography } from '@material-ui/core';
import { submitBan, unbanPlayer, getBanListPlayer } from 'api/player/PlayerService';
import { BanRequestForm } from 'components/player/bans/BanRequestForm';
import { ServerBanList } from 'api/playerManagement/ServerBanList';
import { ServerConfigurationState } from 'reducers/ServerConfigurationReducer';
import { connect } from 'react-redux';
import { mapStateToProps, mapDispatchToProps } from 'container/ServerConfigurationContainer';

interface IBanSearchProps {
    servers: ServerConfigurationState;
}

interface IBanSearchState {
    guidSearch: string;
    serverBans: Array<ServerBanList>;
    banRequest: boolean;
    banServer: number;
    searchFailed: boolean;
}

class BanSearch extends React.Component <IBanSearchProps, IBanSearchState> {
    constructor(props: any) {
        super(props);
        this.state = {
            guidSearch: "",
            serverBans: new Array(0),
            banRequest: false,
            banServer: -1,
            searchFailed: false,
        }
    }

    render() {
        return (<Paper>
            <Grid container={true}>
                <Grid item={true}>
                    <Typography variant="h5">Ban search</Typography>
                </Grid>
                <Grid item={true}>
                    <TextField
                        autoFocus={true}
                        margin="dense"
                        id="guid"
                        label="GUID"
                        type="text"
                        fullWidth={true}
                        value={this.state.guidSearch}
                        onChange={(event: any) => this.setState({guidSearch: event.target.value})}/>
                </Grid>
            </Grid>
            <Toolbar>
                
            </Toolbar>

            <BanRequestForm 
                    serverLocation={this.state.banServer}
                    onSubmit={request => submitBan(request)}
                    getGuid={() => this.state.guidSearch} 
                    open={() => this.state.banRequest}
                    getName={() => this.state.guidSearch}
                    onClose={() => this.setState({banRequest: false})}/>

            <Card>
                <CardContent>
            <TextField
                autoFocus={true}
                margin="dense"
                id="guid"
                label="GUID"
                type="text"
                fullWidth={true}
                value={this.state.guidSearch}
                onChange={(event: any) => this.setState({guidSearch: event.target.value})}/>
                </CardContent>
                <CardActions>
            <Button onClick={() => this.search()}>
                Search
            </Button>
            </CardActions>
            </Card>

            {this.state.searchFailed && <Typography variant="body1">Failed to find any bans with GUID {this.state.guidSearch}</Typography>}

            {this.state.serverBans.map((x, i) => 
                <Card key={i}>
                    <CardContent>
                        {this.getServerName(x.serverMrid)} <br/>
                        {x.banRecords.map((y, j) => <Table key={j}>
                            <TableBody>
                                <TableRow>
                                    <TableCell>{y.duration.localeCompare("PT0S") === 0 ? "Perm" : y.duration}</TableCell>
                                    <TableCell>{y.message}</TableCell>
                                    <TableCell><Button>Modify</Button></TableCell>
                                    <TableCell><Button onClick={() => this.removeBan(x.serverMrid, y.id)}>Remove Ban</Button></TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>)}
                        
                    </CardContent>
                    <CardActions>
                        <Button onClick={() => this.addBan(x.serverMrid, this.state.guidSearch)}>Add ban</Button>
                    </CardActions>
                </Card>
            )}
        </Paper>);
    }

    async search() {
        await Promise.all(this.props.servers.servers
            .map(x => x.data)
            .map(x => getBanListPlayer(x.serverId, this.state.guidSearch).catch(x => Promise.resolve(undefined))))
            .then(x => {
                const bans = x.filter(y => y !== undefined).map(y => y as ServerBanList);
                this.setState({searchFailed: bans.length === 0, serverBans: bans})
            });
    }

    getServerName(id: number) {
        const matching = this.props.servers.servers.map(x=> x.data).filter(x => id.toString().localeCompare(x.serverId.toString()) === 0);
        return matching.length === 0 ? "unknown server" : matching[0].serverName;
    }

    addBan(serverMrid:  number, guid: string) {
        this.setState({banServer: serverMrid, banRequest: true});
    }

    removeBan(serverMrid: number, banId: number) {
        unbanPlayer(serverMrid, banId);
    }
}

const ReduxBanSearch = connect(mapStateToProps, mapDispatchToProps)(BanSearch);
export {ReduxBanSearch}