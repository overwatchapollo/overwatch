import React, { useState } from 'react';
import { Grid, Typography, FormControl, Select, TextField, Button } from '@material-ui/core';
import { BanList } from './BanList';
import { ServerConfigurationState } from 'reducers/ServerConfigurationReducer';
import { mapStateToProps, mapDispatchToProps } from 'container/ServerConfigurationContainer';
import { connect } from 'react-redux';
import { ServerFilter } from 'components/filters/filters';
import { PermissionWrapper } from 'api/PermissionWrapper';
import { ServerConfiguration } from 'api/server/ServerConfiguration';
import { PageContainer } from 'components/pagecontainer/PageContainer';

interface IAggregateServerBanListProps {
    servers: ServerConfigurationState;
}

const AggregateServerBanList: (props:IAggregateServerBanListProps) => JSX.Element = (props: IAggregateServerBanListProps) => {
    const serverList = props.servers.servers;
    const [queryLimit, setQueryLimit] = useState(20);
    const [guidFilterForm, setGuidFilterForm] = useState("");
    const [activeGuidFilter, setActiveGuidFilter] = useState("");
    const [selectedServers, setSelectedServers] = useState(new Array<PermissionWrapper<ServerConfiguration>>(0))
    return <PageContainer>
        <Grid container={true} direction="column" justify="center" alignItems="center" spacing={2} style={{padding: 8}}>
            <Grid item={true}>
                <Typography variant="h4">Server Ban Management</Typography>
            </Grid>
            <Grid item={true}>
                <Grid container={true} direction="row" spacing={2}>
                    <Grid item={true}>
                        <FormControl>
                            GUID Filter
                            <TextField value={guidFilterForm} onChange={event => setGuidFilterForm(event.target.value as string)}/>
                            <Button onClick={event => setActiveGuidFilter(guidFilterForm)}>Search</Button>
                        </FormControl>
                    </Grid>
                    <Grid item={true}>
                        <FormControl>
                            Show Last
                            <Select value={queryLimit} onChange={event => setQueryLimit(event.target.value as number)}>
                                <option value={20}>20</option>
                                <option value={40}>40</option>
                                <option value={60}>60</option>
                                <option value={70}>80</option>
                                <option value={100}>100</option>
                                <option value={25000}>All</option>
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item={true}>
                        <ServerFilter 
                            allServers={serverList.map(x => x.data)} 
                            selectedServers={servers => {
                                const mappedServers = servers
                                    .map(x => serverList.find(y => y.data.serverId === x.serverId) as PermissionWrapper<ServerConfiguration>)
                                setSelectedServers(mappedServers)
                            }}/> 
                    </Grid>
                </Grid>
            </Grid>
            <Grid item={true}>
                <Grid container={true} direction="row" wrap="nowrap" spacing={2}>
                    {selectedServers.map((x, i) => 
                    <Grid item={true} key={i}>
                        <BanList 
                            server={x}
                            guidFilter={activeGuidFilter}
                            limit={queryLimit}/>
                    </Grid>)}
                </Grid>
            </Grid>
        </Grid>
    </PageContainer>
}

const ReduxAggregateBanList = connect(mapStateToProps, mapDispatchToProps)(AggregateServerBanList);
export {ReduxAggregateBanList};