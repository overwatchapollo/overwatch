import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button } from '@material-ui/core'
import { BanRecord } from 'api/playerManagement/BanRecord';
import { BanRequest } from 'api/player/BanRequest';

interface BanModify {
    open: boolean;
    existingBan?: BanRecord;
    banUpdate: (record: BanRequest) => void;
    close:() => void;
}

const BanModifyDialog: (props: BanModify) => JSX.Element = (props: BanModify) => {
    return <Dialog open={props.open} onBackdropClick={() => props.close()}>
        <DialogTitle>Edit Ban</DialogTitle>
        <DialogContent>
            edit form
        </DialogContent>
        <DialogActions>
            <Button>Modify Ban</Button>
            <Button onClick={() => props.close()}>Cancel</Button>
        </DialogActions>
    </Dialog>
}

export {BanModifyDialog}