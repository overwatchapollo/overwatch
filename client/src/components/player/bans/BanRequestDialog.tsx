import React, { useState } from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions, Button, Grid, TextField, Select, MenuItem, FormControlLabel, Checkbox, CircularProgress, Typography } from '@material-ui/core';
import { BanRequest } from 'api/player/BanRequest';
import { ServerConfiguration } from 'api/server/ServerConfiguration';

interface IBanRequest {
    servers?: Array<ServerConfiguration>;
    serverLocation?: number;
    open: boolean;
    guid: string;
    sessionId?: number;
    name: string;
    onSubmit: (request: BanRequest) => Promise<Response>;
    onClose: () => void;
    actions: Array<string>;
}

const BanRequestDialog: (props: IBanRequest) => JSX.Element = (props: IBanRequest) => {
    const [multiplier, setMultiplier] = useState(60);
    const [duration, setDuration] = useState(0);
    const [permanent, setPermanent] = useState(false);
    const [reason, setReason] = useState("");
    const maxDuration = calculateMaxDuration(props.actions, multiplier);
    const [serverLocation, setServerLocation] = useState(props.serverLocation)
    const servers = props.servers === undefined ? new Array<ServerConfiguration>(0) : props.servers;
    const defSelectedServers: Array<number> = new Array(0);

    const [isBanning, setIsBanning] = useState(false);
    const [error, setError] = useState("");

    if (props.serverLocation) {
        defSelectedServers.push(props.serverLocation);
    }
    const [selectedServers, setSelectedServers] = useState(defSelectedServers);
    
    return <Dialog 
        open={props.open}
        aria-labelledby="form-dialog-title"
        style={{minWidth: 600}}>
            <DialogTitle>
                Ban Player {props.name}
            </DialogTitle>
            <DialogContent>
                {isBanning && <div><CircularProgress/> <Typography variant="body1">Ban in progress</Typography></div>}
                {error !== "" && <Typography>An error ocurred when banning: {error}</Typography>}
                <Grid container={true} direction="column" justify="flex-start" alignItems="stretch" style={{minWidth:400}} spacing={2}>
                    <Grid item={true}>
                        <TextField 
                            label="Reason" 
                            multiline={true}
                            value={reason} 
                            onChange={event => setReason(event.target.value)}
                            style={{width:"100%"}}/>
                    </Grid>
                    <Grid item={true} container={true} direction="row" justify="center" alignItems="flex-end" spacing={2}>
                        <Grid item={true} xs={6}>
                            <TextField 
                                label="Ban Duration" 
                                type="number" 
                                inputProps={
                                    {
                                        min: "0",
                                        max: maxDuration.toString(10),
                                        step: "2"
                                    }
                                }
                                value={duration} 
                                onChange={event => {
                                    const inputValue = Number.parseInt(event.target.value);
                                    setDuration(inputValue > maxDuration ? maxDuration : inputValue)
                                }}
                                style={{width:"100%"}}/>
                        </Grid>
                        <Grid item={true} xs={6}>
                            <Select title="Ban Multiplier" 
                                value={multiplier}
                                onChange={event => setMultiplier(event.target.value as number)}
                                style={{width:"100%"}}>
                                <MenuItem value={60}>Hours</MenuItem>
                                <MenuItem value={1440}>Days</MenuItem>
                                <MenuItem value={10080}>Weeks</MenuItem>
                            </Select>
                        </Grid>
                    </Grid>
                    {props.actions.includes("ban") && 
                        <Grid item={true}>
                            <FormControlLabel 
                                label="Pernament?"
                                control={
                                    <Checkbox 
                                        checked={permanent} 
                                        onChange={(event: any, ch: boolean) => setPermanent(ch)} 
                                        color="primary"/>}/>
                        </Grid>}
                    <Grid item={true}>
                        <Grid container={true} direction="column">
                        {servers.map((x, i) => 
                            <Grid item={true} key={x.serverId}>
                                <Checkbox 
                                    checked={selectedServers.includes(x.serverId)}
                                    onChange={(event, checked) => {
                                        if (checked) {
                                            setSelectedServers([...selectedServers, x.serverId])
                                        } else {
                                            setSelectedServers([...selectedServers.filter(y => y !== x.serverId)])
                                        }
                                    }}
                                    key={x.serverId} 
                                    onClick={() => setServerLocation(x.serverId)}/>{x.serverName}
                            </Grid>)}
                        </Grid>
                    </Grid>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button 
                    disabled={serverLocation === undefined || isBanning}
                    onClick={() => {
                        setError("");
                        setIsBanning(true);
                        const results = selectedServers
                            .map(x => new BanRequest(props.guid, permanent ? -1 : duration * multiplier, reason, x.toString()))
                            .map(x => props.onSubmit(x).then(y => y.status))
                        Promise.all(results).then(x => {
                            const success = x.reduce((y,z) => y !== 200 ? y : z, 200) === 200;
                            setIsBanning(false);
                            if (success) {
                                props.onClose();
                            } else {
                                const errors = x.filter(y => y !== 200);
                                setError(`An error ocurred ${errors}`);
                            }
                        })
                    }} 
                    color="primary">Ban</Button>
                <Button onClick={event => props.onClose()}>Cancel</Button>
            </DialogActions>
    </Dialog>
}

function calculateMaxDuration(actions: Array<string>, multiplier: number): number {
    if (actions.includes("ban")) {
        return 10000;
    } else if (actions.includes("ban-2w")) {
        switch(multiplier) {
            case 60:
                return 336
            case 1440:
                return 14;
            case 10080:
                return 2;
            default:
                throw new Error(`Unknown multiplier ${multiplier}`)
        }
    } else if(actions.includes("ban-1w")) {
        switch(multiplier) {
            case 60:
                return 168
            case 1440:
                return 7;
            case 10080:
                return 1;
            default:
                throw new Error(`Unknown multiplier ${multiplier}`)
        }
    }
    return 0;
}

export default BanRequestDialog;