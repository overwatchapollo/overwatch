import React from 'react';
import { TableCell, TableRow } from '@material-ui/core';
import { AggregatePlayerAdminRecord } from 'api/management/playerAdmin/AggregatePlayerAdminRecord';


interface IAdminRecord {
    record: AggregatePlayerAdminRecord;
}

export class PlayerAdminRecord extends React.Component<IAdminRecord, {}> {
    render() {
        return <TableRow>
            <TableCell>{this.props.record.username}</TableCell>
            <TableCell>{this.props.record.guid}</TableCell>
        </TableRow>
    }
}