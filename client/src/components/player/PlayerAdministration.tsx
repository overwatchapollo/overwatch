import React from 'react';
import { getAdmins } from 'api/management/playerAdmin/PlayerAdminService';
import { TableBody, Table, Typography, Toolbar, TableHead, TableRow, TableCell, Button } from '@material-ui/core';
import { PlayerAdminRecord } from './PlayerAdminRecord';
import { PlayerAdminDetailed } from './PlayerAdminDetailed';
import { ServerConfiguration } from 'api/server/ServerConfiguration';
import { getConfigurationList } from 'api/server/ServerConfigurationService';
import { AggregatePlayerAdminRecord } from 'api/management/playerAdmin/AggregatePlayerAdminRecord';
import { PageContainer } from 'components/pagecontainer/PageContainer';

interface IAdminList {
    admins: Array<AggregatePlayerAdminRecord>;
    configs: Array<ServerConfiguration>;
}

class basicPlayerAdministration extends React.Component<{}, IAdminList> {
    constructor(props: {}) {
        super(props);
        this.state = {
            admins: new Array(0),
            configs: new Array(0)
        }
    }

    componentDidMount() {
        getConfigurationList().then(config => {
            const configs = config.map(y => 
                getAdmins(y.serverId)
            );
            Promise.all(configs).then(adminLists => {
                const records: Array<AggregatePlayerAdminRecord> = new Array(0);
                adminLists.forEach(list => {
                    list.forEach(record => {
                        const match = records.filter(x => x.guid.localeCompare(record.guid) === 0);
                        const name = config.filter(x => x.serverId === record.serverId);
                        if (match.length === 0) {
                            const newVal: AggregatePlayerAdminRecord = {username: record.username, guid: record.guid, steamId: record.steamId, records: new Array(0)};
                            newVal.records.push({guid:record.guid, server:record.serverId, serverName:name[0].serverName, level:record.level})
                            records.push(newVal);
                        } else {
                            match[0].records.push({guid:record.guid, server:record.serverId, serverName:name[0].serverName, level:record.level})
                        }
                    })
                })
                this.setState({admins:records});

            });
        })
    }

    render() {
        return <PageContainer>
            <Toolbar>
                <Typography variant="h6" color="inherit" noWrap={true}>
                Player admin list
                </Typography>
            </Toolbar>
            <Button>Add admin</Button>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Username</TableCell>
                        <TableCell>GUID</TableCell>
                    </TableRow>
                </TableHead>
                
                    {this.state.admins.map(x => 
                        <TableBody key={x.steamId+""+x.guid}>
                            <PlayerAdminRecord record={x}/>
                            <PlayerAdminDetailed record={x}/>
                        </TableBody>)}
                
            </Table>
        </PageContainer>
    }
}

export { basicPlayerAdministration as PlayerAdministration }