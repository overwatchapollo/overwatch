import React from 'react';
import { Snackbar } from '@material-ui/core';
import { Toast } from '../../api/toast/Toast';
import './Toast.css'

interface IToast {
    lastMessage: string;
    messageQueue: Array<string>;
    open: boolean;
}

export class ToastComponent extends React.Component<{register:Toast}, IToast> {

    constructor(props: {register:Toast}) {
        super(props);
        this.state = {
            lastMessage: "",
            messageQueue: new Array(0),
            open: false
        }

        props.register
            .registerConsumer(x => this.pushMessage(x));
    }

    handleClose = (event:any, reason:string) => {
        if (reason === 'clickaway') {
          return;
        }
    

        this.setState({ open: this.state.messageQueue.length > 0 });
      };

    pushMessage(message: string) {
        this.state.messageQueue.unshift(message);
        this.setState({open: this.state.messageQueue.length > 0});
    }

    render() {
        return(<Snackbar 
            className={"kicked"}
            anchorOrigin={{vertical:'bottom', horizontal:'right'}}
            open={this.state.open}
            autoHideDuration={5000}
            onClose={this.handleClose}
            message={this.state.messageQueue.pop()}/>);
    }
}