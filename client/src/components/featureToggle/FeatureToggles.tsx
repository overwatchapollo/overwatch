import React from 'react';
import { Paper, Toolbar, Typography, Grid, Switch } from '@material-ui/core';
import { FeatureToggle } from 'api/management/featureToggle/FeatureToggle';
import { getFeatureToggles, setFeatureToggle } from 'api/management/featureToggle/FeatureToggleService';

interface IToggleProps {
}

interface IFeatureToggleState {
    toggles: Array<FeatureToggle>;
}

class BaseFeatureToggles extends React.Component<IToggleProps, IFeatureToggleState> {
    constructor(props: any) {
        super(props);
        this.state = {
            toggles: new Array(0),
        }
    }

    componentDidMount() {
        this.refresh();
    }

    refresh() {
        getFeatureToggles()
            .then(x => this.setState({toggles: x}))
            .catch(x => console.log("ERROR"))
    }

    render() {
        return <Paper>
            <Toolbar>
                <Typography variant="h4">Feature Toggles</Typography>
            </Toolbar>

            <Grid container={true} direction="column" spacing={2} style={{padding: 8}} alignItems="flex-start">

                <Grid item={true}>
                    <Grid container={true} direction="column" spacing={2} alignItems="flex-end">
                        {this.state.toggles.map((x, i) => <Toggle key={x.featureName} toggle={x} setValue={y => this.refresh()}/>)}
                    </Grid>
                </Grid>

            </Grid>
        </Paper>
    }
}

export { BaseFeatureToggles as FeatureToggles }

interface IToggle {
    toggle: FeatureToggle;
    setValue:(value: boolean) => void;
}

const Toggle: (props: IToggle) => JSX.Element = (props: IToggle) => {
    return <Grid item={true}>
        <Typography variant="button">
            {props.toggle.featureName}
        </Typography>
        
        <Switch 
            checked={props.toggle.enabled}
            onChange={event => {
                const val = event.target.checked;
                setFeatureToggle(props.toggle, val)
                    .then(y => {
                        if (y.status === 200) {
                            props.setValue(val)
                        }})
            }}/>
    </Grid>
}