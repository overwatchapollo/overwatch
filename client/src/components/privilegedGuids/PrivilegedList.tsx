import React from 'react';
import { PageContainer } from 'components/pagecontainer/PageContainer';
import { Grid, Typography, TableBody, Table, TableRow, TableCell, Button, Dialog, DialogContent, DialogTitle, DialogActions, TextField, FormControl } from '@material-ui/core';

interface PlayerGuid {
    serverId: number;
    guid: string;
}

interface PrivilegedListState {
    guids: Array<PlayerGuid>;
    addGuidDialog: boolean;
    formGuid: string;
    serverId: number;
}

class PrivilegedList extends React.Component<any, PrivilegedListState> {

    constructor(props: any) {
        super(props);
        this.state = {
            guids: new Array(0),
            addGuidDialog: false,
            formGuid: "",
            serverId: 0
        }
    }

    componentDidMount() {
        fetch("/api/privileged")
            .then(x => x.json())
            .then(x => this.setState({guids: x}))
    }

    addGuid(serverId: number, guid: string) {
        if (guid !== "") {
            fetch(`/api/privileged/${serverId}/${guid}`, {
                method: 'POST'
            }).then(x => this.setState({addGuidDialog: false, formGuid: ""}))
        }
        
    }

    removeGuid(serverId: number, guid: string) {
        fetch(`/api/privileged/${serverId}/${guid}`, {
            method: 'DELETE'
        })
    }

    render() {
        return <PageContainer>
            <Dialog open={this.state.addGuidDialog} onBackdropClick={() => this.setState({addGuidDialog: false})}>
                <DialogTitle>Add GUID to list</DialogTitle>
                <DialogContent>
                    <FormControl>
                        ServerId
                        <TextField value={this.state.serverId} type="number" onChange={event => this.setState({serverId: Number.parseInt(event.target.value, 10)})}/>
                    </FormControl>
                    <FormControl>
                        GUID
                        <TextField value={this.state.formGuid} onChange={event => this.setState({formGuid: event.target.value})}/>
                    </FormControl>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.addGuid(this.state.serverId, this.state.formGuid)}>Submit</Button>
                    <Button onClick={() => this.setState({addGuidDialog: false})}>Close</Button>
                </DialogActions>
            </Dialog>
            <Grid container={true} direction="column" style={{padding: 8}} spacing={2}>
                <Grid item={true}>
                    <Typography variant="h4">Privileged User Management</Typography>
                </Grid>
                <Grid item={true}>
                    <Button variant="contained" color="primary" onClick={() => this.setState({addGuidDialog: true})}>Add GUID</Button>
                </Grid>
                <Grid item={true}>
                    <Table>
                        <TableBody>
                            {this.state.guids.map(x => 
                                <TableRow key={x.guid}>
                                    <TableCell>{x.guid}</TableCell>
                                    <TableCell>
                                        <Button variant="contained" color="secondary" onClick={() => this.removeGuid(x.serverId, x.guid)}>Remove</Button>
                                    </TableCell>
                                </TableRow>)}
                        </TableBody>
                    </Table>
                </Grid>
            </Grid>
        </PageContainer>
    }
}

export { PrivilegedList }