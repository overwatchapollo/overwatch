import React from 'react';
import { Dialog, DialogContent, DialogActions, Button, DialogTitle } from '@material-ui/core';

interface IConfirmationProps {
    open: boolean;
    result: (res:boolean) => void;
    title: string;
    question: string;
    confirmationButtonLabel?: string;
    cancelButtonLabel?: string;
}

const ConfirmationDialog: (props: IConfirmationProps) => JSX.Element = props => {
    return <Dialog open={props.open}
            onBackdropClick={() => props.result(false)}>
        <DialogTitle>{props.title}</DialogTitle>
        <DialogContent>
            {props.question}
        </DialogContent>
        <DialogActions>
            <Button color="primary" variant="contained" onClick={() => props.result(true)}>{props.confirmationButtonLabel ?? `Confirm`}</Button>
            <Button color="secondary" variant="contained" onClick={() => props.result(false)}>{props.cancelButtonLabel ?? `Cancel`}</Button>
        </DialogActions>
    </Dialog>
}

export { ConfirmationDialog }