import React from 'react';
import { TableHead, TableRow, TableCell, Table, TableBody, LinearProgress, Grid, Typography } from '@material-ui/core';
import { getStatus } from 'api/status/StatusService';
import { getConfigurationList } from 'api/server/ServerConfigurationService';
import { StatusUpdate } from 'api/status/StatusUpdate';
import { useState } from 'react';
import { PageContainer } from 'components/pagecontainer/PageContainer';

interface IStatusProps {
    
}

interface IRconVariables {
    confList: Array<StatusUpdate>;
}

class BasicRconStatus extends React.Component<IStatusProps, IRconVariables> {
    constructor(props: IStatusProps) {
        super(props);
        this.state = {
            confList: [],
        };              
    }

    componentDidMount() {
        getConfigurationList()
            .then(x => {
                const ids = x.map((y, i) => y.serverId);
                const proms = Promise.all(ids.map(y => getStatus(y)));
                proms.then(y => this.setState({confList: y}));
            });  
    }

    render() {
        return <PageContainer>
                <Grid container={true} spacing={2} direction="column" alignItems="center" justify="center" style={{padding: 8}}>
                    <Grid item={true}>
                        <Typography variant="h4">Server Status</Typography>
                    </Grid>
                    <Grid item={true}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>ID</TableCell>
                                    <TableCell>Name</TableCell>
                                    <TableCell>Connected</TableCell>
                                    <TableCell>Capacity</TableCell>
                                </TableRow>
                            </TableHead>
                            {this.state.confList.map((x: StatusUpdate, i: number) => <ServerRow key={x.serverId} status={x}/>)}
                        </Table>
                    </Grid>
                </Grid>
            </PageContainer>
    }
}

export { BasicRconStatus as RconStatus }

interface IStatus {
    status: StatusUpdate;
}

const ServerRow:(props: IStatus) => JSX.Element = (props: IStatus) => {
    const [hideDetails, setHideDetails] = useState(false);
    const percentage = (props.status.currentPlayers / props.status.maxPlayers) * 100;
    return< TableBody>
    <TableRow onClick={event => setHideDetails(!hideDetails)}>
        <TableCell>{props.status.serverId}</TableCell>
        <TableCell>{props.status.serverName}</TableCell>
        <TableCell>{props.status.authenticated ? "Yes" : "No"}</TableCell>
        <TableCell><LinearProgress value={percentage} variant="determinate"/></TableCell>
    </TableRow>
    {hideDetails && <TableRow>
        <TableCell colSpan={5}>
            <Grid container={true} spacing={8}>
                <Grid item={true}>
                    Current Players: {props.status.currentPlayers}
                </Grid>
                <Grid item={true}>
                    Max Players: {props.status.maxPlayers}
                </Grid>
            </Grid>
        </TableCell>
    </TableRow>}
</TableBody>
}