import React from 'react';
import { Grid, Divider, useTheme } from '@material-ui/core';
import { PlayerList, PlayerListHeader } from '../player/PlayerList';
import styles from 'styles';
import { ChatViewer } from 'components/chat/ChatViewer';
import { ServerWrapper } from 'api/server/ServerWrapper';

interface IOverviewInput {
    serverId: number;
    config: ServerWrapper;
}

const Overview: (props: IOverviewInput) => JSX.Element = props => {
    
    const classes = styles(useTheme());
    const { config } = props;
    return <Grid container={true} className={classes.OverviewObject} justify="center" direction="column">
        <Grid item={true}>
            <PlayerListHeader config={config}/>
        </Grid>
        <Grid item={true} className={classes.OverviewPlayerList}>
            <PlayerList config={config} nameFilter={""}/>
        </Grid>
        <Grid item={true}>
            <Divider/>
        </Grid>
        <Grid item={true} className={classes.OverviewChat}>
            <ChatViewer server={config} nameFilter={""}/>
        </Grid>
    </Grid>
}

export { Overview };