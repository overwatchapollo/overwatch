import React, { useState } from 'react';
import { Overview } from './Overview';
import { Grid, Typography, Divider, FormControlLabel, Checkbox } from '@material-ui/core';
import { useSelector } from 'react-redux';
import { PageContainer } from 'components/pagecontainer/PageContainer';
import { ServerWrapper } from 'api/server/ServerWrapper';
import { ServerConfiguration } from 'api/server/ServerConfiguration';
import { PermissionWrapper } from 'api/PermissionWrapper';
import { RootState } from 'reducers';

const ReduxOverviewAggregate: () => JSX.Element = () => {
    const serverList = useSelector((state: RootState) => state.servers.servers);
    const [viewAll, setViewAll] = useState(true);
    const [selectedServers, setSelectedServers] = useState<Array<PermissionWrapper<ServerConfiguration>>>(serverList);

    if (viewAll && serverList.length !== selectedServers.length) {
        setSelectedServers(serverList);
    }

    return <PageContainer>
        <Grid container={true} spacing={2} direction="column" justify="center" alignItems="center" style={{padding: 8}}>
            <Grid item={true}>
                <Typography variant="h4">Server Overviews</Typography>
            </Grid>
            <Grid item={true}>
                <Grid container={true} direction="row" justify="center" spacing={2}>
                    <Grid item={true}>
                        <FormControlLabel 
                            value={viewAll ? "true" : "false"}
                            control={<Checkbox 
                                onChange={(_, checked) => {
                                    setViewAll(checked);
                                    if (checked) {
                                        setSelectedServers(serverList);
                                    } else {
                                        setSelectedServers([]);
                                    }
                                
                            }}/>} 
                            checked={viewAll}
                            label={"All"}/>
                    </Grid>
                    {serverList.sort((a,b) => a.data.serverName.localeCompare(b.data.serverName)).map(x => {
                        const { serverId } = x.data;
                        const visible = selectedServers.map(y => y.data.serverId).includes(serverId);
                        return <Grid item={true} key={serverId}>
                            <FormControlLabel label={x.data.serverName}
                                value={visible ? "true" : "false"}
                                control={<Checkbox
                                    checked={visible}
                                    onChange={(_, checked) => {
                                        if (checked) {
                                            const newSelected = [...selectedServers, x]; 
                                            setSelectedServers(newSelected);
                                            if (serverList.length === newSelected.length) {
                                                setViewAll(true);
                                            }

                                        } else {
                                            setSelectedServers([...selectedServers.filter(y => y.data.serverId !== serverId)]);
                                            setViewAll(false);
                                        }
                                        
                                    }}/>}
                            />
                    </Grid>})}
                </Grid>
            </Grid>
            {selectedServers.length !== 0 && <Grid item={true}>
                <Divider/>
            </Grid>}
            <Grid item={true}>
                <RenderAggregate servers={selectedServers.sort((a,b) => a.data.serverName.localeCompare(b.data.serverName)).map(x => x as ServerWrapper)}/>
            </Grid>
        </Grid>
    </PageContainer>
}

export {ReduxOverviewAggregate};

const RenderAggregate: (props: {servers: Array<ServerWrapper>}) => JSX.Element = props => {
    return <Grid 
        className="OverviewAggregate"
        container={true} 
        direction="row"
        justify="center"
        alignItems="flex-start"
        spacing={2}
        wrap="nowrap">
        {props.servers.map(x => 
            <Grid item={true} key={x.data.serverId}>
                <Overview
                    config={x} 
                    serverId={x.data.serverId} />
            </Grid>)}
    </Grid>
}