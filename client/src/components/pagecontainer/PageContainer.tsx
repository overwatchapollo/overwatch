import React from 'react';
import { Paper } from "@material-ui/core";

const Container: React.FunctionComponent<any> = props => {
    return <Paper 
        style={{boxShadow:`#0071b2 -2px 1px, #0071b2 2px 1px`}}>
        {props.children}
    </Paper>
}


const PageContainer = Container;

export { PageContainer }