import { Theme, makeStyles } from '@material-ui/core';

const drawerWidth = 240;
const styles = (theme: Theme) => style(theme)();
const style = (theme: Theme) => makeStyles({
	root: {
		display: 'flex',
		color: theme.palette.background.paper,
		backgroundColor: theme.palette.background.paper,
	},
	backdrop: {
		position: "fixed",
		top: 0,
		left: 0,
		bottom: 0,
		right: 0,
		color: theme.palette.background.paper,
		zIndex: -1,
	},
	drawer: {
		width: drawerWidth,
		flexShrink: 0,
	},
	OverviewPlayerList: {
		height: 600,
		maxHeight: 600,
		overflowY: "auto",
		overflowX: "hidden"
	},
	OverviewChat: {
		overflowY: 'auto',
		overflowX: 'hidden',
		maxHeight: 500,
		flex: '1 1 auto'
	},
	OverviewObject: {
		display: '1 1 flex',
		maxHeight: '100%'
	},
	appBar: {
		transition: theme.transitions.create(['margin', 'width'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
		boxShadow: `#0071b2 0px 6px`
	},
	appBarShift: {
		width: `calc(100% - ${drawerWidth}px)`,
		marginLeft: drawerWidth,
		transition: theme.transitions.create(['margin', 'width'], {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen,
		}),
		boxShadow: `#0071b2 0px 6px`
	},
	toolbarSpace: {
		toolbar: theme.mixins.toolbar
	},
	menuBarContainer: {
		width: '100%'
	},
	menuBar: {
		backgroundColor: theme.palette.background.paper,
		color: theme.palette.primary.contrastText,
		flexGrow: 1
	},
	menuReference: {
		textDecoration: 'none',
	},
	menuDropDown: {
		backgroundColor: theme.palette.primary.light,
		textTransform: 'none',
	},
	banRowOdd: {
		backgroundColor: theme.palette.primary.dark,
	},
	banRowEven: {
		backgroundColor: theme.palette.primary.light,
	},
	toolbar: theme.mixins.toolbar,
	drawerPaper: {
		width: drawerWidth,
	},
	drawerHeader: {
		display: 'flex',
		alignItems: 'center',
		padding: '0 8px',
		...theme.mixins.toolbar,
		justifyContent: 'flex-end',
	},
	content: {
		flexGrow: 1,
		paddingLeft: theme.spacing(3),
		paddingRight: theme.spacing(3),
		transition: theme.transitions.create('margin', {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
		marginLeft: drawerWidth,
		backgroundColor: theme.palette.background.paper,
		color: theme.palette.background.paper,
	},
	contentShift: {
		transition: theme.transitions.create('margin', {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen,
		}),
		marginLeft: 0,
		backgroundColor: theme.palette.background.paper,
		color: theme.palette.background.paper,
	},
	loginBox: {
		backgroundColor: theme.palette.primary.light,
	},
	loginButton: {
		color: theme.palette.common.white,
	},
	appBarButton: {
		color: theme.palette.primary.light,
	},
	accountButton: {
		color: theme.palette.common.white,
	},
	errorbar: {
		backgroundColor: theme.palette.grey[100],
	},
	userDetailsContainer: {
		backgroundColor: theme.palette.divider,
		padding: 8,
		minWidth: 600
	},
	userDetailsTab: {
		backgroundColor: theme.palette.primary.light,
	},
	userDetailsMenu: {
		backgroundColor: theme.palette.background.paper
	},
	userDetailsTabSelected: {
		backgroundColor: theme.palette.primary.main,
		color: theme.palette.common.white,
		fontWeight: theme.typography.fontWeightBold,
	},
	userDetailsPanel: {
		backgroundColor: theme.palette.background.paper,
		flexGrow: 1,
		minWidth: 600,
		padding: 8
	},
	roleGroupIcon: {
		color: '#fff',
		backgroundColor: theme.palette.primary.main,
	}
});

export default styles;