import React from 'react';
// import logo from './77th_logo.png';
import clsx from 'clsx';
import styles from './styles';
import { Grid } from '@material-ui/core';
import { hot } from 'react-hot-loader/root';
import SiteNavigation from './SiteNavigation';
import { BrowserRouter as Router } from "react-router-dom";
import { getMyDetails, loginRequest } from './api/session/LoginService';
import ApolloAppBar from 'components/appBar/ApolloAppBar';
import { useSelector, useDispatch } from 'react-redux';
import ErrorBoundary from 'components/ErrorBoundary/ErrorBoundary';
import { ThemeProvider } from '@material-ui/styles';
import { PlayerKickDialog } from 'components/dialogs/playerKickDialog/PlayerKickDialog';
import { PlayerBanDialog } from 'components/dialogs/playerBanDialog/PlayerBanDialog';
import { PlayerWhisperDialog } from 'components/dialogs/playerWhisperDialog/PlayerWhisperDialog';
import { ServerUpdateSubscription } from 'components/server/ServerUpdateSubscription';
import { ServerDataRegister } from 'components/server/dataRegister/ServerDataRegister';
import { PageSubscriber } from 'components/subscribers/pageSubscriber/PageSubscriber';
import { MqttSubscriber } from 'components/subscribers/MqttSubscriber/MqttSubscriber';
import { AppSwitcher } from 'components/applicationSwitcher/AppSwitcher';
import { PlayerSubscriberAggregate } from 'components/subscribers/PlayerSubscriber/PlayerSubscriberAggregate';
import { ChatSubscriberAggregate } from 'components/subscribers/ChatSubscriber/ChatSubscriberAggregate';
import { useState, useEffect } from 'react';
import { RootState } from 'reducers';
import { ClearSession, StoreUser } from 'actions/SessionActions';
import { Dispatch } from 'redux';

export async function doLogin(dispatch: Dispatch, username: string, password: string) {
	loginRequest(username, password)
		.then(x => {
			localStorage.setItem("username", username);
			localStorage.setItem("password", password);
			dispatch(StoreUser(x));
		}, () => dispatch(ClearSession()))
}

const Application: () => JSX.Element = () => {
	const [openSidebar, setOpenSidebar] = useState(false);
	const dispatch = useDispatch();
	
	useEffect(() => {
		getMyDetails()
			.then(x => dispatch(StoreUser(x)))
			.catch(() => dispatch(ClearSession()));
	}, [dispatch]);
	
	const theme = useSelector((state: RootState) => state.theme.theme);
	const classes = styles(theme);
	
	const style = { backgroundColor: theme.palette.background.paper };
	const mainClassName = clsx(classes.content, { [classes.contentShift]: !openSidebar, });

	return <ThemeProvider theme={theme}>
		<div className={classes.backdrop} style={style} />
		<Router>
			<Grid container={true} style={style} alignItems="stretch" justify="center" direction="column" className={classes.root}>
				<Grid style={style} item={true}>
					<ApolloAppBar
						openSideBar={openSidebar}
						toggleSidebar={x => setOpenSidebar(x)} />
				</Grid>
				<Grid item={true} style={style} className={mainClassName}>
					<SiteNavigation openSidebar={openSidebar} />
					<div className={classes.toolbarSpace} />
					<ErrorBoundary>
						<ServerUpdateSubscription />
						<ServerDataRegister />
						<PageSubscriber />
						<MqttSubscriber />
						<PlayerSubscriberAggregate />
						<ChatSubscriberAggregate />
					</ErrorBoundary>
					
					<ErrorBoundary>
						<PlayerBanDialog />
						<PlayerKickDialog />
						<PlayerWhisperDialog />
					</ErrorBoundary>
					
					<ErrorBoundary>
						<AppSwitcher />
					</ErrorBoundary>
				</Grid>
			</Grid>
		</Router>
	</ThemeProvider>

}

const App = hot(Application);

export default App;
