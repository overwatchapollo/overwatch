import { User } from 'api/users/User';

export const storeUser = 'STORE_USER';

export const clearSession = 'CLEAR_SESSION';

export interface StoreUserAction {
    type: typeof storeUser;
    user: User;
}

export interface ClearSessionAction {
    type: typeof clearSession;
}

export function ClearSession(): SessionActions {
    return {
        type: clearSession
    }
}

export function StoreUser(user: User): SessionActions {
    return {
        user,
        type: storeUser
    }
}

export type SessionActions = StoreUserAction | ClearSessionAction;