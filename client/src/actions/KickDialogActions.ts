import { Player } from 'api/chat/ConnectedPlayer';

export const setKickPlayer = 'SET_KICK_PLAYER';

export interface SetKickPlayer {
    type: typeof setKickPlayer;
    player: Player | undefined;
    serverId: number | undefined;
}

export function SetKickPlayerInfo(player: Player | undefined, serverId: number | undefined): KickPlayerActions {
    return {
        player,
        serverId,
        type: setKickPlayer
    }
}

export type KickPlayerActions = SetKickPlayer;