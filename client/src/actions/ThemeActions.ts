export const toggleDarkMode = "SET_DARK_MODE";

export interface ToggleDarkModeAction {
    type: typeof toggleDarkMode;
    isDarkMode: boolean;
}

export function SetDarkMode(val: boolean): ThemeActions {
    return {
        type: toggleDarkMode,
        isDarkMode: val,
    }
}

export type ThemeActions = ToggleDarkModeAction;