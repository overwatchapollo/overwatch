import { Player } from 'api/chat/chat';

export const setPlayerLog = "SET_PLAYER_LOG";
export const addPlayerLog = "ADD_PLAYER_LOG";
export const updatePlayerLog = "UPDATE_PLAYER_LOG";

export interface SetPlayerLogAction {
    type: typeof setPlayerLog;
    serverId: number;
    players: Array<Player>;
}

export interface UpdatePlayerLogAction {
    type: typeof updatePlayerLog;
    serverId: number;
    player: Player;
}

export function SetPlayerLog(serverId: number, players: Array<Player>): PlayerCacheActions {
    return {
        serverId,
        players,
        type: setPlayerLog
    }
}

export function UpdatePlayerLog(serverId: number, player: Player): PlayerCacheActions {
    return {
        serverId,
        player,
        type: updatePlayerLog
    }
}

export type PlayerCacheActions = SetPlayerLogAction | UpdatePlayerLogAction;