import { Player } from 'api/chat/ConnectedPlayer';

export const setWhisperPlayer = 'SET_WHISPER_PLAYER';

export interface SetWhisperPlayer {
    type: typeof setWhisperPlayer;
    player: Player | undefined;
    serverId: number | undefined;
}

export function SetWhisperPlayerInfo(player: Player | undefined, serverId: number | undefined): WhisperPlayerActions {
    return {
        player,
        serverId,
        type: setWhisperPlayer
    }
}

export type WhisperPlayerActions = SetWhisperPlayer;