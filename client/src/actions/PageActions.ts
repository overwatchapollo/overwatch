export const setPages = 'SET_PAGE';
export const clearPages = 'CLEAR_PAGE';

export interface SetPagesActions {
    type: typeof setPages;
    pages: Array<string>;
}

export interface ClearPagesActions {
    type: typeof clearPages;
}

export function SetPages(pages: Array<string>): PageActions {
    return {
        pages,
        type: setPages
    }
}

export function ClearPages(): PageActions {
    return {
        type: clearPages
    }
}

export type PageActions = SetPagesActions | ClearPagesActions;