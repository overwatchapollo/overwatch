import { Player } from 'api/chat/ConnectedPlayer';

export const setBanPlayer = 'SET_BAN_PLAYER';

export interface SetBanPlayer {
    type: typeof setBanPlayer;
    player: Player | undefined;
    serverId: number | undefined;
}

export function SetBanPlayerInfo(player: Player | undefined, serverId: number | undefined): BanPlayerActions {
    return {
        player,
        serverId,
        type: setBanPlayer
    }
}

export type BanPlayerActions = SetBanPlayer;