import { ServerStatusUpdate } from 'api/server/ServerStatusUpdate';
import { ServerWrapper } from 'api/server/ServerWrapper';

export const addServer = 'ADD_SERVER';
export const connectionUpdate = 'CONNECTION_UPDATE';
export const deleteServer = 'REMOVE_SERVER'

export interface ConnectionUpdate {
    type: typeof connectionUpdate;
    serverId: number;
    status: ServerStatusUpdate;
}

export interface AddServerAction {
    type: typeof addServer;
    config: ServerWrapper;
}

export interface RemoveServerAction {
    type: typeof deleteServer;
    id: number;
}

export function ConnectionUpdate(serverId: number, status: ServerStatusUpdate): ServerConfigurationActions {
    return {
        serverId,
        status,
        type: connectionUpdate
    }
}

export function AddServer(config: ServerWrapper): ServerConfigurationActions {
    return {
        config,
        type: addServer
    }
}

export function RemoveServer(id: number): ServerConfigurationActions {
    return {
        id,
        type: deleteServer
    }
}

export type ServerConfigurationActions = AddServerAction | ConnectionUpdate | RemoveServerAction;