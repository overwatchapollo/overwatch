import { ChatMessage } from 'api/chat/ChatMessage';

export const setChatLog = "SET_CHAT_LOG";
export const updateChatLog = "UPDATE_CHAT_LOG";
export const clearChatLog = "CLEAR_CHAT_LOG";

export interface SetChatLogAction {
    type: typeof setChatLog;
    serverId: number;
    logs: Array<ChatMessage>;
}

export interface UpdateChatLogAction {
    type: typeof updateChatLog;
    serverId: number;
    log: ChatMessage;
}

export interface ClearChatLogAction {
    type: typeof clearChatLog;
    serverId: number;
}

export function SetChatLog(serverId: number, logs: Array<ChatMessage>): ChatCacheActions {
    return {
        serverId,
        logs,
        type: setChatLog
    }
}

export function UpdateChatLog(serverId: number, message: ChatMessage): ChatCacheActions {
    return {
        serverId,
        log: message,
        type: updateChatLog,
    }
}

export function ClearChatLog(serverId: number): ChatCacheActions {
    return {
        serverId,
        type: clearChatLog
    }
}

export type ChatCacheActions = SetChatLogAction | UpdateChatLogAction | ClearChatLogAction;