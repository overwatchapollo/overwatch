import { PageActions } from 'actions/PageActions'
import { Reducer } from 'redux'

export interface PageState {
    pages: Array<string>;
}

const initialState: PageState = {
    pages: new Array(0),
}

export const PageReducer: Reducer<PageState, PageActions> = (state = initialState, action: PageActions) => {
    switch (action.type) {
        case "SET_PAGE":
            return Object.assign({}, state, {pages: action.pages});
        case "CLEAR_PAGE":
            return Object.assign({}, state, {pages: new Array(0)});
        default:
            return state;
    }
}