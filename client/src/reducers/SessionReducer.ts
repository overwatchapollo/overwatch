import { User } from 'api/users/User';
import { SessionActions } from 'actions/SessionActions';
import { Reducer } from 'redux';

export interface SessionState {
    activeUser: User | undefined;
}

const initialState: SessionState = {
    activeUser: undefined
}

export const SessionReducer: Reducer<SessionState, SessionActions> = (state = initialState, action: SessionActions) => {
    switch(action.type) {
        case "CLEAR_SESSION":
            return Object.assign({}, state, {activeUser: undefined});
        case "STORE_USER":
            return Object.assign({}, state, {activeUser: action.user})
        default:
            return state;
    }
}