import { Player } from 'api/chat/chat';
import { Reducer } from 'redux';
import { BanPlayerActions } from 'actions/BanDialogActions';

export interface BanDialogState {
    serverId: number | undefined;
    player: Player | undefined;
}

const initialState: BanDialogState = {
    serverId: undefined,
    player: undefined
}

export const BanDialogReducer: Reducer<BanDialogState, BanPlayerActions> = (state = initialState, action: BanPlayerActions) => {
    switch (action.type) {
        case "SET_BAN_PLAYER":
            return Object.assign({}, state, { serverId: action.serverId, player: action.player});
        default:
            return state;
    }
}