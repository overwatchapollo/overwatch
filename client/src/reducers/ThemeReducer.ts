import { Reducer } from 'redux'
import { ThemeActions } from 'actions/ThemeActions'
import { Theme, createMuiTheme } from '@material-ui/core';

export interface ThemeState {
    darkMode: boolean;
    theme: Theme;
}

const initialDarkMode = localStorage.getItem("darkMode") === 'true';

function createTheme(darkMode: boolean) {
    const theme = createMuiTheme({
        palette: {
          type: darkMode ? 'dark' : 'light',
          primary: {
            main: darkMode ? '#0071b2' : '#56b3e9',
            contrastText: darkMode ? '#FFFFFF' : '#000000',
          },
        },
      });
      return theme;
}

const initialState: ThemeState = {
    darkMode: initialDarkMode,
    theme: createTheme(initialDarkMode)
}

export const ThemeReducer: Reducer<ThemeState, ThemeActions> = (state = initialState, action: ThemeActions) => {
    switch(action.type) {
        case "SET_DARK_MODE":
            localStorage.setItem("darkMode", action.isDarkMode ? 'true' : 'false');
            return Object.assign({}, state, {darkMode: action.isDarkMode, theme: createTheme(action.isDarkMode)});
        default:
            return state;
    }
}