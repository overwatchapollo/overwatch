import { Player } from 'api/chat/chat';
import { Reducer } from 'redux';
import { PlayerCacheActions } from 'actions/PlayerCacheActions';

export interface PlayerCacheState {
    serverMap: Map<number, Map<number, Player>>;
}

const initalState: PlayerCacheState = {
    serverMap: new Map(),
}

export const PlayerCacheReducer: Reducer<PlayerCacheState, PlayerCacheActions> = (state = initalState, action: PlayerCacheActions) => {
    switch(action.type) {
        case "SET_PLAYER_LOG": {
            const newMap = new Map<number, Map<number, Player>>();
            state.serverMap.forEach((value, key) => newMap.set(key, value));
            const childMap = new Map<number, Player>();
            action.players.forEach(x => childMap.set(x.sessionId, x));
            newMap.set(action.serverId, childMap);
            return Object.assign({}, state, { serverMap: newMap })
        }
        case "UPDATE_PLAYER_LOG": {
            const childMap = new Map<number, Player>();
            const existing = state.serverMap.get(action.serverId);
            if (existing !== undefined) {
                const entries = Array.from(existing.entries());
                entries.forEach(x => childMap.set(x[0], x[1]));
            }
            childMap.set(action.serverId, action.player);

            const newMap = new Map<number, Map<number, Player>>();
            const existingMap = Array.from(state.serverMap.entries());
            existingMap.forEach(x => newMap.set(x[0], x[1]));
            newMap.set(action.serverId, childMap);

            return Object.assign({}, state, {serverMap: newMap});
        }
        default: {
            return state;
        }
    }
}