import { ChatMessage } from 'api/chat/chat';
import { ChatCacheActions } from 'actions/ChatCacheActions';
import { Reducer } from 'redux';

export interface ChatCacheState {
    serverMap: Map<number, Map<number, ChatMessage>>;
    cacheDepth: number;
}

const initalState: ChatCacheState = {
    serverMap: new Map(),
    cacheDepth: 100
}

export const ChatCacheReducer: Reducer<ChatCacheState, ChatCacheActions> = (state = initalState, action: ChatCacheActions) => {
    switch (action.type) {
        case "SET_CHAT_LOG": {
            const newMap = new Map<number, Map<number, ChatMessage>>();
            state.serverMap.forEach((value, key) => newMap.set(key, value));
            const childMap = new Map<number, ChatMessage>();
            action.logs.forEach(x => childMap.set(x.messageId, x));
            newMap.set(action.serverId, childMap);
            return Object.assign({}, state, { serverMap: newMap })
        }
        case "UPDATE_CHAT_LOG": {
            const newMap = new Map<number, Map<number, ChatMessage>>();
            state.serverMap.forEach((value, key) => newMap.set(key, value));
            const existingChatLog = newMap.get(action.serverId);

            if (existingChatLog) {
                if (existingChatLog.has(action.log.messageId)) {
                    return state;
                } else {
                    const keys = Array.from(existingChatLog.keys()).sort((a,b) => a < b ? 1 : -1).slice(0, state.cacheDepth - 1);
                    const newChatLog = new Map<number, ChatMessage>();
                    keys.forEach(key => {
                        const extractedValue = existingChatLog.get(key);
                        if(extractedValue) {
                            newChatLog.set(key, extractedValue);
                        }
                    });
                    newChatLog.set(action.log.messageId, action.log)
                    newMap.set(action.serverId, newChatLog);
                }
            } else {
                const newBuild = new Map<number, ChatMessage>();
                newBuild.set(action.log.messageId, action.log);
                newMap.set(action.serverId, newBuild);
            }

            return Object.assign({}, state, { serverMap: newMap });
        }
        case "CLEAR_CHAT_LOG": {
            const newMap = new Map<number, Map<number, ChatMessage>>();
            state.serverMap.forEach((value, key) => newMap.set(key, value));
            newMap.set(action.serverId, new Map());
            return Object.assign({}, state, { serverMap: newMap });
        }
        default:
            return state;
    }
}