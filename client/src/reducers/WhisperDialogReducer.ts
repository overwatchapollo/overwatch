import { Player } from 'api/chat/chat';
import { Reducer } from 'redux';
import { WhisperPlayerActions } from 'actions/WhisperDialogActions';

export interface WhisperDialogState {
    serverId: number | undefined;
    player: Player | undefined;
}

const initialState: WhisperDialogState = {
    serverId: undefined,
    player: undefined
}

export const WhisperDialogReducer: Reducer<WhisperDialogState, WhisperPlayerActions> = (state = initialState, action: WhisperPlayerActions) => {
    switch (action.type) {
        case "SET_WHISPER_PLAYER":
            return Object.assign({}, state, { serverId: action.serverId, player: action.player});
        default:
            return state;
    }
}