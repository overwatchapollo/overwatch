import { Player } from 'api/chat/chat';
import { Reducer } from 'redux';
import { KickPlayerActions } from 'actions/KickDialogActions';

export interface KickDialogState {
    serverId: number | undefined;
    player: Player | undefined;
}

const initialState: KickDialogState = {
    serverId: undefined,
    player: undefined
}

export const KickDialogReducer: Reducer<KickDialogState, KickPlayerActions> = (state = initialState, action: KickPlayerActions) => {
    switch (action.type) {
        case "SET_KICK_PLAYER":
            return Object.assign({}, state, { serverId: action.serverId, player: action.player});
        default:
            return state;
    }
}