import {combineReducers} from 'redux';
import { ServerConfigurationState, ServerConfigurationReducer } from './ServerConfigurationReducer';
import { SessionState, SessionReducer } from './SessionReducer';
import { ThemeState, ThemeReducer } from './ThemeReducer';
import { KickDialogState, KickDialogReducer } from './KickDialogReducer';
import { BanDialogState, BanDialogReducer } from './BanDialogReducer';
import { WhisperDialogReducer, WhisperDialogState } from './WhisperDialogReducer';
import { ChatCacheState, ChatCacheReducer } from './ChatCacheReducer';
import { PageState, PageReducer } from './PageReducer';
import { PlayerCacheState, PlayerCacheReducer } from './PlayerCacheReducer';

export interface RootState {
    servers: ServerConfigurationState;
    session: SessionState;
    theme: ThemeState;
    kickState: KickDialogState;
    banState: BanDialogState;
    whisperState: WhisperDialogState;
    chatCacheState: ChatCacheState;
    pageState: PageState;
    playerCacheState: PlayerCacheState;
}

export const rootReducer = combineReducers<RootState | undefined> ({
    servers: ServerConfigurationReducer,
    session: SessionReducer,
    theme: ThemeReducer,
    kickState: KickDialogReducer,
    banState: BanDialogReducer,
    whisperState: WhisperDialogReducer,
    chatCacheState: ChatCacheReducer,
    pageState: PageReducer,
    playerCacheState: PlayerCacheReducer
})