import { ServerConfiguration } from 'api/server/ServerConfiguration';
import { PermissionWrapper } from 'api/PermissionWrapper';
import { ServerConfigurationActions } from 'actions/ServerConfigurationActions';
import { Reducer } from 'redux';

export interface ServerConfigurationState {
    servers: Array<PermissionWrapper<ServerConfiguration>>;
}

const initialState: ServerConfigurationState = {
    servers: new Array(0),
}

export const ServerConfigurationReducer: Reducer<ServerConfigurationState, ServerConfigurationActions> = (state = initialState, action: ServerConfigurationActions) => {
    switch(action.type) {
        case "CONNECTION_UPDATE": {
            const server = state.servers.filter(x => x.data.serverId === action.serverId)[0];
            const newServer = Object.assign({}, server)
            if (newServer.data === undefined) {
                return state;
            }
            newServer.data.connected = action.status.state === 'CONNECTED';
            newServer.data.state = action.status;
            return Object.assign({}, state, {servers: [...state.servers.filter(x => x.data.serverId !== action.serverId), newServer]});
        }
        case "ADD_SERVER": {
            if (state.servers.filter(x => x.data.serverId === action.config.data.serverId).length !== 0) {
                return state;
            }
            action.config.data.state = {
                authenticated: false,
                connected: false,
                serverId: action.config.data.serverId,
                state: action.config.status
            }
            // action.config.data.state.state = action.config.status;
            return Object.assign({}, state, {servers: [...state.servers, action.config]})
        }
        case "REMOVE_SERVER": {
            return Object.assign({}, state, {servers: [...state.servers.filter(x => x.data.serverId !== action.id)]});
        }
        default:
            return state;
    }
}