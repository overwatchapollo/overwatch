import { MqttStreamer } from 'api/stream/stream';
import { ChatService } from 'api/chat/chat';
import { PlayerService } from 'api/player/PlayerService';
import { ServerConfigurationService } from 'api/server/ServerConfigurationService';

const sock = new MqttStreamer();
export const mqttStream = sock;
export const playerService = new PlayerService(sock);
export const chatService = new ChatService(sock);
export const serverConfigService = new ServerConfigurationService(sock);
