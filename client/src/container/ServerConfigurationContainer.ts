import { RootState } from 'reducers';
import { Dispatch } from 'redux';
import { ServerConfigurationActions, AddServer, ConnectionUpdate, RemoveServer } from 'actions/ServerConfigurationActions';
import { ServerStatusUpdate } from 'api/server/ServerStatusUpdate';
import { ServerWrapper } from 'api/server/ServerWrapper';

export const mapDispatchToProps = (dispatch: Dispatch<ServerConfigurationActions>) => ({
    addConfig: (conf: ServerWrapper) => dispatch(AddServer(conf)),
    removeConfig: (id: number) => dispatch(RemoveServer(id)),
    connectionUpdate: (serverId: number, status: ServerStatusUpdate) => dispatch(ConnectionUpdate(serverId, status))
})

export const mapStateToProps = (state:RootState) => ({
    servers: state.servers
})

export class ConfigurationDispatcher {
    private readonly dispatch: Dispatch<ServerConfigurationActions>;

    constructor(dispatch: Dispatch<ServerConfigurationActions>) {
        this.dispatch = dispatch;
    }

    addConfig = (conf: ServerWrapper) => this.dispatch(AddServer(conf));
    removeConfig = (id: number) => this.dispatch(RemoveServer(id));
    connectionUpdate = (serverId: number, status: ServerStatusUpdate) => this.dispatch(ConnectionUpdate(serverId, status));
}