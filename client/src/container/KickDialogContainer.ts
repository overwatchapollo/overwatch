import { Dispatch } from 'redux';
import { RootState } from 'reducers';
import { KickPlayerActions, SetKickPlayerInfo } from 'actions/KickDialogActions';
import { Player } from 'api/chat/ConnectedPlayer';

export const mapKickDialogDispatch = (dispatch: Dispatch<KickPlayerActions>) => ({
    setPlayerKickInfo: (player: Player, serverId: number) => dispatch(SetKickPlayerInfo(player, serverId)),
    clearPlayerKickInfo: () => dispatch(SetKickPlayerInfo(undefined, undefined))
})

export const mapKickDialogState = (state: RootState) => ({
    kickDialogState: state.kickState,
})

export class KickDispatcher {
    private readonly dispatch: Dispatch<KickPlayerActions>;

    constructor(dispatch: Dispatch<KickPlayerActions>) {
        this.dispatch = dispatch;
    }

    setKickPlayerInfo = (player: Player, serverId: number) => this.dispatch(SetKickPlayerInfo(player, serverId));
    clearPlayerKickInfo = () => this.dispatch(SetKickPlayerInfo(undefined, undefined));
}