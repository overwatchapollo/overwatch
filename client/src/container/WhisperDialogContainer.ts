import { Dispatch } from 'redux';
import { RootState } from 'reducers';
import { WhisperPlayerActions, SetWhisperPlayerInfo } from 'actions/WhisperDialogActions';
import { Player } from 'api/chat/ConnectedPlayer';

export const mapWhisperDialogDispatch = (dispatch: Dispatch<WhisperPlayerActions>) => ({
    setWhisperPlayerInfo: (player: Player, serverId: number) => dispatch(SetWhisperPlayerInfo(player, serverId)),
    clearPlayerWhisperInfo: () => dispatch(SetWhisperPlayerInfo(undefined, undefined))
})

export const mapWhisperDialogState = (state: RootState) => ({
    whisperDialogState: state.whisperState,
})

export class WhisperDispatcher {
    private readonly dispatch: Dispatch<WhisperPlayerActions>;

    constructor(dispatch: Dispatch<WhisperPlayerActions>) {
        this.dispatch = dispatch;
    }

    setWhisperPlayerInfo = (player: Player, serverId: number) => this.dispatch(SetWhisperPlayerInfo(player, serverId))
    clearPlayerWhisperInfo = () => this.dispatch(SetWhisperPlayerInfo(undefined, undefined))
}