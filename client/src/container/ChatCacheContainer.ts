import { Dispatch } from 'redux';
import { ChatCacheActions, SetChatLog, UpdateChatLog, ClearChatLog } from 'actions/ChatCacheActions';
import { ChatMessage } from 'api/chat/ChatMessage';
import { RootState } from 'reducers';

export const mapChatCacheDispatch = (dispatch: Dispatch<ChatCacheActions>) => ({
    setChatLog:(serverId: number, logs: Array<ChatMessage>) => dispatch(SetChatLog(serverId, logs)),
    updateChatLog: (serverId: number, message: ChatMessage) => dispatch(UpdateChatLog(serverId, message)),
    clearChatLog: (serverId: number) => dispatch(ClearChatLog(serverId))
})

export const mapChatCacheProps = (state: RootState) => ({
    chatCache: state.chatCacheState
})

export class ChatDispatcher {
    private readonly dispatch:Dispatch<ChatCacheActions>;
    constructor(dispatch: Dispatch<ChatCacheActions>)  {
        this.dispatch = dispatch;
    }
    setChatLog = (serverId: number, logs: Array<ChatMessage>) => this.dispatch(SetChatLog(serverId, logs));
    updateChatLog = (serverId: number, message: ChatMessage) => this.dispatch(UpdateChatLog(serverId, message));
    clearChatLog = (serverId: number) => this.dispatch(ClearChatLog(serverId));
}