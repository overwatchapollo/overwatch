import { PlayerCacheActions, SetPlayerLog, UpdatePlayerLog } from 'actions/PlayerCacheActions';
import { Dispatch } from 'redux';
import { Player } from 'api/chat/chat';
import { RootState } from 'reducers';

export const mapPlayerCacheDispatch = (dispatch: Dispatch<PlayerCacheActions>) => ({
    setPlayerLog: (serverId: number, players: Array<Player>) => dispatch(SetPlayerLog(serverId, players)),
    updatePLayerLog: (serverId: number, player: Player) => dispatch(UpdatePlayerLog(serverId, player))
})

export const mapPlayerCacheProps = (state: RootState) => ({
    playerCache: state.playerCacheState
})

export class PlayerDispatcher {
    private readonly dispatch: Dispatch<PlayerCacheActions>;

    constructor(dispatch: Dispatch<PlayerCacheActions>) {
        this.dispatch = dispatch;
    }

    setPlayerLog = (serverId: number, players: Array<Player>) => this.dispatch(SetPlayerLog(serverId, players));
    updatePlayerLog = (serverId: number, player: Player) => this.dispatch(UpdatePlayerLog(serverId, player));
}