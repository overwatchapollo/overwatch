import { Dispatch } from 'redux';
import { RootState } from 'reducers';
import { BanPlayerActions, SetBanPlayerInfo } from 'actions/BanDialogActions';
import { Player } from 'api/chat/ConnectedPlayer';

export const mapBanDialogDispatch = (dispatch: Dispatch<BanPlayerActions>) => ({
    setBanPlayerInfo: (player: Player, serverId: number) => dispatch(SetBanPlayerInfo(player, serverId)),
    clearPlayerBanInfo: () => dispatch(SetBanPlayerInfo(undefined, undefined))
})

export const mapBanDialogState = (state: RootState) => ({
    banDialogState: state.banState,
})

export class BanDispatcher {
    private readonly dispatch: Dispatch<BanPlayerActions>;

    constructor(dispatch: Dispatch<BanPlayerActions>) {
        this.dispatch = dispatch;
    }

    setBanPlayerInfo = (player: Player, serverId: number) => this.dispatch(SetBanPlayerInfo(player, serverId));
    clearPlayerBanInfo = () => this.dispatch(SetBanPlayerInfo(undefined, undefined));
}