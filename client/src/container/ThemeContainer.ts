import { ThemeActions, SetDarkMode } from 'actions/ThemeActions';
import { Dispatch } from 'redux';
import { RootState } from 'reducers';

export const mapThemeDispatchToProps = (dispatch: Dispatch<ThemeActions>) => ({
    SetDarkMode: (val: boolean) => dispatch(SetDarkMode(val))
})

export const mapThemeStateToProps = (state:RootState) => ({
    themeState: state.theme
})