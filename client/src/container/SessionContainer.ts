import { SessionActions, ClearSession, StoreUser } from 'actions/SessionActions';
import { Dispatch } from 'redux';
import { RootState } from 'reducers';
import { User } from 'api/users/User';

export const mapSessionDispatchToProps = (dispatch: Dispatch<SessionActions>) => ({
    clearSession: () => dispatch(ClearSession()),
    setUser: (user: User) => dispatch(StoreUser(user)),
})

export const mapSessionStateToProps = (state: RootState) => ({
    sessionState: state.session
})