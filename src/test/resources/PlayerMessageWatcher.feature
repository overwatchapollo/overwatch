
Feature: Listens for player messages coming from RCON.
    Scenario: The user connects to the server.
        Given A setup player message watcher
        When [Sfc.] PaulRPG joins the server from 127.0.0.1:2039 in session slot 44
        Then Message was sent
        Then Message name is [Sfc.] PaulRPG
        Then Player is online
        Then Message slot is 44
    
    Scenario: The users GUID is verified by the server as authentic.
        Given A setup player message watcher
        When The GUID of [Sfc.] PaulRPG in session slot 101 is verified as 1f969977ccb49930a41fe3b0f70753e4
        Then Message was sent
        Then Message GUID is 1f969977ccb49930a41fe3b0f70753e4
        Then Message name is [Sfc.] PaulRPG
        Then Player is online
        Then Message slot is 101

    Scenario: The user disconnects from the server.
        Given A setup player message watcher
        When [Sfc.] PaulRPG disconnects from slot 101
        Then Message was sent
        Then Message name is [Sfc.] PaulRPG
        Then Player is offline
        Then Message slot is 101

    Scenario: The user has been kicked from the server.
        Given A setup player message watcher
        When [Sfc.] PaulRPG with GUID 1f969977ccb49930a41fe3b0f70753e4 is kicked from slot 44
        Then Message was sent
        Then Message GUID is 1f969977ccb49930a41fe3b0f70753e4
        Then Message name is [Sfc.] PaulRPG
        Then Player is offline
        Then Message slot is 44

    Scenario: The user has been banned from the server.
        Given A setup player message watcher
        When [Sfc.] PaulRPG with GUID 1f969977ccb49930a41fe3b0f70753e4 is banned from slot 44
        Then Message was sent
        Then Message GUID is 1f969977ccb49930a41fe3b0f70753e4
        Then Message name is [Sfc.] PaulRPG
        Then Player is offline
        Then Message slot is 44