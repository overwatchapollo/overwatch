package com.jsoc.overwatch.Security.permissions;

import org.junit.Test;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
public class ServerPermissionsGeneratorTest {

    private ServerPermissionGenerator generator = new ServerPermissionGenerator();
    private User mockUser = mock(User.class);
    @Test
    public void canMatchViewPermission() {
        Permission permission = this.generator.view(1L);
        when(this.mockUser.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("server:1:view")));
        assertTrue(permission.hasPermission(this.mockUser));

        when(this.mockUser.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("server:2:view")));
        assertFalse(permission.hasPermission(this.mockUser));

        when(this.mockUser.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("server:(\\d+):view")));
        assertTrue(permission.hasPermission(this.mockUser));

        when(this.mockUser.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("server:1:(.+)")));
        assertTrue(permission.hasPermission(this.mockUser));

        when(this.mockUser.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("server:(\\d+):(.+)")));
        assertTrue(permission.hasPermission(this.mockUser));
    }

    @Test
    public void canMatchUnregister() {
        Permission permission = this.generator.unregister(1L);
        when(this.mockUser.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("server:1:unregister")));
        assertTrue(permission.hasPermission(this.mockUser));

        when(this.mockUser.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("server:2:unregister")));
        assertFalse(permission.hasPermission(this.mockUser));

        when(this.mockUser.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("server:(\\d+):unregister")));
        assertTrue(permission.hasPermission(this.mockUser));

        when(this.mockUser.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("server:1:(.+)")));
        assertTrue(permission.hasPermission(this.mockUser));

        when(this.mockUser.getAuthorities()).thenReturn(Collections.singletonList(new SimpleGrantedAuthority("server:(\\d+):(.+)")));
        assertTrue(permission.hasPermission(this.mockUser));
    }
}
