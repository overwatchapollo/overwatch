package com.jsoc.overwatch.arma.services;

import com.jsoc.overwatch.services.connection.CommandService;
import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import com.jsoc.overwatch.services.playerList.events.PlayerChatEvent;
import com.jsoc.overwatch.services.playerList.events.PlayerChatEventType;
import com.jsoc.overwatch.services.playerList.events.PlayerChatKick;
import com.jsoc.overwatch.services.kickService.events.KickRequestEvent;
import com.jsoc.overwatch.services.kickService.events.PlayerWasKickedEvent;
import com.jsoc.overwatch.services.kickService.events.PlayerWasKickedInGameEvent;
import com.jsoc.overwatch.services.playerList.ConnectedPlayerService;
import com.jsoc.overwatch.services.connection.games.arma.IRconConnection;
import com.jsoc.overwatch.services.connection.RconConnectionPool;
import com.jsoc.overwatch.services.kickService.KickService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.context.ApplicationEventPublisher;

import java.util.Optional;

import static org.mockito.Mockito.*;

public class KickServiceTest {

    private RconConnectionPool pool = mock(RconConnectionPool.class);

    private ConnectedPlayerService playerList = mock(ConnectedPlayerService.class);

    private ApplicationEventPublisher publisher = mock(ApplicationEventPublisher.class);

    private CommandService commandService = mock(CommandService.class);

    private KickService kickService;

    @Before
    public void setup() {
        this.kickService = new KickService(this.playerList, this.publisher, this.commandService);
    }

    @Test
    public void canBuild() {
        Assert.assertNotEquals(null, this.kickService);
    }

    @Test
    public void canKickPlayerViaEvent() {
        ConnectedPlayer cp = new ConnectedPlayer();
        when(this.playerList.getPlayerFromSession(1L, "1")).thenReturn(Optional.of(cp));
        KickRequestEvent event = new KickRequestEvent(this, 1L, "1", 1L, "testKick");
        IRconConnection mockRcon = mock(IRconConnection.class);
        when(this.pool.getConnection(1L)).thenReturn(Optional.of(mockRcon));
        this.kickService.processKickEvent(event);
        ArgumentCaptor<PlayerWasKickedEvent> captor = ArgumentCaptor.forClass(PlayerWasKickedEvent.class);
        verify(this.publisher, times(1)).publishEvent(captor.capture());
        PlayerWasKickedEvent outputEvent = captor.getValue();
        Assert.assertEquals("testKick", outputEvent.getMessage());
    }

    @Test
    public void canDetectPlayerKickViaEvent() {
        ConnectedPlayer cp = new ConnectedPlayer();
        cp.setSessionId("1");
        cp.setGuid("guid");
        cp.setName("name");
        cp.setConnected(false);
        cp.setKicked(true);
        cp.setDisconnectionMessage("kicked");
        cp.setPing(100);
        cp.setIpAddress("172.0.0.1");

        PlayerChatEvent chatEvent = new PlayerChatEvent(this, PlayerChatEventType.KICKED, cp);
        PlayerChatKick kickEvent = new PlayerChatKick(this, chatEvent);
        this.kickService.processPlayerChatKick(kickEvent);
        ArgumentCaptor<PlayerWasKickedInGameEvent> captor = ArgumentCaptor.forClass(PlayerWasKickedInGameEvent.class);
    }
}
