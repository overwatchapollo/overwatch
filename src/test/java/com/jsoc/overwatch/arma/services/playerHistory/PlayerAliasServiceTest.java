package com.jsoc.overwatch.arma.services.playerHistory;

import com.jsoc.overwatch.services.playerHistory.PlayerAliasService;
import com.jsoc.overwatch.services.playerHistory.PlayerHistoryService;
import com.jsoc.overwatch.services.playerHistory.models.PlayerAlias;
import com.jsoc.overwatch.services.playerHistory.models.PlayerConnectionStatus;
import com.jsoc.overwatch.services.playerHistory.models.PlayerJoinHistory;
import com.jsoc.overwatch.services.playerHistory.models.PlayerRecord;
import com.jsoc.overwatch.services.playerHistory.repository.PlayerHistoryRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.time.Instant;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PlayerAliasServiceTest {

    private PlayerAliasService service;

    private PlayerHistoryService historyService = mock(PlayerHistoryService.class);

    private PlayerHistoryRepository repo = mock(PlayerHistoryRepository.class);

    @Before
    public void setup() {
        this.service = new PlayerAliasService(this.repo, this.historyService);
        when(this.historyService.getPlayerRecord(any(), Mockito.eq(5L))).thenReturn(Optional.empty());
    }

    @Test
    public void whenNoRecordThenReturnEmptySet() {
        Set<PlayerAlias> alias = this.service.getPlayerAlias(5L, "badGUid");
        assertEquals(0, alias.size());
    }

    @Test
    public void whenRecordExistsThenReturnAliasSet() {
        PlayerRecord record = new PlayerRecord(1L, 1L, "testGuid");
        this.setPlayerRecord(1L, "testGuid", record);

        PlayerJoinHistory history1 = new PlayerJoinHistory();
        history1.setRecord(record);
        history1.setName("timmy");
        history1.setStatus(PlayerConnectionStatus.CONNECTED);
        this.repo.save(history1);
        Set<PlayerJoinHistory> aliases = new HashSet<>();
        aliases.add(history1);

        this.setHistory(1L, aliases);

        Set<PlayerAlias> alias = this.service.getPlayerAlias(1L, "testGuid");
        assertEquals(1, alias.size());
    }

    @Test
    public void canUseAliasWithNullTimestamp() {
        PlayerRecord record = new PlayerRecord(1L, 1L, "testGuid");
        this.setPlayerRecord(1L, "testGuid", record);

        PlayerJoinHistory history1 = new PlayerJoinHistory();
        history1.setId(1L);
        history1.setRecord(record);
        history1.setName("timmy");
        history1.setStatus(PlayerConnectionStatus.CONNECTED);

        Set<PlayerJoinHistory> aliases = new HashSet<>();
        aliases.add(history1);
        this.setHistory(1L, aliases);

        Set<PlayerAlias> alias = this.service.getPlayerAlias(1L, "testGuid");
        assertEquals(1, alias.size());
        PlayerAlias pa = alias.iterator().next();
        assertEquals("timmy", pa.getName());
        assertEquals(1L, pa.getServerId(), 0);
    }

    @Test
    public void canCountMoreThanOneAliasInstance() {
        PlayerRecord record = new PlayerRecord(1L, 1L, "testGuid");
        this.setPlayerRecord(1L, "testGuid", record);

        PlayerJoinHistory history1 = new PlayerJoinHistory();
        history1.setId(1L);
        history1.setRecord(record);
        history1.setName("timmy");
        history1.setTimeStamp(Instant.parse("2018-01-01T12:00:00Z"));
        history1.setStatus(PlayerConnectionStatus.CONNECTED);

        PlayerJoinHistory history2 = new PlayerJoinHistory();
        history2.setId(2L);
        history2.setRecord(record);
        history2.setName("timmy");
        history2.setTimeStamp(Instant.parse("2018-01-01T12:00:00Z"));
        history2.setStatus(PlayerConnectionStatus.CONNECTED);


        Set<PlayerJoinHistory> aliases = new HashSet<>();
        aliases.add(history1);
        aliases.add(history2);

        this.setHistory(1L, aliases);

        Set<PlayerAlias> alias = this.service.getPlayerAlias(1L, "testGuid");
        assertEquals(1, alias.size());
        PlayerAlias pa = alias.iterator().next();
        assertEquals("timmy", pa.getName());
        assertEquals(1L, pa.getServerId(), 0);
        assertEquals(2, pa.getCount(), 0);
    }

    @Test
    public void onlyConnectedStatusEventsCounted() {
        PlayerRecord record = new PlayerRecord(1L, 1L, "testGuid");
        this.setPlayerRecord(1L, "testGuid", record);

        PlayerJoinHistory history1 = new PlayerJoinHistory();
        history1.setRecord(record);
        history1.setName("timmy");
        history1.setStatus(PlayerConnectionStatus.CONNECTED);

        PlayerJoinHistory history2 = new PlayerJoinHistory();
        history2.setRecord(record);
        history2.setName("timmy");
        history2.setStatus(PlayerConnectionStatus.DISCONNECTED);


        Set<PlayerJoinHistory> aliases = new HashSet<>();
        aliases.add(history1);
        aliases.add(history2);

        this.setHistory(1L, aliases);

        Set<PlayerAlias> alias = this.service.getPlayerAlias(1L, "testGuid");
        assertEquals(1, alias.size());

        PlayerAlias pa = alias.iterator().next();
        assertEquals("timmy", pa.getName());
        assertEquals(1L, pa.getServerId(), 0);
        assertEquals(1, pa.getCount(), 0);
    }

    @Test
    public void canCountMultipleAliasNames() {
        PlayerRecord record = new PlayerRecord(1L, 1L, "testGuid");
        this.setPlayerRecord(1L, "testGuid", record);

        PlayerJoinHistory history1 = new PlayerJoinHistory();
        history1.setRecord(record);
        history1.setName("timmy");
        history1.setStatus(PlayerConnectionStatus.CONNECTED);

        PlayerJoinHistory history2 = new PlayerJoinHistory();
        history2.setRecord(record);
        history2.setName("frank");
        history2.setStatus(PlayerConnectionStatus.CONNECTED);


        Set<PlayerJoinHistory> aliases = new HashSet<>();
        aliases.add(history1);
        aliases.add(history2);

        this.setHistory(1L, aliases);

        Set<PlayerAlias> alias = this.service.getPlayerAlias(1L, "testGuid");

        assertEquals(2, alias.size());

        PlayerAlias aliasTimmy = alias.stream().filter(x -> Objects.equals(x.getName(), "timmy")).findFirst().orElseThrow(RuntimeException::new);

        PlayerAlias aliasFrank = alias.stream().filter(x -> Objects.equals(x.getName(), "frank")).findFirst().orElseThrow(RuntimeException::new);

        assertNotNull(aliasTimmy);
        assertNotNull(aliasFrank);
    }

    private void setPlayerRecord(Long serverId, String guid, PlayerRecord record) {
        when(this.historyService.getPlayerRecord(guid, serverId)).thenReturn(Optional.ofNullable(record));
    }

    private void setHistory(Long serverId, Set<PlayerJoinHistory> history) {
        when(this.repo.findByRecordId(serverId)).thenReturn(history);
    }
}
