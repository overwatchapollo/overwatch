package com.jsoc.overwatch.arma.services;

import com.jsoc.overwatch.services.chatService.ChatMessage;
import com.jsoc.overwatch.services.chatService.HelpRequest;
import com.jsoc.overwatch.services.chatService.HelpRequestService;
import com.jsoc.overwatch.services.chatService.events.ChatMessageReceived;
import com.jsoc.overwatch.services.chatService.model.HelpRequestRecord;
import com.jsoc.overwatch.services.chatService.repository.HelpRequestRepo;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.context.ApplicationEventPublisher;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class HelpRequestServiceTest {

    private ApplicationEventPublisher publisher = mock(ApplicationEventPublisher.class);
    private HelpRequestRepo repo = mock(HelpRequestRepo.class);

    private HelpRequestService service = new HelpRequestService(this.publisher, this.repo);

    @Test
    public void canStoreEventsInRepository() {
        ChatMessage cm = ChatMessage.builder()
            .channel("group")
            .isRcon(false)
            .message("!support help me")
            .sender("timmy")
            .timeStamp(Instant.parse("2019-06-06T00:00:00Z"))
            .build();
        ChatMessageReceived cmr = new ChatMessageReceived(this, cm);
        this.service.handleMessage(cmr);
        ArgumentCaptor captor = ArgumentCaptor.forClass(HelpRequestRecord.class);
        verify(this.repo, times(1)).save((HelpRequestRecord)captor.capture());
        assertNotNull(captor.getValue());
        HelpRequestRecord record = (HelpRequestRecord)captor.getValue();
        assertNotNull(record);
        assertEquals("help me", record.getHelpMessage());
        assertEquals("timmy", record.getSender());
    }

    @Test
    public void canGetEvents() {
        ChatMessage cm = ChatMessage.builder()
                .channel("group")
                .isRcon(false)
                .message("!support help me")
                .sender("timmy")
                .timeStamp(Instant.parse("2019-06-06T00:00:00Z"))
                .build();
        HelpRequest hr = HelpRequest.builder()
                .helpRequest("help me")
                .originalMessage(cm)
                .resolved(false)
                .build();
        HelpRequestRecord record = new HelpRequestRecord(hr);
        when(this.repo.findAll()).thenReturn(Arrays.asList(record));
        List<HelpRequestRecord> records = this.service.getEvents();
        assertEquals(1, records.size());
    }
}
