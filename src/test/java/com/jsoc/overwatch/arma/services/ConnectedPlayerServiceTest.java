package com.jsoc.overwatch.arma.services;

import com.jsoc.overwatch.services.connection.CommandService;
import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import com.jsoc.overwatch.services.connection.events.ServerStateUpdateEvent;
import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import com.jsoc.overwatch.services.playerList.ConnectedPlayerService;
import com.jsoc.overwatch.services.connection.games.arma.IRconConnection;
import com.jsoc.overwatch.services.connection.RconConnectionPool;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import com.jsoc.overwatch.services.playerReport.PlayerReportService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.context.ApplicationEventPublisher;

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ConnectedPlayerServiceTest {

    private final PlayerReportService reportService = mock(PlayerReportService.class);

    private final ApplicationEventPublisher publisher = mock(ApplicationEventPublisher.class);

    private final RconConnectionPool pool = mock(RconConnectionPool.class);

    private ConnectedPlayerService playerList;

    @Before
    public void setup() {
        CommandService cs = new CommandService(this.pool);
        this.playerList  = new ConnectedPlayerService(cs, this.publisher);
    }

    @Test
    public void canRefreshStalePlayerList() {
        IRconConnection rconConnection1 = mock(IRconConnection.class);
        CommandResponse cr = new CommandResponse();
        cr.setSuccess(true);
        when(rconConnection1.sendCommand(any())).thenReturn(CompletableFuture.completedFuture(cr));
        when(this.pool.getConnection(eq(1L))).thenReturn(Optional.of(rconConnection1));
        doAnswer(invocation -> {
            StringBuilder builder = new StringBuilder();
            builder.append("blah \n");
            builder.append("blah \n");
            builder.append("191 82.29.130.23:2304     187  93efb56514e53ece8083e25ea6585b49(OK) PFC.McMilan\n");
            builder.append("192 82.29.130.24:2304     186  93efb56514e53ece8083e25ea6585b48(OK) PFC.tIM\n");
            builder.append("(2 players in total)");
            cr.setResponse(builder.toString());
            return true;
        }).when(rconConnection1).sendCommand(any());
    }

    @Test
    @Ignore
    public void canLoadPlayerTest() throws Exception {
        IRconConnection rconConnection1 = mock(IRconConnection.class);
        CommandResponse cr = new CommandResponse();
        cr.setSuccess(true);
        when(rconConnection1.sendCommand(any())).thenReturn(CompletableFuture.completedFuture(cr));
        when(this.pool.getConnection(eq(1L))).thenReturn(Optional.of(rconConnection1));

        doAnswer(invocation -> {
            StringBuilder builder = new StringBuilder();
            builder.append("blah \n");
            builder.append("blah \n");
            builder.append("191 82.29.130.23:2304     187  93efb56514e53ece8083e25ea6585b49(OK) PFC.McMilan\n");
            builder.append("192 82.29.130.24:2304     186  93efb56514e53ece8083e25ea6585b48(OK) PFC.tIM\n");
            builder.append("(2 players in total)");
            cr.setResponse(builder.toString());
            return true;
        }).when(rconConnection1).sendCommand(any());

        IRconConnection rconConnection2 = mock(IRconConnection.class);
        CommandResponse cr2 = new CommandResponse();
        cr.setSuccess(true);
        when(rconConnection2.sendCommand(any())).thenReturn(CompletableFuture.completedFuture(cr2));

        when(this.pool.getConnection(eq(2L))).thenReturn(Optional.of(rconConnection2));
        doAnswer(invocation -> {
            StringBuilder builder = new StringBuilder();
            builder.append("blah \n");
            builder.append("blah \n");
            builder.append("191 82.29.130.23:2304     187  93efb56514e53ece8083e25ea6585b49(OK) PFC.McMilan\n");
            builder.append("(1 players in total)");
            cr.setResponse(builder.toString());
            return true;
        }).when(rconConnection2).sendCommand(any());

        this.playerList.eventListener(new ServerStateUpdateEvent(this, 1L, "", ServerConnectionStatus.CONNECTED));
        Collection<ConnectedPlayer> players = this.playerList.getPlayers(1L);
        assertEquals(2, players.size());

        this.playerList.eventListener(new ServerStateUpdateEvent(this, 2L, "", ServerConnectionStatus.CONNECTED));
        Collection<ConnectedPlayer> players2 = this.playerList.getPlayers(2L);
        assertEquals(1, players2.size());
    }


}
