package com.jsoc.overwatch.arma.services.PlayerManagement;

import com.google.common.collect.Iterables;
import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import com.jsoc.overwatch.services.playerList.events.PlayerChatEvent;
import com.jsoc.overwatch.services.playerList.events.PlayerChatEventType;
import com.jsoc.overwatch.services.playerList.PlayerList;
import io.reactivex.disposables.Disposable;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Slf4j
public class PlayerListTest {

    private PlayerList playerList;

    @Before
    public void setup() {
        this.playerList = new PlayerList(1L);
    }

    @Test
    public void canBeBuilt() {
        Assert.assertNotEquals(null, this.playerList);
    }

    @Test
    public void connectedPlayerCanBeQueried() throws Exception {
        PlayerChatEventType type = PlayerChatEventType.CONNECTED;
        ConnectedPlayer cp = new ConnectedPlayer();
        cp.setName("timmy");
        cp.setSessionId("1");

        PlayerChatEvent event = new PlayerChatEvent(this, type, cp);
        this.playerList.onPlayerChatEvent(event);
        Collection<ConnectedPlayer> players = this.playerList.getPlayers();
        Assert.assertEquals(1, players.size());
        Assert.assertEquals("timmy", players.iterator().next().getName());
    }

    @Test
    public void messageOutOfOrderDoesntLoseData() throws Exception {
        PlayerChatEventType type = PlayerChatEventType.CONNECTED;
        ConnectedPlayer cp = new ConnectedPlayer();
        cp.setName("timmy");
        cp.setSessionId("1");
        cp.setIpAddress("172.0.0.1");
        PlayerChatEvent connectEvent = new PlayerChatEvent(this, type, cp);

        ConnectedPlayer cpGuid = new ConnectedPlayer();
        cpGuid.setName("timmy");
        cpGuid.setSessionId("1");
        cpGuid.setGuid("GUID");
        PlayerChatEvent guidEvent = new PlayerChatEvent(this, PlayerChatEventType.GUID_VERIFIED, cpGuid);

        this.playerList.onPlayerChatEvent(guidEvent);
        this.playerList.onPlayerChatEvent(connectEvent);

        Collection<ConnectedPlayer> players = this.playerList.getPlayers();
        ConnectedPlayer pl = Iterables.get(players, 0);
        Assert.assertEquals("GUID", pl.getGuid());
        Assert.assertEquals("172.0.0.1", pl.getIpAddress());;
        Assert.assertEquals("timmy", pl.getName());
        Assert.assertEquals("1", pl.getSessionId());
    }

    @Test
    public void playersNotPresentOnRefreshOffline() throws Exception {
        PlayerChatEventType type = PlayerChatEventType.CONNECTED;
        ConnectedPlayer cp = new ConnectedPlayer();
        cp.setName("timmy");
        cp.setSessionId("1");
        cp.setIpAddress("172.0.0.1");
        PlayerChatEvent connectEvent = new PlayerChatEvent(this, type, cp);

        ConnectedPlayer cpGuid = new ConnectedPlayer();
        cpGuid.setName("timmy");
        cpGuid.setSessionId("1");
        cpGuid.setGuid("GUID");
        PlayerChatEvent guidEvent = new PlayerChatEvent(this, PlayerChatEventType.GUID_VERIFIED, cpGuid);

        this.playerList.onPlayerChatEvent(connectEvent);
        this.playerList.onPlayerChatEvent(guidEvent);

        ConnectedPlayer pl2 = new ConnectedPlayer();
        pl2.setSessionId("2");
        pl2.setName("frank");
        pl2.setConnected(true);

        ConnectedPlayer pl3 = new ConnectedPlayer();
        pl3.setSessionId("3");
        pl3.setName("Dom");
        pl3.setConnected(true);

        this.playerList.refresh(Arrays.asList(pl2, pl3));

        Collection<ConnectedPlayer> players = this.playerList.getPlayers();
        Assert.assertEquals(3, players.size());

        ConnectedPlayer p1 = Iterables.get(players, 0);
        Assert.assertEquals("1", p1.getSessionId());
        Assert.assertEquals("timmy", p1.getName());
        Assert.assertEquals(false, p1.isConnected());

        ConnectedPlayer p2 = Iterables.get(players, 1);
        Assert.assertEquals("2", p2.getSessionId());
        Assert.assertEquals("frank", p2.getName());
        Assert.assertEquals(true, p2.isConnected());

        ConnectedPlayer p3 = Iterables.get(players, 2);
        Assert.assertEquals("3", p3.getSessionId());
        Assert.assertEquals("Dom", p3.getName());
        Assert.assertEquals(true, p3.isConnected());
    }

    @Test
    public void connectedPlayerInitiatesBroadcast() throws Exception {
        PlayerChatEventType type = PlayerChatEventType.CONNECTED;
        ConnectedPlayer cp = new ConnectedPlayer();
        cp.setName("timmy");
        cp.setSessionId("1");

        PlayerChatEvent event = new PlayerChatEvent(this, type, cp);

        CountDownLatch latch = new CountDownLatch(1);
        Disposable disposable = this.playerList.playerObservable().subscribe(x -> {
            Assert.assertEquals("timmy", x.getName());
            latch.countDown();
        });
        this.playerList.onPlayerChatEvent(event);

        Assert.assertTrue("broadcast did not complete within 1 second", latch.await(1, TimeUnit.SECONDS));

        Collection<ConnectedPlayer> players = this.playerList.getPlayers();
        Assert.assertEquals(1, players.size());
        Assert.assertEquals("timmy", players.iterator().next().getName());
        disposable.dispose();
    }

    @Test
    public void connectedPlayerUpdatedWithGuid() throws Exception {
        PlayerChatEventType type = PlayerChatEventType.CONNECTED;
        ConnectedPlayer cp = new ConnectedPlayer();
        cp.setName("timmy");
        cp.setSessionId("1");
        PlayerChatEvent connectEvent = new PlayerChatEvent(this, type, cp);

        ConnectedPlayer cpGuid = new ConnectedPlayer();
        cpGuid.setSessionId("1");
        cpGuid.setGuid("GUID");
        PlayerChatEvent guidEvent = new PlayerChatEvent(this, PlayerChatEventType.GUID_VERIFIED, cpGuid);

        CountDownLatch connectLatch = new CountDownLatch(1);
        Disposable disposableConnect = this.playerList.playerObservable()
            .subscribe(x -> connectLatch.countDown());
        this.playerList.onPlayerChatEvent(connectEvent);
        Assert.assertTrue(connectLatch.await(1, TimeUnit.SECONDS));
        disposableConnect.dispose();

        CountDownLatch guidLatch = new CountDownLatch(1);
        Disposable disposableGuid = this.playerList.playerObservable()
            .subscribe(x -> {
                Assert.assertEquals("GUID", x.getGuid());
                guidLatch.countDown();
        });
        this.playerList.onPlayerChatEvent(guidEvent);
        Assert.assertTrue(guidLatch.await(1, TimeUnit.SECONDS));
        disposableGuid.dispose();

        Optional<ConnectedPlayer> maybeCp = StreamEx.of(this.playerList.getPlayers())
            .findFirst(x -> Objects.equals(x.getSessionId(), "1"));
        Assert.assertTrue(maybeCp.isPresent());
        ConnectedPlayer resolvedPlayer = maybeCp.get();

        Assert.assertEquals("GUID", resolvedPlayer.getGuid());
    }
}
