package com.jsoc.overwatch.arma.services;

import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import com.jsoc.overwatch.services.connection.events.ServerStateUpdateEvent;
import com.jsoc.overwatch.services.connection.games.arma.IRconConnection;
import com.jsoc.overwatch.services.connection.RconConnectionPool;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import com.jsoc.overwatch.services.banService.BanCacheService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationEventPublisher;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BanCacheServiceTest {

    private RconConnectionPool pool = mock(RconConnectionPool.class);

    private ApplicationEventPublisher publisher = mock(ApplicationEventPublisher.class);

    private BanCacheService banCacheService;

    @Before
    public void setup() {
        this.banCacheService = new BanCacheService(this.publisher);
    }

    @Test
    public void canConstruct() {
        Assert.assertNotEquals(this.banCacheService, null);
    }

    @Test
    public void canBuildBanListOnConnectionEstablished() {
        ServerStateUpdateEvent event = new ServerStateUpdateEvent(this, 1L, "", ServerConnectionStatus.CONNECTED);
        IRconConnection mockServer = mock(IRconConnection.class);
        CommandResponse cr = new CommandResponse();
        cr.setSuccess(false);
        CompletableFuture<CommandResponse> cfcr = CompletableFuture.completedFuture(cr);
        when(mockServer.sendCommand(any())).thenReturn(cfcr);
        when(this.pool.getConnection(1L)).thenReturn(Optional.of(mockServer));
    }
}
