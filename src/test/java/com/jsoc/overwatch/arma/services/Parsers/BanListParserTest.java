package com.jsoc.overwatch.arma.services.Parsers;

import com.jsoc.overwatch.services.banService.BanListParser;
import com.jsoc.overwatch.services.banService.models.BanRecord;
import org.junit.Test;

import java.time.Duration;
import java.time.Period;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class BanListParserTest {

    @Test
    public void canParseTempBan() {
        assertBanListParse("1242 0067aa016ae9bab1f19263ef1cb830f1 100 you have been bad",
                1242L,
                "0067aa016ae9bab1f19263ef1cb830f1",
                Duration.ofMinutes(100),
                "you have been bad");
    }

    @Test
    public void canParsePermBan() {
        assertBanListParse("1242 0067aa016ae9bab1f19263ef1cb830f1 perm Recruiting (Unit ban) Perm (Permanent) [Sjanten]",
                1242L,
                "0067aa016ae9bab1f19263ef1cb830f1",
                Duration.ofDays(Period.ofYears(100).getDays()),
                "Recruiting (Unit ban) Perm (Permanent) [Sjanten]");
    }

    @Test
    public void canParseManyBans() {

        List<BanRecord> bans = Arrays.asList(
            BanRecord.builder().id(1L).guid("guid1").duration(Duration.ofMinutes(250)).message("ban message 1").build(),
            BanRecord.builder().id(2L).guid("guid2").duration(Duration.ofMinutes(100)).message("ban message 2").build(),
            BanRecord.builder().id(3L).guid("guid3").duration(Duration.ofDays(Period.ofYears(100).getDays())).message("ban message 3").build(),
            BanRecord.builder().id(4L).guid("guid4").duration(Duration.ofMinutes(400)).message("ban message 4").build()
        );

        String banMessage =
                "1 guid1 250 ban message 1\n" +
                "2 guid2 100 ban message 2\n" +
                "3 guid3 perm ban message 3\n" +
                "4 guid4 400 ban message 4";

        assertBanList(banMessage, bans);
    }

    private void assertBanList(String lines, List<BanRecord> records) {
        List<BanRecord> brs = BanListParser.parseBanList(lines);
        assertEquals(records.size(), brs.size());
        for (int i = 0; i < brs.size(); i++) {
            assertBanRecordsEqual(records.get(i), brs.get(i));
        }
    }

    private void assertBanRecordsEqual(BanRecord expected, BanRecord actual) {
        this.assertBanRecord(actual, expected.getId(), expected.getGuid(), expected.getDuration(), expected.getMessage());
    }

    private void assertBanRecord(BanRecord record, Long banId, String guid, Duration banDuration, String message) {
        assertEquals(banId, record.getId());
        assertEquals(guid, record.getGuid());
        assertEquals(banDuration, record.getDuration());
        assertEquals(message, record.getMessage());
    }

    private void assertBanListParse(String line, Long banId, String guid, Duration banDuration, String message) {
        List<BanRecord> brs = BanListParser.parseBanList(line);
        assertEquals(1, brs.size());
        BanRecord br = brs.get(0);
        assertBanRecord(br, banId, guid, banDuration, message);
    }
}
