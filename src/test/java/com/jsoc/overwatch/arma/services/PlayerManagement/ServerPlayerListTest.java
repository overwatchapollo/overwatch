package com.jsoc.overwatch.arma.services.PlayerManagement;

import com.google.common.collect.Iterables;
import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import com.jsoc.overwatch.services.playerList.events.PlayerChatEvent;
import com.jsoc.overwatch.services.playerList.events.PlayerChatEventType;
import com.jsoc.overwatch.services.playerList.ServerPlayerList;
import io.reactivex.disposables.Disposable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class ServerPlayerListTest {

    private ServerPlayerList playerList;

    @Before
    public void setup() {
        this.playerList = new ServerPlayerList(1L);
    }

    @Test
    public void canBuild() {
        Assert.assertNotEquals(null, this.playerList);
    }

    @Test
    public void canUpdateServerListFromChatEvent() throws Exception {
        PlayerChatEventType type = PlayerChatEventType.CONNECTED;
        ConnectedPlayer cp = new ConnectedPlayer();
        cp.setName("timmy");
        cp.setSessionId("1");

        PlayerChatEvent event = new PlayerChatEvent(this, type, cp);
        CountDownLatch latch = new CountDownLatch(1);
        Disposable assertDisposable = this.playerList.playerObservable().subscribe(x -> {
            Assert.assertEquals("timmy", x.getName());
            latch.countDown();
        });
        this.playerList.onPlayerChatEvent(event);
        Assert.assertTrue(latch.await(1, TimeUnit.SECONDS));

        ConnectedPlayer resolved = this.playerList.getPlayers().iterator().next();
        Assert.assertEquals("timmy", resolved.getName());
        assertDisposable.dispose();
    }

    @Test
    public void archivedPlayerList() {
        ConnectedPlayer cp1 = new ConnectedPlayer();
        cp1.setSessionId("1");
        cp1.setName("timmy");
        cp1.setGuid("guid1");
        this.playerList.refresh(Arrays.asList(cp1));

        ConnectedPlayer cp2 = new ConnectedPlayer();
        cp2.setSessionId("1");
        cp2.setName("frank");
        cp2.setGuid("guid2");
        this.playerList.refresh(Arrays.asList(cp2));

        Collection<ConnectedPlayer> players = this.playerList.getPlayers();
        Assert.assertEquals(1, players.size());
        ConnectedPlayer cp1get = Iterables.get(players, 0);

        Assert.assertEquals("1", cp1get.getSessionId());
        Assert.assertEquals("frank", cp1get.getName());
        Assert.assertEquals("guid2", cp1get.getGuid());
    }
}
