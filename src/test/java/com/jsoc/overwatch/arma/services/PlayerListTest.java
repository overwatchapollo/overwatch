package com.jsoc.overwatch.arma.services;

import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import com.jsoc.overwatch.services.playerList.PlayerList;
import com.jsoc.overwatch.services.playerList.events.ConnectedPlayerChange;
import com.jsoc.overwatch.services.playerList.events.PlayerChatEvent;
import com.jsoc.overwatch.services.playerList.events.PlayerChatEventType;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.context.ApplicationEventPublisher;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class PlayerListTest {

    private ApplicationEventPublisher publisher = spy(ApplicationEventPublisher.class);

    private ArgumentCaptor<ConnectedPlayerChange> changeCaptor = ArgumentCaptor.forClass(ConnectedPlayerChange.class);

    @Test
    public void whenPlayerJoinsEventIsFired() throws Exception {
        PlayerList playerList = new PlayerList(1L);
        ConnectedPlayer cp = ConnectedPlayer.builder().sessionId("1")
            .serverId(1L)
            .ping(123)
            .connected(true)
            .name("timmy")
            .ipAddress("1.1.1.1")
            .build();
        PlayerChatEvent playerChatEvent = new PlayerChatEvent(this, PlayerChatEventType.CONNECTED, cp);
        CountDownLatch latch = new CountDownLatch(1);
        playerList.playerObservable().subscribe(x -> latch.countDown());
        playerList.onPlayerChatEvent(playerChatEvent);
        latch.await(1, TimeUnit.SECONDS);
        Assert.assertEquals(0, latch.getCount());
    }
}
