package com.jsoc.overwatch.services.organisationManagement;

import com.jsoc.overwatch.services.organisationManagement.models.Organisation;
import com.jsoc.overwatch.services.organisationManagement.repository.OrganisationRepository;
import com.jsoc.overwatch.services.userManagement.CustomUserDetailsService;
import com.jsoc.overwatch.services.userManagement.DuplicateUsernameException;
import com.jsoc.overwatch.services.userManagement.UserNotFoundException;
import com.jsoc.overwatch.services.userManagement.UserService;
import com.jsoc.overwatch.services.userManagement.model.UserModel;
import com.jsoc.overwatch.services.userManagement.repository.UserRepository;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class OrganisationServiceTest {

    @Autowired
    private OrganisationService service;

    @Autowired
    private UserService userService;

    @Test
    public void serviceGetsBuild() {
        assertNotNull(this.service);
    }

    @DirtiesContext
    @Test
    public void canCreateOrganisation() throws DuplicateNameException, DuplicateUsernameException {
        UserModel userModel = this.userService.createUser("testUser", "testEmail", "password");
        Organisation org = this.service.createOrganisation(userModel, "testOrg");
        assertEquals("testOrg", org.getName());
        assertEquals(1, org.getMembers().size());
    }

    @DirtiesContext
    @Test(expected = DuplicateNameException.class)
    public void cantCreateDuplicateOrganisationName() throws DuplicateNameException, DuplicateUsernameException {
        UserModel userModel = this.userService.createUser("testUser", "testEmail", "password");
        this.service.createOrganisation(userModel, "testOrg2");
        this.service.createOrganisation(userModel, "testOrg2");

    }

    @DirtiesContext
    @Test(expected = UserNotFoundException.class)
    public void whenUserNotFoundForAddingExceptionThrown() throws Exception {
        UserModel userModel = this.userService.createUser("testUser", "testEmail", "password");
        Organisation org = this.service.createOrganisation(userModel, "testOrg");
        this.service.addUserToOrganisation(org.getId(), 2L);
    }
}
