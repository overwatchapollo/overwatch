package com.jsoc.overwatch.services.connection.games.battleye;

import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.TestScheduler;
import io.reactivex.subjects.PublishSubject;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

@Slf4j
public class TimeoutManagerTest {

    private TestScheduler scheduler = new TestScheduler();

    @Test
    public void timeoutRaisesEvent() throws Exception {
        PublishSubject<ServerConnectionStatus> publishSubject = PublishSubject.create();
        TimeoutManager manager = new TimeoutManager(this.scheduler, new ArrayList<>(), publishSubject);
        CountDownLatch latch = new CountDownLatch(1);
        Disposable disp1 = publishSubject.subscribe(x -> latch.countDown());
        this.scheduler.advanceTimeBy(100, TimeUnit.SECONDS);
        latch.await(1, TimeUnit.SECONDS);
        assertEquals(0, latch.getCount());
        assertTrue(manager.isDisposed());
        manager.dispose();
        disp1.dispose();
    }

    @Test
    public void timeoutMessagesOutput() {
        PublishSubject<Integer> pub1 = PublishSubject.create();
        PublishSubject<ServerConnectionStatus> publishSubject = PublishSubject.create();
        TimeoutManager manager = new TimeoutManager(this.scheduler, Arrays.asList(pub1), publishSubject);
        CountDownLatch latch = new CountDownLatch(1);
        this.scheduler.advanceTimeBy(30, TimeUnit.SECONDS);
        assertEquals(1, latch.getCount());
        publishSubject.subscribe(x -> latch.countDown());
        pub1.onNext(1);
        assertEquals(1, latch.getCount());
        this.scheduler.advanceTimeBy(30, TimeUnit.SECONDS);

        manager.dispose();
    }
}
