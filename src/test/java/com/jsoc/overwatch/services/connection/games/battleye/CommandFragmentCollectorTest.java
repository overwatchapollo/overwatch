package com.jsoc.overwatch.services.connection.games.battleye;


import com.jsoc.overwatch.services.connection.games.battleye.CommandFragmentCollector;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class CommandFragmentCollectorTest {

    private Subject<byte[]> testSubject;

    private CommandFragmentCollector collector;

    @Before
    public void setup() {
        this.testSubject = BehaviorSubject.create();
        this.collector = new CommandFragmentCollector(this.testSubject);
    }

    @Test
    public void canBuildCollector() {
        Assert.assertNotEquals(null, this.collector);
    }

    @Test
    public void canStitchPacketTogetherInOrder() throws Exception {
        byte[] packet1 = {'B', 'E', 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x03, 0x00, 'h', 'e'};
        byte[] packet2 = {'B', 'E', 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x03, 0x01, 'l', 'l'};
        byte[] packet3 = {'B', 'E', 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x03, 0x02, 'o'};

        CountDownLatch latch = new CountDownLatch(1);
        this.testSubject.subscribe(x -> {
            Assert.assertTrue(new String(x).contains("hello"));
            latch.countDown();
        });
        this.collector.pushPacket(packet1);
        this.collector.pushPacket(packet2);
        this.collector.pushPacket(packet3);
        Assert.assertTrue(latch.await(1, TimeUnit.SECONDS));
    }

    @Test
    public void canStitchPacketTogetherOutOfOrder() throws Exception {
        byte[] packet1 = {'B', 'E', 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x03, 0x00, 'h', 'e'};
        byte[] packet2 = {'B', 'E', 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x03, 0x01, 'l', 'l'};
        byte[] packet3 = {'B', 'E', 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x03, 0x02, 'o'};

        CountDownLatch latch = new CountDownLatch(1);
        this.testSubject.subscribe(x -> {
            Assert.assertTrue(new String(x).contains("hello"));
            latch.countDown();
        });
        this.collector.pushPacket(packet1);
        this.collector.pushPacket(packet3);
        this.collector.pushPacket(packet2);
        Assert.assertTrue(latch.await(1, TimeUnit.SECONDS));
    }

    @Test
    public void canStitchDuplicates() throws Exception {
        byte[] packet1 = {'B', 'E', 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x03, 0x00, 'h', 'e'};
        byte[] packet2 = {'B', 'E', 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x03, 0x01, 'l', 'l'};
        byte[] packet3 = {'B', 'E', 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x03, 0x01, 'l', 'l'};
        byte[] packet4 = {'B', 'E', 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x03, 0x02, 'o'};


        CountDownLatch latch = new CountDownLatch(1);
        this.testSubject.subscribe(x -> {
            String result = new String(x);
            Assert.assertTrue("Could not handle out of order string. Was " + result , result.contains("hello"));
            latch.countDown();
        });
        this.collector.pushPacket(packet1);
        this.collector.pushPacket(packet2);
        this.collector.pushPacket(packet3);
        this.collector.pushPacket(packet4);
        Assert.assertTrue(latch.await(1, TimeUnit.SECONDS));
    }
}
