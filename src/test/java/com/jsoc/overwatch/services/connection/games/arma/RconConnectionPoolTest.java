package com.jsoc.overwatch.services.connection.games.arma;

import com.jsoc.overwatch.services.connection.RconConnectionPool;
import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import com.jsoc.overwatch.controller.dto.ServerConnectionDto;
import com.jsoc.overwatch.services.serverRegister.repository.ServerRepository;
import com.jsoc.overwatch.services.serverRegister.ServerRegisterService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class RconConnectionPoolTest {
    private ApplicationEventPublisher publisher = mock(ApplicationEventPublisher.class);
    private ServerRepository rep = mock(ServerRepository.class);

    private RconConnectionPool pool;

    @Autowired
    private ServerRegisterService registerService;

    @Test
    public void canRegisterServer() {
        RconFactory mockFactory = mock(RconFactory.class);
        IRconConnection mockCon = mock(IRconConnection.class);
        when(mockFactory.buildWrapper(any())).thenReturn(mockCon);
        this.registerService = new ServerRegisterService(this.rep, this.publisher);
        this.pool = new RconConnectionPool(publisher, mockFactory, this.registerService);
        //this.pool.initialiseFromDatabase();
        ServerConnection con = new ServerConnection();
        con.setId(1L);
        con.setMaxPlayers(70);
        con.setPassword("password");
        con.setServerName("server");
        con.setIpAddress("127.0.0.1");
        con.setPort(1234);
        con.setMock(false);
        when(this.rep.save(any())).thenReturn(con);
        this.registerService.registerServer(con);
        ArgumentCaptor<ServerConnection> captor = ArgumentCaptor.forClass(ServerConnection.class);
        verify(this.rep).save(captor.capture());
        Assert.assertNotEquals(null, captor.getValue());
    }

    @Test
    public void modifyServer() {
        RconFactory mockFactory = mock(RconFactory.class);
        IRconConnection mockCon = mock(IRconConnection.class);
        when(mockFactory.buildWrapper(any())).thenReturn(mockCon);

        this.registerService = new ServerRegisterService(this.rep, this.publisher);
        this.pool = new RconConnectionPool(publisher, mockFactory, this.registerService);

        when(this.rep.findAll()).thenReturn(new ArrayList<>());
        //this.pool.initialiseFromDatabase();
        ServerConnection con = new ServerConnection();
        con.setId(1L);
        con.setMaxPlayers(70);
        con.setPassword("password");
        con.setServerName("server");
        con.setIpAddress("127.0.0.1");
        con.setPort(1234);
        con.setMock(false);
        when(this.rep.findById(1L)).thenReturn(Optional.of(con));
        when(this.rep.save(any())).thenReturn(con);

        this.registerService.registerServer(con);


        ServerConnectionDto dto = new ServerConnectionDto();
        dto.setIpAddress("172.0.0.1");

        when(this.rep.findAll()).thenReturn(Arrays.asList(con));

        this.registerService.modifyServer(1L, dto);

        ArgumentCaptor<ServerConnection> captor = ArgumentCaptor.forClass(ServerConnection.class);
        verify(this.rep, times(2)).save(captor.capture());
        ServerConnection ret = captor.getValue();
        Assert.assertEquals("172.0.0.1", ret.getIpAddress());
        Assert.assertEquals("server", ret.getServerName());
        Assert.assertEquals("password", ret.getPassword());
        Assert.assertEquals(new Integer(70), ret.getMaxPlayers());
    }
}
