package com.jsoc.overwatch.services.connection.games.arma;

import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import org.junit.Ignore;
import org.junit.Test;

import java.util.concurrent.Executors;

import static com.jsoc.overwatch.services.connection.ServerConnectionStatus.CONNECTED;
import static com.jsoc.overwatch.services.connection.ServerConnectionStatus.DISCONNECTED;
import static org.junit.Assert.assertEquals;

@Ignore
public class ServerConnectionRunnerTest {
    private ServerConnectionRunner runner;

    private String testHost = "testHost";
    private int testPort = 2307;
    private String testPassword = "password";

    private Scheduler scheduler = Schedulers.from(Executors.newCachedThreadPool());
    @Test
    public void canConnect() throws Exception {
        this.runner = new ServerConnectionRunner(this.scheduler, testHost, testPort, testPassword);
        this.scheduler.scheduleDirect(() -> this.runner.run());
        Thread.sleep(1000);
        ServerConnectionStatus status = this.runner.getStatus();
        assertEquals(CONNECTED, status);
        this.scheduler.shutdown();
    }

    @Test
    public void canDisconnect() throws Exception {
        this.runner = new ServerConnectionRunner(this.scheduler, testHost, testPort, testPassword);
        this.scheduler.scheduleDirect(() -> this.runner.run());
        Thread.sleep(500);
        assertEquals(CONNECTED, this.runner.getStatus());
        this.runner.raiseEvent(DISCONNECTED);
        Thread.sleep(500);

        assertEquals(DISCONNECTED, this.runner.getStatus());
        this.scheduler.shutdown();
    }
}
