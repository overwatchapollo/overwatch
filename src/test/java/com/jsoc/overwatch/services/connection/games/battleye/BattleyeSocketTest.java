package com.jsoc.overwatch.services.connection.games.battleye;

import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import com.jsoc.overwatch.services.connection.api.Command;
import com.jsoc.overwatch.services.connection.api.CommandFactory;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import lombok.extern.slf4j.Slf4j;
import org.junit.Ignore;
import org.junit.Test;

import static com.jsoc.overwatch.services.connection.ServerConnectionStatus.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Slf4j
@Ignore
public class BattleyeSocketTest {

    private BattleyeSocket socket;
    private Scheduler scheduler;

    private String testHost = "testHost";
    private int testPort = 2307;
    private String testPassword = "password";

    @Test
    public void canConnect() {
        this.scheduler = Schedulers.newThread();
        this.socket = new BattleyeSocket(this.scheduler, testHost, testPort, testPassword);
        ServerConnectionStatus connectionStatus = this.socket.connect();
        assertEquals(CONNECTED, connectionStatus);
        ServerConnectionStatus disconnectStatus = this.socket.disconnect();
        assertEquals(DISCONNECTED, disconnectStatus);
    }

    @Test
    public void unauthorisedWhenBadPassword() {
        this.scheduler = Schedulers.newThread();
        this.socket = new BattleyeSocket(this.scheduler, testHost, testPort, "badpassword");
        ServerConnectionStatus connectionStatus = this.socket.connect();
        assertEquals(UNAUTHORISED, connectionStatus);
    }

    @Test
    public void canSendCommand() {
        this.scheduler = Schedulers.newThread();
        this.socket = new BattleyeSocket(this.scheduler, testHost, testPort, testPassword);
        ServerConnectionStatus connectionStatus = this.socket.connect();
        assertEquals(CONNECTED, connectionStatus);

        CommandFactory cf = new CommandFactory();
        Command players = cf.playerList();
        CommandResponse cr = this.socket.sendCommand(players);
        assertTrue(cr.isSuccess());
        log.info(cr.getResponse());

        ServerConnectionStatus disconnectStatus = this.socket.disconnect();
        assertEquals(DISCONNECTED, disconnectStatus);
    }
}
