package com.jsoc.overwatch.services.connection.games.battleye;

import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import com.jsoc.overwatch.services.connection.api.Packet;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.TestScheduler;
import io.reactivex.subjects.PublishSubject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.*;

public class BattleyeProtocolClientTest {

    private BattleyeProtocolClient client;

    private AsyncBattleyeSocket socket = mock(AsyncBattleyeSocket.class);

    private TestScheduler scheduler;

    private PublishSubject<byte[]> dataPublisher = PublishSubject.create();

    private String password = "password";

    @Before
    public void setup() {
        this.scheduler = new TestScheduler();
        when(this.socket.getDataObservable()).thenReturn(this.dataPublisher);
        this.client = new BattleyeProtocolClient(this.scheduler, "hostname", 1000, this.password, this.socket);
    }

    @Test
    public void canBeBuilt() {
        Assert.assertNotEquals(null, this.client);
    }

    @Test
    public void canLogin() throws Exception {
        doAnswer(answer -> {
            byte[] response = {0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x01};
            dataPublisher.onNext(response);
            return null;
        }).when(this.socket).sendData(any());

        this.client.activate();
        Assert.assertSame("Login did not result in authentication", this.client.getStatus(), ServerConnectionStatus.CONNECTED);
    }

    @Test
    public void canClose() throws Exception {
        doAnswer(answer -> {
            byte[] response = {0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x01};
            dataPublisher.onNext(response);
            return null;
        }).when(this.socket).sendData(any());

        this.client.activate();
        this.client.close();
    }

    @Test
    public void loginSendsPassword() throws Exception {
        CountDownLatch loginLatch = new CountDownLatch(1);
        doAnswer(answer -> {
            byte[] packet = answer.getArgument(0);
            String question = new String(packet);
            byte[] response = {0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x01};
            dataPublisher.onNext(response);
            Assert.assertTrue(question.contains(this.password));
            loginLatch.countDown();
            return null;
        }).when(this.socket).sendData(any());

        this.client.activate();

        Assert.assertTrue(loginLatch.await(1, TimeUnit.SECONDS));
    }

    @Test
    public void commandResponseBroadcasts() throws Exception {
        doAnswer(answer -> {
            byte[] response = {0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x01};
            dataPublisher.onNext(response);
            return null;
        }).when(this.socket).sendData(any());

        this.client.activate();
        doAnswer(answer -> {
            byte[] response = {'B', 'E', 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 'h', 'i'};
            this.dataPublisher.onNext(response);
            return null;
        }).when(this.socket).sendData(any());

        Packet pk = this.client.sendCommand("hello");
        Assert.assertTrue(pk.getSuccess().isPresent() && pk.getSuccess().get());
        String response = new String(pk.getResponse());

        Assert.assertTrue(response.contains("hi"));
    }

    @Test
    public void commandResponseFragmentedBroacasts() throws Exception {
        doAnswer(answer -> {
            byte[] response = {0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x01};
            dataPublisher.onNext(response);
            return null;
        }).when(this.socket).sendData(any());

        this.client.activate();

        Mockito.reset(this.socket);

        doAnswer(answer -> {
            byte[] response0 = {'B', 'E', 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x02, 0x00, 'h', 'i'};
            this.dataPublisher.onNext(response0);
            byte[] response1 = {'B', 'E', 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x02, 0x01, 'h', 'i'};
            this.dataPublisher.onNext(response1);
            return null;
        }).when(this.socket).sendData(any());

        CountDownLatch commandReceived = new CountDownLatch(1);

        Disposable disp = this.client.getCommandObservable().subscribe(x -> {
            Assert.assertTrue(new String(x).contains("hihi"));
            commandReceived.countDown();
        });

        this.client.sendCommand("hello");
        Assert.assertTrue("Command was not received.", commandReceived.await(1, TimeUnit.SECONDS));

        disp.dispose();
    }

    @Test
    public void successfulLoginTriggersStateUpdate() throws Exception {
        CountDownLatch loginLatch = new CountDownLatch(1);
        doAnswer(answer -> {
            byte[] response = {0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x01};
            dataPublisher.onNext(response);
            loginLatch.countDown();
            return null;
        }).when(this.socket).sendData(any());

        CountDownLatch stateLatch = new CountDownLatch(1);
        this.client.getStateObservable().subscribe(x -> stateLatch.countDown());
        this.client.activate();

        Assert.assertTrue(loginLatch.await(1, TimeUnit.SECONDS));
        Assert.assertTrue(stateLatch.await(1, TimeUnit.SECONDS));
    }
}
