package com.jsoc.overwatch.services.playerReport;

import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import com.jsoc.overwatch.services.playerList.events.ConnectedPlayerChange;
import com.jsoc.overwatch.services.userManagement.CustomUserDetailsService;
import com.jsoc.overwatch.services.userManagement.model.UserModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@ActiveProfiles()
@AutoConfigureMockMvc
@SpringBootTest
public class PlayerReportServiceTest {

    @Autowired
    private PlayerReportService reportService;

    @MockBean
    private CustomUserDetailsService userService;


    @Before
    public void setup() {
        UserModel admin = mock(UserModel.class);
        when(admin.getUserName()).thenReturn("admin");
        when(admin.getId()).thenReturn(1L);
        Mockito.when(this.userService.getModelByUsername(any())).thenReturn(admin);
    }

    @Test
    @DirtiesContext
    public void canGetPlayerFromName() {
        ConnectedPlayer player = this.getPlayer1();
        ConnectedPlayerChange change = new ConnectedPlayerChange(this, player);

        this.reportService.onPlayerConnection(change);
        List<PlayerReport> reports = this.reportService.getPlayersByName(this.getAdmin(), "timmy");
        assertEquals(1, reports.size());
    }

    private User getAdmin() {
        User user = mock(User.class);
        when(user.getUsername()).thenReturn("admin");
        return user;
    }


    private ConnectedPlayer getPlayer1() {
        ConnectedPlayer player = new ConnectedPlayer();
        player.setMember(false);
        player.setSessionId("1");
        player.setName("timmy");
        player.setConnected(true);
        player.setGuid("guid1");
        player.setIpAddress("1234");
        player.setPing(10);
        player.setServerId(1L);
        return player;
    }
}
