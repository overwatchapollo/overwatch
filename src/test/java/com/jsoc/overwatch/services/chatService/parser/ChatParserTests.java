package com.jsoc.overwatch.services.chatService.parser;


import com.jsoc.overwatch.services.chatService.ChatMessage;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ChatParserTests {
    private ChatParser parser = new ChatParser();

    @Test
    public void canParseWithColon() {
        RconMessage rconMessage = new RconMessage();
        rconMessage.setRawMessage("(Group) Timmy: test: test");
        ChatMessage message = this.parser.parse(rconMessage);
        assertEquals("test: test", message.getMessage());
        assertEquals("Timmy", message.getSender());
        assertEquals("Group", message.getChannel());
    }

    @Test
    public void canParseWithSpacesAndColon() {
        RconMessage rconMessage = new RconMessage();
        rconMessage.setRawMessage("(Group) Timmy two sticks: test: test");
        ChatMessage message = this.parser.parse(rconMessage);
        assertEquals("test: test", message.getMessage());
        assertEquals("Timmy two sticks", message.getSender());
        assertEquals("Group", message.getChannel());
    }

    @Test
    public void canParseWithSpacesEndAndColon() {
        RconMessage rconMessage = new RconMessage();
        rconMessage.setRawMessage("(Group) Timmy two sticks : test: test");
        ChatMessage message = this.parser.parse(rconMessage);
        assertEquals("test: test", message.getMessage());
        assertEquals("Timmy two sticks ", message.getSender());
        assertEquals("Group", message.getChannel());
    }

    @Test
    public void canParseWithNumbers() {
        RconMessage rconMessage = new RconMessage();
        rconMessage.setRawMessage("(Group) Timmy 2 sticks: test: test");
        ChatMessage message = this.parser.parse(rconMessage);
        assertEquals("test: test", message.getMessage());
        assertEquals("Timmy 2 sticks", message.getSender());
        assertEquals("Group", message.getChannel());
    }

    @Test
    public void canParseWithComma() {
        RconMessage rconMessage = new RconMessage();
        rconMessage.setRawMessage("(Group) Jizzman, 1st of his name: test: test");
        ChatMessage message = this.parser.parse(rconMessage);
        assertEquals("test: test", message.getMessage());
        assertEquals("Jizzman, 1st of his name", message.getSender());
        assertEquals("Group", message.getChannel());
    }
}
