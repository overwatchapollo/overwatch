package com.jsoc.overwatch.services.mqttListener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.UUID;

@Service
@Slf4j
public class MqttService {

    private MqttClient client;

    private Subject<Pair<String, MqttMessage>> publishSubject = PublishSubject.create();

    private Disposable publishDisposable = null;

    @Autowired
    private ObjectMapper mapper;

    private boolean isSetup = false;

    private MqttConnectOptions options;

    private MqttCallback callback;

    public MqttService() throws MqttException {
        this.client = new MqttClient("tcp://localhost", UUID.randomUUID().toString());
        this.options = new MqttConnectOptions();
        this.options.setAutomaticReconnect(true);

        this.options.setMaxInflight(1000);
        this.options.setCleanSession(true);
        this.options.setKeepAliveInterval(0);
        this.callback = new Callback(this.client);
    }

    public void initialise(String username, String password) throws Exception {
        if (this.isSetup || this.client.isConnected()) {
            throw new IllegalStateException("Mqtt service already setup.");
        }
        this.options.setUserName(username);
        this.options.setPassword(password.toCharArray());

        this.client.setCallback(this.callback);

        this.client.connect(this.options);

        log.info("MQTT client connected {}", this.client.isConnected());
        this.publishDisposable = this.publishSubject.subscribe(this::publishToBroker);
        this.isSetup = true;
    }

    @PreDestroy
    public void shutdown() throws MqttException {
        this.client.close();
        if (this.publishDisposable != null) {
            log.info("Tearing down MQTT subscription..");
            this.publishDisposable.dispose();
        }
        log.info("Mqtt shutdown");
        this.isSetup = false;
    }

    public boolean isConnected() {
        return this.client.isConnected();
    }

    public void publish(String topic, Object payload) throws JsonProcessingException {
        MqttMessage message = new MqttMessage(this.mapper.writeValueAsBytes(payload));
        this.publishSubject.onNext(Pair.of(topic, message));
    }

    private void publishToBroker(Pair<String, MqttMessage> values) {
        this.publishToBroker(values.getFirst(), values.getSecond());
    }

    private void publishToBroker(String topic, MqttMessage message) {
        try {
            this.client.publish(topic, message);
        } catch (MqttException e) {
            log.error("Unable to publish to '{}' reason: {}", topic, e.getMessage());
        }
    }
}
