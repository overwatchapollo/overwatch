package com.jsoc.overwatch.services.mqttListener;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.*;

@Slf4j
public class Callback implements MqttCallback {
    private MqttClient client;
    public Callback(MqttClient client) {
        this.client = client;
    }
    @Override
    public void connectionLost(Throwable throwable) {
        while(!client.isConnected()) {
            try {
                log.info("Reconnecting to mqtt broker.");

                client.disconnectForcibly();
                log.info("Disconnected");
                client.connect();
                log.info("reconnected");
            } catch (MqttException e) {
                log.error("Failed to reconnect to broker: {}; {}", e.getMessage(), e.getCause());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    @Override
    public void messageArrived(String s, MqttMessage mqttMessage) {

    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }
}
