package com.jsoc.overwatch.services.mqttListener.controllers;

import com.jsoc.overwatch.Security.permissions.ServerPermissionGenerator;
import com.jsoc.overwatch.services.userManagement.CustomUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping(value = "/api/http", method = { RequestMethod.GET, RequestMethod.POST })
@Slf4j
public class HttpApiController {
    @Autowired
    private CustomUserDetailsService detailsService;

    private ServerPermissionGenerator permissionGenerator = new ServerPermissionGenerator();

    @Autowired
    private AuthenticationManager authenticationManager;

    @RequestMapping(value = "/userPath", produces = MediaType.TEXT_PLAIN_VALUE)
    public String userPath(@RequestParam("username") String username, @RequestParam("password") String password) {
        Authentication auth = new UsernamePasswordAuthenticationToken(username, password);
        Authentication result = this.authenticationManager.authenticate(auth);
        return result.isAuthenticated() ? "allow" : "deny";
    }

    @RequestMapping(value = "/vhost", produces = MediaType.TEXT_PLAIN_VALUE)
    public String vhostPath(@RequestParam("username") String username, @RequestParam("vhost") String vhost, @RequestParam("ip") String ip) {
        return "allow";
    }

    @RequestMapping(value = "/resource", produces = MediaType.TEXT_PLAIN_VALUE)
    public String resourcePath(@RequestParam("username") String username, @RequestParam("vhost") String vhost, @RequestParam("resource") String resource, @RequestParam("name") String name, @RequestParam("permission") String permission) {
        // TODO build up permission checks for resources

        return "allow";
    }

    @RequestMapping(value = "/topic", produces = MediaType.TEXT_PLAIN_VALUE)
    public String topicPath(@RequestParam("username") String username, @RequestParam("vhost") String vhost, @RequestParam("resource") String resource, @RequestParam("name") String name, @RequestParam("permission") String permission, @RequestParam("routing_key") String routingKey) {
        if (name.compareTo("amq.topic") != 0) {
            return "allow";
        }

        // TODO build up permission checks for topics
        return handleMqtt(username, routingKey);
    }

    private String handleMqtt(String username, String routingKey) {
        UserDetails details = this.detailsService.loadUserByUsername(username);
        String[] splitKey = routingKey.split("\\.");
        if (splitKey.length == 0) {
            return "deny";
        }
        switch (splitKey[0]) {
            case "server":
                return handleServerTopicAuth(details, routingKey);
            default:
                return "deny";
        }
    }

    private String handleServerTopicAuth(UserDetails details, String routingKey) {

        Optional<String> status = this.handlePatternMatch(routingKey, "server\\.([\\d]+)\\.status",
            matcher -> this.permissionGenerator.view(matcher.group(1)).hasPermission(details) ? "allow" : "deny");
        if (status.isPresent()) {
            return status.get();
        }

        Optional<String> chatUpdate = this.handlePatternMatch(routingKey, "server\\.([\\d]+)\\.update\\.chat\\.message",
                matcher -> this.permissionGenerator.view(matcher.group(1)).hasPermission(details) ? "allow" : "deny");
        if (chatUpdate.isPresent()) {
            return chatUpdate.get();
        }

        Pattern playerUpdatePattern = Pattern.compile("server\\.([\\d]+)\\.update\\.player");
        Matcher playerMatcher = playerUpdatePattern.matcher(routingKey);
        if (playerMatcher.matches()) {
            return this.permissionGenerator.view(Long.parseLong(playerMatcher.group(1))).hasPermission(details) ? "allow" : "deny";
        }

        Pattern playerUpdateStalePattern = Pattern.compile("server\\.([\\d]+)\\.update\\.player\\.stale");
        Matcher playerStaleMatcher = playerUpdateStalePattern.matcher(routingKey);
        if (playerStaleMatcher.matches()) {
            return this.permissionGenerator.view(Long.parseLong(playerStaleMatcher.group(1))).hasPermission(details) ? "allow" : "deny";
        }

        return "deny";
    }

    private Optional<String> handlePatternMatch(String routingKey, String regex, Function<Matcher, String> action) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(routingKey);
        if (matcher.matches()) {
            return Optional.of(action.apply(matcher));
        } else {
            return Optional.empty();
        }
    }

}
