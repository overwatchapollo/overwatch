package com.jsoc.overwatch.services.mqttListener.config;

import com.jsoc.overwatch.services.mqttListener.MqttService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;

@Configuration
@Profile("mqtt-auto-connect")
@Slf4j
public class MqttStartupConfig {
    @Autowired
    private MqttService service;

    @Value("${broker.username}")
    private String username;

    @Value("${broker.password}")
    private String password;

    @EventListener
    public void appReady(ApplicationReadyEvent event) throws Exception {
        log.info("Connecting to message broker.");
        this.service.initialise(this.username, this.password);
    }
}
