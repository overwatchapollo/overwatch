package com.jsoc.overwatch.services.mqttListener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.jsoc.overwatch.services.banService.events.BanListUpdatedEvent;
import com.jsoc.overwatch.services.kickService.events.PlayerKickedEvent;
import com.jsoc.overwatch.services.playerList.events.PlayerListStaleEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MqttEventListener {

    @Autowired
    private MqttService service;


    @EventListener
    public void playerListRefresh(PlayerListStaleEvent event) {
        this.broadcastTopic("server/" + event.getServerId() + "/update/player/stale", event.getServerId());
    }

    @EventListener
    public void banListUpdate(BanListUpdatedEvent banListUpdate) {
        this.broadcastTopic("/banList/update", banListUpdate.getServerMrid());
    }

    @EventListener
    public void kickedEvent(PlayerKickedEvent kickedEvent) {
        this.broadcastTopic("server/" + kickedEvent.getServerId() + "/update/player/kicked", kickedEvent.getPlayerGuid());
    }

    private void broadcastTopic(String topic, Object payload) {
        try {
            this.service.publish(topic, payload);
        } catch (JsonProcessingException e) {
            log.error("JSON parsing exception", e);
        }
    }
}
