package com.jsoc.overwatch.services.playerAdminService.Commands;

public class MalformedCommandException extends Exception {
    public MalformedCommandException(String message) {
        super(message);
    }
}
