package com.jsoc.overwatch.services.playerAdminService.Commands;

public class UnauthorisedActionException extends Exception {
    public UnauthorisedActionException(String message) {
        super(message);
    }
}
