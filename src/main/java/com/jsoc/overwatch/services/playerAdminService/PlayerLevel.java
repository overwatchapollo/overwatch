package com.jsoc.overwatch.services.playerAdminService;

public enum PlayerLevel {
    USER,
    ADMIN
}
