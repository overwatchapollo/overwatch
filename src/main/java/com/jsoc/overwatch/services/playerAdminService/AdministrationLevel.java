package com.jsoc.overwatch.services.playerAdminService;


public enum AdministrationLevel {
    GUEST(0),
    AMBASSADOR(40),
    ADMIN(60),
    SUPER_ADMIN(100);

    private final int value;

    AdministrationLevel(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }

    public static AdministrationLevel from(int value) {
        if (value >= 100) {
            return SUPER_ADMIN;
        } else if (value >= 60) {
            return ADMIN;
        } else if (value >= 40) {
            return AMBASSADOR;
        } else {
            return GUEST;
        }
    }
}
