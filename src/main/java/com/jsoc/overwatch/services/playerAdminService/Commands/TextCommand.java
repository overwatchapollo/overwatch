package com.jsoc.overwatch.services.playerAdminService.Commands;

import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import com.jsoc.overwatch.services.playerAdminService.PlayerAdministratorService;
import lombok.extern.slf4j.Slf4j;

import java.util.function.BiConsumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class TextCommand {

    private int requiredLevel;

    private Pattern matchingPattern;
    private String readablePattern;

    private BiConsumer<String, ConnectedPlayer> executable;
    private PlayerAdministratorService administratorService;

    public TextCommand(int requiredLevel, String matchingPattern, String readablePattern, BiConsumer<String, ConnectedPlayer> executable, PlayerAdministratorService administratorService) {
        this.requiredLevel = requiredLevel;
        this.matchingPattern = Pattern.compile(matchingPattern);
        this.readablePattern = readablePattern;
        this.executable = executable;
        this.administratorService = administratorService;
    }

    /**
     * Minimum privilege level to execute this command.
     * @return Privilege level.
     */
    public int getRequiredLevel() {
        return this.requiredLevel;
    }

    /**
     * True if message matches the command.
     * @param message
     * @return
     */
    public boolean matches(String message) {
        log.trace("Matching \"" + message + "\" to \"" + this.matchingPattern.pattern() + "\"");
        Matcher matcher = matchingPattern.matcher(message);
        return matcher.find();
    }

    /**
     * Execute the command from a player. Wraps the access right and parsing checks.
     * @param command Command to execute.
     * @param player Player which has requested command.
     * @throws UnauthorisedActionException Thrown if player does not have privilege to execute this command.
     * @throws MalformedCommandException If command does not parse.
     */
    public void execute(String command, ConnectedPlayer player) throws UnauthorisedActionException, MalformedCommandException {
        Integer playerLevel = this.administratorService.getLevelForGuid(player.getServerId(), player.getGuid());
        if (playerLevel < this.getRequiredLevel()) {
            throw new UnauthorisedActionException("You do not have access to this command.");
        }

        if (!this.matches(command)) {
            throw new MalformedCommandException("Malformed command. Expected format is: " + this.readablePattern);
        }

        this.executable.accept(command, player);
    }
}
