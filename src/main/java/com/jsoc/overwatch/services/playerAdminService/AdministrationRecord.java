package com.jsoc.overwatch.services.playerAdminService;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class AdministrationRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long serverId;

    private AdministrationLevel level;
    private String username;
    private String guid;
    private String steamId;
}
