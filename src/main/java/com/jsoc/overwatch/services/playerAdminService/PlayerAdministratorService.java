package com.jsoc.overwatch.services.playerAdminService;

import com.jsoc.overwatch.services.playerAdminService.repository.PlayerAdminRepository;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static com.jsoc.overwatch.services.playerAdminService.AdministrationLevel.from;

@Slf4j
@Service
public class PlayerAdministratorService {

    private Map<Long, List<AdministrationRecord>> adminRecordMap = new HashMap<>();

    @Autowired
    private PlayerAdminRepository repo;

    public List<AdministrationRecord> getAdminMapForServer(Long serverId) {
        return this.adminRecordMap.getOrDefault(serverId, Collections.emptyList());
    }

    public void removeAdmin(Long serverId, String guid) {
        if(!this.adminRecordMap.containsKey(serverId)) {
            log.info("server id {} unknown", serverId);
            return;
        }
        List<AdministrationRecord> records = this.adminRecordMap.get(serverId);
        Optional<AdministrationRecord> mayberecord = StreamEx.of(records)
            .findFirst(x -> Objects.equals(x.getGuid(), guid));

        if (!mayberecord.isPresent()) {
            log.info("couldn't find guid");
        }

        mayberecord.ifPresent(x -> {
            log.info("removing guid {} from server {}", guid, serverId);
            records.remove(x);
        });
    }

    private void setSuperAdmin(String guid, String name) {
        this.setTeamIdToAdmin(1L, guid, 100, name);
        this.setTeamIdToAdmin(2L, guid, 100, name);
    }

    private void setAdmin(String guid, String name) {
        this.setTeamIdToAdmin(1L, guid, 60, name);
        this.setTeamIdToAdmin(2L, guid, 60, name);
    }

    private void setAdmbassador(String guid, String name) {
        this.setTeamIdToAdmin(1L, guid, 40, name);
        this.setTeamIdToAdmin(2L, guid, 40, name);
    }

    private String getGuidFromSteamId(String steamId) {
        Long val = Long.parseLong(steamId);
        byte[] parts = { 0x42, 0x45, 0, 0, 0, 0, 0, 0, 0, 0 };
        byte counter = 2;

        do
        {
            parts[counter++] = (byte)(val & 0xFF);
        } while ((val >>= 8) > 0);


        try {
            MessageDigest md = MessageDigest.getInstance("MD5");

            byte[] beHash = md.digest(parts);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < beHash.length; i++)
            {
                String hexVal = String.format("%02X", beHash[i]);
                sb.append(hexVal);
            }

            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("");
        }
    }

    public void setTeamIdToAdmin(Long serverId, String guid, Integer value, String name) {
        if (!this.adminRecordMap.containsKey(serverId)) {
            this.adminRecordMap.put(serverId, new ArrayList<>());
        }

        String steamId = getGuidFromSteamId(guid);
        AdministrationRecord record = new AdministrationRecord();
        record.setGuid(guid);
        record.setSteamId(steamId);
        record.setUsername(name);
        record.setLevel(from(value));
        record.setServerId(serverId);
        log.info("adding user {}", record);
        this.repo.save(record);
        this.adminRecordMap.get(serverId).add(record);
    }

    public Integer getLevelForGuid(Long serverId, String guid) {
        log.debug("Checking admin level on server {} for guid {}.", serverId, guid);
        List<AdministrationRecord> permission = this.adminRecordMap.getOrDefault(serverId, Collections.emptyList());
        return StreamEx.of(permission)
            .filter(x -> Objects.equals(x.getGuid(), guid))
            .map(x-> x.getLevel().getValue())
            .findFirst()
            .orElse(0);
    }
}
