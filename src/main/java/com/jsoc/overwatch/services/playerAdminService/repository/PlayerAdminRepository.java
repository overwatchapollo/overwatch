package com.jsoc.overwatch.services.playerAdminService.repository;

import com.jsoc.overwatch.services.playerAdminService.AdministrationRecord;
import org.springframework.data.repository.CrudRepository;

public interface PlayerAdminRepository extends CrudRepository<AdministrationRecord, Long> {
}
