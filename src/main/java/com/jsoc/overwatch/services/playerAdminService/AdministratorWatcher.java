package com.jsoc.overwatch.services.playerAdminService;

import com.jsoc.overwatch.config.FeatureToggleBefore;
import com.jsoc.overwatch.services.chatService.ChatMessage;
import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import com.jsoc.overwatch.services.playerList.ConnectedPlayerService;
import com.jsoc.overwatch.services.playerList.UnknownServerIdException;
import com.jsoc.overwatch.services.chatService.events.ChatMessageReceived;
import com.jsoc.overwatch.services.connection.RconConnectionPool;
import com.jsoc.overwatch.services.playerAdminService.Commands.MalformedCommandException;
import com.jsoc.overwatch.services.playerAdminService.Commands.TextCommand;
import com.jsoc.overwatch.services.playerAdminService.Commands.TextCommandFactory;
import com.jsoc.overwatch.services.playerAdminService.Commands.UnauthorisedActionException;
import com.jsoc.overwatch.services.connection.api.CommandFactory;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.jsoc.overwatch.config.Features.CHAT_ADMIN_HANDLER;

@Service
@Slf4j
public class AdministratorWatcher {

    private List<TextCommand> commands;

    private Pattern commandPattern = Pattern.compile("^!");


    private CommandFactory commandFactory = new CommandFactory();


    @Autowired
    private RconConnectionPool connectionPool;

    @Autowired
    private ConnectedPlayerService playerService;

    public AdministratorWatcher(TextCommandFactory textCommandFactory) {
        this.commands = textCommandFactory.getCommandList();
    }

    @FeatureToggleBefore(value = CHAT_ADMIN_HANDLER)
    @EventListener
    public void rconMessage(ChatMessageReceived cmr) {
        ChatMessage cm = cmr.getChatMessage();
        if (cm.isRcon()) {
            return;
        }

        String message = cm.getMessage();
        Matcher matcher = this.commandPattern.matcher(message);
        if (matcher.find()) {
            StreamEx.of(this.commands)
                .findFirst(x -> x.matches(message))
                .ifPresent(x -> executeTextCommand(x, cm));
        } else {
            log.trace("Message {} is not a command.", message);
        }
    }

    private void executeTextCommand(TextCommand command, ChatMessage cm) {
        String message = cm.getMessage();
        try {
            Collection<ConnectedPlayer> players = this.playerService.getPlayers(cm.getServerId());
            Optional<ConnectedPlayer> player = StreamEx.of(players)
                    .findFirst(x -> Objects.equals(x.getName(), cm.getSender()));
            if (player.isPresent()) {
                ConnectedPlayer cp = player.get();
                try {
                    cp.setServerId(cm.getServerId());
                    command.execute(message, cp);
                } catch (UnauthorisedActionException e) {
                    log.error("Player " + cp.getGuid() + " tried to do action.");
                } catch (MalformedCommandException e) {
                    log.error("Command malformed exception: " + message);
                    this.sendMessageToPlayer(cp, e.getMessage());
                }
            } else {
                log.error("Command came from player not on server.");
            }
        } catch (UnknownServerIdException ex) {
            log.error(ex.toString());
        }
    }

    private void sendMessageToPlayer(ConnectedPlayer cp, String message) {
        this.sendMessageToPlayer(cp.getServerId(), cp.getSessionId(), message);
    }

    private void sendMessageToPlayer(Long serverId, String playerid, String message) {
        this.connectionPool.getConnection(serverId).ifPresentOrElse(
            x -> x.sendCommand(this.commandFactory.sayWhisper(playerid, message)),
            () -> log.error("Failed to send message to player. RCon isn't working."));
    }
}
