package com.jsoc.overwatch.services.playerAdminService.Commands;

import com.google.common.base.Splitter;
import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import com.jsoc.overwatch.services.playerList.ConnectedPlayerService;
import com.jsoc.overwatch.services.playerList.UnknownServerIdException;
import com.jsoc.overwatch.services.connection.games.arma.IRconConnection;
import com.jsoc.overwatch.services.connection.RconConnectionPool;
import com.jsoc.overwatch.services.playerAdminService.PlayerAdministratorService;
import com.jsoc.overwatch.services.connection.api.CommandFactory;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Slf4j
public class TextCommandFactory {

    @Autowired
    private PlayerAdministratorService playerAdministratorService;

    @Autowired
    private RconConnectionPool connectionPool;

    @Autowired
    private ConnectedPlayerService playerService;

    private CommandFactory commandFactory = new CommandFactory();

    public List<TextCommand> getCommandList() {
        return Arrays.asList(
            this.adminList(),
            this.playerList(),
            this.playerKick());
    }

    public TextCommand adminList() {
        TextCommand command = new TextCommand(
            20,
            "^!admins",
            "!admins",
            (String com, ConnectedPlayer player) -> this.adminList(com, player),
            this.playerAdministratorService);
        return command;
    }

    public TextCommand playerList() {
        TextCommand command = new TextCommand(
            20,
            "^!list",
            "!list",
            (String com, ConnectedPlayer player) -> this.playerList(com, player),
            this.playerAdministratorService);
        return command;
    }

    public TextCommand playerKick() {
        String pattern = "^!kick (\\d+) (.+)";
        TextCommand command = new TextCommand(
                20,
                pattern,
                "!kick <username> <message>",
                (String com, ConnectedPlayer player) -> this.playerKick(com, player, pattern),
                this.playerAdministratorService);
        return command;
    }

    private void adminList(String command, ConnectedPlayer player) {
        try {
            Collection<ConnectedPlayer> players = this.playerService.getPlayers(player.getServerId());
            List<ConnectedPlayer> admins = StreamEx.of(players)
                    .filter(x -> this.playerAdministratorService.getLevelForGuid(x.getServerId(), x.getGuid()) > 0)
                    .toList();

            String message = String.join(", ", StreamEx.of(admins).map(x -> x.getName() + " [" + x.getSessionId() + "]").toList());
            this.sendMessageToPlayer(player, message);
        } catch (UnknownServerIdException ex) {
            log.error(ex.toString());
        }
    }

    private void playerList(String command, ConnectedPlayer player) {
        try {
            Collection<ConnectedPlayer> players = this.playerService.getPlayers(player.getServerId());
            List<ConnectedPlayer> sorted = StreamEx.of(players).sortedBy(x -> x.getName()).toList();
            String message = String.join(", ", StreamEx.of(sorted).map(x -> x.getName() + " [" + x.getSessionId() + "]").toList());
            this.sendMessageToPlayer(player, message);
        } catch (UnknownServerIdException ex) {
            log.error(ex.toString());
        }
    }

    private void playerKick(String command, ConnectedPlayer player, String patternString) {
        log.info("Kick request from {}", player.getName());
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(command);
        matcher.matches();
        String idToKick = matcher.group(1);
        String message = matcher.group(2);
        Optional<IRconConnection> optCon = this.connectionPool.getConnection(player.getServerId());
        optCon.ifPresent(x -> x.sendCommand(this.commandFactory.kickId(idToKick, message)));
    }

    private void sendMessageToPlayer(ConnectedPlayer player, String message) {
        Optional<IRconConnection> optCon = this.connectionPool.getConnection(player.getServerId());
        Iterable<String> msg = Splitter.fixedLength(64).split(message);
        msg.forEach(x ->
            optCon.ifPresent(y ->
                y.sendCommand(this.commandFactory.sayWhisper(player.getSessionId(), message))));
    }
}
