package com.jsoc.overwatch.services.playerAdminService.events;

import com.jsoc.overwatch.controller.dto.PlayerAdministratorDto;
import org.springframework.context.ApplicationEvent;

public class AddPlayerAdminRequest extends ApplicationEvent {
    public AddPlayerAdminRequest(Object source, PlayerAdministratorDto dto) {
        super(source);
    }
}
