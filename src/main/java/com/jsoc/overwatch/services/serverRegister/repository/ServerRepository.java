package com.jsoc.overwatch.services.serverRegister.repository;

import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import org.springframework.data.couchbase.core.query.N1qlPrimaryIndexed;
import org.springframework.data.couchbase.core.query.ViewIndexed;
import org.springframework.data.repository.CrudRepository;

/**
 * Registry configuration for servers.
 */
@N1qlPrimaryIndexed
@ViewIndexed(designDoc = "serverConnection")
public interface ServerRepository extends CrudRepository<ServerConnection, Long>  {
}
