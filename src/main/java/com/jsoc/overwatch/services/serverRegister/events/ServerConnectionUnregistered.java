package com.jsoc.overwatch.services.serverRegister.events;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class ServerConnectionUnregistered extends ApplicationEvent {
    private final Long serverGuid;
    public ServerConnectionUnregistered(Object source, Long serverGuid) {
        super(source);
        this.serverGuid = serverGuid;
    }
}
