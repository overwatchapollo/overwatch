package com.jsoc.overwatch.services.serverRegister;

import com.jsoc.overwatch.controller.dto.NotFoundException;
import com.jsoc.overwatch.controller.dto.ServerConnectionDto;
import com.jsoc.overwatch.services.serverRegister.events.ServerConnectionRegistered;
import com.jsoc.overwatch.services.serverRegister.events.ServerConnectionUnregistered;
import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import com.jsoc.overwatch.services.serverRegister.repository.ServerRepository;
import com.jsoc.overwatch.services.serverRegister.events.ServerConfigModifiedEvent;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Slf4j
public class ServerRegisterService {
    /**
     * Repo of server connections.
     */
    @Autowired
    private ServerRepository repo;

    /**
     * Publisher for status events.
     */
    @Autowired
    private ApplicationEventPublisher publisher;

    public ServerRegisterService(ServerRepository repo, ApplicationEventPublisher publisher) {
        this.repo = repo;
        this.publisher = publisher;
    }

    public ServerConnection registerServer(ServerConnection connection) {
        log.info("Registering server {}", connection);
        ServerConnection serverConnection = this.repo.save(connection);
        log.info("Registered server {}", serverConnection.getId());
        this.publisher.publishEvent(new ServerConnectionRegistered(this, serverConnection.getId()));
        return serverConnection;
    }

    public Optional<ServerConnection> getConfiguration(final Long id) {
        return StreamEx.of(this.repo.findAll().iterator()).findFirst(x -> Objects.equals(id, x.getId()));
    }

    public List<ServerConnection> getServers() {
        return StreamEx.of(this.repo.findAll().iterator()).toList();
    }

    public void unregisterServer(Long id) {
        this.repo.deleteById(id);
        this.publisher.publishEvent(new ServerConnectionUnregistered(this, id));
    }

    public ServerConnection modifyServer(Long id, ServerConnectionDto dto) {
        log.info("Updating server {}: {}", id, dto);
        ServerConnection con = this.getConfiguration(id).orElseThrow(NotFoundException::new);
        if (dto.getIpAddress() != null && !dto.getIpAddress().isEmpty() && !Objects.equals(dto.getIpAddress(),con.getIpAddress())) {
            log.info("updating address");
            con.setIpAddress(dto.getIpAddress());
        }

        if (dto.getPort() != null && !Objects.equals(dto.getPort(), con.getPort())) {
            log.info("updating port");
            con.setPort(dto.getPort());
        }

        if (dto.getPassword() != null && !dto.getPassword().isEmpty()) {
            log.info("updating password");
            con.setPassword(dto.getPassword());
        }

        if (dto.getMaxPlayers() != null && !Objects.equals(dto.getMaxPlayers(), con.getMaxPlayers())) {
            log.info("updating max players");
            con.setMaxPlayers(dto.getMaxPlayers());
        }

        con.setId(id);
        ServerConnection newConnection = this.doUpdate(con);
        this.publisher.publishEvent(new ServerConfigModifiedEvent(this, id));
        return newConnection;
    }

    @Transactional
    private ServerConnection doUpdate(ServerConnection source) {
        return this.repo.save(source);
    }
}
