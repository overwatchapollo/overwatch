package com.jsoc.overwatch.services.serverRegister.config;

import com.jsoc.overwatch.services.serverRegister.ServerRegisterService;
import com.jsoc.overwatch.services.serverRegister.events.ServerConnectionRegistered;
import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Profile("auto-connect")
public class AutoRegistrationConfig {
    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private ServerRegisterService registerService;

    @EventListener
    public void appReady(ApplicationReadyEvent event) {
        List<ServerConnection> connections = this.registerService.getServers();
        connections.forEach(x -> this.publisher.publishEvent(new ServerConnectionRegistered(this, x.getId())));
    }
}
