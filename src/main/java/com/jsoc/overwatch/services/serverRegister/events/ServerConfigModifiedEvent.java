package com.jsoc.overwatch.services.serverRegister.events;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class ServerConfigModifiedEvent extends ApplicationEvent {
    private final Long serverId;
    public ServerConfigModifiedEvent(Object source, Long serverId) {
        super(source);
        this.serverId = serverId;
    }
}
