package com.jsoc.overwatch.services.serverRegister.models;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "server_configuration_table")
public class ServerConnection {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String serverName;

    @NotNull
    private String ipAddress;

    @NotNull
    private Integer port;

    @NotNull
    private String password;

    private Boolean mock;

    private Integer maxPlayers = 0;

    @Override
    public boolean equals(Object connection) {
        return Objects.equals(((ServerConnection)connection).getId(), this.getId());
    }
}
