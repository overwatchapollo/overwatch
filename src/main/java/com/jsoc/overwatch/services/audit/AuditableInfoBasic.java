package com.jsoc.overwatch.services.audit;

import org.springframework.security.core.userdetails.User;

import java.time.Instant;

public class AuditableInfoBasic implements AuditableInfo{
    private final User invoker;
    private final Instant timestamp;
    private final String message;

    public AuditableInfoBasic(User invoker, Instant timestamp, String message) {
        this.invoker = invoker;
        this.timestamp = timestamp;
        this.message = message;
    }

    @Override
    public User getInvoker() {
        return this.invoker;
    }

    @Override
    public Instant getTimestamp() {
        return this.timestamp;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
