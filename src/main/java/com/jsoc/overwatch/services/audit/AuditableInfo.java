package com.jsoc.overwatch.services.audit;

import org.springframework.security.core.userdetails.User;

import java.time.Instant;

public interface AuditableInfo {
    User getInvoker();
    Instant getTimestamp();
    String getMessage();
}
