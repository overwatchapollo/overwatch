package com.jsoc.overwatch.services.audit;

import org.springframework.security.core.userdetails.User;

import java.time.Instant;

public class BasicCategorisedAuditInfo implements CategorisedAuditInfo {
    private final User invoker;
    private final Instant timestamp;
    private final String message;
    private final Long categoryId;

    public BasicCategorisedAuditInfo(User invoker, Instant timestamp, String message, Long categoryId) {
        this.invoker = invoker;
        this.timestamp = timestamp;
        this.message = message;
        this.categoryId = categoryId;
    }

    @Override
    public User getInvoker() {
        return this.invoker;
    }

    @Override
    public Instant getTimestamp() {
        return this.timestamp;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public Long getCategoryId() {
        return this.categoryId;
    }
}
