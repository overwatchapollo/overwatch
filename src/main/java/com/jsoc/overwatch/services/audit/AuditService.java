package com.jsoc.overwatch.services.audit;

import com.jsoc.overwatch.services.audit.model.AuditCategory;
import com.jsoc.overwatch.services.audit.model.AuditRecord;
import com.jsoc.overwatch.services.audit.repository.AuditRepository;
import com.jsoc.overwatch.services.userManagement.CustomUserDetailsService;
import com.jsoc.overwatch.services.userManagement.UserService;
import one.util.streamex.StreamEx;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AuditService {

    private UserService detailsService;

    private AuditRepository auditRepository;

    public AuditService(UserService detailsService, AuditRepository auditRepository, CustomUserDetailsService service) {
        this.auditRepository = auditRepository;
        this.detailsService = detailsService;
    }

    public List<CategorisedAuditInfo> getAuditLog() {
        return new ArrayList<>();
    }

    public List<AuditableInfo> getAuditCategory(Long id) {
        return new ArrayList<>();
    }

    private CategorisedAuditInfo from(AuditableInfo info, Long categoryId) {
        return new BasicCategorisedAuditInfo(info.getInvoker(), info.getTimestamp(), info.getMessage(), categoryId);
    }

    public void addAuditLog(AuditableInfo info, AuditCategory category) {
        Long id = info.getInvoker() == null ? null : this.detailsService.getModelByUsername(info.getInvoker().getUsername()).getId();
        AuditRecord ar = AuditRecord.builder().invoker(id).timestamp(info.getTimestamp()).message(info.getMessage()).build();
        ar.setCategory(category);
        this.auditRepository.save(ar);
    }
}
