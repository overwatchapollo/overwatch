package com.jsoc.overwatch.services.audit.repository;

import com.jsoc.overwatch.services.audit.model.AuditRecord;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface AuditRepository extends CrudRepository<AuditRecord, Long> {
    Set<AuditRecord> findByCategory(Long id);
}
