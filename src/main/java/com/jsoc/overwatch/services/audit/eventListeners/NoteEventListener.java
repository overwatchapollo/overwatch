package com.jsoc.overwatch.services.audit.eventListeners;

import com.jsoc.overwatch.services.audit.AuditService;
import com.jsoc.overwatch.services.audit.AuditableInfoFactory;
import com.jsoc.overwatch.services.audit.repository.AuditCategoryRepository;
import com.jsoc.overwatch.services.playerNote.events.NoteAddEvent;
import com.jsoc.overwatch.services.playerNote.events.NoteDeleteEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class NoteEventListener extends AuditEventListener {
    NoteEventListener(AuditService auditService, AuditCategoryRepository repo) {
        super(auditService, repo);
    }

    @EventListener
    public void NoteAddListener(NoteAddEvent event) {
        String id = "organisation:" + event.getOrganisationId() + ":noteAdd";
        this.setCategory(id);
        this.auditService.addAuditLog(AuditableInfoFactory.from(event), this.auditCategoryMap.get(id));
    }

    @EventListener
    public void NoteDeleteListener(NoteDeleteEvent event) {
        String id = "organisation:" + event.getOrganisationId() + ":noteDelete";
        this.setCategory(id);
        this.auditService.addAuditLog(AuditableInfoFactory.from(event), this.auditCategoryMap.get(id));
    }
}
