package com.jsoc.overwatch.services.audit.eventListeners;

import com.jsoc.overwatch.services.audit.AuditService;
import com.jsoc.overwatch.services.audit.AuditableInfoFactory;
import com.jsoc.overwatch.services.audit.repository.AuditCategoryRepository;
import com.jsoc.overwatch.services.kickService.events.KickRequestEvent;
import com.jsoc.overwatch.services.kickService.events.PlayerWasKickedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class KickEventListeners extends AuditEventListener {
    KickEventListeners(AuditService auditService, AuditCategoryRepository repo) {
        super(auditService, repo);
    }
    @EventListener
    public void KickRequestListener(KickRequestEvent event) {
        String id = "server:" + event.getSessionId() + ":kickRequest";
        this.setCategory(id);
        this.auditService.addAuditLog(AuditableInfoFactory.from(event), this.auditCategoryMap.get(id));
    }

    @EventListener
    public void KickedEventListener(PlayerWasKickedEvent event) {
        String id = "server:" + event.getSessionId() + ":kick";
        this.setCategory(id);
        this.auditService.addAuditLog(AuditableInfoFactory.from(event), this.auditCategoryMap.get(id));
    }
}
