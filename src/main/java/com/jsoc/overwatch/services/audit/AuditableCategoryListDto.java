package com.jsoc.overwatch.services.audit;

import com.jsoc.overwatch.controller.dto.AuditableInfoDto;
import lombok.Data;

import java.util.List;

@Data
public class AuditableCategoryListDto {
    private List<AuditableInfoDto> info;
    private String categoryName;
    private Long categoryId;
    public AuditableCategoryListDto(Long categoryId, String categoryName, List<AuditableInfoDto> info) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.info = info;
    }
}
