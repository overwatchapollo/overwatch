package com.jsoc.overwatch.services.audit.eventListeners;

import com.jsoc.overwatch.config.FeatureToggleEvent;
import com.jsoc.overwatch.services.audit.AuditService;
import com.jsoc.overwatch.services.audit.AuditableInfoFactory;
import com.jsoc.overwatch.services.audit.ServerStartup;
import com.jsoc.overwatch.services.audit.repository.AuditCategoryRepository;
import com.jsoc.overwatch.services.userManagement.events.UserLoginEvent;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;

@Component
public class SystemEventListener extends AuditEventListener {
    SystemEventListener(AuditService auditService, AuditCategoryRepository repo) {
        super(auditService, repo);
        this.setCategory("system");
        this.setCategory("login");
    }

    @EventListener
    public void appReady(ApplicationReadyEvent event) {
        this.auditService.addAuditLog(new ServerStartup(), this.auditCategoryMap.get("system"));
    }

    @PreDestroy
    public void ServiceShutDown() {
        this.auditService.addAuditLog(AuditableInfoFactory.serverShutdown(), this.auditCategoryMap.get("system"));
    }

    @EventListener
    public void FeatureToggleEventListener(FeatureToggleEvent event) {
        this.auditService.addAuditLog(AuditableInfoFactory.from(event), this.auditCategoryMap.get("system"));
    }

    @EventListener
    public void LoginEventListener(UserLoginEvent event) {
        this.auditService.addAuditLog(AuditableInfoFactory.from(event), this.auditCategoryMap.get("login"));
    }

}
