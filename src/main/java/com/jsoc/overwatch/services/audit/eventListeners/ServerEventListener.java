package com.jsoc.overwatch.services.audit.eventListeners;

import com.jsoc.overwatch.services.audit.AuditService;
import com.jsoc.overwatch.services.audit.AuditableInfoFactory;
import com.jsoc.overwatch.services.audit.repository.AuditCategoryRepository;
import com.jsoc.overwatch.services.connection.events.ServerStateUpdateEvent;
import com.jsoc.overwatch.services.connection.events.SoftRestartRequest;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class ServerEventListener extends AuditEventListener {
    ServerEventListener(AuditService auditService, AuditCategoryRepository repo) {
        super(auditService, repo);
    }

    @EventListener
    @Transactional
    public void ServerStateUpdate(ServerStateUpdateEvent event) {
        String id = "server:" + event.getServerGuid() + ":state";
        this.setCategory(id);
        this.auditService.addAuditLog(AuditableInfoFactory.from(event), this.auditCategoryMap.get(id));
    }

    @EventListener
    public void serverRestartListener(SoftRestartRequest request) {
        String id = "server:" + request.getServerId() + ":restart";
        this.setCategory(id);
        this.auditService.addAuditLog(AuditableInfoFactory.from(request), this.auditCategoryMap.get(id));
    }
}
