package com.jsoc.overwatch.services.audit.eventListeners;

import com.jsoc.overwatch.services.audit.AuditService;
import com.jsoc.overwatch.services.audit.AuditableInfoFactory;
import com.jsoc.overwatch.services.audit.repository.AuditCategoryRepository;
import com.jsoc.overwatch.services.banService.events.BanRemovalRequestEvent;
import com.jsoc.overwatch.services.banService.events.BanRequestEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class BanAuditListener extends AuditEventListener {

    BanAuditListener(AuditService auditService, AuditCategoryRepository repo) {
        super(auditService, repo);
    }

    @EventListener
    public void BanEventListener(BanRequestEvent event) {
        String id = "server:" + event.getRequest().getServerLocation() + ":banRequest";
        this.setCategory(id);
        this.auditService.addAuditLog(AuditableInfoFactory.from(event), this.auditCategoryMap.get(id));
    }


    @EventListener
    public void BanRemovalEventListener(BanRemovalRequestEvent event) {
        String id = "server:" + event.getServerId() + ":banRemove";
        this.setCategory(id);
        this.auditService.addAuditLog(AuditableInfoFactory.from(event), this.auditCategoryMap.get(id));
    }
}
