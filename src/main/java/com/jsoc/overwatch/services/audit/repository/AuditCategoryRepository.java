package com.jsoc.overwatch.services.audit.repository;

import com.jsoc.overwatch.services.audit.model.AuditCategory;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AuditCategoryRepository extends CrudRepository<AuditCategory, Long> {
    Optional<AuditCategory> findByCategory(String category);
}
