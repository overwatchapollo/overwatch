package com.jsoc.overwatch.services.audit.eventListeners;

import com.jsoc.overwatch.services.audit.AuditService;
import com.jsoc.overwatch.services.audit.AuditableInfoFactory;
import com.jsoc.overwatch.services.audit.repository.AuditCategoryRepository;
import com.jsoc.overwatch.services.userManagement.events.UserRoleAssigned;
import com.jsoc.overwatch.services.userManagement.events.UserRoleRemoved;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class RoleEventListener extends AuditEventListener {
    RoleEventListener(AuditService auditService, AuditCategoryRepository repo) {
        super(auditService, repo);
        this.setCategory("roleAssign");
        this.setCategory("roleRemove");
    }

    @EventListener
    public void UserRoleRemovedListener(UserRoleRemoved event) {
        this.auditService.addAuditLog(AuditableInfoFactory.from(event), this.auditCategoryMap.get("roleRemove"));
    }

    @EventListener
    public void UserRoleAssignedListener(UserRoleAssigned event) {
        this.auditService.addAuditLog(AuditableInfoFactory.from(event), this.auditCategoryMap.get("roleAssign"));
    }
}
