package com.jsoc.overwatch.services.audit;

public interface CategorisedAuditInfo extends AuditableInfo {
    Long getCategoryId();
}
