package com.jsoc.overwatch.services.audit.eventListeners;

import com.jsoc.overwatch.services.audit.AuditService;
import com.jsoc.overwatch.services.audit.model.AuditCategory;
import com.jsoc.overwatch.services.audit.repository.AuditCategoryRepository;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public abstract class AuditEventListener {

    protected AuditService auditService;

    private AuditCategoryRepository repo;

    Map<String, AuditCategory> auditCategoryMap;

    AuditEventListener(AuditService auditService, AuditCategoryRepository repo) {
        this.auditService = auditService;
        this.repo = repo;
        this.auditCategoryMap = new HashMap<>();
    }

    final void setCategory(String category) {
        if (!this.auditCategoryMap.containsKey(category)) {
            this.auditCategoryMap.put(category, this.getOrCreate(category));
        }
    }


    @Transactional
    private AuditCategory getOrCreate(String category) {
        Optional<AuditCategory> maybeCategory = this.repo.findByCategory(category);
        if (maybeCategory.isPresent()) {
            return maybeCategory.get();
        } else {
            return this.repo.save(new AuditCategory(null, category));
        }
    }
}
