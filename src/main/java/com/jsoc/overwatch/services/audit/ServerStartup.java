package com.jsoc.overwatch.services.audit;

import org.springframework.security.core.userdetails.User;

import java.time.Instant;

public class ServerStartup implements AuditableInfo {

    private final Instant timestamp;

    public ServerStartup() {
        this.timestamp = Instant.now();
    }

    public User getInvoker() {
        return null;
    }

    public Instant getTimestamp() {
        return this.timestamp;
    }

    public String getMessage() {
        return "Server startup.";
    }

    public String getCategory() {
        return "system";
    }
}
