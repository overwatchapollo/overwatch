package com.jsoc.overwatch.services.audit;

import com.jsoc.overwatch.config.FeatureToggleEvent;
import com.jsoc.overwatch.services.audit.model.AuditRecord;
import com.jsoc.overwatch.services.connection.events.ServerStateUpdateEvent;
import com.jsoc.overwatch.services.connection.events.SoftRestartRequest;
import com.jsoc.overwatch.services.userManagement.CustomUserDetailsService;
import com.jsoc.overwatch.services.banService.events.BanRemovalRequestEvent;
import com.jsoc.overwatch.services.banService.events.BanRequestEvent;
import com.jsoc.overwatch.services.kickService.events.KickRequestEvent;
import com.jsoc.overwatch.services.kickService.events.PlayerWasKickedEvent;
import com.jsoc.overwatch.services.playerNote.events.NoteAddEvent;
import com.jsoc.overwatch.services.playerNote.events.NoteDeleteEvent;
import com.jsoc.overwatch.services.userManagement.events.UserLoginEvent;
import com.jsoc.overwatch.services.userManagement.events.UserRoleAssigned;
import com.jsoc.overwatch.services.userManagement.events.UserRoleRemoved;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.text.MessageFormat;
import java.time.Instant;

public class AuditableInfoFactory {

    private static String BAN_MESSAGE_FORMAT = "Player {0} has been banned by {1}: {2}";

    private static String UNBAN_MESSAGE_FORMAT = "Ban {0} has been lifted";

    private static String KICK_REQUEST_FORMAT = "Player {0} kick request: {1}";

    private static String KICK_MESSAGE_FORMAT = "Player {0} has been kicked by {1}: {2}";

    private static String LOGIN_MESSAGE_FORMAT = "User {0} has logged in.";

    private static String ROLE_REMOVAL_FORMAT = "User {0} has removed role {1} from {2}.";

    private static String ROLE_GRANT_FORMAT = "User {0} has granted role {1} to {2}.";

    private static String SERVER_STATE_FORMAT = "Server {0} connection status {1}.";

    private static String RESTART_FORMAT = "Server {0} has been soft restarted by {1}.";

    private static String FEATURE_TOGGLE = "Feature {0} has been set to {1} by {2}.";

    private static String NOTE_ADD = "User {0} has added a note to guid {1} with id {2}.";

    private static String NOTE_DELETE = "User {0} has removed note {1}.";

    public static AuditableInfo serverShutdown() {
        return new AuditableInfoBasic(null, Instant.now(), "Server shutdown commencing.");
    }

    public static AuditableInfo from(NoteAddEvent event) {
        String message = MessageFormat.format(NOTE_ADD, event.getUser().getUsername(), event.getTargetGuid(), event.getNoteId());
        return new AuditableInfoBasic(event.getUser(), Instant.ofEpochMilli(event.getTimestamp()), message);
    }

    public static AuditableInfo from(NoteDeleteEvent event) {
        String message = MessageFormat.format(NOTE_DELETE, event.getUser().getUsername(), event.getNoteId());
        return new AuditableInfoBasic(event.getUser(), Instant.ofEpochMilli(event.getTimestamp()), message);
    }

    public static AuditableInfo from(PlayerWasKickedEvent event) {
        String message = MessageFormat.format(KICK_MESSAGE_FORMAT, "", event.getRequesterId(), event.getMessage());
        return new AuditableInfoBasic(null, Instant.ofEpochMilli(event.getTimestamp()), message);
    }

    public static AuditableInfo from(FeatureToggleEvent event) {
        String message = MessageFormat.format(FEATURE_TOGGLE, event.getFeature(), event.isValue(), event.getCaller().getUsername());
        return new AuditableInfoBasic(event.getCaller(), Instant.ofEpochMilli(event.getTimestamp()), message);
    }


    public static AuditableInfo from(BanRemovalRequestEvent event) {
        String message = MessageFormat.format(UNBAN_MESSAGE_FORMAT, event.getId());
        return new AuditableInfoBasic(event.getRequester(), Instant.ofEpochMilli(event.getTimestamp()), message);
    }

    public static AuditableInfo from(BanRequestEvent event) {
        String message = MessageFormat.format(BAN_MESSAGE_FORMAT, event.getRequest().getPlayerGuid(), event.getRequester().getUsername(), event.getRequest().getReason());
        return new AuditableInfoBasic(null, Instant.ofEpochMilli(event.getTimestamp()), message);
    }

    public static AuditableInfo from(KickRequestEvent event) {
        String message = MessageFormat.format(KICK_REQUEST_FORMAT, event.getRequesterId(), event.getMessage());
        return new AuditableInfoBasic(null, Instant.ofEpochMilli(event.getTimestamp()), message);
    }

    public static AuditableInfo from(UserLoginEvent event) {
        String message = MessageFormat.format(LOGIN_MESSAGE_FORMAT, event.getUsername());
        return new AuditableInfoBasic(event.getUser(), Instant.ofEpochMilli(event.getTimestamp()), message);
    }

    public static AuditableInfo from(UserRoleRemoved event) {
        String message = MessageFormat.format(ROLE_REMOVAL_FORMAT, event.getCaller().getUsername(), event.getRoleId(), event.getUserId());
        return new AuditableInfoBasic(event.getCaller(), Instant.ofEpochMilli(event.getTimestamp()), message);
    }

    public static AuditableInfo from(UserRoleAssigned event) {
        String message = MessageFormat.format(ROLE_GRANT_FORMAT, event.getCaller().getUsername(), event.getRoleId(), event.getUserId());
        return new AuditableInfoBasic(event.getCaller(), Instant.ofEpochMilli(event.getTimestamp()), message);
    }

    public static AuditableInfo from(ServerStateUpdateEvent event) {
        String message = MessageFormat.format(SERVER_STATE_FORMAT, event.getServerName(), event.getStatus());
        return new AuditableInfoBasic(null, Instant.ofEpochMilli(event.getTimestamp()), message);
    }

    public static AuditableInfo from(SoftRestartRequest request) {
        String message = MessageFormat.format(RESTART_FORMAT, request.getServerId(), request.getInvoker().getUsername());
        return new AuditableInfoBasic(request.getInvoker(), Instant.ofEpochMilli(request.getTimestamp()), message);
    }

    public static AuditableInfo from(CustomUserDetailsService detailsService, AuditRecord record) {
        User user;
        try {
            user = record.getId() == null ? null : detailsService.loadUserById(record.getId());
        } catch (UsernameNotFoundException ex) {
            user = null;
        }

        return new AuditableInfoBasic(user, record.getTimestamp(), record.getMessage());
    }
}
