package com.jsoc.overwatch.services.organisationManagement.controllers.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrganisationDto {
    private Long id;
    private String name;
}
