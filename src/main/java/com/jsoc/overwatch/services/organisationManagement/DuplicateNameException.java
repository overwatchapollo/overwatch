package com.jsoc.overwatch.services.organisationManagement;

import lombok.Data;

@Data
public class DuplicateNameException extends Exception {
    private String name;
    public DuplicateNameException(String name) {
        this.name = name;
    }
}
