package com.jsoc.overwatch.services.organisationManagement.controllers.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrgMemberDto {
    private Long id;
    private String userName;
}
