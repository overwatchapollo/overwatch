package com.jsoc.overwatch.services.organisationManagement.controllers;

import com.jsoc.overwatch.Security.SecurityHelper;
import com.jsoc.overwatch.Security.permissions.OrganisationPermissionGenerator;
import com.jsoc.overwatch.Security.permissions.RolePermissionGenerator;
import com.jsoc.overwatch.controller.dto.*;
import com.jsoc.overwatch.services.organisationManagement.DuplicateNameException;
import com.jsoc.overwatch.services.organisationManagement.controllers.dto.*;
import com.jsoc.overwatch.services.connection.ServerNotFoundException;
import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import com.jsoc.overwatch.services.organisationManagement.OrganisationNotFoundException;
import com.jsoc.overwatch.services.organisationManagement.OrganisationService;
import com.jsoc.overwatch.services.organisationManagement.models.Organisation;
import com.jsoc.overwatch.services.userManagement.RoleNotFoundException;
import com.jsoc.overwatch.services.userManagement.RoleService;
import com.jsoc.overwatch.services.userManagement.UserNotFoundException;
import com.jsoc.overwatch.services.userManagement.controller.WholeRoleDto;
import com.jsoc.overwatch.services.userManagement.controller.WholeRoleDtoFactory;
import com.jsoc.overwatch.services.userManagement.model.RoleModel;
import com.jsoc.overwatch.services.userManagement.model.UserModel;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/groups")
@PreAuthorize("isAuthenticated()")
@Slf4j
public class OrganisationController {

    @Autowired
    private OrganisationService service;

    @Autowired
    private WholeRoleDtoFactory roleDtoFactory;

    @Autowired
    private RoleService roleService;

    private OrganisationPermissionGenerator orgGenerator = new OrganisationPermissionGenerator();

    private RolePermissionGenerator rolePermissionGenerator = new RolePermissionGenerator();

    @PostMapping("/group")
    public OrganisationDto create(HttpServletRequest request, @RequestBody OrganisationDto dto) throws DuplicateNameException {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        Organisation saved = this.service.createOrganisation(maybeUser, dto.getName());
        return this.from(saved);
    }

    @PostMapping("/group/{id}/role")
    public void createRoleOnGroup(HttpServletRequest request, @PathVariable("id") Long id, @RequestBody OrgRoleDto orgRole) throws RoleNotFoundException {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        Organisation org = this.service.getOrganisation(id).orElseThrow(NotFoundException::new);

        if (!this.orgGenerator.createRole(id).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        RoleModel role = this.roleService.createRole(maybeUser, orgRole.getName(), new HashSet<>());
        try {
            this.service.addRoleToOrganisation(org.getId(), role);
        } catch (OrganisationNotFoundException e) {
            throw new NotFoundException();
        }

        RoleModel admin = org.getAdministrationRole();
        if (admin != null) {
            this.roleService.addPermissionToRole(admin.getId(), this.rolePermissionGenerator.delete(role.getId()).getPermission());
            this.roleService.addPermissionToRole(admin.getId(), this.rolePermissionGenerator.addUser(role.getId()).getPermission());
            this.roleService.addPermissionToRole(admin.getId(), this.rolePermissionGenerator.removeUser(role.getId()).getPermission());
        }

    }

    @DeleteMapping("/group/{groupId}/role/{roleId}")
    public void deleteRoleFromGroup(HttpServletRequest request, @PathVariable("groupId") Long groupId, @PathVariable("roleId") Long roleId) throws OrganisationNotFoundException, RoleNotFoundException {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        Organisation org = this.service.getOrganisation(groupId).orElseThrow(NotFoundException::new);

        if (!this.orgGenerator.deleteRole(groupId).hasPermission(maybeUser)) {
            log.error("doesnt have permission to delete role on org");
            throw new ForbiddenException();
        }

        List<RoleModel> roles = org.getRoles().stream().filter(x -> Objects.equals(x.getId(), roleId)).collect(Collectors.toList());
        if (roles.size() == 0) {
            log.error("role is not in org");
            throw new NotFoundException();
        }

        if (org.getAdministrationRole() != null && Objects.equals(org.getAdministrationRole().getId(), roleId)) {
            log.error("role is admin org");
            throw new ForbiddenException();
        }

        this.service.removeRoleFromOrganisation(groupId, roleId);
        log.info("removed role from org");
        this.roleService.deleteRoleById(maybeUser, roleId);
        log.info("deleted role");
        this.roleService.deletePermissionFromRole(roleId, this.rolePermissionGenerator.delete(roleId).getPermission());
        log.info("deleted permission from role");
    }

    @DeleteMapping("/group/{id}")
    public void delete(HttpServletRequest request, @PathVariable("id") Long id) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.orgGenerator.delete(id).hasPermission(maybeUser)) {
            throw new UnauthorisedException();
        }

        this.service.deleteOrganisation(id);
    }

    @Transactional
    @GetMapping("")
    public List<FullOrgDto> getOrgs() {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        String username = maybeUser.getUsername();
        return StreamEx.of(this.service.getOrganisations())
            .filter(x -> this.orgGenerator.view(x.getId()).hasPermission(maybeUser) || x.getMembers().stream().map(y -> y.getUserName()).anyMatch(z -> z.compareTo(username) == 0))
            .map(this::from2)
            .toList();
    }

    private OrganisationDto from(Organisation org) {
        return OrganisationDto.builder().id(org.getId()).name(org.getName()).build();
    }

    private FullOrgDto from2(Organisation org) {
        FullOrgDto dto = new FullOrgDto();
        dto.setId(org.getId());
        dto.setName(org.getName());
        dto.setCreatedOn(org.getCreatedOn());
        if (org.getAdministrationRole() != null) {
            dto.setAdminRole(org.getAdministrationRole().getId());
        }

        dto.setPrivilegedGuids(org.getPlayerGuids().stream().map(x -> {
            OrgPrivilegedGuidDto priv = new OrgPrivilegedGuidDto();
            priv.setGuid(x.getGuid());
            priv.setId(x.getId());
            priv.setName(x.getName());
            priv.setServerId(x.getServer().getId());
            return priv;
        }).collect(Collectors.toSet()));

        Set<RoleModel> roles = org.getRoles();

        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);


        Set<PermissionsWrapper<WholeRoleDto>> permissionRoles = roles.stream().map(x -> {
            PermissionsWrapper<WholeRoleDto> wrapper = new PermissionsWrapper<>();
            wrapper.setData(this.roleDtoFactory.build(x));
            wrapper.setActions(this.roleService.getUserActions(maybeUser, x.getId()));
            return wrapper;
        }).collect(Collectors.toSet());
        dto.setRoles(permissionRoles);
        dto.setMembers(org.getMembers().stream().map(this::strip).collect(Collectors.toSet()));
        dto.setServers(new HashSet<>());
        return dto;
    }

    private OrgMemberDto strip(UserModel model) {
        OrgMemberDto out = new OrgMemberDto();
        out.setId(model.getId());
        out.setUserName(model.getUserName());
        return out;
    }

    @GetMapping("/group/{id}")
    public OrganisationDto getOrg(HttpServletRequest request, @PathVariable("id") Long id) throws OrganisationNotFoundException {
        return this.service.getOrganisation(id).map(this::from).orElseThrow(() -> new OrganisationNotFoundException(id));
    }

    @GetMapping("/group/{id}/members")
    public Set<UserDto> getMembers(HttpServletRequest request, @PathVariable("id") Long id) throws OrganisationNotFoundException {
        Organisation org = this.service.getOrganisation(id).orElseThrow(() -> new OrganisationNotFoundException(id));
        return StreamEx.of(org.getMembers()).map(UserDto::new).toSet();
    }

    @PostMapping("/group/{id}/members/{user}")
    public void addUserToGroup(HttpServletRequest request, @PathVariable("id") Long groupId, @PathVariable("user") Long userId) throws OrganisationNotFoundException, UserNotFoundException {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.orgGenerator.addUser(groupId).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        this.service.addUserToOrganisation(groupId, userId);
    }

    @DeleteMapping("/group/{id}/members/{user}")
    public void removeUserFromGroup(HttpServletRequest request, @PathVariable("id") Long groupId, @PathVariable("user") Long userId) throws OrganisationNotFoundException, UserNotFoundException {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.orgGenerator.removeUser(groupId).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        this.service.removeUserFromOrganisation(groupId, userId);
    }

    @GetMapping("/group/{groupId}/assets")
    public Collection<ServerConnection> getServersForGroup(HttpServletRequest request, @PathVariable("groupId") Long groupId) throws OrganisationNotFoundException {
        Organisation org = this.service.getOrganisation(groupId).orElseThrow(() -> new OrganisationNotFoundException(groupId));
        return org.getServers();
    }

    @PostMapping("/group/{groupId}/assets/{serverId}")
    public void addServerToGroup(HttpServletRequest request, @PathVariable("groupId") Long groupId, @PathVariable("serverId") Long serverId) throws OrganisationNotFoundException, ServerNotFoundException {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);

        boolean isGroupAdmin = StreamEx.of(maybeUser.getAuthorities())
            .map(GrantedAuthority::getAuthority).has("groupAdmin:" + groupId);

        boolean isServerAdmin = StreamEx.of(maybeUser.getAuthorities())
                .map(GrantedAuthority::getAuthority).has("testAdmin");

        if (!isGroupAdmin) {
            log.error("Not group admin");
            throw new UnauthorisedException();
        }

        if (!isServerAdmin) {
            log.error("Not server admin");
            throw new UnauthorisedException();
        }

        this.service.addServerToGroup(groupId, serverId);
    }

    @DeleteMapping("/group/{groupId}/assets/{serverId}")
    public void removeServerFromGroup(HttpServletRequest request, @PathVariable("groupId") Long groupId, @PathVariable("serverId") Long serverId) throws OrganisationNotFoundException, ServerNotFoundException {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);

        boolean isGroupAdmin = StreamEx.of(maybeUser.getAuthorities())
                .map(GrantedAuthority::getAuthority).has("groupAdmin:" + groupId);

        boolean isServerAdmin = StreamEx.of(maybeUser.getAuthorities())
                .map(GrantedAuthority::getAuthority).has("testAdmin");

        if (!isGroupAdmin) {
            log.error("Not group admin");
            throw new UnauthorisedException();
        }

        if (!isServerAdmin) {
            log.error("Not server admin");
            throw new UnauthorisedException();
        }

        this.service.removeServerFromGroup(groupId, serverId);
    }
}
