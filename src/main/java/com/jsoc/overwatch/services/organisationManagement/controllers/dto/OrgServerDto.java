package com.jsoc.overwatch.services.organisationManagement.controllers.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrgServerDto {
    private Long id;
    private String serverName;
    private String ipAddress;
    private Integer port;
    private Integer maxPlayers;
}
