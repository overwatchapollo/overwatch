package com.jsoc.overwatch.services.organisationManagement.repository;

import com.jsoc.overwatch.services.organisationManagement.models.Organisation;
import org.springframework.data.repository.CrudRepository;

public interface OrganisationRepository extends CrudRepository<Organisation, Long> {
}
