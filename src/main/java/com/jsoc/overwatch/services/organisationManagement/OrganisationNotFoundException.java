package com.jsoc.overwatch.services.organisationManagement;

import lombok.Data;

@Data
public class OrganisationNotFoundException extends Exception {
    private Long id;
    public OrganisationNotFoundException(Long id) {
        this.id = id;
    }
}
