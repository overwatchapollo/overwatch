package com.jsoc.overwatch.services.organisationManagement.models;

import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class OrganisationGuid {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "serverId")
    @ManyToOne(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    private ServerConnection server;

    private String name;

    private String guid;
}
