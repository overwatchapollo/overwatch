package com.jsoc.overwatch.services.organisationManagement.controllers.dto;

import lombok.Data;

@Data
public class OrgPrivilegedGuidDto {
    private Long id;
    private Long serverId;
    private String name;
    private String guid;
}
