package com.jsoc.overwatch.services.organisationManagement;

import com.google.common.collect.Sets;
import com.jsoc.overwatch.Security.permissions.OrganisationPermissionGenerator;
import com.jsoc.overwatch.Security.permissions.Permission;
import com.jsoc.overwatch.controller.dto.NotFoundException;
import com.jsoc.overwatch.services.connection.ServerNotFoundException;
import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import com.jsoc.overwatch.services.organisationManagement.models.Organisation;
import com.jsoc.overwatch.services.organisationManagement.repository.OrganisationRepository;
import com.jsoc.overwatch.services.serverRegister.ServerRegisterService;
import com.jsoc.overwatch.services.userManagement.CustomUserDetailsService;
import com.jsoc.overwatch.services.userManagement.RoleAssignmentService;
import com.jsoc.overwatch.services.userManagement.RoleService;
import com.jsoc.overwatch.services.userManagement.UserNotFoundException;
import com.jsoc.overwatch.services.userManagement.model.RoleModel;
import com.jsoc.overwatch.services.userManagement.model.UserModel;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service to manage organisations of players.
 */
@Service
public class OrganisationService {

    @Autowired
    private OrganisationRepository repo;

    @Autowired
    private CustomUserDetailsService details;

    @Autowired
    private RoleService roleService;

    @Autowired
    private RoleAssignmentService roleAssignmentService;

    @Autowired
    private ServerRegisterService registerService;

    private OrganisationPermissionGenerator orgGenerator = new OrganisationPermissionGenerator();

    public Organisation createOrganisation(User creator, String orgName) throws DuplicateNameException {
        UserModel um = this.details.getModelByUsername(creator.getUsername());
        return this.createOrganisation(um, orgName);
    }

    public Organisation createOrganisation(UserModel userModel, String orgName) throws DuplicateNameException {
        Organisation org = new Organisation();
        org.setName(orgName);
        org.setCreatedOn(Instant.now());

        User user = this.details.loadUserById(userModel.getId());

        try {
            Organisation saved = this.repo.save(org);
            RoleModel model = this.roleService.createRole("Group Admin " + orgName, this.getOrgAuthorities(saved.getId()));
            this.roleAssignmentService.mapUserToRole(user, model.getId());
            saved.getMembers().add(userModel);
            saved.getRoles().add(model);
            saved.setAdministrationRole(model);
            return this.repo.save(saved);
        } catch (Exception ex) {
            throw new DuplicateNameException(orgName);
        }
    }

    private Set<GrantedAuthority> getOrgAuthorities(Long orgId) {
        Set<Permission> permissions = new HashSet<>();
        permissions.add(this.orgGenerator.createRole(orgId));
        permissions.add(this.orgGenerator.deleteRole(orgId));
        permissions.add(this.orgGenerator.addUser(orgId));
        permissions.add(this.orgGenerator.removeUser(orgId));
        permissions.add(this.orgGenerator.delete(orgId));
        permissions.add(this.orgGenerator.view(orgId));

        return permissions.stream()
            .map(x -> new SimpleGrantedAuthority(x.getPermission()))
            .collect(Collectors.toSet());
    }

    public void deleteOrganisation(Long id) {
        Organisation org = this.repo.findById(id).orElseThrow(NotFoundException::new);
        this.repo.deleteById(id);
    }

    public List<Organisation> getOrganisations() {
        return StreamEx.of(this.repo.findAll().iterator()).toList();
    }

    public Optional<Organisation> getOrganisation(Long id) {
        return this.repo.findById(id);
    }

    public void addRoleToOrganisation(Long orgId, RoleModel role) throws OrganisationNotFoundException {
        Organisation org = this.repo.findById(orgId).orElseThrow(() -> new OrganisationNotFoundException(orgId));
        org.getRoles().add(role);
        this.repo.save(org);
    }

    public void removeRoleFromOrganisation(Long orgId, Long roleId) throws OrganisationNotFoundException {
        Organisation org = this.repo.findById(orgId).orElseThrow(() -> new OrganisationNotFoundException(orgId));
        org.setRoles(org.getRoles().stream().filter(x -> !Objects.equals(x.getId(), roleId)).collect(Collectors.toSet()));
        this.repo.save(org);
    }

    public void addUserToOrganisation(Long orgId, Long userId) throws OrganisationNotFoundException, UserNotFoundException {
        Organisation org = this.repo.findById(orgId).orElseThrow(() -> new OrganisationNotFoundException(orgId));
        UserModel um = this.details.getUserFromId(userId);
        org.getMembers().add(um);
        this.repo.save(org);
    }

    public void removeUserFromOrganisation(Long orgId, Long userId) throws OrganisationNotFoundException, UserNotFoundException {
        Organisation org = this.repo.findById(orgId).orElseThrow(() -> new OrganisationNotFoundException(orgId));
        UserModel um = this.details.getUserFromId(userId);
        org.getMembers().remove(um);
        this.repo.save(org);
    }

    public void addServerToGroup(Long orgId, Long serverId) throws OrganisationNotFoundException, ServerNotFoundException {
        ServerConnection con = this.registerService.getConfiguration(serverId).orElseThrow(() -> new ServerNotFoundException(serverId));
        Organisation org = this.repo.findById(orgId).orElseThrow(() -> new OrganisationNotFoundException(orgId));
        org.getServers().add(con);
        this.repo.save(org);
    }

    public void removeServerFromGroup(Long orgId, Long serverId) throws OrganisationNotFoundException, ServerNotFoundException {
        Organisation org = this.repo.findById(orgId).orElseThrow(() -> new OrganisationNotFoundException(orgId));
        ServerConnection con = StreamEx.of(org.getServers()).findFirst(x -> Objects.equals(x.getId(), serverId)).orElseThrow(() -> new ServerNotFoundException(serverId));
        org.getServers().remove(con);
        this.repo.save(org);
    }
}
