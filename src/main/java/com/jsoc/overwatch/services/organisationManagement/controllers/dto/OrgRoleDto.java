package com.jsoc.overwatch.services.organisationManagement.controllers.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OrgRoleDto {
    private String name;
}
