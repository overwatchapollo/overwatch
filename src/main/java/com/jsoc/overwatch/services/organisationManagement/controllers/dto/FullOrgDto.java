package com.jsoc.overwatch.services.organisationManagement.controllers.dto;

import com.jsoc.overwatch.controller.dto.PermissionsWrapper;
import com.jsoc.overwatch.services.userManagement.controller.WholeRoleDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FullOrgDto {
    private Long id;
    private String name;
    private Long adminRole;
    private Instant createdOn;
    private Set<OrgPrivilegedGuidDto> privilegedGuids;
    private Set<PermissionsWrapper<WholeRoleDto>> roles;
    private Set<OrgMemberDto> members;
    private Set<PermissionsWrapper<OrgServerDto>> servers;
}
