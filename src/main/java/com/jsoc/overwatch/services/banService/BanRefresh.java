package com.jsoc.overwatch.services.banService;

import com.jsoc.overwatch.services.banService.models.BanList;
import com.jsoc.overwatch.services.connection.CommandService;
import com.jsoc.overwatch.services.connection.api.CommandFactory;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CompletableFuture;

@Slf4j
public class BanRefresh {
    public static CompletableFuture<BanList> getBanList(CommandService service, Long serverId) {
        CommandFactory commandFactory = new CommandFactory();

        log.info("Doing ban list on server {}", serverId);

        CompletableFuture<BanList> banFuture = new CompletableFuture<>();

        CompletableFuture<CommandResponse> resp = service.runCommand(serverId, commandFactory.banList());
        resp.thenAccept(x -> {
            boolean success = x.isSuccess();
            if (success) {
                BanList banList = new BanList(serverId, x.getResponse());
                banFuture.complete(banList);
                log.info("Ban list successful on {}", serverId);
            } else {
                log.error("Failed to get ban list on {}", serverId);
            }
        });

        return banFuture;
    }
}
