package com.jsoc.overwatch.services.banService.events;

import com.jsoc.overwatch.controller.dto.BanRequest;
import lombok.Data;
import org.springframework.context.ApplicationEvent;
import org.springframework.security.core.userdetails.User;

@Data
public class BanRequestEvent extends ApplicationEvent {
    private final User requester;
    private final String id;
    private final BanRequest request;
    public BanRequestEvent(Object source, String banId, User requester, BanRequest request) {
        super(source);
        this.id = banId;
        this.requester = requester;
        this.request = request;
    }
}
