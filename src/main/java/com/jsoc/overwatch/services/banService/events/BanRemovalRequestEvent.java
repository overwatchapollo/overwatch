package com.jsoc.overwatch.services.banService.events;

import lombok.Builder;
import lombok.Data;
import org.springframework.context.ApplicationEvent;
import org.springframework.security.core.userdetails.User;

@Data
public class BanRemovalRequestEvent extends ApplicationEvent {
    private final User requester;
    private final Long id;
    private final Long serverId;
    public BanRemovalRequestEvent(Object source, Long serverId, Long banId, User requester) {
        super(source);
        this.id = banId;
        this.serverId = serverId;
        this.requester = requester;
    }
}
