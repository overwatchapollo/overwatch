/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.banService;

import java.time.Duration;
import java.time.Instant;
import java.util.UUID;
import lombok.Builder;
import lombok.Data;

/**
 * A request to ban a target player.
 */
@Data
@Builder
public class BanRequest {
    /**
     * Unique identifier of ban request.
     */
    private UUID id;
    
    /**
     * Server reference for where the ban should occur.
     */
    private String serverLocation;
    
    /**
     * Timestamp when ban request was received.
     */
    private Instant timestamp;
    
    /**
     * ID of user who is requesting ban.
     */
    private String requesterSessionId;
    
    /**
     * Requester GUID, most preferable identifier;
     */
    private String requesterSessionGuid;
    
    /**
     * ID of user who is target of ban.
     */
    private String targetSessionId;
    
    /**
     * GUID of target, most preferable identifier.
     */
    private String targetGuid;
    
    /**
     * Duration of ban.
     */
    private Duration duration;
    
    /**
     * Reason for ban.
     */
    private String reason;
}
