package com.jsoc.overwatch.services.banService;

import com.jsoc.overwatch.services.banService.events.BanListUpdatedEvent;
import com.jsoc.overwatch.services.banService.models.BanList;
import com.jsoc.overwatch.services.connection.CommandService;
import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import com.jsoc.overwatch.services.connection.events.ServerStateUpdateEvent;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.*;

@Service
@Slf4j
public class BanRefreshHandler {

    private static final int BAN_REFRESH_INITIAL = 30;

    private static final int BAN_REFRESH_PERIOD = 3600;

    private final ExecutorService executorService = Executors.newCachedThreadPool();

    private final Scheduler scheduler = Schedulers.from(this.executorService);

    @Autowired
    private BanCacheService service;

    @Autowired
    private CommandService commandService;

    @Autowired
    private ApplicationEventPublisher publisher;

    private final Map<Long, Disposable> banListRefreshMap = new ConcurrentHashMap<>();

    @EventListener
    public void eventListener(ServerStateUpdateEvent serverUpdate) {
        Long serverId = serverUpdate.getServerGuid();
        if (serverUpdate.getStatus() == ServerConnectionStatus.CONNECTED) {
            Disposable disp = this.createRefreshDisposable(serverId);
            this.banListRefreshMap.put(serverId, disp);
        } else {
            Disposable disp = this.banListRefreshMap.remove(serverId);
            if (disp != null) {
                disp.dispose();
            }
        }
    }

    public CompletableFuture<BanList> refreshBanList(Long serverId) {
        CompletableFuture<BanList> futureBanList = BanRefresh.getBanList(this.commandService, serverId);
        futureBanList.thenAccept(x -> this.service.setBanList(serverId, x));
        return futureBanList;
    }

    private Disposable createRefreshDisposable(Long serverId) {
        return this.scheduler.schedulePeriodicallyDirect(() -> {
            try {
                boolean success = this.updateBanList(serverId);
                if (success) {
                    this.publisher.publishEvent(new BanListUpdatedEvent(this, serverId));
                }
            } catch(Exception ex) {
                log.error("Failed to refresh ban list: {}", ex);
            }
        }, BAN_REFRESH_INITIAL, BAN_REFRESH_PERIOD, TimeUnit.SECONDS);
    }

    private boolean updateBanList(Long serverId) {
        CompletableFuture<BanList> futureBanList = this.refreshBanList(serverId);
        try {
            futureBanList.get();
        } catch (Exception ex) {
            log.error("Failed to refresh ban list {}", ex.getMessage());
            return false;
        }

        return true;
    }
}
