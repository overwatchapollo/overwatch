package com.jsoc.overwatch.services.banService.events;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class BanListStaleEvent extends ApplicationEvent {
    private Long serverId;
    public BanListStaleEvent(Object source, Long serverId) {
        super(source);
        this.serverId = serverId;
    }
}
