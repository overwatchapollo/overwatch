/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.banService;

import com.jsoc.overwatch.Security.SecurityHelper;
import com.jsoc.overwatch.Security.permissions.ServerPermissionGenerator;
import com.jsoc.overwatch.controller.dto.BanRequest;
import com.jsoc.overwatch.controller.dto.ForbiddenException;
import com.jsoc.overwatch.controller.dto.NotFoundException;
import com.jsoc.overwatch.controller.dto.UnauthorisedException;
import com.jsoc.overwatch.services.banService.events.BanEditEvent;
import com.jsoc.overwatch.services.banService.events.BanListUpdatedEvent;
import com.jsoc.overwatch.services.banService.events.BanRemovalRequestEvent;
import com.jsoc.overwatch.services.banService.models.BanList;
import com.jsoc.overwatch.services.banService.models.BanRecord;
import com.jsoc.overwatch.services.connection.CommandService;
import com.jsoc.overwatch.services.connection.ServerNotFoundException;
import com.jsoc.overwatch.services.connection.api.Command;
import com.jsoc.overwatch.services.connection.api.CommandFactory;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 * Service to handle bans.
 */
@Service
@Slf4j
public class BanCacheService {

    /**
     * Event publisher.
     */
    private ApplicationEventPublisher publisher;

    @Autowired
    private BanManager banManager;

    /**
     * Factory to build RCon commands.
     */
    private CommandFactory commandFactory = new CommandFactory();

    @Autowired
    private CommandService commandService;

    /**
     * Ban list cache.
     */
    private final Map<Long, BanList> banListCache = new HashMap<>();

    private ServerPermissionGenerator generator = new ServerPermissionGenerator();

    @Autowired
    public BanCacheService(ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    @EventListener
    public void editBanEvent(BanEditEvent event) {
        BanList bl = this.getBanList(event.getBanId()).orElseThrow(NotFoundException::new);
        BanRecord br = StreamEx.of(bl.getBanRecords()).findFirst(x -> Objects.equals(x.getId(), event.getBanId())).orElseThrow(NotFoundException::new);

        BanRequest newBanRequest = new BanRequest();

        newBanRequest.setServerLocation(event.getBanRequest().getServerLocation());
        newBanRequest.setPlayerGuid(br.getGuid());
        newBanRequest.setDuration(event.getBanRequest().getDuration() == null
            ? br.getDuration().toMinutes()
            : event.getBanRequest().getDuration());

        if (event.getBanRequest().getDuration() != null) {
            br.setDuration(Duration.ofMinutes(event.getBanRequest().getDuration()));
        }

        if (event.getBanRequest().getReason() != null) {
            br.setMessage(event.getBanRequest().getReason());
        }

        this.banManager.createBan(event.getUser(), newBanRequest);
        this.removeBan(event.getBanRequest().getServerLocation(), event.getBanId(), event.getUser());
    }

    public void setBanList(Long serverMrid, BanList banList) {
        this.banListCache.put(serverMrid, banList);
    }

    public Optional<BanList> getBanList(Long serverMrid) {
        return Optional.ofNullable(this.banListCache.get(serverMrid));
    }

    public void removeBan(Long serverId, Long banId, User requester) {
        if (!this.generator.banRemove(serverId).hasPermission(requester)) {
            log.warn("Unauthorised user {} tried to remove ban {} on server {}", requester.getUsername(), banId, serverId);
            throw new ForbiddenException();
        }

        this.publisher.publishEvent(new BanRemovalRequestEvent(this, serverId, banId, requester));

        BanList banList = this.getBanList(serverId).orElseThrow(() -> new IllegalArgumentException("No ban list for server " + serverId));
        banList.getBanForId(banId).orElseThrow(() -> new IllegalArgumentException("ID " + banId + " is not banned on server " + serverId));

        Command command = this.commandFactory.unban(banId.toString());
        log.info("Remove ban request {}", command.getCommand());

        this.commandService.runCommand(serverId, command).thenAccept(x -> {
            boolean success = x.isSuccess();
            if (success) {
                log.info("Ban removal successful, refreshing ban list on server {}", serverId);
                CompletableFuture<BanList> banFuture = BanRefresh.getBanList(this.commandService, serverId);
                try {
                    BanList newBanList = banFuture.get();
                    this.setBanList(serverId, newBanList);
                } catch (Exception ex) {
                    log.error("Failed to refresh ban list after refresh: {}", ex.getMessage());
                }
                this.publisher.publishEvent(new BanListUpdatedEvent(this, serverId));
            } else {
                log.error("Tried to unban {} on {} but failed", banId, serverId);
            }
        });
    }
}
