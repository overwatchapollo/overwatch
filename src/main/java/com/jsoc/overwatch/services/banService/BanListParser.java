package com.jsoc.overwatch.services.banService;

import com.jsoc.overwatch.services.banService.models.BanRecord;

import java.time.Duration;
import java.time.Period;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BanListParser {
    /*
    1242 0067aa016ae9bab1f19263ef1cb830f1 perm Recruiting (Unit ban) Perm (Permanent) [Sjanten]
    1243 34ffe3eec5422f63b4c1d2941336df80 perm Recruiting (Unit ban) Perm (Permanent) [Sjanten]
    1244 04170819a466adbaf19bc7a9d9a21480 perm Recruiting (Unit ban) Perm (Permanent) [Sjanten]
    1245 6b07e3a5642d364ed6fdc45d4d0371a0 perm Recruiting (Unit ban) Perm (Permanent) [Sjanten]
    1246 9ed5442caf2cb8d49f17826f5f0fefe0 perm Recruiting (Unit ban) Perm (Permanent) [Sjanten]
     */

    private static final String banListPattern = "^(\\d+) (\\S+) (\\S+) (.+)$";


    public static List<BanRecord> parseBanList(String rawBanList) {
        List<BanRecord> banList = new CopyOnWriteArrayList<>();
        String[] lines = rawBanList.split("\n");
        Pattern pattern = Pattern.compile(banListPattern);
        for(String line : lines) {
            Matcher matcher = pattern.matcher(line);
            if (matcher.find()) {
                BanRecord br = BanRecord.builder()
                    .id(Long.parseLong(matcher.group(1)))
                    .guid(matcher.group(2))
                    .message(matcher.group(4))
                    .duration(transform(matcher.group(3)))
                    .build();
                banList.add(br);
            }
        }
        return banList;
    }

    private static Duration transform(String durationValue) {
        if (Objects.equals(durationValue, "perm")) {
            Period p = Period.ofYears(100);

            return Duration.ofDays(p.getDays());
        } else {
            try {
                return Duration.ofMinutes(Long.parseLong(durationValue));
            } catch (NumberFormatException ex) {
                return Duration.ZERO;
            }

        }
    }
}
