package com.jsoc.overwatch.services.banService.controllers.dto;

import com.jsoc.overwatch.services.banService.models.BanRecord;
import lombok.Data;

import java.util.List;

@Data
public class BanListDto {
    private Long serverMrid;
    private int totalBans;
    private List<BanRecord> banRecords;
}
