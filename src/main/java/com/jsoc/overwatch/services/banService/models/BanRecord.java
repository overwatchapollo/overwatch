package com.jsoc.overwatch.services.banService.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Duration;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class BanRecord {
    private Long id;
    private String guid;
    private Duration duration;
    private String message;
}
