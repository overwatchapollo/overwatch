package com.jsoc.overwatch.services.banService.events;

import lombok.Data;
import org.springframework.context.ApplicationEvent;

@Data
public class BanListUpdatedEvent extends ApplicationEvent {
    private final Long serverMrid;
    public BanListUpdatedEvent(Object source, Long serverMrid) {
        super(source);
        this.serverMrid = serverMrid;
    }
}
