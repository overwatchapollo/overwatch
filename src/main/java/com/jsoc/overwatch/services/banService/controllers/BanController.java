package com.jsoc.overwatch.services.banService.controllers;

import com.jsoc.overwatch.Security.permissions.ServerPermissionGenerator;
import com.jsoc.overwatch.controller.dto.BanRequest;
import com.jsoc.overwatch.controller.dto.ForbiddenException;
import com.jsoc.overwatch.controller.dto.UnauthorisedException;
import com.jsoc.overwatch.controller.dto.UnbanRequest;
import com.jsoc.overwatch.services.banService.*;
import com.jsoc.overwatch.services.banService.controllers.dto.BanListDto;
import com.jsoc.overwatch.services.banService.events.BanEditEvent;
import com.jsoc.overwatch.services.banService.models.BanList;
import com.jsoc.overwatch.services.banService.models.BanRecord;
import com.jsoc.overwatch.services.connection.ServerNotFoundException;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.jsoc.overwatch.Security.SecurityHelper.getUserFromRequest;

@RestController
@RequestMapping("/api/rcon")
@PreAuthorize("isAuthenticated()")
@Slf4j
public class BanController {

    private static final long defaultMaxBanList = 20;

    @Autowired
    private BanCacheService banCacheService;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private BanManager banManager;

    @Autowired
    private BanRefreshHandler banRefreshHandler;

    private ServerPermissionGenerator generator = new ServerPermissionGenerator();

    @GetMapping("/banList/{serverMrid}")
    public BanListDto getBanList(HttpServletRequest request, @PathVariable("serverMrid") Long mrid, @RequestParam(name = "limit", required=false) Long limit, @PathParam(value="skip") Long skipRequest, @RequestParam(value = "guid", required = false) String guid) throws BanListNotFoundException {
        User maybeUser = getUserFromRequest().orElseThrow(UnauthorisedException::new);

        if (!this.generator.playerLookup(mrid).hasPermission(maybeUser)) {
            throw new UnauthorisedException();
        }

        Optional<BanList> maybeBans = this.banCacheService.getBanList(mrid);
        if (maybeBans.isPresent()) {
            BanList banList = maybeBans.get();
            long banSize = limit == null ? defaultMaxBanList : limit;
            long skipFromEnd = Math.min(banList.getBanRecords().size(), banSize);
            long skip = skipRequest == null ? banList.getBanRecords().size() - skipFromEnd : skipRequest;
            BanListDto outputDto = new BanListDto();
            outputDto.setServerMrid(banList.getServerMrid());
            List<BanRecord> banRecords = banList.getBanRecords();
            StreamEx<BanRecord> recordStream = StreamEx.of(banRecords).skip(skip);
            outputDto.setTotalBans(banRecords.size());
            if (guid == null) {
                outputDto.setBanRecords(recordStream.limit(banSize).toList());
            } else {
                outputDto.setBanRecords(recordStream.filter(x -> Objects.equals(x.getGuid(), guid)).limit(banSize).toList());
            }
            return outputDto;
        } else {
            throw new BanListNotFoundException(mrid);
        }
    }

    @PutMapping("/ban/{banId}")
    public void editBan(HttpServletRequest request, @PathVariable("banId") Long banId, @RequestBody BanRequest banRequest) throws ServerNotFoundException {
        checkNotNull(banId);
        checkNotNull(banRequest);
        checkNotNull(banRequest.getServerLocation());

        User maybeUser = getUserFromRequest().orElseThrow(UnauthorisedException::new);

        this.banCacheService.getBanList(banId).orElseThrow(() -> new ServerNotFoundException(banRequest.getServerLocation()));

        if (!this.generator.banEdit(banRequest.getServerLocation()).hasPermission(maybeUser)) {
            throw new UnauthorisedException();
        }

        this.publisher.publishEvent(new BanEditEvent(this, maybeUser, banId, banRequest));
    }


    @PostMapping("/banRefresh/{serverMrid}")
    public void refreshBanList(HttpServletRequest request, @PathVariable("serverMrid") Long serverMrid) {
        User maybeUser = getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.banRefresh(serverMrid).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }
        try {
            this.banRefreshHandler.refreshBanList(serverMrid).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    @PostMapping("/banRemove")
    public void removeBan(HttpServletRequest request, HttpServletResponse response, @RequestBody UnbanRequest unbanRequest) throws IOException, ServerNotFoundException {
        User maybeUser = getUserFromRequest().orElseThrow(UnauthorisedException::new);

        if (!this.generator.banRemove(unbanRequest.getServerLocation()).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        try {
            this.banCacheService.removeBan(unbanRequest.getServerLocation(), unbanRequest.getBanId(), maybeUser);
        } catch (IllegalArgumentException ex) {
            response.sendError(400);
        }
    }

    @PostMapping("/ban")
    public void submitBan(HttpServletRequest request, @RequestBody BanRequest banRequest) {
        User maybeUser = getUserFromRequest().orElseThrow(UnauthorisedException::new);
        this.banManager.createBan(maybeUser, banRequest);
    }

}
