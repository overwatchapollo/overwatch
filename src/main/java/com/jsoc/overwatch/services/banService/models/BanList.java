package com.jsoc.overwatch.services.banService.models;

import com.jsoc.overwatch.services.banService.BanListParser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import one.util.streamex.StreamEx;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BanList {
    private Long serverMrid;
    private List<BanRecord> banRecords;

    public BanList(Long serverMrid, String rawBanList) {
        this.serverMrid = serverMrid;
        this.banRecords = BanListParser.parseBanList(rawBanList);
    }

    public Collection<BanRecord> getBanForGuid(String guid) {
        return StreamEx.of(banRecords).filter(x -> Objects.equals(guid, x.getGuid())).toList();
    }

    public Optional<BanRecord> getBanForId(Long banId) {
        return StreamEx.of(banRecords).findFirst(x -> Objects.equals(banId, x.getId()));
    }
}
