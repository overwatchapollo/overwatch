package com.jsoc.overwatch.services.banService;

import com.jsoc.overwatch.Security.permissions.Permission;
import com.jsoc.overwatch.Security.permissions.ServerPermissionGenerator;
import com.jsoc.overwatch.controller.dto.ForbiddenException;
import com.jsoc.overwatch.controller.dto.UnauthorisedException;
import com.jsoc.overwatch.services.banService.events.BanRequestEvent;
import com.jsoc.overwatch.services.connection.CommandService;
import com.jsoc.overwatch.services.connection.api.Command;
import com.jsoc.overwatch.services.connection.api.CommandFactory;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import com.jsoc.overwatch.services.playerList.ConnectedPlayerService;
import com.jsoc.overwatch.services.playerList.UnknownServerIdException;
import com.jsoc.overwatch.services.privilegedGuids.PrivilegedGuidService;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.CompletableFuture;

@Service
@Slf4j
public class BanManager {

    /**
     * Event publisher.
     */
    @Autowired
    private ApplicationEventPublisher publisher;

    /**
     * Player service to ask who is connected.
     */
    @Autowired
    private ConnectedPlayerService playerService;

    @Autowired
    private CommandService commandService;

    @Autowired
    private PrivilegedGuidService privilegedGuidService;

    /**
     * Factory to build RCon commands.
     */
    private CommandFactory commandFactory = new CommandFactory();

    private ServerPermissionGenerator generator = new ServerPermissionGenerator();

    /**
     * Issue a ban to a server.
     * @param requester User issuing ban, must have permission.
     * @param request Ban request.
     */
    public boolean createBan(User requester, com.jsoc.overwatch.controller.dto.BanRequest request) {
        Long serverId = request.getServerLocation();
        String playerGuid = request.getPlayerGuid();
        Long duration = request.getDuration();
        String reason = request.getReason();

        this.assertBanAuthorised(requester, serverId, duration);

        this.assertPrivilegeAuthorised(requester, serverId, playerGuid);

        String banId = UUID.randomUUID().toString();
        this.publisher.publishEvent(new BanRequestEvent(this, banId, requester, request));

        boolean success = this.writeBan(serverId, playerGuid, duration, reason);
        if (success) {
            log.info("Ban for guid {} from user {} successful.", playerGuid, requester.getUsername());
        } else {
            log.info("Ban for guid {} from user {} not successful.", playerGuid, requester.getUsername());
        }
        return success;
    }

    /**
     * Perform ban on server.
     * @param serverId ServerID to ban on.
     * @param playerGuid GUID of player.
     * @param duration Duration of ban.
     * @param reason Reason for ban.
     * @return True if successful.
     */
    private boolean writeBan(Long serverId, String playerGuid, Long duration, String reason) {
        Collection<ConnectedPlayer> players = this.getPlayerList(serverId);
        Optional<ConnectedPlayer> maybePlayer = StreamEx.of(players)
                .findFirst(x -> Objects.equals(x.getGuid(), playerGuid) && x.isConnected());

        Command command = maybePlayer
            .map(x -> this.commandFactory.ban(Long.parseLong(x.getSessionId()), Duration.ofMinutes(duration), reason))
            .orElse(this.commandFactory.addban(playerGuid, Duration.ofMinutes(duration), reason));

        CompletableFuture<CommandResponse> future = this.commandService.runCommand(serverId, command);
        try {
            return future.get().isSuccess();
        } catch(Exception ex) {
            return false;
        }
    }

    /**
     * Assert that if the guid is privileged then the requester has authorisation to ban.
     * @param requester Ban requester.
     * @param serverId Server ID of ban.
     * @param playerGuid Target GUID of ban.
     */
    private void assertPrivilegeAuthorised(User requester, Long serverId, String playerGuid) {
        boolean isPrivileged = this.privilegedGuidService.isPrivileged(serverId, playerGuid);
        if (isPrivileged && !this.generator.banPrivileged(serverId).hasPermission(requester)) {
            log.error("{} is not authorised to ban a privileged person", requester.getUsername());
            throw new ForbiddenException();
        }
    }

    /**
     * Assert that the requester is authorised to perform a ban on the server.
     * @param requester Ban requester.
     * @param serverId Server ID of ban.
     * @param duration Duration of ban.
     */
    private void assertBanAuthorised(User requester, Long serverId, Long duration) {
        if (!this.banAuthorised(requester, serverId, duration)) {
            log.error("{} is not authorised to ban on {} for {} seconds", requester.getUsername(), serverId, duration);
            throw new UnauthorisedException();
        }
    }

    private boolean banAuthorised(User requester, Long serverId, Long duration) {
        Permission banPerm = this.generator.ban(serverId);
        Permission ban1w = this.generator.ban1w(serverId);
        Permission ban2w = this.generator.ban2w(serverId);

        if (duration >  20160 && !banPerm.hasPermission(requester)) {
            return false;
        } else if (duration > 10080 && !banPerm.hasPermission(requester) && !ban2w.hasPermission(requester)) {
            return false;
        } else {
            return banPerm.hasPermission(requester) || ban2w.hasPermission(requester) || ban1w.hasPermission(requester);
        }
    }

    private Collection<ConnectedPlayer> getPlayerList(Long serverId) {
        try {
            return this.playerService.getPlayers(serverId);
        } catch (UnknownServerIdException ex) {
            return Collections.emptyList();
        }
    }
}
