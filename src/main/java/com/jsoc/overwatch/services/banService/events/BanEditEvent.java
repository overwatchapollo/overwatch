package com.jsoc.overwatch.services.banService.events;

import com.jsoc.overwatch.controller.dto.BanRequest;
import lombok.Data;
import org.springframework.context.ApplicationEvent;
import org.springframework.security.core.userdetails.User;

@Data
public class BanEditEvent extends ApplicationEvent {
    private User user;
    private Long banId;
    private BanRequest banRequest;
    public BanEditEvent(Object source, User user, Long banId, BanRequest request) {
        super(source);
        this.user = user;
        this.banId = banId;
        this.banRequest = request;
    }
}
