package com.jsoc.overwatch.services.banService;

import lombok.Data;

@Data
public class BanListNotFoundException extends Exception {
    private Long serverId;
    public BanListNotFoundException(Long serverId) {
        this.serverId = serverId;
    }
}
