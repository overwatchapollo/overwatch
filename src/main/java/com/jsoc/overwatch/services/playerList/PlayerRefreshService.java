package com.jsoc.overwatch.services.playerList;

import com.jsoc.overwatch.services.connection.CommandService;
import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import com.jsoc.overwatch.services.connection.events.ServerStateUpdateEvent;
import com.jsoc.overwatch.services.playerList.events.CurrentPlayerListEvent;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class PlayerRefreshService {

    private static final int PLAYER_REFRESH_INITIAL = 1;

    private static final int PLAYER_REFRESH_PERIOD = 30;

    @Autowired
    private ApplicationEventPublisher publisher;

    private final Map<Long, Disposable> playerRefreshMap = new ConcurrentHashMap<>();

    private final ExecutorService executorService = Executors.newCachedThreadPool();

    private final Scheduler scheduler = Schedulers.from(this.executorService);

    private final PlayerListProxy proxy;


    public PlayerRefreshService(CommandService commandService) {
        this.proxy = new PlayerListProxy(commandService);
    }

    @EventListener
    public void eventListener(ServerStateUpdateEvent serverUpdate) {
        Long id = serverUpdate.getServerGuid();
        ServerConnectionStatus status = serverUpdate.getStatus();
        if (status == ServerConnectionStatus.CONNECTED) {
            Disposable disp = this.createRefreshDisposable(id);
            this.playerRefreshMap.put(id, disp);
        } else {
            Disposable disp = this.playerRefreshMap.remove(id);
            if (disp != null) {
                disp.dispose();
            }
        }
    }

    private Disposable createRefreshDisposable(Long serverId) {
        return this.scheduler.schedulePeriodicallyDirect(() -> {
            try {
                Collection<ConnectedPlayer> players = proxy.loadPlayerFromRcon(serverId);
                CurrentPlayerListEvent event = new CurrentPlayerListEvent(this, serverId, players);
                this.publisher.publishEvent(event);
            } catch(Exception ex) {
                log.error("Failed to refresh player list: {}", ex);
            }
        }, PLAYER_REFRESH_INITIAL, PLAYER_REFRESH_PERIOD, TimeUnit.SECONDS);
    }
}
