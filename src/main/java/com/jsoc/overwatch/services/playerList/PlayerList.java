/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.playerList;

import com.jsoc.overwatch.services.playerList.events.PlayerChatEvent;
import com.jsoc.overwatch.services.playerList.events.PlayerChatEventType;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * In-memory, cached representation of current online users.
 */
@Slf4j
public class PlayerList implements IPlayerEventConsumer{
    /**
     * Map of SessionId and ConnectedPlayer.
     */
    private final Map<String, ConnectedPlayer> connectedPlayers = new ConcurrentHashMap<>();

    /**
     * Unique ID for server which this player list represents.
     */
    private final Long serverMrid;
    private final Subject<ConnectedPlayer> playerSubject = PublishSubject.create();

    private boolean archive = false;
    private int maxSessionId = 0;

    public PlayerList(Long serverMrid) {
        this.serverMrid = serverMrid;
    }

    public void refresh(Collection<ConnectedPlayer> players) throws ArchievedPlayerListException {
        if (this.archive) {
            throw new ArchievedPlayerListException("Player list archived.");
        }

        log.debug("Found {} players currently on server {}", players.size(), this.serverMrid);
        this.setPlayerListState(players);
    }

    @Override
    public Observable<ConnectedPlayer> playerObservable() {
        return this.playerSubject;
    }

    private void setPlayerListState(Collection<ConnectedPlayer> players) throws ArchievedPlayerListException {
        synchronized (this.connectedPlayers) {
            for (ConnectedPlayer cp : players) {
                cp.setServerId(this.serverMrid);
                Optional<ConnectedPlayer> optPlayer = Optional.ofNullable(this.connectedPlayers.getOrDefault(cp.getSessionId(), null));
                if (optPlayer.isEmpty()) {
                    log.trace("Adding {}", cp.getName());
                    this.connectedPlayers.put(cp.getSessionId(), cp);
                } else {
                    ConnectedPlayer storedPlayer = optPlayer.get();
                    if (storedPlayer.getGuid() == null || storedPlayer.getGuid().contains(cp.getGuid())) {
                        log.trace("Updating {}", cp.getName());
                        this.connectedPlayers.replace(cp.getSessionId(), cp);
                    } else {
                        log.debug("Player list on server {} has been archived because of ({}|{}).", this.serverMrid, cp.getGuid(), storedPlayer.getGuid());
                        throw new ArchievedPlayerListException("Player list has just been archived.");
                    }
                }
            }

            List<String> playerIds = StreamEx.of(players).map(ConnectedPlayer::getSessionId).toList();
            List<String> stalePlayers = StreamEx.of(this.connectedPlayers.keySet())
                    .filter(x -> !playerIds.contains(x))
                    .toList();
            stalePlayers.forEach(x -> {
                log.debug("Player {} on server {} is missing from refresh, setting as offline.", x, this.serverMrid);
                connectedPlayers.get(x).setConnected(false);
            });

            Optional<Integer> maxRefreshId = StreamEx.of(players).map(x -> Integer.parseInt(x.getSessionId())).max(Integer::compareTo);
            maxRefreshId.ifPresent(x -> this.maxSessionId = Math.max(this.maxSessionId, x));
        }
    }

    void setArchive() {
        this.archive = true;
        this.connectedPlayers.values().forEach(x -> x.setConnected(false));
        this.playerSubject.onComplete();
    }
    
    private void playerConnected(ConnectedPlayer cp) {
        log.trace("Player {} joined", cp.getName());
        synchronized(this.connectedPlayers) {
            if (this.connectedPlayers.containsKey(cp.getSessionId())) {
                log.debug("Received connection message for session {} on server {} but was already listed.", cp.getSessionId(), cp.getServerId());
                ConnectedPlayer existing = this.connectedPlayers.get(cp.getSessionId());
                existing.setIpAddress(cp.getIpAddress());
            } else {
                this.connectedPlayers.put(cp.getSessionId(), cp);
            }

            this.broadcast(cp);
        }
    }

    private void playerGuid(ConnectedPlayer update) {
        synchronized(this.connectedPlayers) {
            ConnectedPlayer cp = this.connectedPlayers.getOrDefault(update.getSessionId(), null);
            if (cp == null) {
                log.debug("GUID {} for player {} in slot {} was recorded but player not in list.", update.getGuid(), update.getName(), update.getSessionId());
                this.playerConnected(update);
                cp = update;
            }
            cp.setGuid(update.getGuid());
            this.broadcast(cp);
        }
    }

    private void playerRemoved(ConnectedPlayer update, boolean kicked, boolean banned, String removalMessage) {
        synchronized(this.connectedPlayers) {
            ConnectedPlayer cp = this.connectedPlayers.getOrDefault(update.getSessionId(), null);
            if (cp == null) {
                log.warn("Player {} in slot {} disconnected but not connected. Player list may be stale..", update.getName(), update.getSessionId());
            } else {
                cp.setConnected(false);
                cp.setKicked(kicked);
                cp.setBanned(banned);
                cp.setDisconnectionMessage(removalMessage);
                this.broadcast(cp);
            }
        }
    }

    @Override
    public boolean isArchive() {
        return this.archive;
    }

    @Override
    public Collection<ConnectedPlayer> getPlayers() {
        return this.connectedPlayers.values();
    }

    private void broadcast(ConnectedPlayer cp) {
        this.playerSubject.onNext(cp);
    }

    @Override
    public void onPlayerChatEvent(PlayerChatEvent playerChatEvent) throws OutOfOrderPlayerConnectionException, ArchievedPlayerListException {
        if (this.archive) {
            throw new ArchievedPlayerListException("Player list archived.");
        }

        PlayerChatEventType type = playerChatEvent.getType();
        ConnectedPlayer pl = playerChatEvent.getConnectedPlayer();
        switch (type) {
            case CONNECTED:
                int id = Integer.parseInt(playerChatEvent.getConnectedPlayer().getSessionId());
                log.debug("CON " + id);
                if (id < (this.maxSessionId - 5)) {
                    log.warn("Player list is probably needing archived.");
                    throw new OutOfOrderPlayerConnectionException("Connected Player overwrites existing player.");
                }
                this.maxSessionId = Math.max(this.maxSessionId, id);
                this.playerConnected(pl);
                break;
            case GUID_VERIFIED:
                this.playerGuid(pl);
                break;
            case DISCONNECTED:
                this.playerRemoved(pl, false, false, "disconnected");
                break;
            case KICKED:
                this.playerRemoved(pl, true, false, "kicked");
                break;
            case BANNED:
                this.playerRemoved(pl, true, true, "banned");
                break;
        }
    }
}
