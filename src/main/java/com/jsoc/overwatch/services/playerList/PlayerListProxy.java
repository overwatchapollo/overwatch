package com.jsoc.overwatch.services.playerList;

import com.jsoc.overwatch.services.connection.CommandService;
import com.jsoc.overwatch.services.connection.games.arma.IRconConnection;
import com.jsoc.overwatch.services.connection.api.Command;
import com.jsoc.overwatch.services.connection.api.CommandFactory;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
class PlayerListProxy {
    private final CommandFactory commandFactory = new CommandFactory();
    private static final Pattern playerPattern = Pattern.compile("(\\d+)\\s+([\\d\\.]+):(\\d+)\\s+(\\d+)\\s+(\\S+)\\s(.+)");

    private CommandService commandService;

    public PlayerListProxy(CommandService commandService) {
        this.commandService = commandService;
    }

    List<ConnectedPlayer> loadPlayerFromRcon(Long serverId) throws InterruptedException, ExecutionException {
        Command command = this.commandFactory.playerList();
        CompletableFuture<CommandResponse> future = this.commandService.runCommand(serverId, command);
        CommandResponse cr = future.get();
        if (cr.isSuccess()) {
            String response = cr.getResponse();
            return this.transformRconPlayer(response);
        } else {
            throw new ExecutionException(new TimeoutException("Failure to run command"));
        }
    }

    private List<ConnectedPlayer> transformRconPlayer(String response) {
        String[] lineArray = response.split("\n");
        return StreamEx.of(lineArray)
                .map(playerPattern::matcher)
                .filter(Matcher::matches)
                .map(x -> {
                    String guid = x.group(5);
                    guid = guid.replace("(OK)", "");
                    String name = x.group(6);
                    name = name.replace(" (Lobby)", "");
                    return ConnectedPlayer
                            .builder()
                            .sessionId(x.group(1))
                            .ipAddress(x.group(2) + ":" + x.group(3))
                            .ping(Integer.parseInt(x.group(4)))
                            .guid(guid)
                            .name(name)
                            .connected(true)
                            .build();
                }).toList();
    }
}
