package com.jsoc.overwatch.services.playerList.events;

import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.util.Collection;

@Getter
public class NewPlayerListEvent extends ApplicationEvent {
    private final Collection<ConnectedPlayer> players;
    public NewPlayerListEvent(Object source, Collection<ConnectedPlayer> players) {
        super(source);
        this.players = players;
    }
}
