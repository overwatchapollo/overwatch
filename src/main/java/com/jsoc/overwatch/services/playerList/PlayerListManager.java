package com.jsoc.overwatch.services.playerList;

import com.jsoc.overwatch.services.playerList.events.PlayerChatEvent;
import com.jsoc.overwatch.services.connection.events.ServerStateUpdateEvent;
import com.jsoc.overwatch.services.playerList.events.ConnectedPlayerChange;
import com.jsoc.overwatch.services.playerList.events.PlayerListStaleEvent;
import io.reactivex.disposables.Disposable;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.context.ApplicationEventPublisher;

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import static com.jsoc.overwatch.services.connection.ServerConnectionStatus.CONNECTED;

@Slf4j
public class PlayerListManager implements AutoCloseable {
    private final Long serverId;
    private final ServerPlayerList serverPlayerList;
    private Disposable staleDisposable;
    private final PlayerListProxy proxy;
    private final ApplicationEventPublisher publisher;
    private Disposable playerUpdateDisposable;

    public PlayerListManager(Long serverId, PlayerListProxy proxy, ApplicationEventPublisher publisher) {
        this.serverId = serverId;
        this.serverPlayerList = new ServerPlayerList(serverId);
        this.proxy = proxy;
        this.publisher = publisher;
        Optional<Collection<ConnectedPlayer>> optPlayers = this.getCurrentPlayers(this.serverId);
        optPlayers.ifPresent(serverPlayerList::refresh);
        Collection<ConnectedPlayer> players = this.serverPlayerList.getPlayers();
        log.trace("Generating reports for pre-existing players");
        StreamEx.of(players)
            .map(x -> new ConnectedPlayerChange(this, x))
            .forEach(this.publisher::publishEvent);
    }

    public void playerUpdate(PlayerChatEvent playerUpdate) {
        this.serverPlayerList.onPlayerChatEvent(playerUpdate);
    }

    public Collection<ConnectedPlayer> getPlayers() {
        return this.serverPlayerList.getPlayers();
    }

    Collection<ConnectedPlayer> getLastArchive() {
        return this.serverPlayerList.getLastArchive();
    }

    public void eventListener(ServerStateUpdateEvent serverUpdate) {
        if (serverUpdate.getStatus() == CONNECTED) {
            this.staleDisposable = this.serverPlayerList.getStalePlayerSubject()
                    .map(x -> new PlayerListStaleEvent(this, x))
                    .subscribe(this.publisher::publishEvent);

            this.playerUpdateDisposable = this.serverPlayerList.playerObservable()
                    .map(x -> new ConnectedPlayerChange(this, x))
                    .subscribe(this.publisher::publishEvent);
        } else {
            if (this.staleDisposable != null) {
                this.staleDisposable.dispose();
            }

            if (this.playerUpdateDisposable != null) {
                this.playerUpdateDisposable.dispose();
            }
        }
    }

    @Override
    public void close() {
        if (this.staleDisposable != null) {
            this.staleDisposable.dispose();
        }

        if (this.playerUpdateDisposable != null) {
            this.playerUpdateDisposable.dispose();
        }
    }

    private Optional<Collection<ConnectedPlayer>> getCurrentPlayers(Long serverGuid) {
        try {
            Collection<ConnectedPlayer> players = this.proxy.loadPlayerFromRcon(serverGuid);
            return Optional.of(players);
        } catch (InterruptedException | ExecutionException ex) {
            return Optional.empty();
        }
    }

    /**
     * Refresh a player list for a given server, query the details and perform the update.
     */
    public void refreshList() {
        ServerPlayerList spl = this.serverPlayerList;
        try {
            Optional<Collection<ConnectedPlayer>> optPlayers = Optional.of(this.proxy.loadPlayerFromRcon(this.serverId));
            optPlayers.ifPresent(spl::refresh);
        } catch (Exception ex) {
            log.error("Failed to refresh player list: {}", ex.getMessage());
        }
    }

    public void updateCurrentPlayerList(Collection<ConnectedPlayer> players) {
        this.serverPlayerList.refresh(players);
    }

}
