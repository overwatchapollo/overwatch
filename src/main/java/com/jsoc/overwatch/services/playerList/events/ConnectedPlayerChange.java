package com.jsoc.overwatch.services.playerList.events;

import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class ConnectedPlayerChange extends ApplicationEvent {
    private ConnectedPlayer connectedPlayer;
    public ConnectedPlayerChange(Object source, ConnectedPlayer connectedPlayer) {
        super(source);
        this.connectedPlayer = connectedPlayer;
    }
}
