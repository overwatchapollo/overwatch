package com.jsoc.overwatch.services.playerList;

import com.jsoc.overwatch.services.playerList.events.NewPlayerListEvent;
import com.jsoc.overwatch.services.playerList.events.PlayerChatEvent;
import com.jsoc.overwatch.services.connection.CommandService;
import com.jsoc.overwatch.services.connection.events.ServerStateUpdateEvent;
import com.jsoc.overwatch.services.playerList.events.CurrentPlayerListEvent;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Spring boot service to manage the player lists for all known servers.
 */
@Service
@Slf4j
public class ConnectedPlayerService {

    private final ApplicationEventPublisher publisher;

    private final Map<Long, PlayerListManager> playerListManagerMap = new ConcurrentHashMap<>();

    private final PlayerListProxy proxy;

    public ConnectedPlayerService(CommandService commandService, ApplicationEventPublisher publisher) {
        this.publisher = publisher;
        this.proxy = new PlayerListProxy(commandService);
    }

    /**
     * Refresh a player list for a given server, query the details and perform the update.
     * @param serverId ID of server to update.
     */
    public void refreshList(Long serverId) {

        PlayerListManager manager = this.playerListManagerMap.get(serverId);
        if (manager != null) {
            manager.refreshList();
        }
    }

    /**
     * Get player by name on server.
     * @param serverId Server ID to search for player name.
     * @param name Name of player.
     * @return Optional of player if name is found on server.
     */
    public Optional<ConnectedPlayer> getPlayer(Long serverId, String name) {
        PlayerListManager manager = this.playerListManagerMap.get(serverId);
        if (manager == null) {
            return Optional.empty();
        } else {
            return StreamEx.of(manager.getPlayers())
                .filter(x -> Objects.equals(x.getName(), name))
                .reduce((connectedPlayer, connectedPlayer2) -> Integer.parseInt(connectedPlayer.getSessionId()) > Integer.parseInt(connectedPlayer2.getSessionId())
                        ? connectedPlayer
                        : connectedPlayer2);
        }
    }

    /**
     * Get connected player from session id.
     * @param serverId Server ID to search for player.
     * @param sessionId Session ID to use for player match.
     * @return Optional of player if server contains player with session id.
     */
    public Optional<ConnectedPlayer> getPlayerFromSession(Long serverId, String sessionId) {
        PlayerListManager manager = this.playerListManagerMap.get(serverId);
        if (manager == null) {
            return Optional.empty();
        } else {
            return StreamEx.of(manager.getPlayers())
                    .filter(x -> Objects.equals(x.getSessionId(), sessionId))
                    .reduce((connectedPlayer, connectedPlayer2) -> Integer.parseInt(connectedPlayer.getSessionId()) > Integer.parseInt(connectedPlayer2.getSessionId())
                            ? connectedPlayer
                            : connectedPlayer2);
        }
    }

    /**
     * Get all players on server.
     * @param serverId ID of server to get players for.
     * @return Collection of all players on server.
     * @throws UnknownServerIdException If server id does not match a known server.
     */
    public Collection<ConnectedPlayer> getPlayers(Long serverId) throws UnknownServerIdException{
        PlayerListManager manager = this.playerListManagerMap.get(serverId);
        if (manager == null) {
            throw new UnknownServerIdException("Server " + serverId + " does not have a player list.");
        } else {
            return manager.getPlayers();
        }
    }

    /**
     * Get the last archived player list.
     * @param serverId ID of server to get archived player list for.
     * @return Collection of players in archive.
     * @throws UnknownServerIdException If server id does not match a known server.
     */
    public Collection<ConnectedPlayer> getPlayerArchive(Long serverId) throws UnknownServerIdException {
        PlayerListManager manager = this.playerListManagerMap.get(serverId);
        if (manager == null) {
            throw new UnknownServerIdException("Server " + serverId + " does not have a player list.");
        } else {
            return manager.getLastArchive();
        }
    }

    @EventListener
    public void eventListener(ServerStateUpdateEvent serverUpdate) {
        Long id = serverUpdate.getServerGuid();
        PlayerListManager manager = this.playerListManagerMap.get(id);
        if (manager == null) {
            log.error("Player chat event raised on server {} but no player list exists!", id);
            this.setupPlayerList(id);
        } else {
            manager.eventListener(serverUpdate);
        }
    }

    @EventListener
    public void eventListener(CurrentPlayerListEvent event) {
        PlayerListManager manager = this.playerListManagerMap.get(event.getServerId());
        if (manager != null) {
            manager.updateCurrentPlayerList(event.getPlayers());
            this.publisher.publishEvent(new NewPlayerListEvent(this, event.getPlayers()));
        }
    }

    private void setupPlayerList(Long serverId) {
        this.playerListManagerMap.put(serverId, new PlayerListManager(serverId, this.proxy, this.publisher));
    }

    @EventListener
    public void playerUpdate(PlayerChatEvent playerUpdate) {
        Long serverId = playerUpdate.getConnectedPlayer().getServerId();
        PlayerListManager manager = this.playerListManagerMap.get(serverId);
        if (manager == null) {
            log.error("Player chat event raised on server {} but no player list exists!", serverId);
        } else {
            manager.playerUpdate(playerUpdate);
        }
    }
}
