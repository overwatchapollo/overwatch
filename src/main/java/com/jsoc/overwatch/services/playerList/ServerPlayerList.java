package com.jsoc.overwatch.services.playerList;

import com.jsoc.overwatch.services.playerList.events.PlayerChatEvent;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import lombok.extern.slf4j.Slf4j;

import java.io.Closeable;
import java.util.*;

/**
 * The {@see ServerPlayerList} class holds the current and previous player lists for a server.
 * Its' main job is to manage archiving of a {@see PlayerList} object.
 */
@Slf4j
public class ServerPlayerList implements IPlayerEventConsumer, Closeable {

    private final Long serverId;

    private final List<PlayerList> playerLists = new ArrayList<>();

    private final BehaviorSubject<ConnectedPlayer> playerSubject;

    private Disposable playerSub;

    private final Subject<Long> stalePlayerListSubject;

    public ServerPlayerList(Long serverId) {
        this.serverId = serverId;
        this.playerSubject = BehaviorSubject.create();
        PlayerList pl = this.createPlayerList();
        this.playerLists.add(pl);
        this.playerSub = pl.playerObservable().subscribe(this.playerSubject::onNext);

        this.stalePlayerListSubject = PublishSubject.create();
    }

    Observable<Long> getStalePlayerSubject() {
        return this.stalePlayerListSubject;
    }

    private PlayerList getActivePlayerList() {
        return this.playerLists.get(this.playerLists.size() - 1);
    }


    @Override
    public Observable<ConnectedPlayer> playerObservable() {
        return this.playerSubject;
    }

    @Override
    public void close() {

    }

    @Override
    public boolean isArchive() {
        return false;
    }

    @Override
    public Collection<ConnectedPlayer> getPlayers() {
        return this.getActivePlayerList().getPlayers();
    }

    Collection<ConnectedPlayer> getLastArchive() {
        if (this.playerLists.size() < 2) {
            return Collections.emptyList();
        }
        return this.playerLists.get(this.playerLists.size() - 2).getPlayers();
    }

    public void refresh(Collection<ConnectedPlayer> players) {
        try {
            log.debug("Refreshing player list {}: found {} players", this.serverId, players.size());
            this.getActivePlayerList().refresh(players);
        } catch (ArchievedPlayerListException ex) {
            log.info("Archiving player list on server {}", this.serverId);
            this.handleArchive();
            try {
                this.getActivePlayerList().refresh(players);
            } catch (Exception ex2) {
                log.error("Failed to pass players {}", ex2);
            }
        }
    }

    @Override
    public void onPlayerChatEvent(PlayerChatEvent playerChatEvent) {
        try {
            this.getActivePlayerList().onPlayerChatEvent(playerChatEvent);
        } catch (OutOfOrderPlayerConnectionException ex) {
            this.handleArchive();
        } catch (ArchievedPlayerListException ex) {
            log.warn("Player archive on chat event for server {}", this.serverId);
        }
    }

    private void handleArchive() {
        this.getActivePlayerList().setArchive();
        this.playerSub.dispose();
        PlayerList pl = this.createPlayerList();
        this.playerSub = pl.playerObservable().subscribe(this.playerSubject::onNext);
        this.playerLists.add(pl);
        this.stalePlayerListSubject.onNext(this.serverId);
    }

    private PlayerList createPlayerList() {
        return new PlayerList(this.serverId);
    }
}
