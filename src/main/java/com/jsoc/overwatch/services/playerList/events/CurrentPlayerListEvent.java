package com.jsoc.overwatch.services.playerList.events;

import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import lombok.Data;
import org.springframework.context.ApplicationEvent;

import java.util.Collection;

@Data
public class CurrentPlayerListEvent extends ApplicationEvent {
    private final Long serverId;
    private final Collection<ConnectedPlayer> players;
    public CurrentPlayerListEvent(Object source, Long serverId, Collection<ConnectedPlayer> players) {
        super(source);
        this.serverId = serverId;
        this.players = players;
    }
}
