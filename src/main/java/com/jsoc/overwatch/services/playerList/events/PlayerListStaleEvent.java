package com.jsoc.overwatch.services.playerList.events;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class PlayerListStaleEvent  extends ApplicationEvent {
    private final Long serverId;
    public PlayerListStaleEvent(Object sender, Long serverId) {
        super(sender);
        this.serverId = serverId;
    }
}
