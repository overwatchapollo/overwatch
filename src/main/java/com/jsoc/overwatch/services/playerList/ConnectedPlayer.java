/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.playerList;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author PaulR
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ConnectedPlayer {
    /**
     * ID of the server player is connected to.
     */
    private Long serverId;

    /**
     * Session ID of the player.
     */
    private String sessionId;

    /**
     * IP Address of player.
     */
    private String ipAddress;
    private int ping;
    private String guid;
    private String name;
    private boolean connected;
    private boolean kicked;
    private boolean banned;
    private String disconnectionMessage;
    private boolean member;
}
