package com.jsoc.overwatch.services.playerList;

class ArchievedPlayerListException extends Exception {
    ArchievedPlayerListException(String message) {
        super(message);
    }
}
