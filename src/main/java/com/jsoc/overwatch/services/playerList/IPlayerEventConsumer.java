package com.jsoc.overwatch.services.playerList;

import com.jsoc.overwatch.services.playerList.events.PlayerChatEvent;
import io.reactivex.Observable;

import java.util.Collection;

public interface IPlayerEventConsumer {

    boolean isArchive();
    Observable<ConnectedPlayer> playerObservable();
    Collection<ConnectedPlayer> getPlayers();
    void onPlayerChatEvent(PlayerChatEvent playerChatEvent) throws OutOfOrderPlayerConnectionException, ArchievedPlayerListException;
}
