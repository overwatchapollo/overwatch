package com.jsoc.overwatch.services.playerList.events;

import lombok.Data;
import org.springframework.context.ApplicationEvent;

@Data
public class PlayerChatKick extends ApplicationEvent {
    private PlayerChatEvent chatEvent;
    public PlayerChatKick(Object source, PlayerChatEvent chatEvent) {
        super(source);
        this.chatEvent = chatEvent;
    }
}
