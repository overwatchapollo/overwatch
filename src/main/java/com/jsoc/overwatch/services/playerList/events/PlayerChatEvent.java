package com.jsoc.overwatch.services.playerList.events;

import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class PlayerChatEvent extends ApplicationEvent {
    private PlayerChatEventType type;
    private ConnectedPlayer connectedPlayer;
    public PlayerChatEvent(Object source, PlayerChatEventType type, ConnectedPlayer player) {
        super(source);
        this.type = type;
        this.connectedPlayer = player;
    }
}
