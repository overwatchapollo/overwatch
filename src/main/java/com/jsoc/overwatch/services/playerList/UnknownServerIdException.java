package com.jsoc.overwatch.services.playerList;

public class UnknownServerIdException extends Exception {
    public UnknownServerIdException(String message) {
        super(message);
    }
}
