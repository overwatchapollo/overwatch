package com.jsoc.overwatch.services.playerList.events;

public enum PlayerChatEventType {
    CONNECTED,
    GUID_VERIFIED,
    DISCONNECTED,
    KICKED,
    BANNED;
}
