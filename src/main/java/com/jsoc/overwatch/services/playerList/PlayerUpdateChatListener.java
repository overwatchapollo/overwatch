package com.jsoc.overwatch.services.playerList;

import com.jsoc.overwatch.services.connection.events.RconMessageReceived;
import com.jsoc.overwatch.services.playerList.events.PlayerChatEvent;
import com.jsoc.overwatch.services.playerList.events.PlayerChatEventType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Service to monitor RCon message traffic for player update events.
 */
@Component
@Slf4j
public class PlayerUpdateChatListener {
    /**
     * Pattern to match player connected messages.
     * Example String 'Player #0 [Sfc.] PaulRPG (127.0.0.1:2316) connected'
     */
    private static final Pattern PLAYER_CONNECTED = Pattern.compile(
        "^Player #(\\d+) (.+) \\((.+)\\) connected$");

    /**
     * Pattern to match player disconnected messages.
     * Example String 'Player #0 [Sfc.] PaulRPG disconnected'
     */
    private static final Pattern PLAYER_DISCONNECTED = Pattern.compile(
        "^Player #(\\d+) (.+) disconnected$");

    /**
     * Pattern to match player kicked messages.
     * Example String 'Player #0 [Sfc.] PaulRPG (1f969977ccb49930a41fe3b0f70753e4) has been kicked by BattlEye: Admin Kick'
     */
    private static final Pattern PLAYER_KICKED = Pattern.compile(
        "^Player #(\\d+) (.+) \\((.+)\\) has been kicked by BattlEye: (.+)");

    /**
     * Pattern to match player GUID messages.
     * Example String 'Verified GUID (1f969977ccb49930a41fe3b0f70753e4) of player #101 [Sfc.] PaulRPG'
     */
    private static final Pattern PLAYER_GUID = Pattern.compile(
        "^Verified GUID \\((.+)\\) of player #(\\d+) (.+)");

    /**
     * Pattern to match player ban messages.
     * Example String 'Player #0 [Sfc.] PaulRPG (1f969977ccb49930a41fe3b0f70753e4) has been banned by BattlEye: Admin Ban'
     */
    private static final Pattern PLAYER_BAN = Pattern.compile(
        "^Player #(\\d+) (.+) \\((.+)\\) has been banned by BattlEye: (.+)");


    //Player #120 Danko Nikolov (cbac437563365948bbc8e95698347a28) has been kicked by BattlEye: Admin Ban (Recruiting (Unit ban) Perm (Permanent) [Sjanten])
    @Autowired
    private ApplicationEventPublisher publisher;

    @EventListener
    public void onRconMessage(RconMessageReceived rconMessageReceived) {
        String message = rconMessageReceived.getMessage();
        Long serverId = rconMessageReceived.getServerId();

        Matcher connectedMatcher = PLAYER_CONNECTED.matcher(message);
        Matcher disconnectedMatcher = PLAYER_DISCONNECTED.matcher(message);
        Matcher kickMatcher = PLAYER_KICKED.matcher(message);
        Matcher guidMatcher = PLAYER_GUID.matcher(message);
        Matcher banMatcher = PLAYER_BAN.matcher(message);

        if (connectedMatcher.find()) {
            this.handleConnect(serverId, connectedMatcher);
        } else if (guidMatcher.find()) {
            this.handleGuid(serverId, guidMatcher);
        } else if (disconnectedMatcher.find()) {
            this.handleDisconnect(serverId, disconnectedMatcher);
        } else if (kickMatcher.find()) {
            this.handleKicked(serverId, kickMatcher);
        } else if(banMatcher.find()) {
            this.handleBan(serverId, banMatcher);
        }
    }

    private void handleConnect(Long serverId, Matcher matcher) {
        ConnectedPlayer cp = ConnectedPlayer.builder()
            .serverId(serverId)
            .sessionId(matcher.group(1))
            .name(matcher.group(2))
            .ipAddress(matcher.group(3))
            .connected(true)
            .build();
        log.trace("Player {} connected on server {}", cp.getName(), serverId);
        this.publisher.publishEvent(new PlayerChatEvent(
        this,
            PlayerChatEventType.CONNECTED,
            cp));
    }

    private void handleDisconnect(Long serverId, Matcher matcher) {
        ConnectedPlayer cp = ConnectedPlayer.builder()
            .serverId(serverId)
            .sessionId(matcher.group(1))
            .name(matcher.group(2))
            .build();
        log.trace("Player {} has disconnected on server {}", cp.getName(), serverId);
        this.publisher.publishEvent(new PlayerChatEvent(
        this,
            PlayerChatEventType.DISCONNECTED,
            cp));
    }

    private void handleGuid(Long serverId, Matcher matcher) {
        ConnectedPlayer cp = ConnectedPlayer.builder()
            .serverId(serverId)
            .guid(matcher.group(1))
            .sessionId(matcher.group(2))
            .name(matcher.group(3))
            .connected(true)
            .build();
        log.trace("Player {} GUID verified on server {}", cp.getName(), serverId);
        this.publisher.publishEvent(new PlayerChatEvent(
        this,
            PlayerChatEventType.GUID_VERIFIED,
            cp));
    }

    private void handleKicked(Long serverId, Matcher matcher) {
        ConnectedPlayer cp = ConnectedPlayer.builder()
            .serverId(serverId)
            .sessionId(matcher.group(1))
            .name(matcher.group(2))
            .guid(matcher.group(3))
            .disconnectionMessage(matcher.group(4))
            .kicked(true)
            .build();
        log.trace("Player {} kicked from server {}", cp.getName(), serverId);
        this.publisher.publishEvent(new PlayerChatEvent(
        this,
            PlayerChatEventType.KICKED,
            cp));
    }

    private void handleBan(Long serverId, Matcher matcher) {
        ConnectedPlayer cp = ConnectedPlayer.builder()
            .serverId(serverId)
            .sessionId(matcher.group(1))
            .name(matcher.group(2))
            .guid(matcher.group(3))
            .disconnectionMessage(matcher.group(4))
            .banned(true)
            .build();
        log.trace("Player {} has been banned from server {}", cp.getName(), serverId);
        this.publisher.publishEvent(new PlayerChatEvent(
        this,
            PlayerChatEventType.BANNED,
            cp));
    }
}
