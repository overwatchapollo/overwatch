package com.jsoc.overwatch.services.playerList;

class OutOfOrderPlayerConnectionException extends Exception {
    OutOfOrderPlayerConnectionException(String message) {
        super(message);
    }
}
