package com.jsoc.overwatch.services.playerNote.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.jsoc.overwatch.services.userManagement.model.UserModel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;

@Entity
@Table(name = "player_notes")
@Setter
@Getter
public class PlayerNote {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String guid;

    private Long organisation;

    @Length(max = 1024)
    private String note;

    private Boolean deleted;

    @Column(nullable = false, updatable = false)
    @CreatedDate
    private Long createdDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id")
    @CreatedBy
    @JsonBackReference
    private UserModel createdBy;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Note: '");
        builder.append(this.note);
        builder.append("' added by: ");
        builder.append(this.createdBy.getUserName());
        return builder.toString();
    }
}
