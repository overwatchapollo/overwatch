package com.jsoc.overwatch.services.playerNote.events;

import lombok.Data;
import org.springframework.context.ApplicationEvent;
import org.springframework.security.core.userdetails.User;

@Data
public class NoteDeleteEvent extends ApplicationEvent {

    private User user;
    private Long noteId;
    private Long organisationId;
    public NoteDeleteEvent(Object source, User user, Long noteId, Long organisationId) {
        super(source);
        this.user = user;
        this.noteId = noteId;
        this.organisationId = organisationId;
    }
}
