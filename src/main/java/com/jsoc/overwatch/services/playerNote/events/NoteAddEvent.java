package com.jsoc.overwatch.services.playerNote.events;

import lombok.Data;
import org.springframework.context.ApplicationEvent;
import org.springframework.security.core.userdetails.User;

@Data
public class NoteAddEvent extends ApplicationEvent {

    private User user;
    private String targetGuid;
    private Long noteId;
    private Long organisationId;
    public NoteAddEvent(Object source, User user, String targetGuid, Long noteId, Long organisationId) {
        super(source);
        this.user = user;
        this.targetGuid = targetGuid;
        this.noteId = noteId;
        this.organisationId = organisationId;
    }
}
