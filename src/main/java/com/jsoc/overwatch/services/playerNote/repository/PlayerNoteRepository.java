package com.jsoc.overwatch.services.playerNote.repository;

import com.jsoc.overwatch.services.playerNote.model.PlayerNote;
import org.springframework.data.repository.CrudRepository;

public interface PlayerNoteRepository extends CrudRepository<PlayerNote, Long> {
}
