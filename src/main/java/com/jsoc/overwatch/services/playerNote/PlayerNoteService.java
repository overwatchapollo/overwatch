package com.jsoc.overwatch.services.playerNote;

import com.jsoc.overwatch.services.playerNote.events.NoteAddEvent;
import com.jsoc.overwatch.services.playerNote.events.NoteDeleteEvent;
import com.jsoc.overwatch.services.playerNote.events.NoteModifiedEvent;
import com.jsoc.overwatch.controller.dto.NotFoundException;
import com.jsoc.overwatch.services.userManagement.model.UserModel;
import com.jsoc.overwatch.services.playerNote.repository.PlayerNoteRepository;
import com.jsoc.overwatch.services.playerNote.model.PlayerNote;
import com.jsoc.overwatch.services.userManagement.CustomUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Objects;
import java.util.Set;

@Slf4j
@Service
@Transactional
public class PlayerNoteService {

    @Autowired
    private CustomUserDetailsService userService;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private PlayerNoteRepository repo;

    public PlayerNote addNoteToPlayer(User user, String guid, Long organisation, String note) {
        UserModel um = this.userService.getModelByUsername(user.getUsername());
        log.info("User {} adding note to player {}", um.getUserName(),  guid);

        PlayerNote pn = new PlayerNote();
        pn.setNote(note);
        pn.setDeleted(false);
        pn.setCreatedDate(Instant.now().toEpochMilli());
        pn.setCreatedBy(um);
        pn.setGuid(guid);
        pn.setOrganisation(organisation);
        log.info("{}", pn);
        PlayerNote saved = this.repo.save(pn);

        this.publisher.publishEvent(new NoteAddEvent(this, user, guid, saved.getId(), organisation));
        return saved;
    }

    public void removeNote(User user, Long id) {
        PlayerNote note = this.repo.findById(id).orElseThrow(NotFoundException::new);
        note.setDeleted(true);
        this.repo.save(note);
        this.publisher.publishEvent(new NoteDeleteEvent(this, user, id, note.getOrganisation()));
    }

    public void deleteNote(Long id) {
        this.repo.deleteById(id);
    }

    public PlayerNote modifyNote(User user, Long id, String message) {
        PlayerNote note = this.repo.findById(id).orElseThrow(NotFoundException::new);
        note.setNote(message);
        PlayerNote pn = this.repo.save(note);
        this.publisher.publishEvent(new NoteModifiedEvent(this, id, user));
        return pn;
    }

    public Set<PlayerNote> getNotesForPlayer(String guid, Long organisation) {
        Set<PlayerNote> notes = StreamEx.of(this.repo.findAll().iterator())
                .filter(x -> Objects.equals(guid, x.getGuid()) &&
                        Objects.equals(organisation, x.getOrganisation()))
                .filter(x -> x.getDeleted() == false)
                .toSet();
        notes.forEach(x -> log.info("note: {}", x));
        return notes;
    }
}
