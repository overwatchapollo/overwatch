package com.jsoc.overwatch.services.playerNote.events;

import lombok.Data;
import org.springframework.context.ApplicationEvent;
import org.springframework.security.core.userdetails.User;

@Data
public class NoteModifiedEvent extends ApplicationEvent {
    private Long id;
    private User user;
    public NoteModifiedEvent(Object source, Long id, User user) {
        super(source);
        this.id = id;
        this.user = user;
    }
}
