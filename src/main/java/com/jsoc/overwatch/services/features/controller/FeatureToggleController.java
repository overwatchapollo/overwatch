package com.jsoc.overwatch.services.features.controller;

import com.jsoc.overwatch.Security.SecurityHelper;
import com.jsoc.overwatch.Security.permissions.SystemPermissionGenerator;
import com.jsoc.overwatch.config.FeatureToggleEvent;
import com.jsoc.overwatch.controller.dto.FeatureToggleRequestDto;
import com.jsoc.overwatch.controller.dto.ForbiddenException;
import com.jsoc.overwatch.controller.dto.UnauthorisedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import org.togglz.core.manager.FeatureManager;
import org.togglz.core.repository.FeatureState;

import javax.servlet.http.HttpServletRequest;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;

@Slf4j
@RestController
@RequestMapping("/api/features")
public class FeatureToggleController {

    @Autowired
    private FeatureManager manager;

    @Autowired
    private ApplicationEventPublisher publisher;

    private SystemPermissionGenerator generator = new SystemPermissionGenerator();

    @PostMapping("")
    public void setFeatureToggle(HttpServletRequest request, @RequestBody FeatureToggleRequestDto dto) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.featureToggle().hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        checkNotNull(dto);
        checkNotNull(dto.getFeatureName());
        checkNotNull(dto.getEnabled());
        this.manager.getFeatures()
            .stream()
            .filter(x -> Objects.equals(x.name(), dto.getFeatureName())).findFirst()
            .ifPresent(x -> {
                log.info("Updating feature {} to {}", x.name(), dto.getEnabled());
                this.manager.setFeatureState(new FeatureState(x, dto.getEnabled()));
                this.publisher.publishEvent(new FeatureToggleEvent(this, maybeUser, "chatAdmin", dto.getEnabled()));
            });
    }

    @GetMapping("")
    public List<FeatureToggleRequestDto> getFeatureToggles(HttpServletRequest request) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.featureToggle().hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        List<FeatureToggleRequestDto> dtos = this.manager.getFeatures()
            .stream()
            .map(x -> new FeatureToggleRequestDto(x.name(), this.manager.isActive(x)))
            .collect(Collectors.toList());
        return dtos;
    }
}
