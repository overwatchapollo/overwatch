/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.connection.api;

import lombok.extern.slf4j.Slf4j;

/**
 * ARMA 3 rcon command.
 */
@Slf4j
public class Command {
    
    /**
     * Full command to send to server.
     */
    private final String command;
    
    /**
     * Creates a new instance of the {@link Command} class.
     * 
     * @param command RCON command to send.
     */
    public Command(String command) {
        this.command = command;
    }
    
    /**
     * Get the command.
     * 
     * @return Command string.
     */
    public String getCommand() {
        return this.command;
    }
}
