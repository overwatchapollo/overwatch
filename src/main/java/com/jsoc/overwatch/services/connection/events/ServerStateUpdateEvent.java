package com.jsoc.overwatch.services.connection.events;

import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import lombok.Data;
import org.springframework.context.ApplicationEvent;

@Data
public class ServerStateUpdateEvent extends ApplicationEvent {
    private final Long serverGuid;
    private final String serverName;
    private final ServerConnectionStatus status;

    public ServerStateUpdateEvent(Object source, Long serverGuid, String serverName, ServerConnectionStatus status) {
        super(source);
        this.serverGuid = serverGuid;
        this.serverName = serverName;
        this.status = status;
    }
}
