package com.jsoc.overwatch.services.connection;

import com.jsoc.overwatch.services.connection.api.Command;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
public class CommandService {
    @Autowired
    private RconConnectionPool pool;

    public CommandService(RconConnectionPool pool) {
        this.pool = pool;
    }

    public CompletableFuture<CommandResponse> runCommand(Long serverId, Command command) {
        return this.pool.getConnection(serverId)
            .orElseThrow(() -> new IllegalArgumentException("Server " + serverId + " is not active."))
            .sendCommand(command);
    }
}
