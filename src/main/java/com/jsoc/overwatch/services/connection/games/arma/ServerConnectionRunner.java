package com.jsoc.overwatch.services.connection.games.arma;

import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import com.jsoc.overwatch.services.connection.api.Command;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import com.jsoc.overwatch.services.connection.games.battleye.BattleyeSocket;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;
import lombok.extern.slf4j.Slf4j;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static com.jsoc.overwatch.services.connection.ServerConnectionStatus.*;

@Slf4j
public class ServerConnectionRunner implements Runnable {

    private ServerConnectionStatus status;

    private BattleyeSocket battleyeSocket;

    private PublishSubject<ServerConnectionStatus> statusPublishSubject = PublishSubject.create();

    private String hostname;
    private int port;
    private Scheduler scheduler;
    private CountDownLatch latch = new CountDownLatch(1);
    private String guid = UUID.randomUUID().toString();
    private Disposable eventPublisher;
    private Disposable stateChangeDisposable = null;
    private Disposable keepAliveDisposable = null;

    public ServerConnectionRunner(Scheduler scheduler, String hostname, int port, String password) {
        this.hostname = hostname;
        this.port = port;
        this.scheduler = scheduler;
        this.status = DISCONNECTED;
        this.battleyeSocket = new BattleyeSocket(scheduler, hostname, port, password);
        this.eventPublisher = this.battleyeSocket.getEventPublisher().subscribe(x -> this.statusPublishSubject.onNext(x));
    }

    public void raiseEvent(ServerConnectionStatus status) {
        this.updateState(status);
        if (status == DISCONNECTED) {
            this.close();
        }
    }

    public CompletableFuture<CommandResponse> sendCommand(Command command) {
        CompletableFuture<CommandResponse> future = new CompletableFuture<>();
        if (this.status == CONNECTED) {
            this.scheduler.scheduleDirect(() -> {
                CommandResponse cr = this.battleyeSocket.sendCommand(command);
                future.complete(cr);
                log.debug("Command {} success? {}", command.getCommand(), cr.isSuccess());
            });

        } else {
            log.error("Can't run command {} | {}", command.getCommand(), this.status);
            future.completeExceptionally(new IllegalStateException());
        }

        return future;
    }

    public ServerConnectionStatus getStatus() {
        return this.status;
    }

    public Observable<ServerConnectionStatus> getStatusObservable() {
        return this.statusPublishSubject.observeOn(this.scheduler);
    }

    public Observable<String> getMessageObservable() {
        return this.battleyeSocket.getRecievedObservable();
    }

    public void close() {
        latch.countDown();
        this.eventPublisher.dispose();

        if (this.stateChangeDisposable != null) {
            this.stateChangeDisposable.dispose();
        }

        if (this.keepAliveDisposable != null && !this.keepAliveDisposable.isDisposed()) {
            this.keepAliveDisposable.dispose();
        }
    }

    @Override
    public void run() {
        log.info("Starting socket to {}:{} with guid {}", this.hostname, this.port, this.guid);
        this.stateChangeDisposable = this.getStatusObservable().subscribe(x -> {
            this.stateChange(x);
        });
        this.statusPublishSubject.onNext(DISCONNECTED);
        try {
            this.latch.await();
        } catch (InterruptedException ex) {

        }

        log.info("Closed socket with status {} to {}:{} with guid {}", this.status, this.hostname, this.port, this.guid);
    }

    private void stateChange(ServerConnectionStatus status) {
        log.info("state change on {}:{} {}", this.hostname, this.port, status);
        switch (status) {
            case DISCONNECTED: {
                log.debug("Server was disconnected. Starting.");
                this.battleyeSocket.disconnect();
                if (this.keepAliveDisposable != null) {
                    this.keepAliveDisposable.dispose();
                }
                this.updateState(CONNECTING);
                break;
            }
            case CONNECTING: {
                log.info("Starting server connection for {}:{}", this.hostname, this.port);
                ServerConnectionStatus result = this.battleyeSocket.connect();
                log.info("Connecting server {}:{} result {}", this.hostname, this.port, result);
                if (result != CONNECTED) {
                    this.battleyeSocket.disconnect();
                }
                this.updateState(result);
                break;
            }
            case CONNECTED: {
                log.debug("connected and happy on {}:{}", this.hostname, this.port);
                this.keepAliveDisposable = this.scheduler.schedulePeriodicallyDirect(() -> this.sendCommand(new Command("")), 30, 30, TimeUnit.SECONDS);
                break;
            }
            case TIMEOUT: {
                log.info("timeout on server {}:{}", this.hostname, this.port);
                this.battleyeSocket.disconnect();
                this.updateState(DISCONNECTED);
                if (this.keepAliveDisposable != null) {
                    this.keepAliveDisposable.dispose();
                }
                break;
            }
            default: {
                log.warn("Did not handle {}", this.status);
            }
        }
    }


    private synchronized void updateState(ServerConnectionStatus status) {
        this.status = status;
        this.statusPublishSubject.onNext(status);
    }
}
