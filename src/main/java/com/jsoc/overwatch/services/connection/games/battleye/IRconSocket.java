package com.jsoc.overwatch.services.connection.games.battleye;

import io.reactivex.Observable;

import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;

public interface IRconSocket extends AutoCloseable {

    /**
     * Connect the socket to the RCON server.
     *
     * @throws SocketException If the socket can't be created.
     * @throws UnknownHostException If the host is unresolvable.
     */
    void activate() throws SocketException, UnknownHostException;

    /**
     * Send data on socket.
     * @param message Byte array to send
     * @throws IOException Exception if socket not available.
     */
    void sendData(byte[] message) throws IOException;

    /**
     * Data observable.
     * @return Observable for data from socket.
     */
    Observable<byte[]> getDataObservable();
}
