package com.jsoc.overwatch.services.connection;

import lombok.Data;

@Data
public class ServerNotFoundException extends Exception {
    private Long id;

    public ServerNotFoundException(Long id) {
        this.id = id;
    }
}
