/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.connection.controller;

import com.jsoc.overwatch.Security.SecurityHelper;
import com.jsoc.overwatch.Security.permissions.ServerPermissionGenerator;
import com.jsoc.overwatch.controller.dto.*;
import com.jsoc.overwatch.services.connection.CommandService;
import com.jsoc.overwatch.services.connection.RconConnectionPool;
import com.jsoc.overwatch.services.connection.ServerNotFoundException;
import com.jsoc.overwatch.services.connection.api.CommandFactory;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import com.jsoc.overwatch.services.connection.events.SoftRestartRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.ExecutionException;

/**
 * Rest controller for RCon.
 */
@RestController
@RequestMapping("/api/rcon")
@PreAuthorize("isAuthenticated()")
@Slf4j
public class RconController {
    
    private final CommandFactory commandFactory = new CommandFactory();

    @Autowired
    private RconConnectionPool pool;

    @Autowired
    private CommandService commandService;

    @Autowired
    private ApplicationEventPublisher publisher;

    private ServerPermissionGenerator generator = new ServerPermissionGenerator();

    @PostMapping("/restart/{serverMrid}")
    public void softRestart(HttpServletRequest request, HttpServletResponse response, @PathVariable("serverMrid") Long mrid) throws ServerNotFoundException {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.restart(mrid).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }
        this.publisher.publishEvent(new SoftRestartRequest(this, maybeUser, mrid));

        try {
            this.commandService.runCommand(mrid, this.commandFactory.softRestart()).get();
        } catch (IllegalArgumentException ex) {
            throw new ServerNotFoundException(mrid);
        } catch (InterruptedException | ExecutionException e) {
            log.error("Unable to soft restart: {}", e.getMessage());
            throw new ServerError();
        }
    }

    @GetMapping("/v1/state/{serverMrid}")
    public ServerStateDto getState(HttpServletRequest request, @PathVariable("serverMrid") Long serverMrid) throws ServerNotFoundException {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.view(serverMrid).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        return this.pool.getConnection(serverMrid)
            .map(x -> new ServerStateDto(serverMrid, x.getStatus()))
            .orElseThrow(() -> new ServerNotFoundException(serverMrid));
    }

    @PostMapping("/v1/state/{serverMrid}")
    public void forceUpdate(HttpServletRequest request, @PathVariable("serverMrid") Long serverMrid, @RequestBody ForceUpdateDto dto) {

    }

    @PostMapping("/v1/runner/{serverMrid}")
    public void setRunner(HttpServletRequest request, @PathVariable("serverMrid") Long serverMrid) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.owner(serverMrid).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        this.pool.setAsRunner(serverMrid);
    }

    @Deprecated
    @PostMapping("/v1/old/{serverMrid}")
    public void setOld(HttpServletRequest request, @PathVariable("serverMrid") Long serverMrid) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.owner(serverMrid).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }
    }

    @GetMapping("/version/{serverMrid}")
    @ResponseBody
    public String getVersion(HttpServletRequest request, @PathVariable("serverMrid") Long serverMrid) throws Exception {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.view(serverMrid).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        CommandResponse cr = this.commandService.runCommand(serverMrid, this.commandFactory.version()).get();
        return "version: " + cr.getResponse();
    }
}
