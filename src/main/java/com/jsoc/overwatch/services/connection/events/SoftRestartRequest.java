package com.jsoc.overwatch.services.connection.events;

import lombok.Data;
import org.springframework.context.ApplicationEvent;
import org.springframework.security.core.userdetails.User;

import static com.google.common.base.Preconditions.checkNotNull;

@Data
public class SoftRestartRequest extends ApplicationEvent {
    private final User invoker;
    private final Long serverId;
    public SoftRestartRequest(Object source, User invoker, Long serverId) {
        super(source);
        checkNotNull(invoker);
        checkNotNull(serverId);
        this.invoker = invoker;
        this.serverId = serverId;
    }
}
