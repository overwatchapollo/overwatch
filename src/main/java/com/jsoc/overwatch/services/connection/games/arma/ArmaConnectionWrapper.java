package com.jsoc.overwatch.services.connection.games.arma;

import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import com.jsoc.overwatch.services.connection.events.RconMessageReceived;
import com.jsoc.overwatch.services.connection.events.ServerStateUpdateEvent;
import com.jsoc.overwatch.services.connection.api.Command;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import io.reactivex.subjects.Subject;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class ArmaConnectionWrapper implements IRconConnection {

    private Map<String, IRconConnection> connectionMap = new HashMap<>();

    private final RconFactory factory;

    private final ServerConnection configuration;
    ArmaConnectionWrapper(ServerConnection configuration, Subject<ServerStateUpdateEvent> statePublisher, Subject<RconMessageReceived> messagePublisher) {
        this.factory = new RconFactory(statePublisher, messagePublisher);
        this.configuration = configuration;
        this.connectionMap.put("MAIN", this.factory.buildServer(configuration));
        this.connectionMap.put("HIGH", this.factory.buildServer(configuration));
        this.connectionMap.get("HIGH").setMaster(true);
    }

    @Override
    public void setMaster(boolean master) {
    }

    @Override
    public boolean isMaster() {
        return true;
    }

    public ServerConnectionStatus getStatus() {
        return this.connectionMap.get("HIGH").getStatus();
    }

    @Override
    public boolean connect() {
        return StreamEx.of(this.connectionMap.keySet())
            .map(x -> {
                IRconConnection con = this.connectionMap.get(x);
                boolean success = con.connect();
                log.info("Connecting {} with {}:{}", this.configuration.getId(), x, success);
                return success;
            })
            .reduce((a, b) -> !a ? false : b)
            .orElse(false);
    }

    @Override
    public void disconnect() {
        this.connectionMap.values().forEach(IRconConnection::disconnect);
    }

    @Override
    public CompletableFuture<CommandResponse> sendCommand(Command command) {
        return this.sendCommand(command, "MAIN");
    }

    @Override
    public CompletableFuture<CommandResponse> sendCommand(Command command, String priority) {
        return this.connectionMap.get(priority).sendCommand(command);
    }

    @Override
    public void close() {
        log.info("Closing wrapper {}", this.configuration.getId());
        this.disconnect();
        this.connectionMap.values().forEach(x -> {
            try {
                x.close();
            } catch (Exception ex) {
                log.error("Failed to close rcon connection {}", ex.getMessage());
            }
        });
    }
}
