package com.jsoc.overwatch.services.connection.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.jsoc.overwatch.controller.dto.ServerStateDto;
import com.jsoc.overwatch.services.connection.events.ServerStateUpdateEvent;
import com.jsoc.overwatch.services.mqttListener.MqttService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@Configuration
public class ConnectionMessageMediator {

    @Autowired
    private MqttService service;

    @EventListener
    public void serverStatusUpdate(ServerStateUpdateEvent event) throws JsonProcessingException {
        this.service.publish("server/" + event.getServerGuid() + "/status", new ServerStateDto(event.getServerGuid(), event.getStatus()));
    }
}
