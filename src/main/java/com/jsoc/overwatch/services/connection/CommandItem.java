package com.jsoc.overwatch.services.connection;

import com.jsoc.overwatch.services.connection.api.Command;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.concurrent.CompletableFuture;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class CommandItem {
    private Command command;
    private CompletableFuture<CommandResponse> future;

    public static CommandItem from(Command command, CompletableFuture<CommandResponse> future) {
        return new CommandItem(command, future);
    }
}
