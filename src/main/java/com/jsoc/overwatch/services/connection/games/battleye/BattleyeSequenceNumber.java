/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.connection.games.battleye;

/**
 * Sequence number for packets to battleye.
 */
public class BattleyeSequenceNumber {
    /**
     * Current sequence number.
     */
    private long current;
    
    /**
     * Creates a new {@link BattleyeSequenceNumber} instance.
     */
    public BattleyeSequenceNumber() {
        this.current = 0x00;
    }

    public void setLastSeen(int value) {
        this.current = value;
    }

    public void reset() {
        this.current = 0;
    }

    /**
     * Get current value and increment.
     * 
     * @return current sequence number, single byte.
     */
    public byte next() {
        synchronized (this) {
            byte out = (byte) this.current;
            this.current++;
            return out;
        }
    }
    
    /**
     * Get current sequence number.
     * 
     * @return Sequence number.
     */
    public byte current() {
        return (byte)this.current;
    }
}
