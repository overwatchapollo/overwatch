/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.connection.games.arma;

import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import com.jsoc.overwatch.services.connection.games.battleye.BattleyeProtocolClient;
import com.jsoc.overwatch.services.connection.games.battleye.BattleyeProtocolClientFactory;
import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import com.jsoc.overwatch.services.connection.events.RconMessageReceived;
import com.jsoc.overwatch.services.connection.events.ServerStateUpdateEvent;
import com.jsoc.overwatch.services.connection.api.Command;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import com.jsoc.overwatch.services.connection.api.Packet;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.Subject;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CompletableFuture;

/**
 * Spring boot service to setup an RCon connection and allow commands to be pinged.
 */
@Slf4j
public class ArmaTerminalService implements IRconConnection {
    
    /**
     * Battleye client.
     */
    private final BattleyeProtocolClient client;

    /**
     * Unique ID for this server config.
     */
    private final Long guid;

    /**
     * User friendly name for server.
     */
    private final String name;

    /**
     * Scheduler for periodic callbacks and async subscriptions.
     */
    private final Scheduler scheduler;

    /**
     * Whether this is master or backup.
     */
    private boolean isMaster = false;

    /**
     * Message subject, to push messages out of the client.
     */
    private Subject<RconMessageReceived> messagePublisher;

    /**
     * State subject, to push state updates, regarding connection status.
     */
    private Subject<ServerStateUpdateEvent> eventPublisher;

    private Disposable stateDisposable;

    private Disposable messageDisposable;

    /**
     * Creates a new instance of the {@link ArmaTerminalService} class.
     * @param configuration Server configuration to use.
     * @param scheduler Scheduler to run subscriptions and periodic calls on.
     * @param eventPublisher Subject to publish events on.
     * @param messagePublisher Subject to publish messages on.
     */
    ArmaTerminalService(ServerConnection configuration, Scheduler scheduler, Subject<ServerStateUpdateEvent> eventPublisher, Subject<RconMessageReceived> messagePublisher) {
        this(configuration, scheduler, eventPublisher, messagePublisher, new BattleyeProtocolClientFactory());
    }

    /**
     * Creates a new instance of the {@link ArmaTerminalService} class.
     * @param configuration Server configuration to use.
     * @param scheduler Scheduler to run subscriptions and periodic calls on.
     * @param eventPublisher Subject to publish events on.
     * @param messagePublisher Subject to publish messages on.
     * @param clientFactory Factory to create protocol clients on.
     */
    private ArmaTerminalService(ServerConnection configuration, Scheduler scheduler, Subject<ServerStateUpdateEvent> eventPublisher, Subject<RconMessageReceived> messagePublisher, BattleyeProtocolClientFactory clientFactory) {
        this.guid = configuration.getId();
        this.name = configuration.getServerName();
        this.scheduler = scheduler;
        this.messagePublisher = messagePublisher;
        this.eventPublisher = eventPublisher;
        this.client = clientFactory.build(
            this.scheduler,
            configuration.getIpAddress(),
            configuration.getPort(),
            configuration.getPassword());

        this.stateDisposable = this.client.getStateObservable().subscribe(x ->
            this.eventPublisher.onNext(this.buildUpdate())
        );

        this.messageDisposable = this.client.getMessageObservable().subscribeOn(this.scheduler).subscribe(x -> {
            String publishedMessage = new String(x).substring(9);
            this.consumeUpdates(publishedMessage);
        });
    }

    /**
     * Build a server state update event, used to easily just broadcast changes to the system.
     * @return Currently accurate server state event.
     */
    private ServerStateUpdateEvent buildUpdate() {
        return new ServerStateUpdateEvent(this, this.guid, this.name, this.client.getStatus());
    }

    /**
     * Consumer callback for RCON Messages.
     * @param message Message from Battleye client.
     */
    private void consumeUpdates(String message) {
        log.trace("Server {} received RCON message: {}", this.guid, message);
        if (this.isMaster) {
            this.messagePublisher.onNext(new RconMessageReceived(this, this.guid, message));
        }
    }

    @Override
    public void setMaster(boolean master) {
        this.isMaster = master;
    }

    @Override
    public boolean isMaster() {
        return this.isMaster;
    }

    public ServerConnectionStatus getStatus() {
        return this.client.getStatus();
    }


    @Override
    public boolean connect() {
        if (this.getStatus() == ServerConnectionStatus.CONNECTED) {
            return true;
        } else {
            log.info("Connecting {} {}", this.name, this.isMaster);
            this.client.activate();
            boolean connected = getStatus() == ServerConnectionStatus.CONNECTED;
            this.eventPublisher.onNext(new ServerStateUpdateEvent(this, this.guid, this.name, this.getStatus()));
            return connected;
        }
    }

    @Override
    public void disconnect() {
        this.close();
    }

    @Override
    public CompletableFuture<CommandResponse> sendCommand(Command command) {
        CompletableFuture<CommandResponse> future = new CompletableFuture<>();
        this.scheduler.scheduleDirect(() -> {
            CommandResponse cr = new CommandResponse();
            cr.setCommand(command.getCommand());
            Packet packet = executeCommand(command);
            if (packet == null) {
                cr.setSuccess(false);
            } else {
                cr.setSuccess(packet.getSuccess().orElse(false));
                cr.setResponse(new String(packet.getResponse()).substring(8));
            }
            
            future.complete(cr);
        });
        return future;
    }

    @Override
    public CompletableFuture<CommandResponse> sendCommand(Command command, String priority) {
        return this.sendCommand(command);
    }

    private Packet executeCommand(Command command) {
        try {
            Packet packet = this.client.sendCommand(command.getCommand());

            return packet;
        } catch (InterruptedException ex) {
            log.error("Unable to send command: {}", ex.getMessage());
        }
        return null;
    }

    @Override
    public void close() {
        try {
            this.client.close();
            this.stateDisposable.dispose();
            this.messageDisposable.dispose();
            this.eventPublisher.onNext(new ServerStateUpdateEvent(this, this.guid, this.name, this.getStatus()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
