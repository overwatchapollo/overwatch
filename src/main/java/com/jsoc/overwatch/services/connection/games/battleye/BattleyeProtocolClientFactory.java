package com.jsoc.overwatch.services.connection.games.battleye;

import io.reactivex.Scheduler;

import static com.google.common.base.Preconditions.checkNotNull;

public class BattleyeProtocolClientFactory {

    public BattleyeProtocolClient build(Scheduler scheduler, String hostname, int port, String password) {
        checkNotNull(hostname);
        checkNotNull(password);
        checkNotNull(port);
        AsyncBattleyeSocket socket = new AsyncBattleyeSocket(hostname, port);
        BattleyeProtocolClient client = new BattleyeProtocolClient(scheduler, hostname, port, password, socket);
        return client;
    }

    public BattleyeProtocolClient build(Scheduler scheduler, String hostname, int port, String password, AsyncBattleyeSocket socket) {
        checkNotNull(hostname);
        checkNotNull(password);
        checkNotNull(port);
        BattleyeProtocolClient client = new BattleyeProtocolClient(scheduler, hostname, port, password, socket);
        return client;
    }
}
