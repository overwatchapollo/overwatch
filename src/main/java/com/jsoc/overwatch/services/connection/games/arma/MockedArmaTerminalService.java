/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.connection.games.arma;

import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import com.jsoc.overwatch.services.connection.events.RconMessageReceived;
import com.jsoc.overwatch.services.connection.api.Command;
import com.jsoc.overwatch.services.connection.api.CommandFactory;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.Subject;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Mock implementation for the IRcon Connection.
 */
@Slf4j
public class MockedArmaTerminalService implements IRconConnection {

    private CommandFactory commandFactory = new CommandFactory();

    private Scheduler scheduler = Schedulers.from(Executors.newSingleThreadExecutor());

    private List<Disposable> scheduledDisposables = new ArrayList<>();

    private final Pattern versionPattern = Pattern.compile("^version");

    private final Pattern playersPattern = Pattern.compile("^players");

    private final Pattern sayPattern = Pattern.compile("^say (.+) (.+)$");

    private final Pattern kickPattern = Pattern.compile("^kick (.+) (.+)");

    private final Pattern banPattern = Pattern.compile("^ban (.+) (.+) (.+)");

    private final Map<String, ConnectedPlayer> playerMap = new HashMap<>();

    private final Long guid;

    private Subject<RconMessageReceived> messagePublisher;

    MockedArmaTerminalService(Subject<RconMessageReceived> messagePublisher, Long guid) {
        this.messagePublisher = messagePublisher;
        log.info("Starting mock arma server.");
        this.guid = guid;
        this.serverRestart();
    }

    @Override
    public ServerConnectionStatus getStatus() {
        return ServerConnectionStatus.CONNECTED;
    }

    @Override
    public void setMaster(boolean master) {

    }

    @Override
    public boolean isMaster() {
        return true;
    }

    @Override
    public boolean connect() {
        return true;
    }

    @Override
    public void disconnect() {

    }

    @Override
    public CompletableFuture<CommandResponse> sendCommand(Command command) {
        CommandResponse cr = new CommandResponse();
        CompletableFuture<CommandResponse> future = new CompletableFuture<>();
        cr.setCommand(command.getCommand());
        this.scheduler.scheduleDirect(() ->
        {
            log.info("Got command {}", command.getCommand());
            this.handleCommand(command);
            StringBuilder sb = new StringBuilder();
            sb.append("Command ");
            sb.append(command.getCommand());
            sb.append(" returns \n");
            sb.append(cr.getResponse());
            log.info(sb.toString());
            future.complete(cr);
        });
        return future;
    }

    @Override
    public CompletableFuture<CommandResponse> sendCommand(Command command, String priority) {
        return this.sendCommand(command);
    }

    private Integer getNextSessionId() {
        return StreamEx.of(this.playerMap.keySet())
                .map(x -> Integer.parseInt(x))
                .max(Integer::compareTo).orElse(0) + 1;
    }

    private void serverRestart() {
        log.info("Restarting server" + this.guid);
        this.scheduledDisposables.forEach(x -> x.dispose());
        this.scheduledDisposables.clear();
        this.playerMap.clear();


        Disposable d1 = this.scheduler.schedulePeriodicallyDirect(this::mockPlayer, 1,5, TimeUnit.SECONDS);
        Disposable d2 = this.scheduler.schedulePeriodicallyDirect(this::playerSpeak, 5, 5, TimeUnit.SECONDS);
        //Disposable d3 = this.scheduler.schedulePeriodicallyDirect(() -> this.serverRestart(), 600, 600, TimeUnit.SECONDS);
        this.scheduledDisposables.addAll(Arrays.asList(d1, d2));
    }

    private void mockPlayer() {
        log.debug("mock new player");
        Integer maxId = this.getNextSessionId();
        String name = UUID.randomUUID().toString();
        String guid = UUID.randomUUID().toString();

        Random broadcastLiklyhood = new Random();
        boolean broadcast = broadcastLiklyhood.nextBoolean();

        this.playerJoin(
            name,
        "123.123.123.123",
            guid,
            maxId.toString(),
            true);
        Random r = new Random();
        int disconnect = 10 + r.nextInt(100);
        Disposable d1 = this.scheduler.scheduleDirect(() -> this.playerLeave(name, maxId.toString(), guid), disconnect, TimeUnit.SECONDS);
        this.scheduledDisposables.add(d1);
    }

    private void playerSpeak() {
        Optional<ConnectedPlayer> cp = StreamEx
            .of(this.playerMap.values()).findAny(ConnectedPlayer::isConnected);

        if (!cp.isPresent()) {
            log.debug("No player exists to speak.");
            return;
        }

        String msg = this.buildMessage(cp.get().getName());

        log.debug("sending {}", msg);
        this.publishMessage(msg);
    }

    private String buildMessage(String name) {
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        Random random = new Random();
        int i = random.nextInt(100);
        if (i < 20) {
            sb.append("Group");
        } else if (i < 40) {
            sb.append("Side");
        } else if (i < 60) {
            sb.append("Command");
        } else if (i < 80) {
            sb.append("Global");
        } else {
            sb.append("Vehicle");
        }

        sb.append(") ");
        sb.append(name);
        sb.append(": ");

        int msgVal = random.nextInt(100);
        if (msgVal < 90) {
            sb.append("my message");
        } else {
            sb.append("!support a player said a mean thing");
        }


        return sb.toString();
    }

    private void playerLeave(String name, String session, String guid) {
        Random random = new Random();
        int i = random.nextInt(100);
        if (i < 50) {
            this.playerDisconnect(name, session);
        } else if (i < 75) {
            this.playerKicked(name, guid, session, "kicked");
        } else {
            this.playerBanned(name, guid, session, "banned");
        }
    }

    private CommandResponse handleCommand(Command command) {

        final String com = command.getCommand();
        Matcher versionMatcher = versionPattern.matcher(com);
        Matcher playerMatcher = playersPattern.matcher(com);
        Matcher sayMatcher = sayPattern.matcher(com);
        Matcher kickMatcher = kickPattern.matcher(com);
        Matcher banMatcher = banPattern.matcher(com);

        if (versionMatcher.find()) {
            return this.handleVersion(versionMatcher, command);
        } else if (playerMatcher.find()) {
            return this.handlePlayer(playerMatcher, command);
        } else if (sayMatcher.find()) {
            return this.handleSay(sayMatcher, command);
        } else if (kickMatcher.find()) {
            return this.handleKick(kickMatcher, command);
        } else if (banMatcher.find()) {
            return this.handleBan(kickMatcher, command);
        } else {
            throw new UnsupportedOperationException();
        }
    }

    private CommandResponse handleKick(Matcher matcher, Command command) {
        CommandResponse cr = new CommandResponse();
        cr.setResponse("");
        String id = matcher.group(1);
        String message = matcher.group(2);
        ConnectedPlayer cp = this.playerMap.get(id);
        this.playerKicked(cp.getName(), cp.getGuid(), cp.getSessionId(), message);
        return cr;
    }

    private CommandResponse handleBan(Matcher matcher, Command command) {
        CommandResponse cr = new CommandResponse();
        cr.setResponse("");
        cr.setSuccess(true);
        String guid = matcher.group(1);
        String duration = matcher.group(2);
        String message = matcher.group(3);
        ConnectedPlayer cp = StreamEx.of(this.playerMap.values()).findFirst(x -> Objects.equals(guid, x.getGuid())).orElseThrow(() -> new IllegalArgumentException());
        this.playerBanned(cp.getName(), cp.getGuid(), cp.getSessionId(), message);
        return cr;
    }

    private CommandResponse handleVersion(Matcher matcher, Command command) {
        CommandResponse cr = new CommandResponse();
        cr.setResponse("XXXXXXXX 1.217");
        cr.setSuccess(true);
        return cr;
    }

    private CommandResponse handlePlayer(Matcher matcher, Command command) {
        CommandResponse cr = new CommandResponse();
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append("\n");
        StreamEx.of(this.playerMap.values()).map(x -> {
            StringBuilder playerBuilder = new StringBuilder();
            playerBuilder.append(x.getSessionId());
            playerBuilder.append(" ");
            playerBuilder.append(x.getIpAddress());
            playerBuilder.append(":2304 ");
            playerBuilder.append(x.getPing());
            playerBuilder.append(" ");
            playerBuilder.append(x.getGuid());
            playerBuilder.append(" ");
            playerBuilder.append(x.getName());
            return playerBuilder.toString();
        }).forEach(x -> sb.append(x +"\n"));
        cr.setResponse(sb.toString());
        cr.setSuccess(true);
        return cr;
    }

    private CommandResponse handleSay(Matcher matcher, Command command) {
        CommandResponse cr = new CommandResponse();
        cr.setSuccess(true);
        cr.setResponse("");
        String message = matcher.group(2);
        this.publishMessage("(group) paul: " + message);
        return cr;
    }

    private void playerJoin(String name, String ip, String guid, String sessionId, boolean broadcast) {
        ConnectedPlayer cp = ConnectedPlayer.builder().connected(true).name(name).sessionId(sessionId).guid(guid).ipAddress(ip).build();
        this.playerMap.put(sessionId, cp);
        if (broadcast) {
            this.announcePlayerJoin(cp);
        }
    }

    private void announcePlayerJoin(ConnectedPlayer cp) {
        String sessionId = cp.getSessionId();
        String name = cp.getName();
        String ip = cp.getIpAddress();
        StringBuilder connectedBuilder = new StringBuilder();
        connectedBuilder.append("Player #");
        connectedBuilder.append(sessionId);
        connectedBuilder.append(" ");
        connectedBuilder.append(name);
        connectedBuilder.append(" (");
        connectedBuilder.append(ip);
        connectedBuilder.append(") connected");
        this.publishMessage(connectedBuilder.toString());
        StringBuilder guidBuilder = new StringBuilder();
        guidBuilder.append("Verified GUID (");
        guidBuilder.append(cp.getGuid());
        guidBuilder.append(") of player #");
        guidBuilder.append(sessionId);
        guidBuilder.append(" ");
        guidBuilder.append(name);
        this.publishMessage(guidBuilder.toString());
    }

    private void removePlayer(String sessionId) {
        ConnectedPlayer cp = this.playerMap.get(sessionId);
        cp.setConnected(false);
    }

    private void playerDisconnect(String name, String sessionId) {
        StringBuilder disconnectedBuilder = new StringBuilder();
        disconnectedBuilder.append("Player #");
        disconnectedBuilder.append(sessionId);
        disconnectedBuilder.append(" ");
        disconnectedBuilder.append(name);
        disconnectedBuilder.append(" disconnected");
        this.removePlayer(sessionId);
        this.publishMessage(disconnectedBuilder.toString());
    }

    private void playerKicked(String name, String guid, String sessionId, String message) {
        StringBuilder kickedPlayer = new StringBuilder();
        kickedPlayer.append("Player #");
        kickedPlayer.append(sessionId);
        kickedPlayer.append(" ");
        kickedPlayer.append(name);
        kickedPlayer.append(" (");
        kickedPlayer.append(guid);
        kickedPlayer.append(") has been kicked by BattlEye: Admin Kick");
        if (message != null) {
            kickedPlayer.append(" (");
            kickedPlayer.append(message);
            kickedPlayer.append(")");
        }

        this.removePlayer(sessionId);
        this.publishMessage(kickedPlayer.toString());
    }

    private void playerBanned(String name, String guid, String sessionId, String message) {
        StringBuilder bannedPlayer = new StringBuilder();
        bannedPlayer.append("Player #");
        bannedPlayer.append(sessionId);
        bannedPlayer.append(" ");
        bannedPlayer.append(name);
        bannedPlayer.append(" (");
        bannedPlayer.append(guid);
        bannedPlayer.append(") has been banned by BattlEye: Admin Ban ");
        if (message != null) {
            bannedPlayer.append(" (");
            bannedPlayer.append(message);
            bannedPlayer.append(")");
        }

        this.removePlayer(sessionId);
        this.publishMessage(bannedPlayer.toString());
    }

    private void publishMessage(String message) {
        this.messagePublisher.onNext(new RconMessageReceived(this, this.guid, message));
    }

    @Override
    public void close() {
        this.scheduler.shutdown();
    }
}
