package com.jsoc.overwatch.services.connection.games.arma;

import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import com.jsoc.overwatch.services.connection.api.Command;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import com.jsoc.overwatch.services.connection.events.RconMessageReceived;
import com.jsoc.overwatch.services.connection.events.ServerStateUpdateEvent;
import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.Subject;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CompletableFuture;

@Slf4j
public class ServerRunnerWrapper implements IRconConnection {
    private ServerConnectionRunner runner;
    private Scheduler scheduler;
    private Disposable statusDisposable;
    private Disposable messageDisposable;
    public ServerRunnerWrapper(Scheduler scheduler, ServerConnection configuration, Subject<ServerStateUpdateEvent> statePublisher, Subject<RconMessageReceived> messagePublisher) {
        this.scheduler = scheduler;
        this.runner = new ServerConnectionRunner(scheduler, configuration.getIpAddress(), configuration.getPort(), configuration.getPassword());
        this.statusDisposable = this.runner.getStatusObservable()
            .subscribe(x -> {
                log.trace("State update for server {}: {}", configuration.getId(), x);
                statePublisher.onNext(new ServerStateUpdateEvent(this, configuration.getId(), configuration.getServerName(), x));
            });

        this.messageDisposable = this.runner.getMessageObservable().subscribe(x -> {
            log.trace("Message update for server {}: {}", configuration.getId(), x);
            messagePublisher.onNext(new RconMessageReceived(this, configuration.getId(), x.substring(9)));
        });
    }

    @Override
    public void setMaster(boolean master) {

    }

    @Override
    public boolean isMaster() {
        return true;
    }

    @Override
    public ServerConnectionStatus getStatus() {
        return this.runner.getStatus();
    }

    @Override
    public boolean connect() {
        this.scheduler.scheduleDirect(() -> this.runner.run());
        return true;
    }

    @Override
    public void disconnect() {
        log.info("Wrapper disconnect");
        this.runner.raiseEvent(ServerConnectionStatus.DISCONNECTED);
    }

    @Override
    public CompletableFuture<CommandResponse> sendCommand(Command command) {
        return this.runner.sendCommand(command);
    }

    @Override
    public CompletableFuture<CommandResponse> sendCommand(Command command, String priority) {
        return this.runner.sendCommand(command);
    }

    @Override
    public void close() {
        log.info("Wrapper close");
        this.disconnect();
        this.runner.close();
        this.scheduler.shutdown();
        this.statusDisposable.dispose();
        this.messageDisposable.dispose();
    }
}
