/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.connection.api;

import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;

/**
 * Packet containing question and response.
 */
@Slf4j
public class Packet {
    /**
     * Byte array for packet question.
     */
    private final byte[] question;
    
    /**
     * Byte array for packet response.
     */
    private byte[] response;
    
    /**
     * Latch for waiting for response.
     */
    private final CountDownLatch latch;
    private final boolean waitForResponse;

    private Optional<Boolean> successful;

    private final byte sequenceNumber;
    /**
     * Create a packet for sending to battleye.
     * 
     * @param question Byte array to send.
     */
    public Packet(byte[] question, boolean waitForResponse) {
        this.question = question;
        this.response = new byte[0];
        this.sequenceNumber = this.question[8];
        this.latch = new CountDownLatch(1);
        this.waitForResponse = waitForResponse;
        this.successful = Optional.empty();
    }

    public byte getSequenceNumber() {
        return this.sequenceNumber;
    }

    public boolean waitRequired() {
        return this.waitForResponse;
    }

    public void setSuccess(boolean success) {
        this.successful = Optional.of(success);
    }

    public Optional<Boolean> getSuccess() {
        return this.successful;
    }

    public static PacketType getPacketType(byte[] arr) {
        return PacketType.fromInt(arr[7]);
    }
    
    /**
     * Get byte array being sent.
     * 
     * @return byte array which is to be sent.
     */
    public byte[] getQuestion() {
        return this.question;
    }
    
    /**
     * Set the response of the packet.
     * 
     * @param response Response of packet.
     */
    public void setResponse(byte[] response) {
        this.response = response;
        this.latch.countDown();
    }
    
    /**
     * Get the response from the packet.
     * 
     * @return raw response.
     */
    public byte[] getResponse() {
        return this.response;
    }
    
    /**
     * Wait on response being set.
     * 
     * @param timeout Timeout numerator.
     * @param unit Timeout unit.
     * @return False if timeout occurred.
     * @throws InterruptedException If interrupted.
     */
    public boolean await(long timeout, TimeUnit unit) throws InterruptedException {
        return this.latch.await(timeout, unit);
    }
}
