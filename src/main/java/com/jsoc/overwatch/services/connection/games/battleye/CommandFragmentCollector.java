package com.jsoc.overwatch.services.connection.games.battleye;

import io.reactivex.subjects.Subject;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class to build together fragmented Battleye packets.
 */
@Slf4j
public class CommandFragmentCollector {

    /**
     * Location of the index of a the fragmented packet within it.
     */
    private final static int INDEX_LOCATION = 11;
    private final static int HEADER_SIZE = 12;
    private final static int OUTPUT_HEADER_SIZE = 9;
    private final static int TOTAL_FRAGMENT_LOCATION = 10;
    private final static int SEQUENCE_NUMBER_LOCATION = 8;

    /**
     * Subject to push completed packets out.
     */
    private final Subject<byte[]> commandPublisher;

    /**
     * Packet map to collect fragments.
     */
    private final Map<Byte, List<byte[]>> packetMap = new HashMap<>();

    /**
     * Create a new instance of the {@link CommandFragmentCollector} class.
     * @param commandPublisher Subject to push out completed packets.
     */
    public CommandFragmentCollector(Subject<byte[]> commandPublisher) {
        this.commandPublisher = commandPublisher;
    }

    /**
     * Clear the local cache of packets.
     */
    public void clear() {
        synchronized (this.packetMap) {
            this.packetMap.clear();
        }
    }

    /**
     * Push a packet to the collector.
     * @param rcv Packet to push.
     */
    void pushPacket(byte[] rcv) {
        Byte seq = rcv[SEQUENCE_NUMBER_LOCATION];
        byte max = rcv[TOTAL_FRAGMENT_LOCATION];
        byte current = rcv[INDEX_LOCATION]; // Current position in fragment list.
        synchronized (this.packetMap) {
            if (!this.packetMap.containsKey(seq)) {
                this.packetMap.put(seq, new ArrayList<>());
            }

            List<byte[]> arr = this.packetMap.get(seq);
            if (StreamEx.of(arr).noneMatch(x -> x[INDEX_LOCATION] == current)) {
                arr.add(rcv);
            }

            if (arr.size() == max) {
                this.packetMap.remove(seq);
                byte[] output = this.stitchPacket(arr);
                this.commandPublisher.onNext(output);
            }
        }
    }

    /**
     * Stitch a fragmented packet together. The fragments are have their headers cut off and are then stitched
     * together in order. The header from the first packet is used to create a non-fragmented header.
     * @param packet Packet fragments.
     */
    private byte[] stitchPacket(List<byte[]> packet) {
        byte[][] megapacket = new byte[packet.size()][];
        int size = OUTPUT_HEADER_SIZE;
        for(byte[] fragment : packet) {
            byte[] temp = new byte[fragment.length-HEADER_SIZE];
            System.arraycopy(fragment, HEADER_SIZE, temp, 0, fragment.length - HEADER_SIZE);
            byte index = fragment[INDEX_LOCATION];
            megapacket[index] = temp;
            size += temp.length;
        }

        byte[] output = new byte[size];
        System.arraycopy(packet.get(0), 0, output, 0, OUTPUT_HEADER_SIZE);
        /* Concatenate the fragments */
        int x = OUTPUT_HEADER_SIZE;
        for (byte[] bytes : megapacket) {
            if (bytes != null) {
                for (byte b : bytes) {
                    output[x] = b;
                    x++;
                }
            }
        }

        return output;
    }
}
