/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.connection.api;

/**
 * Configuration interface for connecting to rcon client.
 */
public @interface RemoteConsoleConfiguration {
    /**
     * IP Address of the server.
     * 
     * @return IP address to connect to.
     */
    String ipaddress();
    
    /**
     * Port of the server.
     * 
     * @return Open port for rcon client.
     */
    int portNumber();
    
    /**
     * Password to authenticate self with.
     * 
     * @return String password.
     */
    String password();
}
