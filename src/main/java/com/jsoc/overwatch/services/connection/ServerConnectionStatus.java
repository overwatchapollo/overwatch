package com.jsoc.overwatch.services.connection;

public enum ServerConnectionStatus {
    CLOSED("closed"),
    DISCONNECTED("disconnected"),
    CONNECTING("connecting"),
    CONNECTED("connected"),
    REFUSED("refused"),
    UNAUTHORISED("unauthorised"),
    TIMEOUT("timeout"),
    BADHOST("badhost");

    private String status;
    ServerConnectionStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }
}
