/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.connection.games.arma;

import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import com.jsoc.overwatch.services.connection.api.ICommandExecutor;

/**
 * Interface describing a server connection. This can be used to execute commands or for general server management.
 */
public interface IRconConnection extends ICommandExecutor, AutoCloseable {

    void setMaster(boolean master);
    boolean isMaster();

    /**
     * Get connection status.
     * @return Connection status.
     */
    ServerConnectionStatus getStatus();

    /**
     * Connect to remote server.
     * @return True if successful.
     */
    boolean connect();

    /**
     * Disconnect from remote server.
     */
    void disconnect();
}
