package com.jsoc.overwatch.services.connection.games.battleye;

import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.jsoc.overwatch.services.connection.ServerConnectionStatus.TIMEOUT;

@Slf4j
public class TimeoutManager implements Disposable {
    private Disposable disposable;
    private Scheduler scheduler;
    private PublishSubject<ServerConnectionStatus> publisher;
    private Observable<?> observable;
    public TimeoutManager(Scheduler scheduler, List<Observable<?>> observables, PublishSubject<ServerConnectionStatus> publisher) {
        this.scheduler = scheduler;
        this.publisher = publisher;
        this.observable = Observable.merge(observables);
        this.disposable = this.buildObserver();
    }

    @Override
    public void dispose() {
        log.info("Disposing of timeout manager.");
        this.disposable.dispose();
    }

    @Override
    public boolean isDisposed() {
        return this.disposable.isDisposed();
    }

    private Observable<?> getTimeoutObservable() {
        return this.observable.timeout(60, TimeUnit.SECONDS).observeOn(this.scheduler);
    }

    private Disposable buildObserver() {
        return this.getTimeoutObservable()
            .subscribe(
                x -> log.trace("msg"),
                err -> {
                    this.publisher.onNext(TIMEOUT);
                },
                () -> {
                    this.publisher.onNext(TIMEOUT);
                });
    }
}
