package com.jsoc.overwatch.services.connection.games.battleye;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.*;

import static com.google.common.base.Preconditions.checkNotNull;

@Slf4j
public class AsyncBattleyeSocket implements IRconSocket {
    /**
     * Hostname of the battleye server.
     */
    private final String hostname;

    /**
     * Port of the battleye server.
     */
    private final int port;

    /**
     * UDP socket used to connect to battleye.
     */
    private DatagramSocket socket;

    private PublishSubject<DatagramPacket> packetPublisher = PublishSubject.create();

    public AsyncBattleyeSocket(String hostname, int port) {
        checkNotNull(hostname);
        this.hostname = hostname;
        this.port = port;
    }

    @Override
    public void activate() throws SocketException, UnknownHostException {
        this.packetPublisher = PublishSubject.create();
        this.socket = new DatagramSocket();
        if (this.socket.isClosed() || !this.socket.isConnected()) {
            log.debug("Connecting to battleye server {}:{}", this.hostname, this.port);
            this.socket.connect(InetAddress.getByName(this.hostname), this.port);
            log.debug("Socket open to to {}:{}", this.hostname, this.port);
        } else {
            log.warn("Already connected to battleye server");
        }
    }

    @Override
    public Observable<byte[]> getDataObservable() {
        return this.packetPublisher.map(packet -> {
            byte[] rcv = new byte[packet.getLength()];
            System.arraycopy(packet.getData(), 0, rcv, 0, rcv.length);
            return rcv;
        });
    }

    public void sendData(byte[] message) throws IOException {
        DatagramPacket outgoingPacket = new DatagramPacket(message, message.length, socket.getRemoteSocketAddress());
        this.socket.send(outgoingPacket);
    }

    @Override
    public void close() {
        this.packetPublisher.onComplete();

        if (this.socket != null && !this.socket.isClosed()) {
            this.socket.close();
        }
    }

    /**
     * Main loop for listening to returning data.
     */
    public void listenLoop() {
        log.debug("Async protocol listener started");
        while(!this.socket.isClosed()) {
            byte[] rcv = new byte[4096];

            /* Receive the Datagram */
            DatagramPacket packet = new DatagramPacket(rcv, rcv.length);
            try {
                this.socket.receive(packet);
                this.packetPublisher.onNext(packet);
            } catch (IOException ex) {
                if (this.socket.isClosed()) {
                    log.debug("Listen aborted on {} - socket closed.", this.hostname);
                } else {
                    log.error("Unable to receive on {}.", this.hostname, ex);
                }
                break;
            }
        }
        log.debug("Protocol listener stopping.");
    }
}
