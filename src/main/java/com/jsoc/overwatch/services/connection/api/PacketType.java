/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.connection.api;

import java.text.MessageFormat;

/**
 * Enumeration for the different types of packet for battleye.
 */
public enum PacketType {
    /**
     * Login packet.
     */
    LOGIN(0),
    
    /**
     * Command packet.
     */
    COMMAND(1),
    
    /**
     * Message packet.
     */
    MESSAGE(2);
    
    /**
     * Numerical value for packet type.
     */
    private final int val;
    
    /**
     * Initialises a new instance of the {@link PacketType} class.
     * 
     * @param val Numerical value of type.
     */
    private PacketType(int val) {
        this.val = val;
    }

    /**
     * Gets the numerical type of packet.
     * @return 
     */
    public int getType() {
        return this.val;
    }
    
    public static PacketType fromInt(int val) {
        switch (val) {
            case 0:
                return LOGIN;
            case 1:
                return COMMAND;
            case 2:
                return MESSAGE;
            default:
                throw new IllegalArgumentException(MessageFormat.format("Packet Type {} is not valid.", val));
        }
        
    }
}
