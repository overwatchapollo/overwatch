package com.jsoc.overwatch.services.connection.games.battleye;

import io.reactivex.Observable;

import java.net.SocketException;
import java.net.UnknownHostException;

public interface IRconClient extends AutoCloseable {

    void activate() throws UnknownHostException, SocketException;
    Observable<byte[]> getMessageObservable();
    Observable<byte[]> getCommandObservable();
}
