package com.jsoc.overwatch.services.connection.games.arma;

import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import com.jsoc.overwatch.services.connection.events.RconMessageReceived;
import com.jsoc.overwatch.services.connection.events.ServerStateUpdateEvent;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.Subject;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RconFactory {

    private final ExecutorService executorService = Executors.newCachedThreadPool();

    private Subject<ServerStateUpdateEvent> statePublisher;

    private Subject<RconMessageReceived> messagePublisher;

    public RconFactory(Subject<ServerStateUpdateEvent> statePublisher, Subject<RconMessageReceived> messagePublisher) {
        this.statePublisher = statePublisher;
        this.messagePublisher = messagePublisher;
    }

    public IRconConnection buildWrapper(ServerConnection connection) {
        return new ArmaConnectionWrapper(connection, this.statePublisher, this.messagePublisher);
    }

    public IRconConnection buildServer(ServerConnection connection) {
        if (connection.getMock() != null && connection.getMock()) {
            return new MockedArmaTerminalService(this.messagePublisher, connection.getId());
        } else {
            return new ArmaTerminalService(connection, Schedulers.from(this.executorService), this.statePublisher, this.messagePublisher);
        }
    }

    public IRconConnection buildRunner(ServerConnection connection) {
        return new ServerRunnerWrapper(Schedulers.from(this.executorService), connection, this.statePublisher, this.messagePublisher);
    }
}
