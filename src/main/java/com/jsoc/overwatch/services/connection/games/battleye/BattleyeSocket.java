package com.jsoc.overwatch.services.connection.games.battleye;

import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import com.jsoc.overwatch.services.connection.api.Command;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import com.jsoc.overwatch.services.connection.api.Packet;
import com.jsoc.overwatch.services.connection.api.PacketType;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.jsoc.overwatch.services.connection.ServerConnectionStatus.*;
import static com.jsoc.overwatch.services.connection.api.Packet.getPacketType;

@Slf4j
public class BattleyeSocket {
    /**
     * Hostname of the battleye server.
     */
    private final String hostname;

    /**
     * Port of the battleye server.
     */
    private final int port;

    /**
     * Password for the battleye connection.
     */
    private final String password;

    /**
     * UDP socket used to connect to battleye.
     */
    private DatagramSocket socket;

    /**
     * Message subject, to push messages out of the client.
     */
    private PublishSubject<String> messagePublisher = PublishSubject.create();

    private final CommandFragmentCollector fragmentCollector;
    private final Subject<byte[]> commandPublisher;
    private final Observable<byte[]> commandObserver;

    private Disposable listenThread = null;
    /**
     * Sequence number generator.
     */
    private BattleyeSequenceNumber sequencer = new BattleyeSequenceNumber();
    private final PublishSubject<ServerConnectionStatus> eventPublisher = PublishSubject.create();

    private Disposable timeoutDisposable;

    private Scheduler scheduler;

    private CountDownLatch latch;

    public BattleyeSocket(Scheduler scheduler, String hostname, int port, String password) {
        checkNotNull(hostname);
        checkNotNull(password);
        this.hostname = hostname;
        this.port = port;
        this.password = password;
        this.commandPublisher = PublishSubject.create();
        this.scheduler = scheduler;
        this.commandObserver = this.commandPublisher.subscribeOn(this.scheduler);
        this.fragmentCollector = new CommandFragmentCollector(this.commandPublisher);
    }

    public ServerConnectionStatus connect() {
        log.info("Starting connection to {}:{}", this.hostname, this.port);
        try {
            this.socket = new DatagramSocket();
        } catch (SocketException ex) {
            log.error("Failed to connect to {}:{}, reason: {}", this.hostname, this.port, ex.getLocalizedMessage());
            return ServerConnectionStatus.DISCONNECTED;
        }
        if (this.socket.isClosed() || !this.socket.isConnected()) {
            log.info("Connecting to battleye server {}:{}", this.hostname, this.port);
            try {
                this.socket.connect(InetAddress.getByName(this.hostname), this.port);

                if (!this.socket.isConnected()) {
                    log.info("Could not connect to battleye server {}:{}", this.hostname, this.port);
                    return BADHOST;
                }
                log.info("Connected to battleye server {}:{}", this.hostname, this.port);
            } catch (UnknownHostException | IllegalArgumentException e) {
                log.info("Rejected connection to battleye server {}:{}, reason: {}", this.hostname, this.port, e.getMessage());
                return BADHOST;
            }
            log.info("Socket open to to {}:{}", this.hostname, this.port);
        } else {
            log.info("Socket already connected for {}:{}!", this.hostname, this.port);
            return ServerConnectionStatus.CONNECTED;
        }

        log.info("Socket connection created: {}:{}", this.hostname, this.port);
        this.sequencer = new BattleyeSequenceNumber();
        log.info("Starting listen loop: {}:{}", this.hostname, this.port);
        this.listenThread = this.scheduler.scheduleDirect(this::listenLoop);

        log.info("Starting timeout manager: {}:{}", this.hostname, this.port);
        this.timeoutDisposable = new TimeoutManager(
            this.scheduler,
            Arrays.asList(this.messagePublisher, this.commandObserver),
            this.eventPublisher);

        log.info("Starting login to {}:{}", this.hostname, this.port);
        ServerConnectionStatus login = this.login();
        log.info("Login response on server {}:{} | {}", this.hostname, this.port, login.getStatus());
        this.latch = new CountDownLatch(1);
        return login;
    }

    public ServerConnectionStatus disconnect() {
        if (this.socket != null) {
            this.socket.close();
        }

        if (this.timeoutDisposable != null) {
            this.timeoutDisposable.dispose();
        }

        if (this.listenThread != null && !this.listenThread.isDisposed()) {
            this.listenThread.dispose();
        }

        try {
            boolean success = this.latch.await(1, TimeUnit.SECONDS);
            if (!success) {
                log.error("Count down latch not successful {}:{}", this.hostname, this.port);
                return DISCONNECTED;
            }
        } catch(Exception ex) {
            log.error("Failed to disconnect socket on {}:{} - {}", this.hostname, this.port, ex.getMessage());
            return DISCONNECTED;
        }
        
        return ServerConnectionStatus.DISCONNECTED;
    }

    public Observable<ServerConnectionStatus> getEventPublisher() {
        return this.eventPublisher;
    }

    public Observable<String> getRecievedObservable() {
        return this.messagePublisher.observeOn(this.scheduler);
    }

    public CommandResponse sendCommand(Command command) {
        CommandResponse cr = new CommandResponse();
        cr.setCommand(command.getCommand());
        Packet packet = this.sendCommand(command.getCommand(), true, 1);
        String response = new String(packet.getResponse());
        cr.setResponse(response);
        cr.setSuccess(packet.getSuccess().orElse(false));
        return cr;
    }

    private Packet sendCommand(String command, boolean waitForResponse, int attempts) {
        Packet commandPacket;

        byte seq = this.sequencer.next();
        byte[] commandByteArray = this.generateCommandByteArray(seq, command);
        commandPacket = new Packet(commandByteArray, waitForResponse);

        List<byte[]> receivedList = new ArrayList<>();
        Disposable sub = this.commandObserver
            .filter(x -> x.length > 8 && getSeqNumber(x) == seq)
            .subscribeOn(this.scheduler)
            .subscribe(data -> this.ConsumeCommandUpdate(commandPacket, receivedList, data));

        boolean success = false;
        try {
            success = this.sendPacket(seq, commandPacket);
        } catch (Exception ex) {
            log.error("An error occurred when trying to send packet to {}.", this.hostname, ex);
        }
        sub.dispose();
        if (!success) {
            log.debug("Failed to send command '{}' to {}:{}, remaining attempts {}", command, this.hostname, this.port, attempts);
            if (attempts > 0) {
                return this.sendCommand(command, waitForResponse, attempts - 1);
            }
        }

        log.trace("Command process complete on server {}", this.hostname);

        return commandPacket;
    }

    private void ConsumeCommandUpdate(Packet commandPacket, List<byte[]> receivedList, byte[] data) {
        if (receivedList.size() == 0) {
            receivedList.add(data);
        }

        byte seq = data[8];
        if (!commandPacket.getSuccess().orElse(false)) {
            log.trace("SEQ MATCH {} {}", this.hostname, seq);
            commandPacket.setSuccess(true);
            commandPacket.setResponse(data);
        } else {
            log.trace("DUPLICATE SEQ {} {}", this.hostname, seq);
        }
    }

    private boolean sendPacket(byte seq, Packet packet) {
        Disposable disposable = this.commandObserver.filter(x -> getSeqNumber(x) == seq).subscribe(packet::setResponse);

        boolean success = false;

        if (this.socket == null || this.socket.isClosed()) {
            log.warn("Socket is not ready.");
            return false;
        }

        try {
            if (packet.waitRequired()) {
                log.trace("Waiting for response on command: {}", packet.getQuestion());
                int timeout = 10;
                packet.setSuccess(false);
                for (int i = 0; i < timeout; i++) {
                    byte[] message = packet.getQuestion();
                    DatagramPacket datagramPacket = new DatagramPacket(message, message.length, InetAddress.getByName(this.hostname), this.port);
                    this.socket.send(datagramPacket);
                    if (packet.await(1, TimeUnit.SECONDS)) {
                        success = true;
                        break;
                    }
                }

                if (!success) {
                    success = packet.await(20, TimeUnit.SECONDS);
                }

                log.trace("Command {} on {} success? {}", packet.getQuestion(), this.hostname, success);
            } else {
                packet.setSuccess(true);
                success = true;
            }
        } catch (IOException | InterruptedException ex) {
            log.error("Unable to send command to RCON Client.", ex);
        } finally {
            disposable.dispose();
        }

        packet.setSuccess(success);
        return success;

    }

    /**
     * Generate the command byte array from string.
     *
     * @param message String to convert to command byte array.
     *
     * @return Byte array representing a command packet.
     */
    private byte[] generateCommandByteArray(byte sequence, String message) {
        byte[] messageBytes = message.getBytes();
        byte[] temp = new byte[messageBytes.length + 1];
        temp[0] = sequence;
        System.arraycopy(messageBytes, 0, temp, 1, messageBytes.length);
        return PacketHelper.generatePacket(PacketType.COMMAND, temp);
    }

    /**
     * Login to battleye server.
     *
     * @return true if authenticated.
     */
    private ServerConnectionStatus login() {
        byte[] loginPacket = PacketHelper.generatePacket(PacketType.LOGIN, this.password.getBytes());
        Packet command = new Packet(loginPacket, true);
        Disposable sub = this.commandObserver.subscribe(command::setResponse);
        log.info("Attempting to login to {}:{}", this.hostname, this.port);
        try {
            byte[] message = command.getQuestion();
            this.socket.send(new DatagramPacket(message, message.length, socket.getRemoteSocketAddress()));
            boolean timeout = !command.await(10, TimeUnit.SECONDS);
            if (timeout) {
                log.error("Login timeout");
                return TIMEOUT;
            }
        } catch (IOException | InterruptedException ex) {
            log.warn("Unable to login to RCON Client: {}", ex.getMessage());
            return BADHOST;
        }

        byte[] rec = command.getResponse();
        sub.dispose();
        boolean success = rec.length != 0 && rec[rec.length - 1] == (byte)0x01;

        if (success) {
            log.info("Login successful to {}:{}", this.hostname, this.port);
            return CONNECTED;
        } else {
            log.warn("Login not successful to {}:{} {}", this.hostname, this.port, rec);
            return UNAUTHORISED;
        }
    }

    /**
     * Main loop for listening to returning data.
     */
    private void listenLoop() {
        log.debug("Async protocol listener started");
        while(!this.socket.isClosed()) {
            this.listen();
        }
        log.debug("Protocol listener stopping.");
        this.latch.countDown();
    }



    private static void acknowledgePacket(DatagramSocket socket, byte id) {
        byte[] messageBytes = new byte[] {id};
        byte[] fullPacket = PacketHelper.generatePacket(PacketType.MESSAGE, messageBytes);
        Packet commandPacket = new Packet(fullPacket, false);
        try {
            byte[] message = commandPacket.getQuestion();
            socket.send(new DatagramPacket(message, message.length, socket.getRemoteSocketAddress()));
        } catch (IOException ex) {
            log.error("Failed to acknowledge packet", ex);
        }
    }

    private void listen() {
        byte[] rcv = new byte[4096];

        /* Receive the Datagram */
        DatagramPacket packet = new DatagramPacket(rcv, rcv.length);
        try {
            this.socket.receive(packet);
            byte[] receivedData = new byte[packet.getLength()];
            System.arraycopy(packet.getData(), 0, receivedData, 0, receivedData.length);
            this.handleReceive(receivedData);
        } catch (IOException ex) {
            if (this.socket.isClosed()) {
                log.info("Listen aborted on {}:{} - socket closed.", this.hostname, this.port);
            } else {
                log.error("Unable to receive on {}.", this.hostname, ex);
            }
        }
    }

    private static byte getSeqNumber(byte[] data) {
        return data[8];
    }

    private void handleReceive(byte[] rcv) {
        /* Drop unnecessary bytes */
        byte seq = getSeqNumber(rcv);
        log.trace("received data! of length {}", rcv.length);
        /* Discard messages */
        PacketType type = getPacketType(rcv);
        switch (type) {
            case MESSAGE:
                log.trace("Packet is message on {} seq {}", this.hostname, seq);
                acknowledgePacket(this.socket, seq);
                String message = new String(rcv);
                this.messagePublisher.onNext(message);
                break;
            case COMMAND:
                log.trace("Packet is command on {} seq {}", this.hostname, seq);
                // 0 at the 9th position means that there is a header indicating the packet is fragmented
                if (rcv.length > 9 && rcv[9] == 0) {
                    log.trace("Command fragmented, publishing.");
                    try {
                        this.fragmentCollector.pushPacket(rcv);
                    } catch (Exception ex) {
                        log.error("Failure on receive fragment on {}", this.hostname, ex);
                    }
                } else {
                    log.trace("Command not fragmented.");
                    this.commandPublisher.onNext(rcv);
                }

                break;
            case LOGIN:
                log.trace("Packet is login response");
                this.commandPublisher.onNext(rcv);
                break;
            default:
                log.error("Unknown packet.");
        }
    }
}
