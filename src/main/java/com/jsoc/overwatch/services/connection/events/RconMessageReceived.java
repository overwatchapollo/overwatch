package com.jsoc.overwatch.services.connection.events;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class RconMessageReceived extends ApplicationEvent {
    private Long serverId;
    private String message;
    public RconMessageReceived(Object source, Long serverId, String message) {
        super(source);
        this.serverId = serverId;
        this.message = message;
    }
}
