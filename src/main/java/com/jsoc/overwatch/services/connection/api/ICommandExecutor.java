/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.connection.api;

import java.util.concurrent.CompletableFuture;

/**
 * Command executor.
 */
public interface ICommandExecutor {
    /**
     * Send command and return a completable.
     * @param command Command to send.
     * @return Completable determining success.
     */
    CompletableFuture<CommandResponse> sendCommand(Command command);

    CompletableFuture<CommandResponse> sendCommand(Command command, String priority);
}
