/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.connection.api;

import java.text.MessageFormat;
import java.time.Duration;

/**
 * Factory class for generating commands.
 */
public class CommandFactory {
    
    /**
     * Generate ban command.
     * 
     * @param sessionId SESSION ID OF PLAYER.
     * @param length Length of ban, zero if indefinite.
     * @param reason Reason for ban.
     * 
     * @return Command object for ban.
     */
    public Command ban(Long sessionId, Duration length, String reason) {
        return new Command(MessageFormat.format(Commands.BAN.getTemplate(), sessionId, String.valueOf(length.toMinutes()), reason));
    }

    /**
     * Generate ban command.
     *
     * @param guid GUID of player.
     * @param length Length of ban, zero if indefinite.
     * @param reason Reason for ban.
     *
     * @return Command object for ban.
     */
    public Command addban(String guid, Duration length, String reason) {
        return new Command(MessageFormat.format(Commands.ADD_BAN.getTemplate(), guid, String.valueOf(length.toMinutes()), reason));
    }

    /**
     * Soft restart server.
     * @return Command object for restart.
     */
    public Command softRestart() {
        return new Command("restart");
    }
    
    /**
     * Send say message globally.
     * 
     * @param message Message to send to all.
     * 
     * @return Command object for say.
     */
    public Command sayAll(String message) {
        return this.sayWhisper(-1, message);
    }
    
    /**
     * Send say command to individual player (by id).
     * 
     * @param id ID of player to send message to.
     * @param message Message to send.
     * 
     * @return Command object for say.
     */
    public Command sayWhisper(int id, String message) {
        return new Command(MessageFormat.format(Commands.SAY.getTemplate(), id, message));
    }

    /**
     * Send say command to individual player (by id).
     *
     * @param id ID of player to send message to.
     * @param message Message to send.
     *
     * @return Command object for say.
     */
    public Command sayWhisper(String id, String message) {
        return new Command(MessageFormat.format(Commands.SAY.getTemplate(), id, message));
    }

    /**
     * Get the ban list from the server.
     * @return Ban list of server.
     */
    public Command banList() {
        return new Command(Commands.BAN_LIST.getTemplate());
    }

    /**
     * Remove GUID from ban list.
     * @return Command to remove guid from ban list.
     */
    public Command unban(String guid) {
        return new Command(MessageFormat.format(Commands.UNBAN.getTemplate(), guid));
    }

    public Command writeBan() {
        return new Command(Commands.WRITE_BANS.getTemplate());
    }
    
    /**
     * Get the player list.
     * 
     * @return Command object for players.
     */
    public Command playerList() {
        return new Command(Commands.PLAYERS.getTemplate());
    }
    
    /**
     * Send kick command for identifier.
     * 
     * @param id ID or name of player.
     * @param message Reason for kick.
     * 
     * @return Command object for kick.
     */
    public Command kickId(String id, String message) {
        return new Command(MessageFormat.format(Commands.KICK.getTemplate(), id, message));
    }
    
    /**
     * Get the version of battleye.
     * 
     * @return Command object for version.
     */
    public Command version() {
        return new Command(Commands.VERSION.getTemplate());
    }
}
