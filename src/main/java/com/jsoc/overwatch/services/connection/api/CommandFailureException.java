/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.connection.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception when command fails.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class CommandFailureException extends RuntimeException {
    public CommandFailureException(String message) {
        super(message);
    }
}
