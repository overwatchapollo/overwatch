package com.jsoc.overwatch.services.connection.games.battleye;

import com.jsoc.overwatch.services.connection.api.Packet;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.Subject;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Slf4j
public class SendPacketHelper {
    private final IRconSocket asyncBattleyeSocket;
    private final String hostname;
    private final Subject<byte[]> commandPacketPublisher;
    private boolean hasReceivedResponse = false;

    public SendPacketHelper(IRconSocket asyncBattleyeSocket, String hostname, Subject<byte[]> commandPacket) {
        this.asyncBattleyeSocket = asyncBattleyeSocket;
        this.hostname = hostname;
        this.commandPacketPublisher = commandPacket;
    }

    public boolean sendPacket(Packet packet) {
            Disposable commandSend = this.commandPacketPublisher.subscribe(packet::setResponse);
            boolean success = false;
            try {
                if (packet.waitRequired()) {
                    log.trace("Waiting for response on command: {}", packet.getQuestion());
                    int timeout = 10;

                    for (int i = 0; i < timeout; i++) {
                        this.asyncBattleyeSocket.sendData(packet.getQuestion());
                        if (packet.await(1, TimeUnit.SECONDS)) {
                            break;
                        }
                    }

                    success = packet.await(20, TimeUnit.SECONDS);
                    packet.setSuccess(success);
                    log.trace("Command {} on {} success? {}", packet.getQuestion(), this.hostname, success);
                } else {
                    packet.setSuccess(true);
                    success = true;
                }
            } catch (IOException | InterruptedException ex) {
                log.error("Unable to send command to RCON Client.", ex);
            } finally {
                commandSend.dispose();
                return success;
            }
    }
}
