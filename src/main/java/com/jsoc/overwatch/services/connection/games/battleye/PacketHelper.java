/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.connection.games.battleye;

import com.jsoc.overwatch.services.connection.api.PacketType;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

/**
 * Helper class for manipulating packets for battleye client.
 */
@Slf4j
public class PacketHelper {
    /**
     * Header size in bytes.
     */
    public static final int HEADER_SIZE = 7;
    
    /**
     * Generate packet as byte array.
     * 
     * @param id Type of packet to generate.
     * @param data Data payload to send.
     * 
     * @return Fully constructed packet as byte array.
     */
    public static byte[] generatePacket(PacketType id, byte[] data) {
        ByteBuffer buffer = ByteBuffer.allocate(HEADER_SIZE + data.length + 1);
    	buffer.order(ByteOrder.LITTLE_ENDIAN);
        int crc = generateCrcFromData(id, data);
    	header(buffer, crc);
    	buffer.put((byte)id.getType());
    	buffer.put(data);
    	return buffer.array();
    }
    
    /**
     * Generate packet header.
     * 
     * @param buffer Byte buffer to push to.
     * @param crc CRC to use.
     */
    public static void header(ByteBuffer buffer, int crc) {
        buffer.put((byte) 0x42);
    	buffer.put((byte) 0x45);
    	buffer.putInt(crc);
    	buffer.put((byte) 0xFF);
    }
    
    /**
     * Generate CRC from packet id and byte array.
     * @param id Packet type.
     * @param data
     * @return 
     */
    public static int generateCrcFromData(PacketType id, byte[] data) {
        byte[] bytesToCrc = new byte[data.length + 2];
        bytesToCrc[0] = (byte)0xff;
        bytesToCrc[1] = (byte)id.getType();
        System.arraycopy(data, 0, bytesToCrc, 2, data.length);
        return generateCrc(bytesToCrc);
    }
    
    /**
     * Generate 32 bit CRC code from byte array.
     * 
     * @param bytes Byte array to generate from.
     * 
     * @return 32 bit CRC code.
     */
    public static int generateCrc(byte[] bytes) {
        Checksum checksum = new CRC32();
        checksum.update(bytes, 0, bytes.length);
        return (int)checksum.getValue();
    }
}
