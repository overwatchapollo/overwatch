/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.connection.api;

/**
 * Enumeration for commands.
 */
public enum Commands {
    /**
     * Ban player command.
     */
    BAN("ban {0} {1} {2}"),

    /**
     * Add ban to offline player.
     */
    ADD_BAN("addBan {0} {1} {2}"),

    /**
     * Battleye ban list.
     */
    BAN_LIST("bans"),

    /**
     * Remove expired bans.
     */
    WRITE_BANS("writeBans"),

    /**
     * Reload bans from bans.txt.
     */
    LOAD_BANS("loadBans"),

    /**
     * Remove ban from bans.txt.
     */
    UNBAN("removeBan {0}"),

    /**
     * Say command.
     */
    SAY("say {0} {1}"),
    
    /**
     * Get players on the server.
     */
    PLAYERS("players"),
    
    /**
     * Kick a player with an identifier.
     */
    KICK("kick {0} {1}"),
    
    /**
     * Version of battleye on the server.
     */
    VERSION("version");
    
    /**
     * String template for command.
     */
    private final String template;
    
    /**
     * Creates a new instance of the {@link Commands} enum.
     * 
     * @param template String template to use for command.
     */
    Commands(String template) {
        this.template = template;
    }
    
    /**
     * Get the template.
     * 
     * @return String template for command.
     */
    public String getTemplate() {
        return this.template;
    }
}
