package com.jsoc.overwatch.services.connection;

import com.jsoc.overwatch.controller.dto.NotFoundException;
import com.jsoc.overwatch.services.connection.api.CommandFactory;
import com.jsoc.overwatch.services.connection.events.*;
import com.jsoc.overwatch.services.connection.games.arma.IRconConnection;
import com.jsoc.overwatch.services.connection.games.arma.RconFactory;
import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import com.jsoc.overwatch.services.serverRegister.ServerRegisterService;
import com.jsoc.overwatch.services.serverRegister.events.ServerConfigModifiedEvent;
import com.jsoc.overwatch.services.serverRegister.events.ServerConnectionRegistered;
import com.jsoc.overwatch.services.serverRegister.events.ServerConnectionUnregistered;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Service to manage active RCon connections.
 */
@Service
@Slf4j
public class RconConnectionPool {
    /**
     * Publisher for status events.
     */
    private final ApplicationEventPublisher publisher;

    /**
     * Factory for creating RCon connections.
     */
    private final RconFactory factory;

    /**
     * Executor for all RCon clients.
     */
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    /**
     * Scheduler for RCon clients.
     */
    private final Scheduler scheduler = Schedulers.from(this.executorService);

    @Autowired
    private ServerRegisterService registerService;

    private final Map<Long, IRconConnection> activeConnections = new ConcurrentHashMap<>();

    /**
     * Reconnect pool.
     */
    private final Map<Long, Disposable> reconnectPool = new HashMap<>();

    @Autowired
    public RconConnectionPool(ApplicationEventPublisher publisher, ServerRegisterService registerService) {
        this(publisher, PublishSubject.create(), PublishSubject.create(), registerService);
    }

    public RconConnectionPool(ApplicationEventPublisher publisher, Subject<ServerStateUpdateEvent> stateSubject, Subject<RconMessageReceived> rconSubject, ServerRegisterService registerService) {
        this(publisher, new RconFactory(stateSubject, rconSubject), registerService);
        stateSubject.subscribe(this.publisher::publishEvent);
        rconSubject.subscribe(this.publisher::publishEvent);
    }

    public RconConnectionPool(ApplicationEventPublisher publisher, RconFactory factory, ServerRegisterService registerService) {
        this.publisher = publisher;
        this.factory = factory;
        this.registerService = registerService;
    }

    public Optional<IRconConnection> getConnection(final Long id) {
        return Optional.ofNullable(this.activeConnections.get(id));
    }

    @EventListener
    public void eventListener(ServerConfigModifiedEvent event) {
        this.setAsRunner(event.getServerId());
    }

    @EventListener
    public void eventListener(SoftRestartRequest request) {
        CommandFactory cf = new CommandFactory();
        Optional.ofNullable(this.activeConnections.get(request.getServerId()))
            .ifPresent(x -> x.sendCommand(cf.softRestart()).thenAccept(y -> log.info("Restart command success? {}", y.isSuccess())));
    }

    @EventListener
    public void eventListener(ServerConnectionRegistered serverConnectionRegistered) {
        Long guid = serverConnectionRegistered.getServerGuid();
        log.info("Getting server connection {}", guid);
        Optional<ServerConnection> maybeCon = this.registerService.getConfiguration(guid);
        maybeCon.ifPresent(x -> {
            IRconConnection con = this.factory.buildRunner(x);
            this.activeConnections.put(x.getId(), con);
        });
        IRconConnection con = this.getConnection(guid).orElseThrow(() -> new IllegalStateException(""));
        log.info("Connecting to server {}", guid);
        boolean success = con.connect();
        if (success) {
            log.info("Connected to server {}", guid);
        } else {
            log.error("Failed to connect to server {}. Attempting reconnect in 10 seconds.", serverConnectionRegistered.getServerGuid());
            Disposable dis = this.scheduler.scheduleDirect(() -> this.eventListener(serverConnectionRegistered), 10, TimeUnit.SECONDS);
            this.reconnectPool.put(guid, dis);
        }
    }

    public void setAsRunner(Long serverId) {
        log.info("Setting server {} as runner.", serverId);
        Long id = serverId;
        if (this.reconnectPool.containsKey(id)) {
            Disposable disposable = this.reconnectPool.remove(id);
            disposable.dispose();
        }

        ServerConnection newConnection = this.registerService.getConfiguration(id).orElseThrow(NotFoundException::new);

        log.info("Closing server {}", serverId);
        IRconConnection active = this.activeConnections.remove(newConnection.getId());
        active.disconnect();
        try {
            active.close();
        } catch (Exception ex) {

        }

        this.publisher.publishEvent(new ServerConnectionLost(this, id));
        log.info("Building new server {}", serverId);
        IRconConnection con = this.factory.buildRunner(newConnection);
        this.activeConnections.put(newConnection.getId(), con);
        con.connect();
        log.info("New server built {}", serverId);
    }

    @EventListener
    public void eventListener(ServerConnectionUnregistered event) {
        Long id = event.getServerGuid();
        if (this.reconnectPool.containsKey(id)) {
            Disposable disposable = this.reconnectPool.remove(id);
            disposable.dispose();
        }

        Optional<IRconConnection> maybeConnection = Optional.ofNullable(this.activeConnections.get(id));
        try {
            if (maybeConnection.isPresent()) {
                this.activeConnections.remove(id);
                maybeConnection.get().close();
            }
            this.publisher.publishEvent(new ServerConnectionLost(this, id));
            log.info("Deleted server {}", id);
        } catch (Exception ex) {
            log.error("Unable to close connection to server {}", id, ex);
        }
    }
}
