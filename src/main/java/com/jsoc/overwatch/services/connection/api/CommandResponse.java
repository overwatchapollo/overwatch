package com.jsoc.overwatch.services.connection.api;

import lombok.Data;

@Data
public class CommandResponse {
    private String command;
    private boolean success;
    private String response;
}
