/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.connection.games.battleye;

import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import com.jsoc.overwatch.services.connection.events.ServerStateUpdateEvent;
import com.jsoc.overwatch.services.connection.api.Packet;
import com.jsoc.overwatch.services.connection.api.PacketType;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.jsoc.overwatch.services.connection.ServerConnectionStatus.*;

/**
 * An implementation of the battleye protocol from "https://www.battleye.com/downloads/BERConProtocol.txt".
 */
@Slf4j
public class BattleyeProtocolClient implements IRconClient {
    /**
     * Hostname of the battleye server.
     */
    private final String hostname;
    
    /**
     * Port of the battleye server.
     */
    private final int port;
    
    /**
     * Password for the battleye server.
     */
    private final String password;
    
    /**
     * Sequence number generator.
     */
    private final BattleyeSequenceNumber sequencer = new BattleyeSequenceNumber();

    private final Scheduler scheduler;
    
    private Disposable keepAliveDisposable = null;

    private final Subject<byte[]> commandPublisher;
    private final Subject<byte[]> messagePublisher = PublishSubject.create();
    private final Subject<byte[]> commandPacketPublisher = PublishSubject.create();

    private final Observable<byte[]> receivePublisher;

    private final AsyncBattleyeSocket asyncBattleyeSocket;

    private final CommandFragmentCollector fragmentCollector;

    private Disposable socketSubscription;

    private Disposable receiveSubscription;

    private SendPacketHelper helper;

    private Subject<ServerStateUpdateEvent> statePublisher;

    private ServerConnectionStatus status = ServerConnectionStatus.DISCONNECTED;

    private Disposable listenDisposable;

    private PublishSubject<ServerConnectionStatus> statusPublisher = PublishSubject.create();

    /**
     * Initialises a new instance of the {@link BattleyeProtocolClient} class.
     *
     * @param scheduler Scheduler for listening.
     * @param hostname Hostname to connect to.
     * @param port Port to connect to.
     * @param password Password to use.
     * @param socket Battleye socket in use.
     */
    BattleyeProtocolClient(Scheduler scheduler, String hostname, int port, String password, AsyncBattleyeSocket socket) {
        checkNotNull(hostname);
        checkNotNull(password);
        this.scheduler = scheduler;
        this.hostname = hostname;
        this.port = port;
        this.password = password;
        this.statePublisher = PublishSubject.create();
        this.commandPublisher = PublishSubject.create();
        this.receivePublisher =  Observable.merge(this.messagePublisher, this.commandPublisher);
        this.fragmentCollector = new CommandFragmentCollector(this.commandPublisher);
        this.asyncBattleyeSocket = socket;
        this.helper = new SendPacketHelper(this.asyncBattleyeSocket, this.hostname, this.commandPublisher);
    }

    private void setState(ServerConnectionStatus status) {
        if (this.status == status) {
            return;
        }

        this.status = status;
        this.statusPublisher.onNext(status);
    }

    private Observable<byte[]> getTimeoutObservable() {
        return this.receivePublisher.timeout(60, TimeUnit.SECONDS).observeOn(this.scheduler);
    }

    private void handleTimeout() {
        try {
            log.error("Possible dead connection. Attempting reconnect.");
            this.disconnect();
            this.activate();
        } catch (Exception ex) {
            log.error("Failed to reconnect to {} attempting reconnect.", this.hostname, ex);
            this.close();
            this.scheduler.scheduleDirect(this::handleTimeout, 5, TimeUnit.SECONDS);
        }
    }

    public Observable<ServerStateUpdateEvent> getStateObservable() {
        return this.statePublisher;
    }

    @Override
    public void activate() {
        if (this.status == CONNECTED) {
            return;
        }

        this.setState(this.connecting());
        this.statePublisher.onNext(new ServerStateUpdateEvent(this, null, null, this.getStatus()));
    }

    @Override
    public Observable<byte[]> getMessageObservable() {
        return this.messagePublisher;
    }

    @Override
    public Observable<byte[]> getCommandObservable() {
        return this.commandPublisher;
    }

    public ServerConnectionStatus getStatus() {
        return this.status;
    }

    @Override
    public void close() {
        this.setState(DISCONNECTED);
        this.statePublisher.onNext(new ServerStateUpdateEvent(this, null, null, this.getStatus()));
        this.disconnect();
    }

    private void disconnect() {
        this.sequencer.reset();
        try {
            this.asyncBattleyeSocket.close();
        } catch (Exception ex) {
            log.error("Failed to close socket: {}", ex);
        }
        this.fragmentCollector.clear();

        if (this.listenDisposable != null && !this.listenDisposable.isDisposed()) {
            this.listenDisposable.dispose();
        }

        if (this.socketSubscription != null && !this.socketSubscription.isDisposed()) {
            this.socketSubscription.dispose();
        }

        if (this.receiveSubscription != null && !this.receiveSubscription.isDisposed()) {
            this.receiveSubscription.dispose();
        }

        if (this.keepAliveDisposable != null && !this.keepAliveDisposable.isDisposed()) {
            this.keepAliveDisposable.dispose();
        }
    }

    /**
     * Send keep alive message to server.
     */
    private void sendKeepAlive() {
        if (this.status != CONNECTED) {
            return;
        }
        log.trace("Sending keep alive.");
        try {
            this.sendCommand("", true, 0);
        } catch (InterruptedException ex) {
            log.error("Failed to send keep alive on {}.", this.hostname, ex.getMessage());
        }
    }
    
    /**
     * Login to battleye server.
     * 
     * @return true if authenticated.
     */
    private ServerConnectionStatus login() {
        byte[] loginPacket = PacketHelper.generatePacket(PacketType.LOGIN, this.password.getBytes());
        Packet command = new Packet(loginPacket, true);
        Disposable sub = this.commandPublisher.subscribe(command::setResponse);
        log.info("Attempting to login to {}:{}", this.hostname, this.port);
        try {
            this.asyncBattleyeSocket.sendData(command.getQuestion());
            boolean timeout = !command.await(10, TimeUnit.SECONDS);
            if (timeout) {
                log.error("Login timeout");
                return TIMEOUT;
            }
        } catch (IOException | InterruptedException ex) {
            log.info("Unable to login to RCON Client: {}", ex.getMessage());
            return BADHOST;
        }
        
        byte[] rec = command.getResponse();
        sub.dispose();
        boolean success = rec.length != 0 && rec[rec.length - 1] == (byte)0x01;

        if (success) {
            log.info("Login successful to {}:{}", this.hostname, this.port);
            return CONNECTED;
        } else {
            String response = new String(rec);
            log.warn("Login not successful to {}:{} {}", this.hostname, this.port, response);
            return UNAUTHORISED;
        }
    }
    
    /**
     * Send command to battleye server.
     * @param command Command to send. 
     * @return Response from server.
     * @throws InterruptedException If interrupted.
     */
    public Packet sendCommand(String command) throws InterruptedException {
        return this.sendCommand(command, true, 1);
    }

    private void ConsumeCommandUpdate(Packet commandPacket, List<byte[]> receivedList, byte[] data) {
        if (receivedList.size() == 0) {
            receivedList.add(data);
        }

        byte seq = data[8];
        if (!commandPacket.getSuccess().orElse(false)) {
            log.trace("SEQ MATCH {} {}", this.hostname, seq);
            commandPacket.setSuccess(true);
            commandPacket.setResponse(data);
        } else {
            log.trace("DUPLICATE SEQ {} {}", this.hostname, seq);
        }
    }

    private Packet sendCommand(String command, boolean waitForResponse, int attempts) throws InterruptedException {
        if (this.status != CONNECTED) {
            log.error("Unable to send command to {}:{} when not authenticated with server. Current state: {}", this.hostname, this.port, this.status.getStatus());
            throw new InterruptedException("Not logged in with server.");
        }
        Packet commandPacket;

        byte[] commandByteArray = this.generateCommandByteArray(command);
        commandPacket = new Packet(commandByteArray, waitForResponse);

        List<byte[]> receivedList = new ArrayList<>();
        Disposable sub = this.commandPublisher
            .filter(x -> x.length > 8 && x[8] == commandPacket.getSequenceNumber())
            .subscribe(data -> this.ConsumeCommandUpdate(commandPacket, receivedList, data));

        boolean success = false;
        try {
             success = this.sendPacket(commandPacket);
        } catch (Exception ex) {
            log.error("An error occurred when trying to send packet to {}.", this.hostname, ex);
        }
        sub.dispose();
        if (!success) {
            log.info("Failed to send command '{}' to {}:{}, remaining attempts {}", command, this.hostname, this.port, attempts);
            if (attempts > 0) {
                return this.sendCommand(command, waitForResponse, attempts - 1);
            }
        }

        log.trace("Command process complete on server {}", this.hostname);

        return commandPacket;
    }

    private void acknowledgePacket(byte id) {
        byte[] messageBytes = new byte[] {id};
        byte[] fullPacket = PacketHelper.generatePacket(PacketType.MESSAGE, messageBytes);
        Packet commandPacket = new Packet(fullPacket, false);
        try {
            this.asyncBattleyeSocket.sendData(commandPacket.getQuestion());
        } catch (IOException ex) {
            log.error("Failed to acknowledge packet", ex);
        }
    }

    /**
     * Generate the command byte array from string.
     * 
     * @param message String to convert to command byte array.
     * 
     * @return Byte array representing a command packet.
     */
    private byte[] generateCommandByteArray(String message) {
        byte[] messageBytes = message.getBytes();
        byte[] temp = new byte[messageBytes.length + 1];
        byte sqn = this.sequencer.next();
        temp[0] = sqn;
        System.arraycopy(messageBytes, 0, temp, 1, messageBytes.length);
        return PacketHelper.generatePacket(PacketType.COMMAND, temp);
    }

    private boolean sendPacket(Packet packet) {
        synchronized (this.asyncBattleyeSocket) {
            Disposable disposable = this.commandPublisher.subscribe(packet::setResponse);
            boolean result = this.helper.sendPacket(packet);
            disposable.dispose();
            return result;
        }
    }

    private void handleReceive(byte[] rcv) {
        /* Drop unnecessary bytes */
        byte seq = rcv[8];

        /* Discard messages */
        switch (com.jsoc.overwatch.services.connection.api.Packet.getPacketType(rcv)) {
            case MESSAGE:
                log.trace("Packet is message on {} seq {}", this.hostname, seq);
                this.acknowledgePacket(rcv[8]);
                this.messagePublisher.onNext(rcv);
                break;
            case COMMAND:
                log.trace("Packet is command on {} seq {}", this.hostname, seq);
                this.commandPacketPublisher.onNext(rcv);
                // 0 at the 9th position means that there is a header indicating the packet is fragmented
                if (rcv.length > 9 && rcv[9] == 0) {
                    log.trace("Command fragmented, publishing.");
                    try {
                        this.fragmentCollector.pushPacket(rcv);
                    } catch (Exception ex) {
                        log.error("Failure on receive fragment on {}", this.hostname, ex);
                    }
                } else {
                    log.trace("Command not fragmented.");
                    this.commandPublisher.onNext(rcv);
                }

                break;
            case LOGIN:
                log.trace("Packet is login response");
                this.commandPublisher.onNext(rcv);
                break;
            default:
                log.error("Unknown packet.");
        }
    }

    private ServerConnectionStatus connecting() {
        try {
            this.asyncBattleyeSocket.activate();
        } catch (UnknownHostException | SocketException ex) {
            return BADHOST;
        }

        this.socketSubscription = this.asyncBattleyeSocket.getDataObservable().subscribe(this::handleReceive);
        this.listenDisposable = this.scheduler.scheduleDirect(this.asyncBattleyeSocket::listenLoop);
        ServerConnectionStatus loginAttempt = this.login();
        if (loginAttempt != CONNECTED) {
            log.error("Failed to login to {}:{}", hostname, port);
            return loginAttempt;
        }

        this.keepAliveDisposable = this.scheduler.schedulePeriodicallyDirect(this::sendKeepAlive, 10, 10, TimeUnit.SECONDS);
        this.receiveSubscription = this.getTimeoutObservable().subscribe(x -> { }, x -> {
            log.error("Timeout on connection {}:{} at state {}", this.hostname, this.port, this.status);
            this.setState(TIMEOUT);
            this.handleTimeout();
        });

        return CONNECTED;
    }
}
