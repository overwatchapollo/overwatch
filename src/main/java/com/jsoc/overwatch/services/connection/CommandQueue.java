package com.jsoc.overwatch.services.connection;

import com.jsoc.overwatch.services.connection.api.Command;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import com.jsoc.overwatch.services.connection.games.battleye.BattleyeSocket;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArrayList;

@Slf4j
public class CommandQueue implements Disposable {

    private BattleyeSocket socket;

    private Scheduler scheduler;

    private Disposable processDisposable;

    private List<CommandItem> commandQueue = new CopyOnWriteArrayList<>();

    public CommandQueue(Scheduler scheduler, BattleyeSocket socket) {
        this.socket = socket;
        this.scheduler = scheduler;
        this.processDisposable = this.scheduler.scheduleDirect(() -> {
            while(true) {
                if (this.commandQueue.size() == 0) {
                    try {
                        Thread.sleep(100);
                    } catch (Exception ex) {

                    }
                    continue;
                } else {
                    CommandItem ci = this.commandQueue.remove(0);
                    this.command(ci.getFuture(), ci.getCommand());
                }
            }
        });
    }

    public CompletableFuture<CommandResponse> sendCommand(Command command) {
        if (this.processDisposable.isDisposed()) {
            throw new IllegalStateException("Command queue is disposed.");
        }

        CompletableFuture<CommandResponse> future = new CompletableFuture<>();
        CommandItem ci = CommandItem.from(command, future);
        // this.commandQueue.add(ci);
        this.command(ci.getFuture(), ci.getCommand());

        return future;
    }

    private void command(CompletableFuture<CommandResponse> future, Command command) {
        log.debug("Processing command: '{}' | queue {}", command.getCommand(), this.commandQueue.size());
        CommandResponse cr = this.socket.sendCommand(command);
        future.complete(cr);
    }

    @Override
    public void dispose() {
        this.processDisposable.dispose();
    }

    @Override
    public boolean isDisposed() {
        return this.processDisposable.isDisposed();
    }


}
