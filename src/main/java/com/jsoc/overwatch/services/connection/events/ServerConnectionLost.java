package com.jsoc.overwatch.services.connection.events;

import lombok.Data;
import org.springframework.context.ApplicationEvent;

@Data
public class ServerConnectionLost extends ApplicationEvent {
    private Long serverId;
    public ServerConnectionLost(Object source, Long id) {
        super(source);
        this.serverId = id;
    }
}
