package com.jsoc.overwatch.services.privilegedGuids.model;

import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class PrivilegedGuid {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "serverId")
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    private ServerConnection server;

    private String guid;
}
