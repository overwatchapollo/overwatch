package com.jsoc.overwatch.services.privilegedGuids.repository;

import com.jsoc.overwatch.services.privilegedGuids.model.PrivilegedGuid;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface PrivilegedGuidRepository extends CrudRepository<PrivilegedGuid, Long> {
    Collection<PrivilegedGuid> findByServerIdAndGuid(Long serverId, String guid);
}
