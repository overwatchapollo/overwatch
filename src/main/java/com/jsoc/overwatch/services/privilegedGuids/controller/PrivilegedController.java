package com.jsoc.overwatch.services.privilegedGuids.controller;

import com.jsoc.overwatch.services.connection.ServerNotFoundException;
import com.jsoc.overwatch.services.privilegedGuids.PrivilegedGuidService;
import com.jsoc.overwatch.services.privilegedGuids.model.PrivilegedGuid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

import static com.jsoc.overwatch.Security.SecurityHelper.authoriseUserFromSession;

@RestController
@RequestMapping("/api/privileged")
@PreAuthorize("isAuthenticated()")
@Slf4j
public class PrivilegedController {

    @Autowired
    private PrivilegedGuidService service;

    @GetMapping()
    public Collection<PrivilegedGuid> getGuids() {
        authoriseUserFromSession("experimental");
        return this.service.getGuids();
    }

    @PostMapping("/{serverId}/{guid}")
    public PrivilegedGuid addGuid(@PathVariable("serverId") Long serverId, @PathVariable("guid") String guid) throws ServerNotFoundException {
        authoriseUserFromSession("experimental");
        return this.service.addGuid(serverId, guid);
    }

    @DeleteMapping("/{serverId}/{guid}")
    public void removeGuid(@PathVariable("serverId") Long serverId, @PathVariable("guid") String guid) {
        authoriseUserFromSession("experimental");
        this.service.removeGuid(serverId, guid);
    }
}
