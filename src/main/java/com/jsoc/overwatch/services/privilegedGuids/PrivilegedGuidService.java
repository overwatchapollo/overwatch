package com.jsoc.overwatch.services.privilegedGuids;

import com.jsoc.overwatch.services.connection.ServerNotFoundException;
import com.jsoc.overwatch.services.privilegedGuids.model.PrivilegedGuid;
import com.jsoc.overwatch.services.privilegedGuids.repository.PrivilegedGuidRepository;
import com.jsoc.overwatch.services.serverRegister.ServerRegisterService;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class PrivilegedGuidService {

    @Autowired
    private PrivilegedGuidRepository repo;

    @Autowired
    private ServerRegisterService registerService;

    public PrivilegedGuid addGuid(Long serverId, String guid) throws ServerNotFoundException {
        PrivilegedGuid privilegedGuid = new PrivilegedGuid();
        this.registerService.getConfiguration(serverId).orElseThrow(() -> new ServerNotFoundException(serverId));
        privilegedGuid.setGuid(guid);
        return this.repo.save(privilegedGuid);
    }

    public void removeGuid(Long serverId, String guid) {
        Collection<PrivilegedGuid> guids = this.repo.findByServerIdAndGuid(serverId, guid);
        guids.forEach(x -> this.repo.delete(x));
    }

    public Collection<PrivilegedGuid> getGuids() {
        return StreamEx.of(this.repo.findAll().iterator()).toList();
    }

    public boolean isPrivileged(Long serverId, String guid) {
        return this.repo.findByServerIdAndGuid(serverId, guid).size() != 0;
    }
}
