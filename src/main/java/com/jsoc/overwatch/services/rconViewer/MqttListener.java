package com.jsoc.overwatch.services.rconViewer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.jsoc.overwatch.services.mqttListener.MqttService;
import com.jsoc.overwatch.services.rconViewer.events.CachedRconMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MqttListener {
    @Autowired
    private MqttService service;

    @EventListener
    public void eventListener(CachedRconMessage event) {
        this.broadcastTopic("server/" + event.getServerId() +"/update/rcon", event);
    }

    private void broadcastTopic(String topic, Object payload) {
        try {
            this.service.publish(topic, payload);
        } catch (JsonProcessingException e) {
            log.error("JSON parsing exception", e);
        }
    }
}
