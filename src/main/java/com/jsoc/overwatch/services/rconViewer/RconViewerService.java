package com.jsoc.overwatch.services.rconViewer;

import com.jsoc.overwatch.services.connection.events.RconMessageReceived;
import com.jsoc.overwatch.services.rconViewer.events.CachedRconMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static com.google.common.base.Preconditions.checkNotNull;

@Service
@Slf4j
public class RconViewerService {

    private Map<Long, RconCache> rconMap = new ConcurrentHashMap<>();

    @Autowired
    private ApplicationEventPublisher publisher;

    @EventListener
    public void eventListener(RconMessageReceived event) {
        synchronized (this) {
            RconCache serverCache = this.rconMap.get(event.getServerId());

            if (serverCache == null) {
                RconCache newCache = new RconCache();
                this.rconMap.put(event.getServerId(), newCache);
                serverCache = newCache;
            }

            RconEntry entry = serverCache.pushLog(Instant.now(), event.getMessage());
            this.publisher.publishEvent(new CachedRconMessage(this, event.getServerId(), entry.getMessageId(), entry.getTimeStamp(), entry.getMessage()));
        }
    }

    public List<RconEntry> getMessages(Long serverId) {
        checkNotNull(serverId);
        return Optional.ofNullable(this.rconMap.getOrDefault(serverId, null))
            .map(RconCache::getEntries)
            .orElse(new ArrayList<>());
    }
}
