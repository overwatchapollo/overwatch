package com.jsoc.overwatch.services.rconViewer;

public class UniqueMessageGenerator {
    private Long messageId = 0L;
    synchronized Long getNext() {
        Long newMessageId = this.messageId;
        this.messageId++;
        return newMessageId;
    }
}
