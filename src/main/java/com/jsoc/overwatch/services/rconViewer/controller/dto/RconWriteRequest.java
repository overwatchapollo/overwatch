package com.jsoc.overwatch.services.rconViewer.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RconWriteRequest {
    private String data;
}
