package com.jsoc.overwatch.services.rconViewer;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.Instant;

@AllArgsConstructor
@Getter
public class RconEntry {
    private Long messageId;
    private Instant timeStamp;
    private String message;
}
