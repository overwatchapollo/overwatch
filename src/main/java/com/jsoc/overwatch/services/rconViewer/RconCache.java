package com.jsoc.overwatch.services.rconViewer;

import lombok.Getter;

import java.time.Instant;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Getter
public class RconCache {
    private int depth = 100;
    private List<RconEntry> entries = new CopyOnWriteArrayList<>();

    private UniqueMessageGenerator messageIdGenerator = new UniqueMessageGenerator();

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public RconEntry pushLog(Instant timeStamp, String message) {
        RconEntry entry = new RconEntry(this.messageIdGenerator.getNext(), timeStamp, message);
        this.entries.add(entry);
        if (this.entries.size() > this.depth) {
            this.entries = this.entries.subList(this.entries.size() - this.depth, entries.size());
        }
        return entry;
    }
}
