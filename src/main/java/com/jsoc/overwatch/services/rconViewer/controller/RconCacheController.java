package com.jsoc.overwatch.services.rconViewer.controller;

import com.jsoc.overwatch.Security.SecurityHelper;
import com.jsoc.overwatch.Security.permissions.ServerPermissionGenerator;
import com.jsoc.overwatch.controller.dto.ForbiddenException;
import com.jsoc.overwatch.controller.dto.UnauthorisedException;
import com.jsoc.overwatch.services.connection.CommandService;
import com.jsoc.overwatch.services.connection.api.Command;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import com.jsoc.overwatch.services.rconViewer.RconEntry;
import com.jsoc.overwatch.services.rconViewer.RconViewerService;
import com.jsoc.overwatch.services.rconViewer.controller.dto.RconWriteRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/rconLog")
@PreAuthorize("isAuthenticated()")
@Slf4j
public class RconCacheController {

    @Autowired
    private RconViewerService service;

    @Autowired
    private CommandService commandService;

    private ServerPermissionGenerator generator = new ServerPermissionGenerator();

    @GetMapping("/server/{serverMrid}")
    public List<RconEntry> getEntries(HttpServletRequest request, @PathVariable("serverMrid") Long serverId) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.rcon(serverId).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        return this.service.getMessages(serverId);
    }

    @PostMapping("/server/{serverMrid}")
    public void writeRconCommand(HttpServletRequest request, @PathVariable("serverMrid") Long serverId, @RequestBody RconWriteRequest command) throws ExecutionException, InterruptedException {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.rcon(serverId).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        Command writeCommand = new Command(command.getData());
        CompletableFuture<CommandResponse> resp = this.commandService.runCommand(serverId, writeCommand);
        CommandResponse cr = resp.get();
        log.info("Wrote command {}, response {}", command.getData(), cr.getResponse());
    }
}
