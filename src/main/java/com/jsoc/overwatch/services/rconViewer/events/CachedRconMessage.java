package com.jsoc.overwatch.services.rconViewer.events;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.time.Instant;

@Getter
public class CachedRconMessage extends ApplicationEvent {
    private Long serverId;
    private Long messageId;
    private Instant timeStamp;
    private String message;
    public CachedRconMessage(Object source, Long serverId, Long messageId, Instant timeStamp, String message) {
        super(source);
        this.serverId = serverId;
        this.messageId = messageId;
        this.timeStamp = timeStamp;
        this.message = message;
    }
}
