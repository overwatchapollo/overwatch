/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.playerReport.repository;

import com.jsoc.overwatch.services.playerReport.model.PlayerRecord;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

/**
 * Registry configuration for players.
 */
public interface PlayerRepository extends CrudRepository<PlayerRecord, Long> {
    Collection<PlayerRecord> findByGuid(String guid);
    Collection<PlayerRecord> findByAliasRecords_AliasContainingIgnoreCase(String alias);
}
