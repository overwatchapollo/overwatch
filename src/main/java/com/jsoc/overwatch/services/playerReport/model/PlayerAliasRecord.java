/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.playerReport.model;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;

/**
 * Record for a player alias, an occurrance of their name and when it was used.
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@ToString
public class PlayerAliasRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JsonBackReference
    @ToString.Exclude
    private PlayerRecord player;

    private Long serverId;

    private String alias;

    private int used;

    private Instant firstUsed;

    private Instant lastUsed;
    
    /**
     * Increment the used counter by one.
     */
    public void incrementUsed() {
        this.used++;
    }
}
