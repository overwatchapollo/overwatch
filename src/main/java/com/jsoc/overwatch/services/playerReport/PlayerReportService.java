/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.playerReport;

import com.jsoc.overwatch.Security.SecurityHelper;
import com.jsoc.overwatch.controller.dto.NoteDto;
import com.jsoc.overwatch.services.kickService.events.KickRequestEvent;
import com.jsoc.overwatch.services.kickService.events.PlayerKickedEvent;
import com.jsoc.overwatch.services.kickService.model.KickRequestRecord;
import com.jsoc.overwatch.services.kickService.repository.KickRequestRepository;
import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import com.jsoc.overwatch.services.playerList.ConnectedPlayerService;
import com.jsoc.overwatch.services.playerList.events.ConnectedPlayerChange;
import com.jsoc.overwatch.services.playerNote.PlayerNoteService;
import com.jsoc.overwatch.services.playerNote.model.PlayerNote;
import com.jsoc.overwatch.services.playerReport.model.PlayerRecord;
import com.jsoc.overwatch.services.playerReport.repository.PlayerRepository;
import com.jsoc.overwatch.services.privilegedGuids.PrivilegedGuidService;
import com.jsoc.overwatch.services.userManagement.CustomUserDetailsService;
import com.jsoc.overwatch.services.userManagement.UserNotFoundException;
import com.jsoc.overwatch.services.userManagement.model.UserModel;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.*;

/**
 * Service used to query for players and to generate player reports.
 */
@Service
@Slf4j
public class PlayerReportService {

    @Autowired
    private PlayerNoteService noteService;

    @Autowired
    private PlayerRepository repo;

    @Autowired
    private ConnectedPlayerService playerService;

    @Autowired
    private CustomUserDetailsService userService;

    @Autowired
    private KickRequestRepository kickRepo;

    @Autowired
    private PrivilegedGuidService privilegedGuidService;

    public Optional<PlayerReport> getPlayerReport(User requester, String guid) {

        if (this.privilegedGuidService.isPrivileged(null, guid) && !SecurityHelper.userHasPermission("canSearchPrivileged")) {
            return Optional.empty();
        }

        log.info("Player lookup {} by user {}", guid, requester.getUsername());
        Optional<PlayerReport> record = this.getPlayerByGuid(guid).map(this::mapPlayer);
        Set<PlayerNote> notes = this.noteService.getNotesForPlayer(guid, 1L);
        Set<NoteDto> noteDto = StreamEx.of(notes)
            .map(this::mapNote)
            .toSet();
        record.ifPresent(x -> x.setNotes(noteDto));
        return record;
    }

    private NoteDto mapNote(PlayerNote x) {
        return NoteDto.builder()
            .id(x.getId())
            .note(x.getNote())
            .createdBy(x.getCreatedBy().getUserName())
            .createdOn(x.getCreatedDate().toString())
            .build();
    }

    private PlayerReport mapPlayer(PlayerRecord x) {
        return PlayerReport
            .builder()
            .id(x.getId())
            .guid(x.getGuid())
            .organisation(1L)
            .ipAddresses(Collections.singletonList(x.getIpAddress()))
            .firstSeen(x.getFirstSeen())
            .lastSeen(x.getLastSeen())
            .recordCount(x.getRecordCount())
            .aliasRecords(x.getAliasRecords())
            .kickHistory(x.getKickHistory())
            .build();
    }

    private Optional<PlayerRecord> getPlayerByGuid(String guid) {
        Collection<PlayerRecord> prs = this.repo.findByGuid(guid);
        if (prs.size() == 0) {
            return Optional.empty();
        }
        PlayerRecord pr = prs.iterator().next();
        return Optional.of(pr);
    }

    public List<PlayerReport> getPlayersByName(User user, String name) {
        log.trace("Players by name user {} name {}", user.getUsername(), name);
        return this.getPlayersByName(name);
    }

    private List<PlayerReport> getPlayersByName(String name) {
        boolean canSeeRestricted = SecurityHelper.userHasPermission("canSearchPrivileged");
        List<PlayerReport> records = StreamEx.of(this.repo.findByAliasRecords_AliasContainingIgnoreCase(name))
                .map(this::mapPlayer)
                .filter(x -> !this.privilegedGuidService.isPrivileged(null, x.getGuid()) || canSeeRestricted)
                .toList();
        records.forEach(x -> {
            Set<PlayerNote> notes = this.noteService.getNotesForPlayer(x.getGuid(), 1L);
            Set<NoteDto> noteDto = StreamEx.of(notes)
                    .map(this::mapNote)
                    .toSet();
            x.setNotes(noteDto);
        });

        return records;
    }

    @EventListener
    public void kickRequestListener(KickRequestEvent request) throws UserNotFoundException {
        ConnectedPlayer cp = this.playerService.getPlayerFromSession(request.getServerId(), request.getSessionId()).orElseThrow(IllegalArgumentException::new);
        String guid = cp.getGuid();
        Optional<PlayerRecord> report = this.getPlayerByGuid(guid);
        UserModel um = this.userService.getUserFromId(request.getRequesterId());
        report.ifPresent(x -> {
            KickRequestRecord record = report.get().addKickRequest(um.getUserName(), request.getServerId(), request.getMessage());
            record.setSuccessful(true);
            log.info("kick request: {}", record.getId());
            if (record.getId() != null) {
                this.repo.save(report.get());
            }
        });
    }

    @EventListener
    public void eventListener(PlayerKickedEvent event) throws UserNotFoundException {
        String guid = event.getPlayerGuid();
        Optional<PlayerRecord> report = this.getPlayerByGuid(guid);
        UserModel um = this.userService.getUserFromId(event.getRequester());
        report.ifPresent(x -> {
            KickRequestRecord record = x.addKickRequest(um.getUserName(), event.getServerId(), event.getMessage());
            record.setSuccessful(true);
            log.info("kick request: {}", record.getId());
            if (record.getId() != null) {
                this.repo.save(x);
            }
        });
    }

    public List<KickRequestRecord> getKickRequests(String... serverMrids) {
        List<String> mridList = Arrays.asList(serverMrids);
        return StreamEx.of(this.kickRepo.findAll().iterator())
            .filter(x -> mridList.contains(x.getServerLocation().toString()))
            .toList();
    }

    @EventListener
    public void onPlayerConnection(ConnectedPlayerChange connectedPlayerChange) {
        ConnectedPlayer cp = connectedPlayerChange.getConnectedPlayer();
        if (cp.getGuid() == null || cp.getGuid().isEmpty()) {
            return;
        }

        this.updatePlayerRecord(cp);
    }

    @Transactional
    private void updatePlayerRecord(ConnectedPlayer cp) {
        log.trace("Player report generating for {} - {}", cp.getName(), cp.getGuid());

        String key = cp.getGuid();

        Collection<PlayerRecord> opt = this.repo.findByGuid(key);
        if (opt.size() == 0) {
            opt.add(PlayerRecord.builder()
                    .guid(cp.getGuid())
                    .recordCount(0)
                    .firstSeen(Instant.now())
                    .build());
        }
        PlayerRecord pr = opt.iterator().next();
        pr.addAlias(cp.getName(), cp.getServerId());
        pr.setRecordCount(pr.getRecordCount() + 1);

        if (pr.getIpAddress() == null) {
            pr.setIpAddress("127.0.0.1");
        }

        pr.setLastSeen(Instant.now());

        this.repo.save(pr);
    }
}
