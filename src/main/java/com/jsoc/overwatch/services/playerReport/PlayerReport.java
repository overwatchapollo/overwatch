package com.jsoc.overwatch.services.playerReport;

import com.jsoc.overwatch.services.kickService.model.KickRequestRecord;
import com.jsoc.overwatch.controller.dto.NoteDto;
import com.jsoc.overwatch.services.playerReport.model.PlayerAliasRecord;
import lombok.*;

import java.time.Instant;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class PlayerReport {

    private Long id;

    private String guid;

    private Long organisation;

    private List<String> ipAddresses;

    private Set<NoteDto> notes;

    private Set<PlayerAliasRecord> aliasRecords;

    private Set<KickRequestRecord> kickHistory;

    private Instant firstSeen;

    private Instant lastSeen;

    private int recordCount;
}
