/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.playerReport.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.google.common.base.Objects;
import com.jsoc.overwatch.services.kickService.model.KickRequestRecord;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;

import javax.persistence.*;
import java.time.Instant;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Record of a player on the server.
 */

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "player_record_table")
@Slf4j
@ToString
public class PlayerRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "guid", unique = true, nullable = false)
    private String guid;

    private int recordCount;

    private String ipAddress;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="player_id", nullable = false, insertable = false, updatable = false)
    @JsonManagedReference
    @EqualsAndHashCode.Exclude
    private Set<PlayerAliasRecord> aliasRecords;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="player_id", nullable = false, insertable = false, updatable = false)
    @JsonManagedReference
    @EqualsAndHashCode.Exclude
    private Set<KickRequestRecord> kickHistory;

    private Instant firstSeen;

    private Instant lastSeen;

    public KickRequestRecord addKickRequest(String requester, Long serverLocation, String message) {
        if (this.kickHistory == null) {
            this.kickHistory = new HashSet<>();
        }
        KickRequestRecord record = KickRequestRecord.builder()
                .serverLocation(serverLocation)
                .message(message)
                .player(this)
                .requester(requester)
                .successful(true)
                .timeStamp(Instant.now())
                .build();
        this.kickHistory.add(record);
        return record;
    }

    /**
     * Add alias occurrance to player.
     * @param name Player name.
     */
    public void addAlias(String name, Long serverId) {
        if (this.aliasRecords == null) {
            this.aliasRecords = new HashSet<>();
        }
        
        Optional<PlayerAliasRecord> optAliasRecord = StreamEx.of(this.aliasRecords)
            .findFirst(x -> Objects.equal(x.getAlias(), name) && Objects.equal(x.getServerId(), serverId));
        PlayerAliasRecord aliasRecord = optAliasRecord
                .orElse(PlayerAliasRecord.builder()
                    .alias(name)
                    .serverId(serverId)
                    .used(0)
                    .player(this)
                    .firstUsed(Instant.now())
                .build());
        aliasRecord.incrementUsed();
        aliasRecord.setLastUsed(Instant.now());
        if (!optAliasRecord.isPresent()) {
            this.aliasRecords.add(aliasRecord);
        }
    }
}
