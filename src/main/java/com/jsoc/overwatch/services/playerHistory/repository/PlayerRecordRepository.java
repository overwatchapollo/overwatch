package com.jsoc.overwatch.services.playerHistory.repository;

import com.jsoc.overwatch.services.playerHistory.models.PlayerRecord;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PlayerRecordRepository extends CrudRepository<PlayerRecord, Long> {
    Optional<PlayerRecord> findByGuidAndServerId(String guid, Long serverId);
}
