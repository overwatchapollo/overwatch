package com.jsoc.overwatch.services.playerHistory.events;

import com.jsoc.overwatch.services.playerHistory.models.PlayerRecord;
import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
@EqualsAndHashCode
public class PlayerHistoryUpdateEvent extends ApplicationEvent {
    private final PlayerRecord playerRecord;
    private final ConnectedPlayer player;
    public PlayerHistoryUpdateEvent(Object source, PlayerRecord playerRecord, ConnectedPlayer player) {
        super(source);
        this.playerRecord = playerRecord;
        this.player = player;
    }
}
