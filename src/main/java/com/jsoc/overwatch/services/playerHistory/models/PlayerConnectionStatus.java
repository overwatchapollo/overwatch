package com.jsoc.overwatch.services.playerHistory.models;

public enum PlayerConnectionStatus {
    CONNECTED,
    DISCONNECTED,
    KICKED,
    BANNED;
}
