package com.jsoc.overwatch.services.playerHistory.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class PlayerName {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;
}
