package com.jsoc.overwatch.services.playerHistory.models;

import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Data
@Entity
@Table
public class PlayerJoinHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name="recordId", nullable = false)
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private PlayerRecord record;

    private String name;

    @JoinColumn(name="playerNameId")
    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    private PlayerName playerName;

    @Column(nullable = false)
    private Instant timeStamp;

    @Column(nullable = false)
    private PlayerConnectionStatus status;

    private String ipAddress;

    private String message;
}
