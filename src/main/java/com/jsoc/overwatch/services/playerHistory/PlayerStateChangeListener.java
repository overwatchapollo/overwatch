package com.jsoc.overwatch.services.playerHistory;

import com.jsoc.overwatch.services.playerHistory.events.NewPlayerRecord;
import com.jsoc.overwatch.services.playerHistory.events.PlayerHistoryUpdateEvent;
import com.jsoc.overwatch.services.playerHistory.models.PlayerConnectionStatus;
import com.jsoc.overwatch.services.playerHistory.models.PlayerJoinHistory;
import com.jsoc.overwatch.services.playerHistory.models.PlayerRecord;
import com.jsoc.overwatch.services.playerHistory.repository.PlayerHistoryRepository;
import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
public class PlayerStateChangeListener {

    @Autowired
    private PlayerHistoryRepository repo;


    @EventListener
    public void eventListener(NewPlayerRecord event) {
        this.recordPlayer(event.getRecord(), event.getPlayer());
    }

    @EventListener
    public void eventListener(PlayerHistoryUpdateEvent event) {
        this.recordPlayer(event.getPlayerRecord(), event.getPlayer());
    }

    private PlayerJoinHistory recordPlayer(PlayerRecord record, ConnectedPlayer player) {
        PlayerJoinHistory history = new PlayerJoinHistory();
        history.setName(player.getName());
        history.setRecord(record);
        history.setTimeStamp(Instant.now());
        PlayerConnectionStatus status = this.getStatus(player);
        history.setStatus(status);
        history.setIpAddress(player.getIpAddress());
        if (status == PlayerConnectionStatus.KICKED || status == PlayerConnectionStatus.BANNED) {
            history.setMessage(player.getDisconnectionMessage());
        }
        return this.repo.save(history);
    }

    private PlayerConnectionStatus getStatus(ConnectedPlayer player) {
        if (player.isConnected()) {
            return PlayerConnectionStatus.CONNECTED;
        } else if (player.isKicked()){
            return PlayerConnectionStatus.KICKED;
        } else if (player.isBanned()) {
            return PlayerConnectionStatus.BANNED;
        } else {
            return PlayerConnectionStatus.DISCONNECTED;
        }
    }
}
