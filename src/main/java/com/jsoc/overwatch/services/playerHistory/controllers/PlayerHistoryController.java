package com.jsoc.overwatch.services.playerHistory.controllers;

import com.jsoc.overwatch.Security.permissions.ServerPermissionGenerator;
import com.jsoc.overwatch.controller.dto.ForbiddenException;
import com.jsoc.overwatch.controller.dto.NoteDto;
import com.jsoc.overwatch.controller.dto.PlayerNotFoundException;
import com.jsoc.overwatch.controller.dto.UnauthorisedException;
import com.jsoc.overwatch.services.kickService.model.KickRequestRecord;
import com.jsoc.overwatch.services.playerHistory.PlayerAliasService;
import com.jsoc.overwatch.services.playerHistory.PlayerHistoryService;
import com.jsoc.overwatch.services.playerHistory.PlayerKickHistoryService;
import com.jsoc.overwatch.services.playerHistory.models.PlayerAlias;
import com.jsoc.overwatch.services.playerHistory.models.PlayerJoinHistory;
import com.jsoc.overwatch.services.playerHistory.models.PlayerKickRecord;
import com.jsoc.overwatch.services.playerNote.PlayerNoteService;
import com.jsoc.overwatch.services.playerNote.model.PlayerNote;
import com.jsoc.overwatch.services.playerReport.PlayerReport;
import com.jsoc.overwatch.services.playerReport.model.PlayerAliasRecord;
import com.sun.xml.fastinfoset.stax.events.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.time.Instant;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.jsoc.overwatch.Security.SecurityHelper.authoriseUserFromSession;
import static com.jsoc.overwatch.Security.SecurityHelper.getUserFromRequest;

@RestController
@RequestMapping("/api/playerHistory")
@PreAuthorize("isAuthenticated()")
@Slf4j
public class PlayerHistoryController {
    @Autowired
    private PlayerHistoryService historyService;

    @Autowired
    private PlayerAliasService aliasService;

    @Autowired
    private PlayerKickHistoryService kickHistoryService;

    @Autowired
    private PlayerNoteService noteService;

    private ServerPermissionGenerator serverGenerator = new ServerPermissionGenerator();

    @GetMapping("/by-guid/{guid}")
    @ResponseBody
    public PlayerReport getPlayerByGuid(HttpServletRequest request, @PathVariable("guid") String guid) {
        User user = getUserFromRequest().orElseThrow(ForbiddenException::new);
        String investigatePattern = this.serverGenerator.investigate().getPermission();
        Pattern pattern = Pattern.compile(investigatePattern);
        List<Long> serverIds = user
            .getAuthorities()
            .stream()
            .map(x -> pattern.matcher(x.getAuthority()))
            .filter(Matcher::find)
            .map(x -> x.group(1))
            .map(Long::parseLong)
            .collect(Collectors.toList());
        return this.getPlayer(serverIds, guid);
    }

    private PlayerReport getPlayer(List<Long> serverIds, String guid) {
        Set<PlayerJoinHistory> history = serverIds.stream().map(x -> this.aliasService.getPlayerHistory(x, guid)).flatMap(Collection::stream).collect(Collectors.toSet());
        List<PlayerAlias> aliases = serverIds.stream().map(x -> this.aliasService.getPlayerAlias(x, guid)).flatMap(Collection::stream).collect(Collectors.toList());
        Set<PlayerAliasRecord> records = aliases.stream()
            .map(x -> PlayerAliasRecord
                .builder()
                .serverId(x.getServerId())
                .alias(x.getName())
                .used(x.getCount())
                .firstUsed(x.getFirstSeen())
                .lastUsed(x.getLastSeen())
                .build())
            .collect(Collectors.toSet());

        Instant firstSeen = records.stream()
            .reduce(BinaryOperator.minBy(Comparator.comparing(PlayerAliasRecord::getFirstUsed)))
            .map(PlayerAliasRecord::getFirstUsed)
            .orElse(Instant.now());

        Instant lastSeen = records.stream()
                .reduce(BinaryOperator.maxBy(Comparator.comparing(PlayerAliasRecord::getLastUsed)))
                .map(PlayerAliasRecord::getLastUsed)
                .orElse(Instant.now());

        Integer timesSeen = records.stream().map(x -> x.getUsed()).reduce(0, (a,b) -> a + b);
        Set<KickRequestRecord> kicks = serverIds
            .stream()
            .map(x -> this.kickHistoryService.getKickHistory(x, guid))
            .flatMap(Collection::stream)
            .map(this::map)
            .collect(Collectors.toSet());

        List<String> ipAddresses = history.stream().map(PlayerJoinHistory::getIpAddress).distinct().filter(Util::isEmptyString).collect(Collectors.toList());

        Set<NoteDto> notes = this.noteService.getNotesForPlayer(guid, 1L).stream().map(this::mapNote).collect(Collectors.toSet());

        PlayerReport report = new PlayerReport();
        report.setGuid(guid);
        report.setOrganisation(0L);
        report.setIpAddresses(ipAddresses);
        report.setNotes(notes);
        report.setAliasRecords(records);
        report.setKickHistory(kicks);
        report.setFirstSeen(firstSeen);
        report.setLastSeen(lastSeen);
        report.setRecordCount(timesSeen);
        return report;
    }

    private KickRequestRecord map(PlayerKickRecord record) {
        KickRequestRecord outRecord = new KickRequestRecord();
        outRecord.setSuccessful(true);
        outRecord.setRequester("unknown");
        outRecord.setIpAddress(record.getIpAddress());
        outRecord.setServerLocation(record.getServerId());
        outRecord.setMessage(record.getMessage());
        outRecord.setTimeStamp(record.getTimeStamp());
        return outRecord;
    }

    private NoteDto mapNote(PlayerNote x) {
        return NoteDto.builder()
                .id(x.getId())
                .note(x.getNote())
                .createdBy(x.getCreatedBy().getUserName())
                .createdOn(x.getCreatedDate().toString())
                .build();
    }
}
