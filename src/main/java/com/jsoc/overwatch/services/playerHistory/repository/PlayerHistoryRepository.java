package com.jsoc.overwatch.services.playerHistory.repository;

import com.jsoc.overwatch.services.playerHistory.models.PlayerJoinHistory;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface PlayerHistoryRepository extends CrudRepository<PlayerJoinHistory, Long> {
    Set<PlayerJoinHistory> findByRecordId(Long recordId);
}
