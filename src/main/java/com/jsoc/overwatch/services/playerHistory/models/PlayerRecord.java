package com.jsoc.overwatch.services.playerHistory.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "playerRecord")
@Table(name="playerRecords", uniqueConstraints=@UniqueConstraint(columnNames = {"serverId", "guid"}))
public class PlayerRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Long serverId;

    @Column(nullable = false)
    private String guid;
}
