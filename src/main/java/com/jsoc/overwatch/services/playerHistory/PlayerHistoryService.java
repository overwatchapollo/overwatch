package com.jsoc.overwatch.services.playerHistory;

import com.jsoc.overwatch.services.playerHistory.events.PlayerHistoryUpdateEvent;
import com.jsoc.overwatch.services.playerHistory.models.PlayerRecord;
import com.jsoc.overwatch.services.playerHistory.repository.PlayerRecordRepository;
import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import com.jsoc.overwatch.services.playerList.events.ConnectedPlayerChange;
import com.jsoc.overwatch.services.playerList.events.NewPlayerListEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class PlayerHistoryService {
    @Autowired
    private PlayerRecordRepository repo;

    @Autowired
    private ApplicationEventPublisher publisher;

    @EventListener
    public void eventListener(ConnectedPlayerChange event) {
        String guid = event.getConnectedPlayer().getGuid();
        if (guid == null) {
            return;
        }

        this.handlePlayerUpdate(event.getConnectedPlayer());
    }

    @EventListener
    public void eventListener(NewPlayerListEvent event) {
        event.getPlayers().forEach(this::handlePlayerUpdate);
    }

    private void handlePlayerUpdate(ConnectedPlayer player) {
        PlayerRecord record = this.storePlayer(player);
        this.publisher.publishEvent(new PlayerHistoryUpdateEvent(this, record, player));
    }

    private synchronized PlayerRecord storePlayer(ConnectedPlayer player) {
        String guid = player.getGuid();
        Long serverId = player.getServerId();
        return this.repo.findByGuidAndServerId(guid, serverId).orElseGet(() -> {
            PlayerRecord record = new PlayerRecord();
            record.setGuid(guid);
            record.setServerId(serverId);
            return this.repo.save(record);
        });
    }

    public Optional<PlayerRecord> getPlayerRecord(String guid, Long serverId) {
        return this.repo.findByGuidAndServerId(guid, serverId);
    }
}
