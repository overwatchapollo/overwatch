package com.jsoc.overwatch.services.playerHistory.events;

import com.jsoc.overwatch.services.playerHistory.models.PlayerRecord;
import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class NewPlayerRecord extends ApplicationEvent {

    private PlayerRecord record;
    private ConnectedPlayer player;
    public NewPlayerRecord(Object source, PlayerRecord record, ConnectedPlayer player) {
        super(source);
        this.record = record;
        this.player = player;
    }
}
