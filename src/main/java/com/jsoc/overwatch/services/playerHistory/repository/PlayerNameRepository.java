package com.jsoc.overwatch.services.playerHistory.repository;

import com.jsoc.overwatch.services.playerHistory.models.PlayerName;
import org.springframework.data.repository.CrudRepository;

public interface PlayerNameRepository extends CrudRepository<PlayerName, Long> {
}
