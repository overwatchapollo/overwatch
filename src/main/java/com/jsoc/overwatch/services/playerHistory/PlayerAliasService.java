package com.jsoc.overwatch.services.playerHistory;

import com.jsoc.overwatch.services.playerHistory.models.*;
import com.jsoc.overwatch.services.playerHistory.repository.PlayerHistoryRepository;
import com.jsoc.overwatch.services.playerNote.PlayerNoteService;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.*;
import java.util.function.BinaryOperator;

@Service
@Slf4j
public class PlayerAliasService {

    @Autowired
    private PlayerHistoryRepository repo;

    @Autowired
    private PlayerHistoryService historyService;

    public PlayerAliasService(PlayerHistoryRepository repo, PlayerHistoryService historyService) {
        this.repo = repo;
        this.historyService = historyService;
    }

    public Set<PlayerJoinHistory> getPlayerHistory(Long serverId, String guid) {
        return this.historyService.getPlayerRecord(guid, serverId).map(x -> this.repo.findByRecordId(x.getId())).orElse(new HashSet<>());
    }

    public Set<PlayerAlias> getPlayerAlias(Long serverId, String guid) {
        Optional<PlayerRecord> record = this.historyService.getPlayerRecord(guid, serverId);
        Set<PlayerAlias> aliases = record
            .map(x -> getPlayerAlias(serverId, x.getId()))
            .orElse(Collections.emptySet());

        log.info("Found {} alises for {}", aliases.size(), guid);
        return aliases;
    }

    private Set<PlayerAlias> getPlayerAlias(Long serverId, Long recordId) {
        log.info("looking up {} on server {}", recordId, serverId);
        Set<PlayerJoinHistory> historySet = this.repo.findByRecordId(recordId);
        Map<String, List<PlayerJoinHistory>> nameMap = StreamEx.of(historySet)
            .filter(y -> y.getStatus() == PlayerConnectionStatus.CONNECTED)
            .groupingBy(y -> {
                PlayerName name = y.getPlayerName();
                if (name == null) {
                    return y.getName();
                }
                return name.getName();
            });
        log.info("Found {} aliases for record {}: {}", nameMap.keySet().size(), recordId, nameMap.keySet());
        return StreamEx.of(nameMap.keySet()).map(y -> {
            log.info("Compressing {}", y);
            List<PlayerJoinHistory> historyList = nameMap.get(y);
            Instant firstSeen = historyList.stream()
                .map(x -> {
                    if (x.getTimeStamp() == null) {
                        x.setTimeStamp(Instant.now());
                    }
                    return x;
                })
                .reduce(BinaryOperator.minBy(Comparator.comparing(PlayerJoinHistory::getTimeStamp)))
                .map(PlayerJoinHistory::getTimeStamp)
                .orElse(Instant.now());
            Instant lastSeen = historyList.stream()
                .map(x -> {
                    if (x.getTimeStamp() == null) {
                        x.setTimeStamp(Instant.now());
                    }
                    return x;
                })
                .reduce(BinaryOperator.maxBy(Comparator.comparing(PlayerJoinHistory::getTimeStamp)))
                .map(PlayerJoinHistory::getTimeStamp)
                .orElse(Instant.now());
            log.info("Compressing {}: size: {}, first: {}, last: {}", y, historyList.size(), firstSeen, lastSeen);
            return new PlayerAlias(serverId, y, historyList.size(), firstSeen, lastSeen);
        }).toSet();
    }
}
