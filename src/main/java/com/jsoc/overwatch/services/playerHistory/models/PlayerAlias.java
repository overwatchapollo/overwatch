package com.jsoc.overwatch.services.playerHistory.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.Instant;

@AllArgsConstructor
@Getter
public class PlayerAlias {
    private Long serverId;
    private String name;
    private int count;
    private Instant firstSeen;
    private Instant lastSeen;
}
