package com.jsoc.overwatch.services.playerHistory;

import com.jsoc.overwatch.services.playerHistory.models.PlayerConnectionStatus;
import com.jsoc.overwatch.services.playerHistory.models.PlayerJoinHistory;
import com.jsoc.overwatch.services.playerHistory.models.PlayerKickRecord;
import com.jsoc.overwatch.services.playerHistory.models.PlayerRecord;
import com.jsoc.overwatch.services.playerHistory.repository.PlayerHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PlayerKickHistoryService {

    @Autowired
    private PlayerHistoryRepository repo;

    @Autowired
    private PlayerHistoryService historyService;

    public Set<PlayerKickRecord> getKickHistory(Long serverId, String guid) {
        Optional<PlayerRecord> record = this.historyService.getPlayerRecord(guid, serverId);
        return record
            .map(x -> this.getPlayerKickHistory(serverId, x.getId()))
            .orElse(new HashSet<>());
    }

    private Set<PlayerKickRecord> getPlayerKickHistory(Long serverId, Long recordId) {
        Set<PlayerJoinHistory> historySet = this.repo.findByRecordId(recordId);
        return historySet.stream()
            .filter(x -> x.getStatus() == PlayerConnectionStatus.KICKED || x.getStatus() == PlayerConnectionStatus.BANNED)
            .map(x -> {
                PlayerKickRecord record = new PlayerKickRecord(serverId, x.getIpAddress(), x.getTimeStamp(), x.getMessage());
                return record;
            }).collect(Collectors.toSet());
    }
}
