package com.jsoc.overwatch.services.playerHistory.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.Instant;

@AllArgsConstructor
@Getter
public class PlayerKickRecord {
    private Long serverId;
    private String ipAddress;
    private Instant timeStamp;
    private String message;
}
