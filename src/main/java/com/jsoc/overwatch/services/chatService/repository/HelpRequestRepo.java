package com.jsoc.overwatch.services.chatService.repository;

import com.jsoc.overwatch.services.chatService.model.HelpRequestRecord;
import org.springframework.data.repository.CrudRepository;

public interface HelpRequestRepo extends CrudRepository<HelpRequestRecord, String> {
}
