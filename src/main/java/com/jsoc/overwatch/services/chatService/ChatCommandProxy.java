package com.jsoc.overwatch.services.chatService;

import com.jsoc.overwatch.services.connection.CommandService;
import com.jsoc.overwatch.services.connection.api.CommandFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
public class ChatCommandProxy {
    @Autowired
    private CommandService commandService;

    private final CommandFactory factory = new CommandFactory();

    public CompletableFuture<Boolean> whisper(Long serverId, Integer sessionId, String message) {
        CompletableFuture<Boolean> response = new CompletableFuture<>();
        this.commandService.runCommand(serverId, factory.sayWhisper(sessionId, message))
            .thenAccept(x -> response.complete(x.isSuccess()));
        return response;
    }

    public CompletableFuture<Boolean> broadcast(Long serverId, String message) {
        CompletableFuture<Boolean> response = new CompletableFuture<>();
        this.commandService.runCommand(serverId, factory.sayAll(message))
                .thenAccept(x -> response.complete(x.isSuccess()));
        return response;
    }
}
