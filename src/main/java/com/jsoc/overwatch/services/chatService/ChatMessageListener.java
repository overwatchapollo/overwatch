package com.jsoc.overwatch.services.chatService;

import com.jsoc.overwatch.services.chatService.events.ChatMessageReceived;
import com.jsoc.overwatch.services.connection.events.RconMessageReceived;
import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import com.jsoc.overwatch.services.playerList.ConnectedPlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Listener component which attempts to convert RCON messages into chat messages.
 */
@Component
public class ChatMessageListener {

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private ConnectedPlayerService playerService;

    /**
     * Chat pattern for a connected user chat message.
     */
    private static final Pattern chatPattern = Pattern.compile("^\\((.+)\\) (.+): (.+)");

    /**
     * Chat pattern for an RCON admin message.
     */
    private static final Pattern rconChatPattern = Pattern.compile("RCon admin \\#(\\d+): \\((.+)\\) (.+)");

    @EventListener
    public void listener(RconMessageReceived event) {
        Optional<ChatMessage> maybeCM = this.parseChatMessage(event);
        maybeCM.ifPresent(this::broadcastChatMessage);
    }
    private Optional<ChatMessage> parseChatMessage(RconMessageReceived rconEvent) {
        String message = rconEvent.getMessage();
        Matcher chatMatcher = chatPattern.matcher(message);
        Matcher rconMatcher = rconChatPattern.matcher(message);
        Long serverId = rconEvent.getServerId();

        if (chatMatcher.find()) {
            String sender = chatMatcher.group(2);
            ConnectedPlayer player = this.playerService.getPlayer(serverId, sender).orElse(null);
            return Optional.of(chatMap(serverId, chatMatcher, player));
        } else if (rconMatcher.find()) {
            String sender = rconMatcher.group(2);
            ConnectedPlayer player = this.playerService.getPlayer(serverId, sender).orElse(null);
            return Optional.of(rconMap(serverId, rconMatcher, player));
        }

        return Optional.empty();
    }

    /**
     * Broadcast chat message to other services.
     * @param cm Chat message to broadcast.
     */
    private void broadcastChatMessage(ChatMessage cm) {
        this.publisher.publishEvent(new ChatMessageReceived(this, cm));
    }

    private static ChatMessage chatMap(Long serverId, Matcher chatMatcher, ConnectedPlayer player) {
        return ChatMessage.builder()
                .isRcon(false)
                .channel(chatMatcher.group(1))
                .sender(chatMatcher.group(2))
                .message(chatMatcher.group(3))
                .senderObject(player)
                .timeStamp(Instant.now())
                .serverId(serverId)
                .build();
    }

    private static ChatMessage rconMap(Long serverId, Matcher rconMatcher, ConnectedPlayer player) {
        return ChatMessage.builder()
                .isRcon(true)
                .channel(rconMatcher.group(2))
                .sender("RCON Client " + rconMatcher.group(1))
                .message(rconMatcher.group(3))
                .senderObject(player)
                .timeStamp(Instant.now())
                .serverId(serverId)
                .build();
    }
}
