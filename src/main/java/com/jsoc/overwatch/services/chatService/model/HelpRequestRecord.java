package com.jsoc.overwatch.services.chatService.model;

import com.jsoc.overwatch.services.chatService.HelpRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "help_request_table")
public class HelpRequestRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private boolean resolved;
    private String helpMessage;
    private Long serverId;
    private boolean isRcon;
    private Instant timeStamp;
    private String sender;
    private String channel;
    private String message;

    public HelpRequestRecord(HelpRequest request) {
        this.resolved = request.isResolved();
        this.helpMessage = request.getHelpRequest();
        this.serverId = request.getOriginalMessage().getServerId();
        this.isRcon = request.getOriginalMessage().isRcon();
        this.timeStamp = request.getOriginalMessage().getTimeStamp();
        this.sender = request.getOriginalMessage().getSender();
        this.channel = request.getOriginalMessage().getChannel();
        this.message = request.getOriginalMessage().getMessage();
    }
}
