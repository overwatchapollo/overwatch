/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.chatService.parser;

import java.util.Arrays;
import java.util.List;

/**
 * Factory to create message parsers.
 */
public class MessageParserFactory {
    public static ChatParser chatParser() {
        return new ChatParser();
    }
    
    public static RconChatParser rconChatParser() {
        return new RconChatParser();
    }
    
    public static List<MessageParser<?>> all() {
        return Arrays.asList(chatParser(), rconChatParser());
    }
}
