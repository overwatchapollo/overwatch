/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.chatService.parser;

import com.jsoc.overwatch.services.banService.BanRequest;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Ban Parser for message requests.
 */
public final class BanParser extends MessageParser<BanRequest> {
    /**
     * Ban Pattern.
     */
    private static final Pattern BAN_PATTERN = Pattern.compile("!ban #(.+) (\\d+) (.+)");
    
    @Override
    protected BanRequest parse(RconMessage message) {
        Matcher matcher = this.getPattern().matcher(message.getRawMessage());
        return BanRequest.builder()
            .timestamp(message.getTimeStamp())
            .serverLocation(message.getServerLocation())
            .id(UUID.randomUUID())
            .build();
    }

    @Override
    protected Pattern getPattern() {
        return BAN_PATTERN;
    }
    
}
