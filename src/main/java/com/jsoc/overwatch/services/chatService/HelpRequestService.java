/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.chatService;

import com.jsoc.overwatch.config.FeatureToggleBefore;
import com.jsoc.overwatch.config.Features;
import com.jsoc.overwatch.services.chatService.events.ChatHelpRequest;
import com.jsoc.overwatch.services.chatService.events.ChatMessageReceived;
import com.jsoc.overwatch.services.chatService.model.HelpRequestRecord;
import com.jsoc.overwatch.services.chatService.repository.HelpRequestRepo;

import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

/**
 * Service which consumes chat events and checks if the message contains a help request.
 */
@Service
@Slf4j
public class HelpRequestService {

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private HelpRequestRepo repo;

    private static final Pattern watchPattern = Pattern.compile("^!support (.+)");

    public HelpRequestService(ApplicationEventPublisher publisher, HelpRequestRepo repo) {
        this.publisher = publisher;
        this.repo = repo;
    }

    @FeatureToggleBefore(Features.CHAT_HELP_REQUEST)
    @EventListener
    public void handleMessage(ChatMessageReceived cmr) {
        ChatMessage cm = cmr.getChatMessage();
        if (cm.isRcon()) {
            return;
        }

        Matcher matcher = this.watchPattern.matcher(cm.getMessage());
        if (matcher.find()) {
            String helpMessage = matcher.group(1);
            HelpRequest hr = HelpRequest.builder().helpRequest(helpMessage).originalMessage(cm).build();
            log.info("Help request received from player: {}", hr.getOriginalMessage().getSender());
            this.repo.save(new HelpRequestRecord(hr));
            this.publisher.publishEvent(new ChatHelpRequest(this, hr));
        }
    }

    public List<HelpRequestRecord> getEvents() {
        return StreamEx.of(this.repo.findAll().iterator())
            .sortedBy(x -> x.getTimeStamp())
            .toList();
    }
}
