package com.jsoc.overwatch.services.chatService.events;

import com.jsoc.overwatch.services.chatService.HelpRequest;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class ChatHelpRequest extends ApplicationEvent {
    private HelpRequest request;
    public ChatHelpRequest(Object source, HelpRequest request) {
        super(source);
        this.request = request;
    }
}
