/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.chatService;

import java.time.Instant;

import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Chat message received from RCON.
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class ChatMessage {
    private Long messageId;
    private Long serverId;
    private boolean isRcon;
    private Instant timeStamp;
    private String sender;
    private String channel;
    private String message;
    private ConnectedPlayer senderObject;
}
