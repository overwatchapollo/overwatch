/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.chatService.parser;

import com.jsoc.overwatch.services.chatService.ChatMessage;
import lombok.extern.slf4j.Slf4j;

import java.time.Instant;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses a message.
 */
@Slf4j
public class ChatParser extends MessageParser<ChatMessage> {
    /**
     * Pattern for chat.
     */
    private static final Pattern CHAT_PATTERN = Pattern.compile("^\\((\\w+)\\) ([^:]+): (.+)");
    
    @Override
    protected ChatMessage parse(RconMessage message) {
        log.info("Parsing {}", message.getRawMessage());
        Matcher chatMatcher = this.getPattern().matcher(message.getRawMessage());
        if (!chatMatcher.find()) {
            throw new IllegalArgumentException("Message is not of type ChatMessage.");
        }
        return ChatMessage.builder()
            .isRcon(false)
            .channel(chatMatcher.group(1))
            .sender(chatMatcher.group(2))
            .message(chatMatcher.group(3))
            .timeStamp(Instant.now())
            .build();
    }

    @Override
    protected Pattern getPattern() {
        return CHAT_PATTERN;
    }
}
