package com.jsoc.overwatch.services.chatService;

import com.jsoc.overwatch.services.chatService.events.ChatMessageReceived;
import com.jsoc.overwatch.services.chatService.events.ChatMessageStored;
import com.jsoc.overwatch.services.connection.events.ServerStateUpdateEvent;
import com.jsoc.overwatch.services.serverRegister.events.ServerConnectionRegistered;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Chat cache for servers.
 * stream from rcon for chat messages.
 */
@Service
@Slf4j
public class ChatMessageCache {

    @Autowired
    private ApplicationEventPublisher publisher;

    private final Map<Long, ServerChatManager> chatManagerMap = new ConcurrentHashMap<>();

    @EventListener
    public void listener(ChatMessageReceived event) {
        this.recordChatMessage(event.getChatMessage().getServerId(), event.getChatMessage());
    }

    @EventListener
    public void eventListener(ServerConnectionRegistered event) {
        Long id = event.getServerGuid();
        ServerChatManager manager = this.chatManagerMap.get(id);
        if (manager == null) {
            ServerChatManager newManager = new ServerChatManager();
            this.chatManagerMap.put(id, newManager);
        }
    }

    @EventListener
    public void eventListener(ServerStateUpdateEvent event) {
        Long id = event.getServerGuid();
        ServerChatManager manager = this.chatManagerMap.get(id);
        if (manager == null) {
            log.error("Player chat event raised on server {} but no player list exists!", id);
            ServerChatManager newManager = new ServerChatManager();
            this.chatManagerMap.put(id, newManager);
        }
    }

    /**
     * Get the chat log for a server.
     * @param serverId Server id to get.
     * @return List of all chat records.
     */
    public Optional<List<ChatMessage>> getChatLog(Long serverId) {
        return Optional.ofNullable(this.chatManagerMap.get(serverId)).map(ServerChatManager::getMessages);
    }

    /**
     * Record chat message in log.
     * @param serverId Server id of chat message.
     * @param cm Chat message to log.
     */
    private void recordChatMessage(Long serverId, ChatMessage cm) {
        if (!this.chatManagerMap.containsKey(serverId)) {
            log.debug("First record for server {}", serverId);
            this.chatManagerMap.put(serverId, new ServerChatManager());
        }

        if (this.chatManagerMap.containsKey(serverId)) {
            this.chatManagerMap.get(serverId).addChatMessage(cm);
            this.publisher.publishEvent(new ChatMessageStored(this, cm));
        } else {
            log.error("Failed to record chat message for server {}", serverId);
        }
    }
}
