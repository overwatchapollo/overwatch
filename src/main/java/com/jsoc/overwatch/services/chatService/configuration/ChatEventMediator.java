package com.jsoc.overwatch.services.chatService.configuration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.jsoc.overwatch.services.chatService.events.ChatHelpRequest;
import com.jsoc.overwatch.services.chatService.events.ChatMessageReceived;
import com.jsoc.overwatch.services.chatService.events.ChatMessageStored;
import com.jsoc.overwatch.services.playerList.events.PlayerChatEvent;
import com.jsoc.overwatch.services.mqttListener.MqttService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@Configuration
public class ChatEventMediator {

    @Autowired
    private MqttService service;

    @EventListener
    public void playerUpdate(PlayerChatEvent playerEvent) throws JsonProcessingException {
        this.service.publish("server/" + playerEvent.getConnectedPlayer().getServerId() + "/update/player", playerEvent.getConnectedPlayer());
    }

    @EventListener
    public void chatUpdate(ChatMessageStored message) throws JsonProcessingException {
        this.service.publish("server/" + message.getChatMessage().getServerId() + "/update/chat/message", message.getChatMessage());
    }

    @EventListener
    public void helpUpdate(ChatHelpRequest chatRequest) throws JsonProcessingException {
        this.service.publish("server/request/help", chatRequest.getRequest());
    }

}
