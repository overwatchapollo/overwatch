package com.jsoc.overwatch.services.chatService;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ServerChatManager {

    private Long chatNumber = 0L;

    private List<ChatMessage> messages = new CopyOnWriteArrayList<>();

    public void addChatMessage(ChatMessage chatMessage) {
        chatMessage.setMessageId(this.generateId());
        this.messages.add(chatMessage);
    }

    public List<ChatMessage> getMessages() {
        return this.messages;
    }

    private Long generateId() {
        synchronized (this) {
            Long returnVal = this.chatNumber;
            this.chatNumber++;
            return returnVal;
        }
    }
}
