package com.jsoc.overwatch.services.chatService.events;

import com.jsoc.overwatch.services.chatService.ChatMessage;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class ChatMessageReceived extends ApplicationEvent {
    private ChatMessage chatMessage;
    public ChatMessageReceived(Object source, ChatMessage chatMessage) {
        super(source);
        this.chatMessage = chatMessage;
    }
}
