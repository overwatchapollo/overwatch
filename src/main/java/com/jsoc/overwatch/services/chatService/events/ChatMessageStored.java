package com.jsoc.overwatch.services.chatService.events;

import com.jsoc.overwatch.services.chatService.ChatMessage;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class ChatMessageStored extends ApplicationEvent {
    private ChatMessage chatMessage;
    public ChatMessageStored(Object source, ChatMessage chatMessage) {
        super(source);
        this.chatMessage = chatMessage;
    }
}
