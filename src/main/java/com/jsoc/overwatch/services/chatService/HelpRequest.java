/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.chatService;

import com.jsoc.overwatch.services.chatService.ChatMessage;
import lombok.Builder;
import lombok.Data;

/**
 * Model for a help request. Used when a player types '!help' in chat.
 */
@Data
@Builder
public class HelpRequest {
    
    private boolean resolved;
    private String helpRequest;
    private ChatMessage originalMessage;
}
