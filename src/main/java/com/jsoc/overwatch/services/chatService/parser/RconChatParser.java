/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.chatService.parser;

import com.jsoc.overwatch.services.chatService.ChatMessage;
import java.time.Instant;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses a message which was sent via RCON.
 */
public class RconChatParser extends MessageParser<ChatMessage> {

    /**
     * Pattern for rcon chat.
     */
    private static final Pattern CHAT_PATTERN = Pattern.compile("\"RCon admin \\\\#(\\\\d+): \\\\((.+)\\\\) (.+)\"");
    
    @Override
    protected ChatMessage parse(RconMessage message) {
        Matcher chatMatcher = this.getPattern().matcher(message.getRawMessage());
        if (!chatMatcher.find()) {
            throw new IllegalArgumentException("Message is not of type ChatMessage from RCON.");
        }
        
        return ChatMessage.builder()
            .isRcon(true)
            .channel(chatMatcher.group(2))
            .sender("RCON Client " + chatMatcher.group(1))
            .message(chatMatcher.group(3))
            .timeStamp(Instant.now())
            .build();
    }

    @Override
    protected Pattern getPattern() {
        return CHAT_PATTERN;
    }
    
}
