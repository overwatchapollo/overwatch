/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.chatService.parser;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import one.util.streamex.StreamEx;

/**
 * Container for a regex pattern with subsequent patterns to check after.
 * 
 * For example, a chat message can also contain a ban request.
 */
public abstract class MessageParser<T> implements Predicate<RconMessage>, Function<RconMessage, List<Object>> {
    
    /**
     * Child parsers to check on success of current parse.
     */
    private List<MessageParser> childParsers = Collections.emptyList();

    @Override
    public boolean test(RconMessage message) {
        return this.getPattern().matcher(message.getRawMessage()).find();
    }
    
    @Override
    public List<Object> apply(RconMessage message) {
        if (this.test(message)) {
            return Arrays.asList(
                this.parse(message),
                StreamEx.of(this.childParsers)
                    .map(x -> x.apply(message))
                    .flatMap(x -> x.stream())
                    .toArray());
        }
        
        return Collections.emptyList();
    }
    
    /**
     * Parse for this type.
     * @param message message to parse.
     * @return Object of type T.
     */
    protected abstract T parse(RconMessage message);
    
    /**
     * Get the pattern for parser.
     * 
     * @return The pattern.
     */
    protected abstract Pattern getPattern();
}
