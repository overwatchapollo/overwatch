/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.services.chatService.parser;

import java.time.Instant;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Message from the server from the RCON message stream.
 */
@AllArgsConstructor
@Data
@Builder
@NoArgsConstructor
public class RconMessage {
    /**
     * Unique identifier of message.
     */
    private UUID id;
    
    /**
     * Message received from 
     */
    private String rawMessage;
    
    /**
     * Server reference where message originated from.
     */
    private String serverLocation;
    
    /**
     * Timestamp when message was received.
     */
    private Instant timeStamp;
}
