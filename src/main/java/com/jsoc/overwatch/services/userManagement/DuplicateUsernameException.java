package com.jsoc.overwatch.services.userManagement;

import lombok.Data;

@Data
public class DuplicateUsernameException extends Exception {
    private String username;
    public DuplicateUsernameException(String username) {
        this.username = username;
    }
}
