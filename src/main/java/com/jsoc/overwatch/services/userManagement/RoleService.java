package com.jsoc.overwatch.services.userManagement;

import com.jsoc.overwatch.Security.permissions.RolePermissionGenerator;
import com.jsoc.overwatch.Security.permissions.ServerPermissionGenerator;
import com.jsoc.overwatch.Security.permissions.SystemPermissionGenerator;
import com.jsoc.overwatch.controller.dto.ForbiddenException;
import com.jsoc.overwatch.services.userManagement.model.AuthorityModel;
import com.jsoc.overwatch.services.userManagement.model.RoleModel;
import com.jsoc.overwatch.services.userManagement.repository.RoleRepository;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Slf4j
public class RoleService {

    private RolePermissionGenerator generator = new RolePermissionGenerator();

    private ServerPermissionGenerator serverGenerator = new ServerPermissionGenerator();

    private SystemPermissionGenerator systemPermissionGenerator = new SystemPermissionGenerator();

    @Autowired
    private RoleRepository roleRepository;

    public final RoleModel createRole(User user, String name, Set<GrantedAuthority> authority) {
        
        return this.createRole(name, authority);
    }

    public final RoleModel createRole(String name, Set<GrantedAuthority> authority) {
        Set<AuthorityModel> models = StreamEx.of(authority).map(x -> AuthorityModel.builder().authority(x.getAuthority()).build()).toSet();
        RoleModel rm = RoleModel.builder().authoritySet(models).name(name).build();
        return this.roleRepository.save(rm);
    }

    public void deleteRoleById(User requester, Long roleId) {
        if (!this.generator.delete(roleId).hasPermission(requester)) {
            throw new ForbiddenException();
        }

        this.deleteRoleById(roleId);
    }

    public void deleteRoleById(Long roleId) {
        this.roleRepository.deleteById(roleId);
    }

    public Set<RoleModel> getRoles() {
        return StreamEx.of(this.roleRepository.findAll().iterator()).toSet();
    }



    public Set<RoleModel> getRoles(User requester) {

        return StreamEx.of(this.roleRepository.findAll().iterator())
            .filter(x -> this.generator.view(x.getId()).hasPermission(requester))
            .toSet();
    }

    public void addPermissionToRole(User user, Long roleId, String permission) throws RoleNotFoundException {
        if (!this.generator.addAction(roleId).hasPermission(user)) {
            throw new ForbiddenException();
        }

        if (!canUserAddPermissionToRole(user, permission)) {
            throw new ForbiddenException();
        }

        this.addPermissionToRole(roleId, permission);
    }

    public void addPermissionToRole(Long roleId, String permission) throws RoleNotFoundException {
        RoleModel model = this.findRoleById(roleId).orElseThrow(() -> new RoleNotFoundException(roleId));
        Set<GrantedAuthority> auth = StreamEx.of(model.getAuthoritySet())
                .map(x -> (GrantedAuthority)new SimpleGrantedAuthority(x.getAuthority()))
                .toSet();
        GrantedAuthority am = new SimpleGrantedAuthority(permission);
        auth.add(am);
        Set<AuthorityModel> models = StreamEx.of(auth).map(x -> AuthorityModel.builder().authority(x.getAuthority()).build()).toSet();
        model.setAuthoritySet(models);
        this.roleRepository.save(model);
    }


    private boolean canUserAddPermissionToRole(User user, String permission) {
        if (this.systemPermissionGenerator.systemAdministrator().hasPermission(user)) {
            return true;
        }

        Pattern serverPermission = Pattern.compile("^server\\.(\\d+)");
        Matcher serverMatcher = serverPermission.matcher(permission);
        if (serverMatcher.find()) {
            if (this.serverGenerator.buildRole(Long.parseLong(serverMatcher.group(1))).hasPermission(user)) {
                return true;
            }
        }

        return false;
    }

    public List<String> getUserActions(User maybeUser, Long id) {
        List<String> actions = new ArrayList<>();
        boolean allPermission = this.generator.all(id).hasPermission(maybeUser);

        if (allPermission || this.generator.addAction(id).hasPermission(maybeUser)) {
            actions.add("addAction");
        }

        if (allPermission || this.generator.removeAction(id).hasPermission(maybeUser)) {
            actions.add("removeAction");
        }

        if (allPermission || this.generator.delete(id).hasPermission(maybeUser)) {
            actions.add("delete");
        }

        if (allPermission || this.generator.addUser(id).hasPermission(maybeUser)) {
            actions.add("addUser");
        }

        if (allPermission || this.generator.removeUser(id).hasPermission(maybeUser)) {
            actions.add("removeUser");
        }
        return actions;
    }

    public void deletePermissionFromRole(User user, Long roleId, String permission) throws RoleNotFoundException {
        if (!this.generator.removeAction(roleId).hasPermission(user)) {
            throw new ForbiddenException();
        }

        this.deletePermissionFromRole(roleId, permission);
    }

    public void deletePermissionFromRole(Long roleId, String permission) throws RoleNotFoundException {
        RoleModel model = this.findRoleById(roleId).orElseThrow(() -> new RoleNotFoundException(roleId));

        Set<AuthorityModel> auth = model.getAuthoritySet();
        AuthorityModel am = StreamEx.of(auth)
                .findFirst(x -> Objects.equals(x.getAuthority(), permission))
                .orElseThrow(() -> new IllegalArgumentException(""));
        auth.remove(am);
        this.roleRepository.save(model);
    }

    public Optional<RoleModel> findRoleById(User user, Long roleId) {
        if (!this.generator.view(roleId).hasPermission(user)) {
            throw new ForbiddenException();
        }
        return this.roleRepository.findById(roleId);
    }

    public Optional<RoleModel> findRoleById(Long roleId) {
        return this.roleRepository.findById(roleId);
    }
}
