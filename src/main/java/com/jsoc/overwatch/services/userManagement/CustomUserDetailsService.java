package com.jsoc.overwatch.services.userManagement;

import com.jsoc.overwatch.services.userManagement.model.AuthorityModel;
import com.jsoc.overwatch.services.userManagement.model.UserModel;
import com.jsoc.overwatch.services.userManagement.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
@Slf4j
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private RoleAssignmentService roleAssignmentService;

    @Autowired
    private UserService userService;

    public final UserModel getUserFromId(Long id) throws UserNotFoundException {
        return this.userService.getUserFromId(id);
    }

    public final void updatePassword(String username, String plaintextPassword) {
        this.userService.updatePassword(username, plaintextPassword);
    }

    public UserModel getModelByUsername(String username) throws UsernameNotFoundException{
        return this.userService.getModelByUsername(username);
    }

    public User loadUserById(Long id) {
        try {
            UserModel um = this.userService.getUserFromId(id);
            return this.loadUserFromModel(um);
        } catch (UserNotFoundException ex) {
            throw new UsernameNotFoundException(String.format("The user id %s doesn't exist", id));
        }
    }

    public void addPermissionToUser(String username, String permission) {
        this.userService.addPermissionToUser(username, permission);
    }

    public void deletePermissionFromUser(String username, String permission) {
        this.userService.deletePermissionFromUser(username, permission);
    }

    private User loadUserFromModel(UserModel model) {
        List<SimpleGrantedAuthority> authorities = StreamEx.of(this.roleAssignmentService.getRolesForUser(model.getUserName()))
                .flatMap(x -> x.getAuthoritySet().stream())
                .map(x -> new SimpleGrantedAuthority(x.getAuthority()))
                .toList();
        Set<AuthorityModel> userAuthories = model.getUserPermissions();
        if (userAuthories == null) {
            userAuthories = new HashSet<>();
        }

        userAuthories.forEach(x -> authorities.add(new SimpleGrantedAuthority(x.getAuthority())));

        return new User(model.getUserName(), model.getPassword(), authorities);
    }

    private User loadUserProfileByUsername(String s) {
        UserModel um = this.userService.getModelByUsername(s);
        User user = this.loadUserFromModel(um);

        if (um == null) {
            throw new UsernameNotFoundException(String.format("The username %s doesn't exist", s));
        }

        List<SimpleGrantedAuthority> authorities = StreamEx.of(this.roleAssignmentService.getRolesForUser(user))
            .flatMap(x -> x.getAuthoritySet().stream())
            .map(x -> new SimpleGrantedAuthority(x.getAuthority()))
            .toList();

        Set<AuthorityModel> userAuthories = um.getUserPermissions();
        if (userAuthories == null) {
            userAuthories = new HashSet<>();
        }

        userAuthories.forEach(x -> authorities.add(new SimpleGrantedAuthority(x.getAuthority())));

        return new User(um.getUserName(), um.getPassword(), authorities);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        User user = this.loadUserProfileByUsername(s);

        return new org.springframework.security.core.userdetails.
                User(user.getUsername(), user.getPassword(), user.getAuthorities());
    }
}
