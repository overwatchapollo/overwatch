package com.jsoc.overwatch.services.userManagement.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user_table")
public class UserModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "userName", unique = true, length = 64)
    private String userName;

    @Column
    private String email;

    @Column
    private String password;

    @ElementCollection
    @CollectionTable(name="user_authorities_table", joinColumns = @JoinColumn(name = "id"))
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<AuthorityModel> userPermissions = new HashSet<>();
}
