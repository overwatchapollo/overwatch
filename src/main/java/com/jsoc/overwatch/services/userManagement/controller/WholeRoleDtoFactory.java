package com.jsoc.overwatch.services.userManagement.controller;

import com.jsoc.overwatch.Security.permissions.Permission;
import com.jsoc.overwatch.Security.permissions.ServerPermissionGenerator;
import com.jsoc.overwatch.services.serverRegister.ServerRegisterService;
import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import com.jsoc.overwatch.services.userManagement.RoleAssignmentService;
import com.jsoc.overwatch.services.userManagement.RoleService;
import com.jsoc.overwatch.services.userManagement.model.RoleModel;
import com.jsoc.overwatch.services.userManagement.model.UserModel;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.jsoc.overwatch.Security.permissions.RolePermissionGenerator.from;

@Component
public class WholeRoleDtoFactory {

    @Autowired
    private RoleService roleService;

    @Autowired
    private ServerRegisterService registerService;

    @Autowired
    private RoleAssignmentService assignmentService;

    private static final Pattern ROLE_PATTERN = Pattern.compile("role:");
    private static final Pattern ROLE_ID_WILDCARD = Pattern.compile("role:(\\*):");
    private static final Pattern ROLE_ID_PATTERN = Pattern.compile("role:(\\d+):");


    private static final Pattern SERVER_PATTERN = Pattern.compile("server:");
    private static final Pattern SERVER_ID_PATTERN = Pattern.compile("server:(\\d+):");

    public WholeRoleDto build(RoleModel model) {
        return this.build(model, this.assignmentService.getMembersOfRole(model.getId()));
    }

    public WholeRoleDto build(RoleModel model, Set<UserModel> members) {
        Set<RolePermissionDto> permissions = StreamEx.of(model.getAuthoritySet())
            .map(x -> x.getAuthority())
            .map(x -> {
                if (ROLE_PATTERN.matcher(x).find()) {
                    return this.buildRole(x);
                } else if (SERVER_PATTERN.matcher(x).find()) {
                    return this.buildServer(x);
                }

                return new Permission(x, x, x);
            })
            .map(x -> new RolePermissionDto(x))
            .toSet();
        return new WholeRoleDto(model, members, permissions);
    }

    private Permission buildRole(String x) {
        try {
            Permission permission = from(x);
            String name = permission.getName();
            Matcher idMatcher = ROLE_ID_PATTERN.matcher(x);
            if (ROLE_ID_WILDCARD.matcher(x).matches()) {
                name = name + "All roles";
            } else if (idMatcher.find()) {
                Long roleId = Long.parseLong(idMatcher.group(1));
                Optional<RoleModel> opt = this.roleService.findRoleById(roleId);
                if (opt.isPresent()) {
                    name = name + opt.get().getName();
                } else {
                    name = "Role no longer exists, safe to delete.";
                }
            }
            return new Permission(
                permission.getPermission(),
                name,
                permission.getDescription());
        } catch (Exception ex) {
            return new Permission(x, x, x);
        }
    }

    private Permission buildServer(String x) {
        try {
            Permission permission = ServerPermissionGenerator.from(x);
            String name = permission.getName();
            Matcher idMatcher = SERVER_ID_PATTERN.matcher(x);
            if (idMatcher.find()) {
                Long serverId = Long.parseLong(idMatcher.group(1));
                Optional<ServerConnection> opt = this.registerService.getConfiguration(serverId);
                if (opt.isPresent()) {
                    name = name + opt.get().getServerName();
                } else {
                    name = "Server is no longer valid, ID is safe to delete.";
                }
            }
            return new Permission(
                permission.getPermission(),
                name,
                permission.getDescription());
        } catch (Exception ex) {
            return new Permission(x, x, x);
        }
    }
}
