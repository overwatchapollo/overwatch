package com.jsoc.overwatch.services.userManagement.repository;

import com.jsoc.overwatch.services.userManagement.model.UserModel;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<UserModel, Long> {
    Optional<UserModel> findByUserName(String username);
}
