package com.jsoc.overwatch.services.userManagement.controller;

import com.jsoc.overwatch.Security.SecurityHelper;
import com.jsoc.overwatch.Security.permissions.RolePermissionGenerator;
import com.jsoc.overwatch.Security.permissions.SystemPermissionGenerator;
import com.jsoc.overwatch.controller.dto.*;
import com.jsoc.overwatch.services.userManagement.RoleAssignmentService;
import com.jsoc.overwatch.services.userManagement.RoleNotFoundException;
import com.jsoc.overwatch.services.userManagement.RoleService;
import com.jsoc.overwatch.services.userManagement.model.AuthorityModel;
import com.jsoc.overwatch.services.userManagement.model.RoleModel;
import com.jsoc.overwatch.services.userManagement.model.UserModel;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping("/api/roles")
@PreAuthorize("isAuthenticated()")
@Slf4j
public class RoleManagementController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private RoleAssignmentService assignmentService;

    @Autowired
    private WholeRoleDtoFactory roleDtoFactory;

    private RolePermissionGenerator generator = new RolePermissionGenerator();

    private SystemPermissionGenerator systemPermissionGenerator = new SystemPermissionGenerator();

    @GetMapping("/visible")
    public Set<PermissionsWrapper<WholeRoleDto>> getRolesVisible(HttpServletRequest request) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        return StreamEx.of(this.assignmentService.getRolesForUser(maybeUser))
            .map(x -> {
                PermissionsWrapper<WholeRoleDto> wrapper = new PermissionsWrapper<>();
                wrapper.setData(this.roleDtoFactory.build(x));
                wrapper.setActions(this.roleService.getUserActions(maybeUser, x.getId()));
                return wrapper;
            })
            .toSet();
    }

    @GetMapping("")
    public Set<RoleDto> getRoles(HttpServletRequest request) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        return StreamEx.of(this.roleService.getRoles(maybeUser)).map(RoleDto::new).toSet();
    }

    @PostMapping("")
    public RoleDto createRole(HttpServletRequest request, @RequestBody RoleCreationDto roleName) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.systemPermissionGenerator.systemAdministrator().hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        // TODO we will need to be able to create roles for orgs.
        RoleModel model = this.roleService.createRole(maybeUser, roleName.getRoleName(), Collections.emptySet());
        return new RoleDto(model);
    }

    @DeleteMapping("/{roleId}")
    public void deleteRole(HttpServletRequest request, @PathVariable("roleId") Long roleId) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        // TODO we will need to be able to delete roles by org.
        this.roleService.deleteRoleById(maybeUser, roleId);
    }

    @PostMapping("/{roleId}/permissions")
    public void addPermissionToRole(HttpServletRequest request, @PathVariable("roleId") Long roleId, @RequestBody PermissionDto permission) throws RoleNotFoundException {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        // TODO this will need to be allowed within an organisation.
        this.roleService.addPermissionToRole(maybeUser, roleId, permission.getPermission());
    }

    @DeleteMapping("/{roleId}/permissions")
    public void deletePermissionFromRole(HttpServletRequest request, @PathVariable("roleId") Long roleId, @RequestBody PermissionDto permission) throws RoleNotFoundException {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        // TODO this will need to be allowed within an organisation.
        this.roleService.deletePermissionFromRole(maybeUser, roleId, permission.getPermission());
    }

    @GetMapping("/{roleId}/permissions")
    public List<String> getPermissionsForRole(HttpServletRequest request, @PathVariable("roleId") Long roleId) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        // TODO We will need to open this up to an org admin.
        Optional<RoleModel> maybeModel = this.roleService.findRoleById(maybeUser, roleId);
        return maybeModel.map(x -> StreamEx.of(x.getAuthoritySet()).map(AuthorityModel::getAuthority).toList()).orElseThrow(NotFoundException::new);
    }
}
