package com.jsoc.overwatch.services.userManagement.events;

import lombok.Data;
import org.springframework.context.ApplicationEvent;
import org.springframework.security.core.userdetails.User;

@Data
public class UserRoleAssigned extends ApplicationEvent {

    private final User caller;
    private final Long userId;
    private final Long roleId;
    public UserRoleAssigned(Object source, User caller, Long userId, Long roleId) {
        super(source);
        this.caller = caller;
        this.userId = userId;
        this.roleId = roleId;
    }
}
