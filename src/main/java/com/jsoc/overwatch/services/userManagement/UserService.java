package com.jsoc.overwatch.services.userManagement;

import com.jsoc.overwatch.services.userManagement.events.StaleUserSessionEvent;
import com.jsoc.overwatch.services.userManagement.model.AuthorityModel;
import com.jsoc.overwatch.services.userManagement.model.UserModel;
import com.jsoc.overwatch.services.userManagement.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@Slf4j
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private ApplicationEventPublisher publisher;

    public final UserModel createUser(String username, String email, String plaintextPassword) throws DuplicateUsernameException {
        try {
            String encodedPassword = this.encoder.encode(plaintextPassword);
            UserModel model = UserModel.builder().userName(username).email(email).password(encodedPassword).build();
            return this.userRepository.save(model);
        } catch (DataIntegrityViolationException ex) {
            throw new DuplicateUsernameException(username);
        }
    }

    public final UserModel getUserFromId(Long id) throws UserNotFoundException {
        return this.userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    public UserModel getModelByUsername(String username) throws UsernameNotFoundException {
        UserModel um = this.userRepository.findByUserName(username).orElse(null);

        if (um == null) {
            throw new UsernameNotFoundException(String.format("The username %s doesn't exist", username));
        }

        return um;
    }

    public void removeUserById(Long id) {
        this.userRepository.deleteById(id);
    }

    public final void updatePassword(String username, String plaintextPassword) {
        String encodedPassword = this.encoder.encode(plaintextPassword);
        UserModel um = this.userRepository.findByUserName(username).orElse(null);
        if (um == null) {
            log.error("Failed to get user from database {}", username);
        } else {
            um.setPassword(encodedPassword);
            this.userRepository.save(um);
        }
    }

    public final void updateEmail(String username, String email) {
        UserModel um = this.userRepository.findByUserName(username).orElse(null);
        if (um == null) {
            log.error("Failed to get user from database {}", username);
        } else {
            um.setEmail(email);
            this.userRepository.save(um);
        }
    }

    public void addPermissionToUser(String username, String permission) {
        UserModel um = this.userRepository.findByUserName(username).orElse(null);

        if (um == null) {
            throw new UsernameNotFoundException(String.format("The user %s doesn't exist", username));
        }

        Set<AuthorityModel> auths = um.getUserPermissions();
        if (auths == null) {
            auths = new HashSet<>();
        }

        Optional<AuthorityModel> maybeAuth = StreamEx.of(auths).findFirst(x -> Objects.equals(x.getAuthority(), permission));
        if (maybeAuth.isEmpty()) {
            auths.add(AuthorityModel.builder().authority(permission).build());
            this.userRepository.save(um);
        }

        this.publisher.publishEvent(new StaleUserSessionEvent(this, username));
    }

    public void deletePermissionFromUser(String username, String permission) {
        UserModel um = this.userRepository.findByUserName(username).orElse(null);

        if (um == null) {
            throw new UsernameNotFoundException(String.format("The user %s doesn't exist", username));
        }

        Set<AuthorityModel> auths = um.getUserPermissions();
        if (auths == null) {
            auths = new HashSet<>();
        }

        Set<AuthorityModel> safeAuths = auths;
        Optional<AuthorityModel> maybeAuth = StreamEx.of(safeAuths).findFirst(x -> Objects.equals(x.getAuthority(), permission));
        maybeAuth.ifPresent(x -> {
            safeAuths.remove(x);
            this.userRepository.save(um);
        });

        this.publisher.publishEvent(new StaleUserSessionEvent(this, username));
    }

    public List<UserModel> getUsers() {
        return StreamEx.of(this.userRepository.findAll().iterator()).toList();
    }

    public List<String> getUsernames() {
        return StreamEx.of(this.userRepository.findAll().iterator()).map(UserModel::getUserName).toList();
    }
}
