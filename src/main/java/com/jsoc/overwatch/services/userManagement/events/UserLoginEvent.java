package com.jsoc.overwatch.services.userManagement.events;

import lombok.Data;
import org.springframework.context.ApplicationEvent;
import org.springframework.security.core.userdetails.User;

@Data
public class UserLoginEvent extends ApplicationEvent {
    private final User user;
    private final String username;
    public UserLoginEvent(Object source, String username, User user) {
        super(source);
        this.user = user;
        this.username = username;
    }
}
