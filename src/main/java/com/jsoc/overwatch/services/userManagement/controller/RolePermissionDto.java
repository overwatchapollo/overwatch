package com.jsoc.overwatch.services.userManagement.controller;

import com.jsoc.overwatch.Security.permissions.Permission;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class RolePermissionDto {
    private String permission;
    private String name;
    private String description;

    public RolePermissionDto(Permission role) {
        this.permission = role.getPermission();
        this.name = role.getName();
        this.description = role.getDescription();
    }

    public boolean equals(RolePermissionDto dto) {
        return this.permission.compareTo(dto.getPermission()) == 0;
    }
}
