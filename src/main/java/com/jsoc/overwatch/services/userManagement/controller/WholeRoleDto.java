package com.jsoc.overwatch.services.userManagement.controller;

import com.jsoc.overwatch.services.userManagement.model.RoleModel;
import com.jsoc.overwatch.services.userManagement.model.UserModel;
import lombok.Data;

import java.util.Set;

@Data
public class WholeRoleDto {
    private Long id;
    private String name;
    private Set<RolePermissionDto> permissions;
    private Set<UserModel> members;
    public WholeRoleDto(RoleModel model, Set<UserModel> members, Set<RolePermissionDto> permissions) {
        this.id = model.getId();
        this.name = model.getName();
        this.permissions = permissions;
        this.members = members;
    }
}
