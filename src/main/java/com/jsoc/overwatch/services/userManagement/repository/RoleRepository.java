package com.jsoc.overwatch.services.userManagement.repository;

import com.jsoc.overwatch.services.userManagement.model.RoleModel;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository for role storage.
 */
public interface RoleRepository extends CrudRepository<RoleModel, Long> {
}
