package com.jsoc.overwatch.services.userManagement;

import com.jsoc.overwatch.services.userManagement.events.StaleUserSessionEvent;
import com.jsoc.overwatch.services.userManagement.model.RoleModel;
import com.jsoc.overwatch.services.userManagement.model.UserModel;
import com.jsoc.overwatch.services.userManagement.model.UserRoleMap;
import com.jsoc.overwatch.services.userManagement.repository.UserRepository;
import com.jsoc.overwatch.services.userManagement.repository.UserRoleMapRepository;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RoleAssignmentService {
    @Autowired
    private UserRoleMapRepository userRoleMap;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleService roleService;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private CustomUserDetailsService detailsService;

    public void mapUserToRole(User user, Long role) {
        Optional<UserModel> um = this.userRepository.findByUserName(user.getUsername());
        um.ifPresent(userModel -> {
            Set<RoleModel> roles = this.getRolesForUser(user);
            Optional<RoleModel> duplicate = StreamEx.of(roles).findFirst(x -> Objects.equals(x.getId(), role));
            if (duplicate.isEmpty()) {
                this.userRoleMap.save(UserRoleMap.builder().roleId(role).userId(userModel.getId()).build());
                this.publisher.publishEvent(new StaleUserSessionEvent(this, userModel.getUserName()));
            }
        });
    }

    public void mapUserToRole(UserModel user, Long role) {
        List<RoleModel> roles = this.getRolesForUser(user.getUserName());
        Optional<RoleModel> duplicate = StreamEx.of(roles).findFirst(x -> Objects.equals(x.getId(), role));
        if (duplicate.isEmpty()) {
            this.userRoleMap.save(UserRoleMap.builder().roleId(role).userId(user.getId()).build());
            this.publisher.publishEvent(new StaleUserSessionEvent(this, user.getUserName()));
        }
    }

    public void setRolesForUser(String username, List<String> roles) {

        UserModel um = userRepository.findByUserName(username).get();
        User user = this.detailsService.loadUserById(um.getId());

        List<UserRoleMap> roleMap = StreamEx.of(this.userRoleMap.getByUserId(um.getId())).toList();
        for(UserRoleMap roleMapping : roleMap) {
            this.userRoleMap.delete(roleMapping);
        }

        List<RoleModel> rolesToAdd = StreamEx.of(this.roleService.getRoles())
            .filter(x -> roles.contains(x.getName()))
            .toList();

        rolesToAdd.forEach(x -> this.mapUserToRole(user, x.getId()));
        this.publisher.publishEvent(new StaleUserSessionEvent(this, um.getUserName()));
    }

    public Set<UserModel> getMembersOfRole(Long roleId) {
        return this.userRoleMap.getByRoleId(roleId).stream().map(x -> {
            try {
                UserModel model = this.detailsService.getUserFromId(x.getUserId());
                model.setPassword(null);
                model.setUserPermissions(new HashSet<>());
                return model;
            } catch (Exception ex) {
                log.error("error on user {}: {}", x, ex.getLocalizedMessage());
                return null;
            }
        }).filter(x -> x != null)
        .collect(Collectors.toSet());
    }

    public List<RoleModel> getRolesForUser(String username) {
        UserModel um = userRepository.findByUserName(username).get();
        Collection<UserRoleMap> map = this.userRoleMap.getByUserId(um.getId());
        return map.stream().map(x -> this.roleService.findRoleById(x.getRoleId())).filter(x -> x.isPresent()).map(x -> x.get()).collect(Collectors.toList());
    }

    public Set<RoleModel> getRolesForUser(User user) {
        return new HashSet<>(this.getRolesForUser(user.getUsername()));
    }

    public void deleteUserToRoleMap(Long user, Long role) {
        Collection<UserRoleMap> roles = this.userRoleMap.getByUserId(user);
        log.info("Found {} roles", roles.size());
        UserRoleMap urm = StreamEx.of(roles).peekFirst(x -> log.info("Role {}", x.getRoleId())).findFirst(x -> Objects.equals(x.getRoleId(), role)).orElse(null);
        if (urm != null) {
            this.userRoleMap.delete(urm);
            Optional<UserModel> um = this.userRepository.findById(user);
            um.ifPresent(userModel ->
                    this.publisher.publishEvent(new StaleUserSessionEvent(this, userModel.getUserName()))
            );


        } else {
            log.info("Could not find role {} for user {}", role, user);
        }
    }

    public List<RoleModel> getRoleModelsForUserId(Long userId) {
        List<UserRoleMap> rolemap = StreamEx.of(this.userRoleMap.getByUserId(userId)).toList();

        return StreamEx.of(rolemap)
                .map(x -> this.roleService.findRoleById(x.getRoleId()))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();
    }
}
