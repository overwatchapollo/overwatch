package com.jsoc.overwatch.services.userManagement;

import lombok.Data;

@Data
public class UserNotFoundException extends Exception {
    private Long id;
    public UserNotFoundException(Long id) {
        this.id = id;
    }
}
