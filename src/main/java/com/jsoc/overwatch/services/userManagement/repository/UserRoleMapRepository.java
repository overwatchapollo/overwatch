package com.jsoc.overwatch.services.userManagement.repository;

import com.jsoc.overwatch.services.userManagement.model.UserRoleMap;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface UserRoleMapRepository extends CrudRepository<UserRoleMap, Long> {
    Collection<UserRoleMap> getByRoleId(Long id);
    Collection<UserRoleMap> getByUserId(Long id);
}
