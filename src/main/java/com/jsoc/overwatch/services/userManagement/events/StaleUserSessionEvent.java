package com.jsoc.overwatch.services.userManagement.events;

import lombok.Data;
import org.springframework.context.ApplicationEvent;

@Data
public class StaleUserSessionEvent extends ApplicationEvent {
    private String username;
    public StaleUserSessionEvent(Object source, String username) {
        super(source);
        this.username = username;
    }
}
