package com.jsoc.overwatch.services.userManagement;

import lombok.Data;

@Data
public class RoleNotFoundException extends Exception {
    /**
     * ID of role which was not found.
     */
    private Long id;

    public RoleNotFoundException(Long id) {
        this.id = id;
    }

}
