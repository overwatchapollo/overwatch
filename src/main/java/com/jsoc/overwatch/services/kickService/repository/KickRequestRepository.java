package com.jsoc.overwatch.services.kickService.repository;

import com.jsoc.overwatch.services.kickService.model.KickRequestRecord;
import org.springframework.data.repository.CrudRepository;

public interface KickRequestRepository extends CrudRepository<KickRequestRecord, String> {
}
