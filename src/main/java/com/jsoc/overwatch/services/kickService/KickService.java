package com.jsoc.overwatch.services.kickService;

import com.jsoc.overwatch.services.playerList.events.PlayerChatEvent;
import com.jsoc.overwatch.services.playerList.events.PlayerChatKick;
import com.jsoc.overwatch.services.connection.CommandService;
import com.jsoc.overwatch.services.connection.api.Command;
import com.jsoc.overwatch.services.connection.api.CommandFactory;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import com.jsoc.overwatch.services.kickService.events.KickRequestEvent;
import com.jsoc.overwatch.services.kickService.events.PlayerKickedEvent;
import com.jsoc.overwatch.services.kickService.events.PlayerWasKickedEvent;
import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import com.jsoc.overwatch.services.playerList.ConnectedPlayerService;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 * Service to mediate the kicking of players from the server.
 */
@Service
@Slf4j
@NoArgsConstructor
public class KickService {

    @Autowired
    private ConnectedPlayerService playerList;

    private CommandFactory commandFactory = new CommandFactory();

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private CommandService commandService;

    public KickService(ConnectedPlayerService playerList, ApplicationEventPublisher publisher, CommandService commandService) {
        this.playerList = playerList;
        this.publisher = publisher;
        this.commandService = commandService;
    }

    @EventListener
    public void processKickEvent(KickRequestEvent kickRequestEvent) {
        Long server = kickRequestEvent.getServerId();
        String session = kickRequestEvent.getSessionId();
        Optional<ConnectedPlayer> cp = this.playerList.getPlayerFromSession(server, session);
        cp.ifPresentOrElse(x -> {
            log.info("kick request going in");
            this.doKick(kickRequestEvent.getServerId(), kickRequestEvent.getSessionId(), kickRequestEvent.getMessage());
            this.publisher.publishEvent(new PlayerWasKickedEvent(this, server, session, kickRequestEvent.getRequesterId(), kickRequestEvent.getMessage()));
        }, () -> {
           log.error("player {} not found on server {}", kickRequestEvent.getSessionId(), kickRequestEvent.getServerId());
        });
    }

    public CompletableFuture<CommandResponse> kickPlayer(Long requesterId, Long serverId, Long sessionId, String message) {
        Command command = this.commandFactory.kickId(sessionId.toString(), message);
        CompletableFuture<CommandResponse> response = this.commandService.runCommand(serverId, command);
        response.thenAccept(x -> {
            if (x.isSuccess()) {
                this.publisher.publishEvent(new PlayerWasKickedEvent(this, serverId, sessionId.toString(), requesterId, message));
            }
        });
        return response;
    }

    @EventListener
    public void processPlayerChatKick(PlayerChatKick event) {
        log.info("in game kick {}", event);
        PlayerChatEvent chatEvent = event.getChatEvent();
        ConnectedPlayer cp = chatEvent.getConnectedPlayer();

        log.info("cp server {} session {} msg {}", cp.getServerId(), cp.getSessionId(), cp.getDisconnectionMessage());
        Optional<ConnectedPlayer> maybePlayer = this.playerList.getPlayerFromSession(cp.getServerId(), cp.getSessionId());

        maybePlayer.ifPresent(x -> {
            this.publisher.publishEvent(new PlayerKickedEvent(this, cp.getServerId(), cp.getGuid(), null, cp.getDisconnectionMessage()));
        });
    }

    private CompletableFuture<CommandResponse> doKick(Long server, String session, String message) {
        Command command = this.commandFactory.kickId(session, message);
        return this.commandService.runCommand(server, command);
    }
}
