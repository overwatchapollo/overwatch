package com.jsoc.overwatch.services.kickService.events;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;
import org.springframework.security.core.userdetails.User;

@Getter
public class PlayerKickedEvent extends ApplicationEvent {
    private final Long serverId;
    private final String playerGuid;
    private final Long requester;
    private final String message;
    public PlayerKickedEvent(Object source, Long serverId, String playerGuid, Long requester, String message) {
        super(source);
        this.serverId = serverId;
        this.playerGuid = playerGuid;
        this.requester = requester;
        this.message = message;
    }
}
