package com.jsoc.overwatch.services.kickService.events;

import lombok.Data;
import org.springframework.context.ApplicationEvent;

@Data
public class KickRequestEvent extends ApplicationEvent {
    private final Long serverId;
    private final String sessionId;
    private final Long requesterId;
    private final String message;
    public KickRequestEvent(Object source, Long serverId, String sessionId, Long requesterId, String message) {
        super(source);
        this.serverId = serverId;
        this.sessionId = sessionId;
        this.requesterId = requesterId;
        this.message = message;
    }
}
