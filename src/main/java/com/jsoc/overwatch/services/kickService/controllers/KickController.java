package com.jsoc.overwatch.services.kickService.controllers;

import com.jsoc.overwatch.Security.SecurityHelper;
import com.jsoc.overwatch.Security.permissions.ServerPermissionGenerator;
import com.jsoc.overwatch.controller.dto.*;
import com.jsoc.overwatch.services.kickService.KickService;
import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import com.jsoc.overwatch.services.playerReport.PlayerReportService;
import com.jsoc.overwatch.services.kickService.events.KickRequestEvent;
import com.jsoc.overwatch.services.kickService.model.KickRequestRecord;
import com.jsoc.overwatch.services.serverRegister.ServerRegisterService;
import com.jsoc.overwatch.services.userManagement.UserService;
import com.jsoc.overwatch.services.userManagement.model.UserModel;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/rcon")
@PreAuthorize("isAuthenticated()")
@Slf4j
public class KickController {

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private UserService userService;

    @Autowired
    private PlayerReportService reportService;

    @Autowired
    private ServerRegisterService registerService;

    @Autowired
    private KickService kickService;

    private ServerPermissionGenerator generator = new ServerPermissionGenerator();

    @PostMapping("/kick")
    public Long kickPlayer(HttpServletRequest request, @RequestBody KickRequest kickRequest) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.kick(kickRequest.getServerLocation()).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        UserModel um = this.userService.getModelByUsername(maybeUser.getUsername());

        log.info("kick request authorised");
        this.publisher.publishEvent(new KickRequestEvent(this, kickRequest.getServerLocation(), kickRequest.getSessionId(), um.getId(), kickRequest.getMessage()));
        return 0L;
    }

    @PostMapping("/v1/kick")
    public void kickPlayerSync(HttpServletRequest request, @RequestBody KickRequest kickRequest) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.kick(kickRequest.getServerLocation()).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        UserModel um = this.userService.getModelByUsername(maybeUser.getUsername());

        log.info("kick request authorised");
        CompletableFuture<?> resp = this.kickService.kickPlayer(um.getId(), kickRequest.getServerLocation(), Long.parseLong(kickRequest.getSessionId()), kickRequest.getMessage());
        try {
            resp.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new ServerError();
        }
    }


    @GetMapping("/kickHistory")
    public List<KickRequestAuditDto> getKickRequests(@RequestBody String[] servers) {
        // TODO apply permissions
        return StreamEx.of(this.reportService.getKickRequests(servers))
                .map(this::transformKickRequest)
                .toList();
    }

    @GetMapping("/kickHistory/{serverMrid}")
    public List<KickRequestAuditDto> getKickRequests(@PathVariable("serverMrid") String server) {
        // TODO apply permissions
        return StreamEx.of(this.reportService.getKickRequests(server))
                .map(this::transformKickRequest)
                .toList();
    }

    private KickRequestAuditDto transformKickRequest(KickRequestRecord record) {
        return KickRequestAuditDto.builder()
                .requester(record.getRequester())
                .serverName(this.registerService.getConfiguration(record.getServerLocation()).map(ServerConnection::getServerName).orElse("unknown"))
                .message(record.getMessage())
                .timeStamp(record.getTimeStamp())
                .successful(record.getSuccessful())
                .build();
    }
}
