package com.jsoc.overwatch.services.kickService.events;

import lombok.Data;
import org.springframework.context.ApplicationEvent;

@Data
public class PlayerWasKickedInGameEvent extends ApplicationEvent {
    private Long serverId;
    private String sessionId;
    private String requesterSessionId;
    private String message;
    public PlayerWasKickedInGameEvent(Object source, Long serverId, String sessionId, String requesterSessionId, String message) {
        super(source);
        this.serverId = serverId;
        this.sessionId = sessionId;
        this.requesterSessionId = requesterSessionId;
        this.message = message;
    }
}
