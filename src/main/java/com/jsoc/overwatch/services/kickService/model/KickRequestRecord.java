package com.jsoc.overwatch.services.kickService.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.jsoc.overwatch.services.playerReport.model.PlayerRecord;
import lombok.*;

import javax.persistence.*;
import java.time.Instant;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table()
public class KickRequestRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String requester;
    private String ipAddress;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JsonBackReference
    @ToString.Exclude
    private PlayerRecord player;

    private Long serverLocation;
    private String message;
    private Instant timeStamp;
    private Boolean successful = false;
}
