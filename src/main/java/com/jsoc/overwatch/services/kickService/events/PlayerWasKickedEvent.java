package com.jsoc.overwatch.services.kickService.events;

import lombok.Data;
import org.springframework.context.ApplicationEvent;

/**
 * Event to raise when a player was kicked from a server.
 */
@Data
public class PlayerWasKickedEvent extends ApplicationEvent {
    private Long serverId;
    private String sessionId;
    private Long requesterId;
    private String message;
    public PlayerWasKickedEvent(Object source, Long serverId, String sessionId, Long requesterId, String message) {
        super(source);
        this.serverId = serverId;
        this.sessionId = sessionId;
        this.requesterId = requesterId;
        this.message = message;
    }
}
