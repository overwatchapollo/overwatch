package com.jsoc.overwatch.controller;

import com.jsoc.overwatch.Security.SecurityHelper;
import com.jsoc.overwatch.Security.permissions.Permission;
import com.jsoc.overwatch.Security.permissions.ServerPermissionGenerator;
import com.jsoc.overwatch.services.userManagement.DuplicateUsernameException;
import com.jsoc.overwatch.services.userManagement.RoleAssignmentService;
import com.jsoc.overwatch.services.userManagement.UserService;
import com.jsoc.overwatch.services.userManagement.events.UserLoginEvent;
import com.jsoc.overwatch.controller.dto.*;
import com.jsoc.overwatch.services.userManagement.model.RoleModel;
import com.jsoc.overwatch.services.userManagement.model.UserModel;
import com.jsoc.overwatch.services.userManagement.CustomUserDetailsService;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

@RestController
@RequestMapping("/api/session")
@PreAuthorize("isFullyAuthenticated()")
@Slf4j
public class SessionController {

    @Autowired
    private SessionRegistry registry;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private CustomUserDetailsService details;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private RoleAssignmentService roleAssignmentService;

    @Autowired
    private UserService userService;

    private Counter loginCounter;

    private ServerPermissionGenerator generator = new ServerPermissionGenerator();

    public SessionController(MeterRegistry registry) {
        this.loginCounter = registry.counter("overwatch.session", "login", "0");
    }

    @PreAuthorize("permitAll")
    @PostMapping("/v1/register")
    public UserModel register(HttpServletRequest request, @RequestBody RegisterDto dto) throws DuplicateUsernameException {
        checkNotNull(dto.getUsername());
        checkNotNull(dto.getPassword());
        //checkNotNull(dto.getEmail());

        UserModel model = this.userService.createUser(dto.getUsername(), dto.getEmail(), dto.getPassword());
        LoginDto login = new LoginDto();
        login.setUsername(dto.getUsername());
        login.setPassword(dto.getPassword());
        this.login(request, login);
        return model;
    }

    @PreAuthorize("permitAll")
    @GetMapping("/v1/me")
    public ResponseEntity<?> getCurrentUserV1(HttpServletRequest request) {
        Optional<User> user = SecurityHelper.getUserFromRequest();
        if (user.isPresent()) {
            UserModel um = this.userService.getModelByUsername(user.get().getUsername());
            List<String> roles = StreamEx.of(this.roleAssignmentService.getRolesForUser(user.get()))
                .map(RoleModel::getName)
                .toList();
            UserDto dto = UserDto.builder().id(um.getId()).username(um.getUserName()).email(um.getEmail()).roleNames(roles).build();
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }

        throw new NotFoundException();
    }

    @PreAuthorize("permitAll")
    @GetMapping("/v2/me")
    public RestResponse<UserDto> getCurrentUserV2(HttpServletRequest request) {
        Optional<User> user = SecurityHelper.getUserFromRequest();
        RestResponse<UserDto> resp = new RestResponse<>();
        if (user.isPresent()) {
            UserModel um = this.userService.getModelByUsername(user.get().getUsername());
            List<String> roles = StreamEx.of(this.roleAssignmentService.getRolesForUser(user.get()))
                    .map(RoleModel::getName)
                    .toList();
            UserDto dto = UserDto.builder().id(um.getId()).username(um.getUserName()).email(um.getEmail()).roleNames(roles).build();
            resp.setData(dto);
        } else {
            resp.setError("You are not logged in");
        }

        return resp;
    }


    @PreAuthorize("permitAll")
    @PostMapping("/v1/login")
    @Timed()
    public ResponseEntity<?> login(HttpServletRequest request, @RequestBody LoginDto dto) {
        this.doLogin(request, dto);
        return ResponseEntity.ok(this.getCurrentUserV2(request).getData());
    }

    @PreAuthorize("permitAll")
    @PostMapping("/v2/login")
    @Timed()
    public RestResponse<UserDto> loginV2(HttpServletRequest request, HttpServletResponse response, @RequestBody LoginDto dto) {
        log.info("logging in");
        try {
            this.doLogin(request, dto);
            log.info("login success");
        } catch (Exception ex) {
            log.info("failure to login {}", ex.getLocalizedMessage());
            RestResponse<UserDto> fail = new RestResponse<>();
            fail.setError("Failed to authenticate.");
            response.setStatus(403);
            return fail;
        }


        return this.getCurrentUserV2(request);
    }

    private void doLogin(HttpServletRequest request, @RequestBody LoginDto dto) {
        Authentication auth = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
        Authentication result = this.authenticationManager.authenticate(auth);

        if (result.isAuthenticated()) {
            SecurityContext sc = SecurityContextHolder.getContext();
            sc.setAuthentication(result);
            HttpSession session = request.getSession(true);
            log.info("login principle {}", result.getPrincipal());
            log.info("session {}", session);
            this.registry.registerNewSession(session.getId(), result.getPrincipal());
            session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, sc);

            UserDetails details = this.details.loadUserByUsername(dto.getUsername());
            User user =  new User(details.getUsername(), details.getPassword(), details.getAuthorities());

            this.publisher.publishEvent(new UserLoginEvent(this, dto.getUsername(), user));
            log.info("login counter increment");
            this.loginCounter.increment();
            log.info("login now " + this.loginCounter.count());
        } else {
            log.info("auth failed");
            throw new UnauthorisedException();
        }
    }

    @PreAuthorize("permitAll")
    @PostMapping("/v1/logout")
    public void logout(HttpServletRequest request) {
        Authentication auth = SecurityHelper.getAuthenticationFromRequest();
        auth.setAuthenticated(false);
        SecurityContextHolder.clearContext();
        HttpSession session = SecurityHelper.getSessionFromRequest(request);
        if (session != null) {
            session.invalidate();
            for (Cookie cookie : request.getCookies()) {
                cookie.setMaxAge(0);
            }
        }
    }

    @GetMapping("/v1/getUser/{name}")
    public User getUser(@PathVariable("name") String username) {
        UserDetails details = this.details.loadUserByUsername(username);
        return new User(details.getUsername(), details.getPassword(), details.getAuthorities());
    }

    @PreAuthorize("permitAll")
    @GetMapping("/v1/pages")
    public List<String> getViewablePages(HttpServletRequest request) {
        List<String> pages = new ArrayList<>();
        Optional<User> user = SecurityHelper.getUserFromRequest();
        if (user.isPresent()) {
            Collection<GrantedAuthority> auths = user.get().getAuthorities();
            if (this.authFound(auths, this.generator.view())) { pages.add("status"); }
            if (this.authFound(auths, "^registerServer")) { pages.add("serverAdmin"); }
            if (this.authFound(auths, "^siteAdmin")) { pages.add("siteAdmin"); }
            if (this.authFound(auths, "^investigate")) { pages.add("investigate"); }
            if (this.authFound(auths, "^userManager")) { pages.add("userManager"); }
            if (this.authFound(auths, "^experimental$")) { pages.add("experimental"); }
        }

        return pages;
    }

    @GetMapping("/v1/auths")
    public List<String> getAuths() {
        return StreamEx.of(SecurityContextHolder.getContext().getAuthentication().getAuthorities())
            .map(GrantedAuthority::getAuthority)
            .toList();
    }

    private boolean authFound(Collection<GrantedAuthority> auths, String pattern) {
        Pattern p = Pattern.compile(pattern);
        return StreamEx.of(auths).findFirst(x -> p.matcher(x.getAuthority()).find()).isPresent();
    }

    private boolean authFound(Collection<GrantedAuthority> auths, Permission pattern) {
        Pattern p = Pattern.compile(pattern.getPermission());
        return StreamEx.of(auths).findFirst(x -> p.matcher(x.getAuthority()).find()).isPresent();
    }

    @GetMapping("/v1/loggedIn")
    public Boolean isloggedIn(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return false;
        }

        log.info("Session {}", session.getId());
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object username = auth.getPrincipal();
        log.info("logged in: {}", auth.isAuthenticated());
        log.info("user: {}", username);
        log.info("type: {}", auth.getClass());
        return true;
    }
}
