package com.jsoc.overwatch.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

/**
 * Api Error DTO. Encapsulates why a REST command failed.
 */
@AllArgsConstructor
@Getter
@Setter
@ToString
public class ApiError {
    /**
     * HTTP status code being returned.
     */
    private HttpStatus status;

    /**
     * Human readable error message.
     */
    private String message;

    /**
     * List of all errors contributing to failure.
     */
    private List<String> errors;

    /**
     * Build an instance of the {@link ApiError} class with a single error.
     * @param status Status code of failure.
     * @param message Human readable message of failure.
     * @param error Detailed error message.
     */
    public ApiError(HttpStatus status, String message, String error) {
        this(status, message, Arrays.asList(error));
    }
}
