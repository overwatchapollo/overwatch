/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.controller;

import com.jsoc.overwatch.Security.SecurityHelper;
import com.jsoc.overwatch.Security.permissions.ServerPermissionGenerator;
import com.jsoc.overwatch.Security.permissions.SystemPermissionGenerator;
import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import com.jsoc.overwatch.services.connection.api.CommandResponse;
import com.jsoc.overwatch.services.connection.games.arma.IRconConnection;
import com.jsoc.overwatch.services.connection.RconConnectionPool;
import com.jsoc.overwatch.controller.dto.ForbiddenException;
import com.jsoc.overwatch.controller.dto.NotFoundException;
import com.jsoc.overwatch.controller.dto.UnauthorisedException;
import com.jsoc.overwatch.services.connection.api.Command;
import com.jsoc.overwatch.services.connection.api.CommandFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.CompletableFuture;

/**
 * Status controller.
 */
@RestController
@RequestMapping("/api/status")
@PreAuthorize("isAuthenticated()")
public class StatusController {
    private final CommandFactory commandFactory = new CommandFactory();

    @Autowired
    private RconConnectionPool pool;

    private ServerPermissionGenerator serverPermissionGenerator = new ServerPermissionGenerator();
    private SystemPermissionGenerator generator = new SystemPermissionGenerator();

    @GetMapping("/rcon/active/{serverMrid}")
    public Boolean isRconActive(@PathVariable("serverMrid") Long serverMrid) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.serverPermissionGenerator.view(serverMrid).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        return this.pool.getConnection(serverMrid).isPresent();
    }
    
    @GetMapping("/rcon/loggedin/{serverMrid}")
    public Boolean isRconLoggedIn(@PathVariable("serverMrid") Long serverMrid) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.serverPermissionGenerator.view(serverMrid).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        return this.pool.getConnection(serverMrid)
            .map(x -> x.getStatus() == ServerConnectionStatus.CONNECTED)
            .orElse(false);
    }
    
    @GetMapping("/rcon/version/{serverMrid}")
    public String getRconVersion(@PathVariable("serverMrid") Long serverMrid) throws Exception {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.serverPermissionGenerator.view(serverMrid).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        Command command = this.commandFactory.version();
        IRconConnection con = this.pool.getConnection(serverMrid).orElseThrow(IllegalArgumentException::new);
        CompletableFuture<CommandResponse> future = con.sendCommand(command);
        CommandResponse cr = future.get();
        String version = cr.getResponse();
        return version.substring(1);
    }

    @PostMapping("/disconnect/{serverMrid}")
    public void disconnect(HttpServletRequest request, @PathVariable("serverMrid") Long serverMrid) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.experimental().hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        IRconConnection con = this.pool.getConnection(serverMrid).orElseThrow(NotFoundException::new);
        con.disconnect();
    }

    @PostMapping("/connect/{serverMrid}")
    public void connect(HttpServletRequest request, @PathVariable("serverMrid") Long serverMrid) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.experimental().hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        IRconConnection con = this.pool.getConnection(serverMrid).orElseThrow(NotFoundException::new);
        if (con.getStatus() != ServerConnectionStatus.CONNECTED) {
            con.connect();
        }
    }
}
