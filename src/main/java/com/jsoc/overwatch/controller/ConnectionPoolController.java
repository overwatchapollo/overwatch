package com.jsoc.overwatch.controller;

import com.jsoc.overwatch.Security.SecurityHelper;
import com.jsoc.overwatch.Security.permissions.Permission;
import com.jsoc.overwatch.Security.permissions.ServerPermissionGenerator;
import com.jsoc.overwatch.controller.dto.*;
import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import com.jsoc.overwatch.services.serverRegister.ServerRegisterService;
import com.jsoc.overwatch.services.userManagement.CustomUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping("/api/server")
@PreAuthorize("isAuthenticated()")
@Slf4j
public class ConnectionPoolController {

    @Autowired
    private CustomUserDetailsService detailsService;

    @Autowired
    private ServerRegisterService registerService;

    private ServerPermissionGenerator generator = new ServerPermissionGenerator();

    /**
     * Register and connect a server.
     * @param request Servlet request.
     * @param con Server connection uploaded from client.
     * @return Server connection details.
     */
    @PostMapping("/v1/connection")
    public ServerConnectionDto setupServer(HttpServletRequest request, @RequestBody ServerRegistrationDto con) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        log.info("Registering server with ip {}", con.getHostName());
        ServerConnection register = ServerConnection.builder()
            .ipAddress(con.getHostName())
            .port(con.getPort())
            .mock(false)
            .password(con.getPassword())
            .serverName(con.getServerName())
            .maxPlayers(con.getPlayers())
            .build();
        ServerConnection newConnection = this.registerService.registerServer(register);

        this.getServerRegistrationPermissions(newConnection.getId())
            .forEach(x -> this.detailsService.addPermissionToUser(maybeUser.getUsername(), x.getPermission()));
        return new ServerConnectionDto(newConnection);
    }

    private List<Permission> getServerRegistrationPermissions(Long serverId) {
        List<Permission> permissions = new ArrayList<>();
        permissions.add(this.generator.assignPermissions(serverId));
        permissions.add(this.generator.view(serverId));
        permissions.add(this.generator.kick(serverId));
        permissions.add(this.generator.ban(serverId));
        permissions.add(this.generator.banPrivileged(serverId));
        permissions.add(this.generator.update(serverId));
        permissions.add(this.generator.unregister(serverId));
        permissions.add(this.generator.restart(serverId));
        permissions.add(this.generator.banRemove(serverId));
        permissions.add(this.generator.whisper(serverId));
        permissions.add(this.generator.broadcast(serverId));
        permissions.add(this.generator.owner(serverId));
        permissions.add(this.generator.rcon(serverId));
        permissions.add(this.generator.buildRole(serverId));
        return permissions;
    }


    /**
     * Get servers which you are able to see.
     * @param request HTTP request, injected.
     * @return A set of server connections.
     */
    @GetMapping("/v1/connection")
    public Set<ServerConnectionDto> getServers(HttpServletRequest request) {
        User user = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);

        List<ServerConnection> connections = this.registerService.getServers();
        Set<ServerConnectionDto> authedServers = new HashSet<>();

        connections.forEach(x -> {
            x.setPassword(null);
            if (this.generator.view(x.getId()).hasPermission(user)) {
                authedServers.add(new ServerConnectionDto(x));
            }
        });

        return authedServers;
    }

    /**
     * Get the details of a specific server.
     * @param request HTTP servlet request.
     * @param id Server mRID.
     * @return Server connection parameters.
     */
    @GetMapping("/v1/connection/{id}")
    public ServerConnectionDto getServer(HttpServletRequest request, @PathVariable("id") Long id) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.view(id).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }
        return this.registerService.getConfiguration(id)
            .map(ServerConnectionDto::new)
            .orElseThrow(IllegalArgumentException::new);
    }

    @PutMapping("/v1/connection/{id}")
    public void updateServer(HttpServletRequest request, @PathVariable("id") Long id, @RequestBody ServerConfigUpdateDto dto) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.update(id).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }
        ServerConnectionDto inputDto = ServerConnectionDto.builder()
            .serverId(dto.getServerId())
            .ipAddress(dto.getHostName())
            .port(dto.getPort())
            .mock(false)
            .password(dto.getPassword())
            .serverName(dto.getHostName())
            .maxPlayers(dto.getPlayers())
            .build();
        this.registerService.modifyServer(id, inputDto);
    }

    /**
     * Delete the details of a specific server.
     * @param id Server mRID.
     */
    @DeleteMapping("/v1/connection/{id}")
    public void removeServer(HttpServletRequest request, @PathVariable("id") Long id) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.unregister(id).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }
        this.registerService.unregisterServer(id);

        this.getServerRegistrationPermissions(id)
            .forEach(x -> this.detailsService.deletePermissionFromUser(maybeUser.getUsername(), x.getPermission()));
    }
}
