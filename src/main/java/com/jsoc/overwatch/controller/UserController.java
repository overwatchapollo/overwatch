/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.controller;

import com.jsoc.overwatch.Security.SecurityHelper;
import com.jsoc.overwatch.Security.permissions.SystemPermissionGenerator;
import com.jsoc.overwatch.controller.dto.ForbiddenException;
import com.jsoc.overwatch.controller.dto.NotFoundException;
import com.jsoc.overwatch.controller.dto.UnauthorisedException;
import com.jsoc.overwatch.controller.dto.UserDto;
import com.jsoc.overwatch.services.userManagement.CustomUserDetailsService;
import com.jsoc.overwatch.services.userManagement.RoleAssignmentService;
import com.jsoc.overwatch.services.userManagement.UserNotFoundException;
import com.jsoc.overwatch.services.userManagement.UserService;
import com.jsoc.overwatch.services.userManagement.model.RoleModel;
import com.jsoc.overwatch.services.userManagement.model.UserModel;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Objects;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * REST controller for users.
 */
@RestController
@RequestMapping("/api/users")
@PreAuthorize("isAuthenticated()")
@Slf4j
public class UserController {

    @Autowired
    private RoleAssignmentService roleAssignmentService;

    @Autowired
    private UserService userService;

    @Autowired
    private CustomUserDetailsService detailsService;

    private SystemPermissionGenerator generator = new SystemPermissionGenerator();

    @GetMapping("/names")
    public List<String> getUserList() {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.systemAdministrator().hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        return this.userService.getUsernames();
    }

    @GetMapping("/detailed")
    public List<UserDto> getDetailedList() {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.systemAdministrator().hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        return StreamEx.of(this.userService.getUsers())
            .map(this::from)
            .toList();
    }

    @GetMapping("/user/{user}")
    public UserDto getUser(@PathVariable("user") Long userId) throws UserNotFoundException {
        UserModel um = this.userService.getUserFromId(userId);
        return this.from(um);
    }

    /**
     * Map user model to DTO.
     * @param model User model to translate.
     * @return User DTO for front end.
     */
    private UserDto from(UserModel model) {
        User user = this.detailsService.loadUserById(model.getId());
        return UserDto.builder()
                .username(model.getUserName())
                .id(model.getId())
                .roleNames(StreamEx.of(this.roleAssignmentService.getRolesForUser(user))
                    .map(RoleModel::getName).toList()).build();
    }


    @GetMapping("/exist/{username}")
    public void doesUserExist(@PathVariable("username") String username) {
        try {
            this.detailsService.loadUserByUsername(username);
        } catch (UsernameNotFoundException ex) {
            throw new NotFoundException();
        }
    }

    @DeleteMapping("/user/{user}")
    public void deleteUser(HttpServletRequest request, HttpServletResponse response, @PathVariable("user") Long userId) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.systemAdministrator().hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        this.userService.removeUserById(userId);
        response.setStatus(204);
    }

    @PutMapping("/user/{user}")
    public void updateUser(HttpServletRequest request, @PathVariable("user") Long userId, @RequestBody UserDto user) throws UserNotFoundException {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);

        UserModel um = this.userService.getUserFromId(userId);
        boolean selfUpdate = Objects.equals(maybeUser.getUsername(), um.getUserName());
        boolean hasAuth = this.generator.systemAdministrator().hasPermission(maybeUser);

        if (selfUpdate) {
            this.selfUpdate(um.getUserName(), user);
        } else {
            log.info("admin update of user");
            if (hasAuth) {
                this.adminUpdate(um.getUserName(), user);
            } else {
                throw new ForbiddenException();
            }
        }
    }

    private void selfUpdate(String username, UserDto user) {
        if (user.getPassword() != null && !user.getPassword().isEmpty()) {
            this.userService.updatePassword(username, user.getPassword());
        }

        if (user.getEmail() != null && !user.getEmail().isEmpty()) {
            this.userService.updateEmail(username, user.getEmail());
        }
    }

    private void adminUpdate(String username, UserDto user) {
        checkArgument(user.getUsername() == null || user.getUsername().isEmpty(), "Can't update username");
        if (user.getPassword() != null && !user.getPassword().isEmpty()) {
            log.info("admin changing password");
            this.userService.updatePassword(username, user.getPassword());
        }

        if (user.getEmail() != null && !user.getEmail().isEmpty()) {
            log.info("admin changing email");
            this.userService.updateEmail(username, user.getEmail());
        }

        if (user.getRoleNames() != null && user.getRoleNames().size() > 0) {
            log.info("admin changing roles");
            this.roleAssignmentService.setRolesForUser(username, user.getRoleNames());
        }
    }
}
