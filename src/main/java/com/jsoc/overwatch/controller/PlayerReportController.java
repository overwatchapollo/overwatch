/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.controller;

import com.jsoc.overwatch.Security.permissions.ServerPermissionGenerator;
import com.jsoc.overwatch.config.Features;
import com.jsoc.overwatch.services.playerHistory.controllers.PlayerHistoryController;
import com.jsoc.overwatch.services.playerNote.PlayerNoteService;
import com.jsoc.overwatch.services.playerReport.PlayerReport;
import com.jsoc.overwatch.services.playerReport.PlayerReportService;
import com.jsoc.overwatch.controller.dto.NoteDto;
import com.jsoc.overwatch.controller.dto.PlayerNotFoundException;
import com.jsoc.overwatch.controller.steam.PlayerSummaryDto;
import com.jsoc.overwatch.services.playerNote.model.PlayerNote;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.data.util.Pair;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.togglz.core.manager.FeatureManager;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static com.jsoc.overwatch.Security.SecurityHelper.authoriseUserFromSession;

/**
 *
 * @author PaulR
 */
@RestController
@RequestMapping("/api/player")
@PreAuthorize("isAuthenticated()")
@Slf4j
public class PlayerReportController {
    @Autowired
    private PlayerReportService service;

    @Autowired
    private PlayerNoteService noteService;

    @Autowired
    private PlayerHistoryController childController;

    private ServerPermissionGenerator serverGenerator = new ServerPermissionGenerator();

    @Autowired
    private FeatureManager featureManager;

    @GetMapping("/by-name")
    @ResponseBody
    public List<PlayerReport> getPlayerByName(HttpServletRequest request, @RequestParam("name") String name) {
        User maybeUser = authoriseUserFromSession("investigate");
        log.info("player query {}", name);
        return this.service.getPlayersByName(maybeUser, name);
    }

    @GetMapping("/notes/{guid}")
    public Set<PlayerNote> getNotes(HttpServletRequest request, @PathVariable("guid") String guid) {
        User maybeUser = authoriseUserFromSession("viewNote");
        return this.noteService.getNotesForPlayer(guid, 1L);
    }

    @DeleteMapping("/note/{id}")
    public void remoteNote(HttpServletRequest request, @PathVariable("id") Long id) {
        User maybeUser = authoriseUserFromSession("deleteNote");
        this.noteService.removeNote(maybeUser, id);
    }

    @DeleteMapping("/noteDelete/{id}")
    public void deleteNote(HttpServletRequest request, @PathVariable("id") Long id) {
        authoriseUserFromSession("deleteNote");
        this.noteService.deleteNote(id);
    }

    @PostMapping("/notes/{guid}")
    public void addNote(HttpServletRequest request, @PathVariable("guid") String guid, @RequestBody NoteDto dto) {
        User maybeUser = authoriseUserFromSession("addNote");
        this.noteService.addNoteToPlayer(maybeUser, guid, 1L, dto.getNote());
    }

    @PutMapping("/notes/{note}")
    public void modifyNote(HttpServletRequest request, @PathVariable("note") Long note, @RequestBody NoteDto dto) {
        User maybeUser = authoriseUserFromSession("addNote");
        this.noteService.modifyNote(maybeUser, note, dto.getNote());
    }
    
    @GetMapping("/by-guid/{guid}")
    @ResponseBody
    public PlayerReport getPlayerByGuid(HttpServletRequest request, @PathVariable("guid") String guid) {
        if (this.featureManager.getFeatureState(Features.NEW_REPORT_ROUTE).isEnabled()) {
            log.info("New route.");
            return this.childController.getPlayerByGuid(request, guid);
        }

        User user = authoriseUserFromSession("investigate");
        log.info("Old route.");
        return this.service.getPlayerReport(user, guid)
            .orElseThrow(() -> new PlayerNotFoundException(guid));
    }
    
    @GetMapping("/by-guid-list")
    @ResponseBody
    public Map<String, Long> getPlayerNames(@RequestBody List<String> guids) {
        Map<String, Long> str = new HashMap<>();
        User user = authoriseUserFromSession("investigate");
        StreamEx.of(guids)
            .map(x -> this.service.getPlayerReport(user, x).map(y -> Pair.of(x, y.getId())))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .toList()
            .forEach(x -> str.put(x.getFirst(), x.getSecond()));
        return str;
    }

    @GetMapping("/steam/summary/{steamid}")
    public PlayerSummaryDto getPlayerSummary(@PathVariable("steamid") String steamId) {
        RestTemplateBuilder builder = new RestTemplateBuilder();
        RestTemplate rt = builder.build();
        return rt.getForObject("http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=A5F5E811599DE90093C0C881D39E326B&steamids=" + steamId, PlayerSummaryDto.class);
    }
}
