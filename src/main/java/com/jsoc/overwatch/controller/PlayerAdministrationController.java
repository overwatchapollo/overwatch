package com.jsoc.overwatch.controller;

import com.jsoc.overwatch.Security.SecurityHelper;
import com.jsoc.overwatch.Security.permissions.SystemPermissionGenerator;
import com.jsoc.overwatch.controller.dto.ForbiddenException;
import com.jsoc.overwatch.controller.dto.PlayerAdministratorDto;
import com.jsoc.overwatch.controller.dto.PlayerGuidDto;
import com.jsoc.overwatch.controller.dto.UnauthorisedException;
import com.jsoc.overwatch.services.playerAdminService.AdministrationRecord;
import com.jsoc.overwatch.services.playerAdminService.PlayerAdministratorService;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/admin/ingame")
@PreAuthorize("isAuthenticated()")
@Slf4j
public class PlayerAdministrationController {

    @Autowired
    private PlayerAdministratorService service;

    private SystemPermissionGenerator generator = new SystemPermissionGenerator();

    @GetMapping("/list/{serverMrid}")
    public List<PlayerAdministratorDto> getAdministratorList(HttpServletRequest request, @PathVariable("serverMrid") Long serverMrid) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.experimental().hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        return StreamEx.of(this.service.getAdminMapForServer(serverMrid)).map(PlayerAdministrationController::from).toList();
    }

    @PostMapping("/players/{serverMrid}")
    public void addAdministrator(HttpServletRequest request, @PathVariable("serverMrid") Long serverMrid, PlayerAdministratorDto dto) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.experimental().hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        this.service.setTeamIdToAdmin(serverMrid, dto.getGuid(), dto.getLevel(), dto.getUsername());
    }

    @DeleteMapping("/players/{serverMrid}")
    public void removeAdministrator(HttpServletRequest request, @PathVariable("serverMrid") Long serverMrid, @RequestBody PlayerGuidDto dto) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.experimental().hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        log.info("Removing admin from server {} with guid {}", serverMrid, dto.getGuid());
        this.service.removeAdmin(serverMrid, dto.getGuid());
    }

    private static PlayerAdministratorDto from(AdministrationRecord record) {
        return new PlayerAdministratorDto(record.getLevel().getValue(), record.getUsername(), record.getGuid(), record.getSteamId());
    }
}
