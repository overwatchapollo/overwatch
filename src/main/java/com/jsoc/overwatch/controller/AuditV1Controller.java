package com.jsoc.overwatch.controller;

import com.jsoc.overwatch.Security.SecurityHelper;
import com.jsoc.overwatch.Security.permissions.SystemPermissionGenerator;
import com.jsoc.overwatch.controller.dto.ForbiddenException;
import com.jsoc.overwatch.controller.dto.UnauthorisedException;
import com.jsoc.overwatch.services.audit.AuditService;
import com.jsoc.overwatch.services.audit.AuditableCategoryListDto;
import com.jsoc.overwatch.services.audit.AuditableInfo;
import com.jsoc.overwatch.controller.dto.AuditableInfoDto;
import com.jsoc.overwatch.services.audit.repository.AuditCategoryRepository;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/audit/v1")
@PreAuthorize("isAuthenticated()")
@Slf4j
public class AuditV1Controller {

    @Autowired
    private AuditService auditService;

    @Autowired
    private AuditCategoryRepository categoryRepository;

    private SystemPermissionGenerator generator = new SystemPermissionGenerator();


    @GetMapping("/logs")
    public List<AuditableInfoDto> getAuditLog() {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.systemAdministrator().hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        return StreamEx.of(this.auditService.getAuditLog())
            .reverseSorted((x, y) -> x.getTimestamp().isBefore(y.getTimestamp()) ? -1 : 1)
            .limit(100)
            .map(x -> this.transformAuditableInfo(x, this.categoryRepository.findById(x.getCategoryId()).map(y -> y.getCategory()).orElse("unknown")))
            .toList();
    }

    /**
     * Get all audit categories.
     * @return List of visible audit categories.
     */
    @GetMapping("/categories")
    public List<AuditableCategoryListDto> getCategories() {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.systemAdministrator().hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        // TODO filter categories by what user should be able to see.
        return StreamEx.of(this.categoryRepository.findAll().iterator())
            .map(x -> new AuditableCategoryListDto(x.getId(), x.getCategory(), new ArrayList<>()))
            .toList();
    }

    @GetMapping("/logs/{id}")
    public AuditableCategoryListDto getAuditCategory(@PathVariable("id") Long categoryId) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.systemAdministrator().hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        String categoryName = this.categoryRepository.findById(categoryId).map(x -> x.getCategory()).orElse("unknown");
        List<AuditableInfo> infos = this.auditService.getAuditCategory(categoryId);
        if (infos == null) {
            infos = new ArrayList<>();
        }
        List<AuditableInfoDto> dtos = StreamEx.of(infos).map(x -> this.transformAuditableInfo(x, categoryName)).toList();
        return new AuditableCategoryListDto(
                categoryId,
                categoryName,
                dtos);
    }


    private AuditableInfoDto transformAuditableInfo(AuditableInfo info, String categoryName) {
        String username = info.getInvoker() == null ? "ROOT" : info.getInvoker().getUsername();
        return AuditableInfoDto.builder()
            .invoker(username)
            .timestamp(info.getTimestamp())
            .category(categoryName)
            .message(info.getMessage()).build();
    }
}
