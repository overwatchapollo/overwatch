package com.jsoc.overwatch.controller.dto;

import lombok.Data;

@Data
public class RoleUpdateRequest {
    private Long userId;
    private Long roleId;
}
