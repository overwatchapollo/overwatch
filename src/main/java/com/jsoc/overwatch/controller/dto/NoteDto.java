package com.jsoc.overwatch.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class NoteDto {
    private Long id;
    private String note;
    private String createdBy;
    private String createdOn;
}
