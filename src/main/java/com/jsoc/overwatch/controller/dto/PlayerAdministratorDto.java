package com.jsoc.overwatch.controller.dto;

import lombok.Data;

@Data
public class PlayerAdministratorDto {
    private final int level;
    private final String username;
    private final String guid;
    private final String steamId;
}
