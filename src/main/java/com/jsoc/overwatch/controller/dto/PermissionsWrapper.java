package com.jsoc.overwatch.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class PermissionsWrapper<T> {
    private List<String> actions;
    private T data;
}
