/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Request to ban a player from the server which originated from the UI.
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class BanRequest {
    /**
     * Location of the ban request.
     */
    private Long serverLocation;
    
    /**
     * GUID of player to ban.
     */
    private String playerGuid;
    
    /**
     * Duration of the ban.
     */
    private Long duration;
    
    /**
     * Reason of the ban.
     */
    private String reason;

    /**
     * Session ID of the player.
     */
    private Long sessionId;
}
