package com.jsoc.overwatch.controller.dto;

import com.jsoc.overwatch.services.userManagement.model.UserModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private Long id;
    private String username;
    private String password;
    private String email;
    private List<String> roleNames;

    public UserDto(UserModel um) {
        this.id = um.getId();
        this.username = um.getUserName();
        this.password = um.getPassword();
        this.email = um.getEmail();
    }
}
