package com.jsoc.overwatch.controller.dto;

import com.jsoc.overwatch.services.playerReport.model.PlayerAliasRecord;
import com.jsoc.overwatch.services.playerReport.model.PlayerRecord;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Builder
@Data
public class PlayerRecordDto {
    private String playerGuid;
    private List<String> ipAddress;
    private Set<PlayerAliasRecord> aliasRecords;
    private List<KickRequestAuditDto> kickAudit;
    private Instant firstSeen;
    private Instant lastSeen;

    public static PlayerRecordDto fromPlayerRecord(PlayerRecord record) {
        return PlayerRecordDto.builder()
                .lastSeen(record.getLastSeen())
                .firstSeen(record.getFirstSeen())
                .aliasRecords(record.getAliasRecords())
                .playerGuid(record.getGuid())
                .ipAddress(Arrays.asList(record.getIpAddress()))
                .build();
    }
}
