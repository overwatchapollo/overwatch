package com.jsoc.overwatch.controller.dto;

import com.jsoc.overwatch.services.userManagement.model.RoleModel;
import lombok.Data;
import one.util.streamex.StreamEx;
import org.springframework.security.core.GrantedAuthority;

import java.util.Set;

@Data
public class RoleDto {
    private Long id;
    private String name;
    private Set<String> permissions;
    public RoleDto(RoleModel model) {
        this.id = model.getId();
        this.name = model.getName();
        this.permissions = StreamEx.of(model.getAuthoritySet()).map(x -> x.getAuthority()).toSet();
    }
}
