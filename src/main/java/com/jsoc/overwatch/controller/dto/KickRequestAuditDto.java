package com.jsoc.overwatch.controller.dto;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
public class KickRequestAuditDto {
    private String requester;
    private String serverName;
    private String message;
    private Instant timeStamp;
    private boolean successful;
}
