package com.jsoc.overwatch.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class FeatureToggleRequestDto {
    private String featureName;
    private Boolean enabled;
}
