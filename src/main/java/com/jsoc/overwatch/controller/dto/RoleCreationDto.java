package com.jsoc.overwatch.controller.dto;

import lombok.Data;

@Data
public class RoleCreationDto {
    private String roleName;
}
