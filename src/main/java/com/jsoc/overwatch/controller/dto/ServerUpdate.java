package com.jsoc.overwatch.controller.dto;

import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import com.jsoc.overwatch.services.connection.events.ServerConnectionLost;
import com.jsoc.overwatch.services.connection.events.ServerStateUpdateEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServerUpdate {
    private Long serverId;
    private boolean connected;
    private ServerConnectionStatus status;

    public ServerUpdate(ServerConnectionLost lost) {
        this.serverId = lost.getServerId();
        this.connected = false;
    }

    public ServerUpdate(ServerStateUpdateEvent event) {
        this.serverId = event.getServerGuid();
        this.connected = event.getStatus() == ServerConnectionStatus.CONNECTED;
        this.status = event.getStatus();
    }
}
