package com.jsoc.overwatch.controller.dto;

import lombok.Data;

@Data
public class PlayerGuidDto {
    private String guid;
}
