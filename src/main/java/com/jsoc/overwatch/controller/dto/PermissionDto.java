package com.jsoc.overwatch.controller.dto;

import lombok.Data;

@Data
public class PermissionDto {
    private String permission;
}
