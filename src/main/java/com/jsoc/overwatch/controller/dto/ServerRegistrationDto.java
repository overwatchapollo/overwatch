package com.jsoc.overwatch.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServerRegistrationDto {
    private String serverName;
    private String hostName;
    private Integer port;
    private String password;
    private Integer players;
}
