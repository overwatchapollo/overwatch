package com.jsoc.overwatch.controller.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class RestResponse<T> {
    private T data;
    private String error;
}
