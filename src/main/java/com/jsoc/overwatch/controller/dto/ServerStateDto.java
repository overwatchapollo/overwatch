package com.jsoc.overwatch.controller.dto;

import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import lombok.Data;

@Data
public class ServerStateDto {
    private Long serverId;
    private boolean connected;
    private boolean authenticated;
    private ServerConnectionStatus state;

    public ServerStateDto(Long serverId, ServerConnectionStatus status) {
        this.serverId = serverId;
        this.state = status;
        this.connected = status == ServerConnectionStatus.CONNECTED;
        this.authenticated = status == ServerConnectionStatus.CONNECTED;
    }
}
