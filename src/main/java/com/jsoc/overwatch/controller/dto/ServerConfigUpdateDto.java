package com.jsoc.overwatch.controller.dto;

import lombok.Data;

@Data
public class ServerConfigUpdateDto {
    private Long serverId;
    private String serverName;
    private String hostName;
    private Integer port;
    private String password;
    private Integer players;
}
