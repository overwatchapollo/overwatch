package com.jsoc.overwatch.controller.dto;

import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class ServerWrapper extends PermissionsWrapper<ServerConnectionDto> {
    private ServerConnectionStatus status;

}
