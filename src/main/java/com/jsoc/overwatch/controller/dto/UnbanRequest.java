package com.jsoc.overwatch.controller.dto;

import lombok.Data;

@Data
public class UnbanRequest {
    /**
     * Location of the ban request.
     */
    private Long serverLocation;

    /**
     * ID of ban to remove.
     */
    private Long banId;
}
