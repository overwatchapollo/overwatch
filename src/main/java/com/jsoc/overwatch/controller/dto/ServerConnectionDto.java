package com.jsoc.overwatch.controller.dto;

import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import lombok.*;
import org.springframework.data.annotation.Id;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ServerConnectionDto {
    @Id
    private Long serverId;

    private Long ownerId;

    private String serverName;

    private String ipAddress;

    private Integer port;

    private String password;

    private Boolean mock;

    private Integer maxPlayers;

    private Boolean connected;

    public ServerConnectionDto(ServerConnection con) {
        this.serverId = con.getId();
        this.serverName = con.getServerName();
        this.ipAddress = con.getIpAddress();
        this.port = con.getPort();
        this.password = con.getPassword();
        this.mock = con.getMock();
        this.maxPlayers = con.getMaxPlayers();
    }
}
