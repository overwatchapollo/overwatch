package com.jsoc.overwatch.controller.dto;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Builder
@Data
public class AuditableInfoDto {
    private String invoker;
    private Instant timestamp;
    private String category;
    private String message;
}
