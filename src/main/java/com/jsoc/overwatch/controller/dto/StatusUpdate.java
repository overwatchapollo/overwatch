package com.jsoc.overwatch.controller.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StatusUpdate {
    private Long serverId;
    private String serverName;
    private String version;
    private boolean authenticated;
    private boolean active;
    private int maxPlayers;
    private int currentPlayers;
}
