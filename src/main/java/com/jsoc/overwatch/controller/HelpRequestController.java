package com.jsoc.overwatch.controller;

import com.jsoc.overwatch.services.chatService.HelpRequestService;
import com.jsoc.overwatch.services.chatService.model.HelpRequestRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/helpRequest")
public class HelpRequestController {

    @Autowired
    private HelpRequestService service;

    @GetMapping("/requests")
    public List<HelpRequestRecord> getRequests() {
        return this.service.getEvents();
    }
}
