package com.jsoc.overwatch.controller;

import com.jsoc.overwatch.Security.SecurityHelper;
import com.jsoc.overwatch.Security.permissions.RolePermissionGenerator;
import com.jsoc.overwatch.Security.permissions.SystemPermissionGenerator;
import com.jsoc.overwatch.controller.dto.ForbiddenException;
import com.jsoc.overwatch.controller.dto.RoleUpdateRequest;
import com.jsoc.overwatch.controller.dto.UnauthorisedException;
import com.jsoc.overwatch.services.userManagement.CustomUserDetailsService;
import com.jsoc.overwatch.services.userManagement.RoleAssignmentService;
import com.jsoc.overwatch.services.userManagement.UserNotFoundException;
import com.jsoc.overwatch.services.userManagement.events.StaleUserSessionEvent;
import com.jsoc.overwatch.services.userManagement.events.UserRoleAssigned;
import com.jsoc.overwatch.services.userManagement.events.UserRoleRemoved;
import com.jsoc.overwatch.services.userManagement.model.UserModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/roles")
@PreAuthorize("isAuthenticated()")
@Slf4j
public class RoleController {
    @Autowired
    private CustomUserDetailsService details;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    private RoleAssignmentService roleAssignmentService;

    private SystemPermissionGenerator systemPermissionGenerator = new SystemPermissionGenerator();

    private RolePermissionGenerator rolePermissionGenerator = new RolePermissionGenerator();

    @PostMapping("/{roleId}/members")
    public void addRoleToUser(HttpServletRequest request, @PathVariable("roleId") Long roleId, @RequestBody RoleUpdateRequest roleRequest) throws UserNotFoundException {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.rolePermissionGenerator.addUser(roleId).hasPermission(maybeUser)) {
            if (!this.systemPermissionGenerator.systemAdministrator().hasPermission(maybeUser)) {
                throw new ForbiddenException();
            }
        }

        // TODO this will need to be allowed within an organisation.
        this.addRoleToUser(maybeUser, roleRequest.getUserId(), roleId);
    }

    @PostMapping("/v1/{roleId}/members/{username}")
    public void addRoleToUserName(HttpServletRequest request, @PathVariable("roleId") Long roleId, @PathVariable("username") String username) throws UserNotFoundException {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.rolePermissionGenerator.addUser(roleId).hasPermission(maybeUser)) {
            if (!this.systemPermissionGenerator.systemAdministrator().hasPermission(maybeUser)) {
                throw new ForbiddenException();
            }
        }

        UserModel details = this.details.getModelByUsername(username);
        this.addRoleToUser(maybeUser, details.getId(), roleId);
    }

    private void addRoleToUser(User requester, Long userId, Long roleId) throws UserNotFoundException {
        log.info("Adding role {} to user {}", roleId, userId);
        UserModel details = this.details.getUserFromId(userId);
        this.roleAssignmentService.mapUserToRole(details, roleId);

        this.publisher.publishEvent(new UserRoleAssigned(this, requester, userId, roleId));
        String targetUsername = details.getUserName();
        this.publisher.publishEvent(new StaleUserSessionEvent(this, targetUsername));
    }

    @DeleteMapping("/{roleId}/members")
    public void removeRoleFromUser(HttpServletRequest request, @PathVariable("roleId") Long roleId, @RequestBody RoleUpdateRequest roleRequest) throws UserNotFoundException {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.rolePermissionGenerator.removeUser(roleId).hasPermission(maybeUser)) {
            if (!this.systemPermissionGenerator.systemAdministrator().hasPermission(maybeUser)) {
                throw new ForbiddenException();
            }
        }

        // TODO this will need to be allowed within an organisation.
        this.removeRoleFromUser(maybeUser, roleRequest.getUserId(), roleId);
    }

    private void removeRoleFromUser(User requester, Long userId, Long roleId) throws UserNotFoundException {
        log.info("Deleting role {} to user {}", roleId, userId);
        this.roleAssignmentService.deleteUserToRoleMap(userId, roleId);
        this.publisher.publishEvent(new UserRoleRemoved(this, requester, userId, roleId));
        String targetUsername = this.details.getUserFromId(userId).getUserName();
        this.publisher.publishEvent(new StaleUserSessionEvent(this, targetUsername));
    }
}
