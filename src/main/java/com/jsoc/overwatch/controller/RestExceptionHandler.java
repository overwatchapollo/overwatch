package com.jsoc.overwatch.controller;

import com.jsoc.overwatch.controller.dto.ForbiddenException;
import com.jsoc.overwatch.services.banService.BanListNotFoundException;
import com.jsoc.overwatch.services.connection.ServerNotFoundException;
import com.jsoc.overwatch.services.organisationManagement.DuplicateNameException;
import com.jsoc.overwatch.services.organisationManagement.OrganisationNotFoundException;
import com.jsoc.overwatch.services.userManagement.DuplicateUsernameException;
import com.jsoc.overwatch.services.userManagement.RoleNotFoundException;
import com.jsoc.overwatch.services.userManagement.UserNotFoundException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<ApiError> handleException(ForbiddenException exception) {
        ApiError error = new ApiError(HttpStatus.FORBIDDEN, "Forbidden", "Forbidden");
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(error);
    }

    @ExceptionHandler(ServerNotFoundException.class)
    public ResponseEntity<ApiError> handleException(ServerNotFoundException exception) {
        ApiError error = new ApiError(HttpStatus.NOT_FOUND, "Could not find server " + exception.getId(), "Could not find server " + exception.getId());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }

    @ExceptionHandler(OrganisationNotFoundException.class)
    public ResponseEntity<ApiError> handleException(OrganisationNotFoundException exception) {
        ApiError error = new ApiError(HttpStatus.NOT_FOUND, "Could not find organisation " + exception.getId(), "Could not find organisation " + exception.getId());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<ApiError> handleException(UserNotFoundException exception) {
        ApiError error = new ApiError(HttpStatus.NOT_FOUND, "Could not find user " + exception.getId(), "Could not find user " + exception.getId());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }

    @ExceptionHandler(RoleNotFoundException.class)
    public ResponseEntity<ApiError> handleException(RoleNotFoundException exception) {
        ApiError error = new ApiError(HttpStatus.NOT_FOUND, "Could not find role " + exception.getId(), "Could not find role " + exception.getId());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }

    @ExceptionHandler(DuplicateNameException.class)
    public ResponseEntity<ApiError> handleException(DuplicateNameException exception) {
        ApiError error = new ApiError(HttpStatus.BAD_REQUEST, exception.getName() + " is already in use.", exception.getName() + " is already in use.");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    @ExceptionHandler(DuplicateUsernameException.class)
    public ResponseEntity<ApiError> handleException(DuplicateUsernameException exception) {
        ApiError error = new ApiError(HttpStatus.BAD_REQUEST, "Username `" + exception.getUsername() + "` is already in use.", "Username `" + exception.getUsername() + "` is already in use.");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    @ExceptionHandler(BanListNotFoundException.class)
    public ResponseEntity<ApiError> handleException(BanListNotFoundException exception) {
        ApiError error = new ApiError(HttpStatus.NOT_FOUND, "Could not find ban list for server " + exception.getServerId(), "Could not find ban list for server " + exception.getServerId());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
    }
}
