package com.jsoc.overwatch.controller;

import com.jsoc.overwatch.Security.SecurityHelper;
import com.jsoc.overwatch.Security.permissions.ServerPermissionGenerator;
import com.jsoc.overwatch.controller.dto.*;
import com.jsoc.overwatch.services.connection.RconConnectionPool;
import com.jsoc.overwatch.services.connection.ServerConnectionStatus;
import com.jsoc.overwatch.services.connection.games.arma.IRconConnection;
import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import com.jsoc.overwatch.services.playerList.ConnectedPlayerService;
import com.jsoc.overwatch.services.playerList.UnknownServerIdException;
import com.jsoc.overwatch.services.serverRegister.ServerRegisterService;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping("/api/server")
@PreAuthorize("isAuthenticated()")
@Slf4j
public class ServerStateController {
    @Autowired
    private RconConnectionPool pool;

    @Autowired
    private ConnectedPlayerService playerService;

    @Autowired
    private ServerRegisterService registerService;

    private ServerPermissionGenerator generator = new ServerPermissionGenerator();

    @GetMapping("/status/{serverMrid}")
    public StatusUpdate oldServerStatus(HttpServletRequest request, final @PathVariable("serverMrid") Long mrid) {
        return this.serverStatus(request, mrid);
    }

    /**
     * Get the status of a specific server.
     * @param request Servlet request.
     * @param mrid Server mRID.
     * @return Status report for server.
     */
    @GetMapping("/v1/status/{serverMrid}")
    public StatusUpdate serverStatus(HttpServletRequest request, final @PathVariable("serverMrid") Long mrid) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.view(mrid).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }
        ServerConnection con = this.registerService.getConfiguration(mrid).orElseThrow(IllegalArgumentException::new);
        IRconConnection rcon = this.pool.getConnection(mrid).orElseThrow(IllegalArgumentException::new);
        try {
            Collection<ConnectedPlayer> players = this.playerService.getPlayers(mrid);

            ServerConnectionStatus status = rcon.getStatus();
            return StatusUpdate.builder()
                    .active(status == ServerConnectionStatus.CONNECTED)
                    .authenticated(status == ServerConnectionStatus.CONNECTED)
                    .serverId(mrid)
                    .serverName(con.getServerName())
                    .version("1.217")
                    .maxPlayers(con.getMaxPlayers() == null ? 0 : con.getMaxPlayers())
                    .currentPlayers(StreamEx.of(players).filter(ConnectedPlayer::isConnected).toList().size())
                    .build();
        } catch (UnknownServerIdException ex) {
            throw new NotFoundException();
        }
    }

    @GetMapping("/v1/connectionPermissions")
    List<ServerWrapper> getServerPermissions(HttpServletRequest request) {
        User user = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        return StreamEx.of(this.getServers())
                .map(x -> {
                    List<String> permissions = new ArrayList<>();
                    Long id = x.getServerId();

                    Boolean auth = this.pool.getConnection(id).map(y -> y.getStatus() == ServerConnectionStatus.CONNECTED).orElse(false);
                    x.setConnected(auth);

                    if (this.generator.assignPermissions(id).hasPermission(user)) {
                        permissions.add("assignPermissions");
                    }

                    if (this.generator.unregister(id).hasPermission(user)) {
                        permissions.add("delete");
                    }

                    if (this.generator.restart(id).hasPermission(user)) {
                        permissions.add("restart");
                    }

                    if (this.generator.rcon(id).hasPermission(user)) {
                        permissions.add("rcon");
                    }

                    if (this.generator.update(id).hasPermission(user)) {
                        permissions.add("modify");
                    }

                    if (this.generator.broadcast(id).hasPermission(user)) {
                        permissions.add("broadcast");
                    }

                    if (this.generator.whisper(id).hasPermission(user)) {
                        permissions.add("whisper");
                    }

                    if (this.generator.owner(id).hasPermission(user)) {
                        permissions.add("owner");
                    }

                    if (this.generator.kick(id).hasPermission(user)) {
                        permissions.add("kick");
                    }

                    if (this.generator.ban(id).hasPermission(user)) {
                        permissions.add("ban");
                    }

                    if (this.generator.ban2w(id).hasPermission(user)) {
                        permissions.add("ban-2w");
                    }

                    if (this.generator.ban1w(id).hasPermission(user)) {
                        permissions.add("ban-1w");
                    }

                    if (this.generator.banRemove(id).hasPermission(user)) {
                        permissions.add("ban-remove");
                    }

                    if (this.generator.banRefresh(id).hasPermission(user)) {
                        permissions.add("ban-refresh");
                    }

                    if (this.generator.playerLookup(id).hasPermission(user)) {
                        permissions.add("investigate");
                    }
                    ServerWrapper output = new ServerWrapper();
                    output.setActions(permissions);
                    output.setData(x);
                    this.pool.getConnection(x.getServerId())
                        .map(IRconConnection::getStatus)
                        .ifPresent(y -> output.setStatus(y));
                    return output;
                })
                .toList();
    }

    private Set<ServerConnectionDto> getServers() {
        User user = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);

        List<ServerConnection> connections = this.registerService.getServers();
        Set<ServerConnectionDto> authedServers = new HashSet<>();

        connections.forEach(x -> {
            x.setPassword(null);
            if (this.generator.view(x.getId()).hasPermission(user)) {
                authedServers.add(new ServerConnectionDto(x));
            }
        });

        return authedServers;
    }
}
