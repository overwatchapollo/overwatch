package com.jsoc.overwatch.controller.steam;

import lombok.Data;

@Data
public class PlayerSummaryDto {
    private String steamid;
    private String personaname;
    private String avatar;
}
