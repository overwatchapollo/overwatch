package com.jsoc.overwatch.controller;

import com.jsoc.overwatch.Security.SecurityHelper;
import com.jsoc.overwatch.Security.permissions.ServerPermissionGenerator;
import com.jsoc.overwatch.controller.dto.ForbiddenException;
import com.jsoc.overwatch.controller.dto.UnauthorisedException;
import com.jsoc.overwatch.services.playerList.ConnectedPlayer;
import com.jsoc.overwatch.services.playerList.ConnectedPlayerService;
import com.jsoc.overwatch.services.playerList.UnknownServerIdException;
import com.jsoc.overwatch.controller.dto.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

@RestController
@RequestMapping("/api/rcon")
@PreAuthorize("isAuthenticated()")
@Slf4j
public class PlayerListController {

    @Autowired
    private ConnectedPlayerService playerList;

    private ServerPermissionGenerator generator = new ServerPermissionGenerator();

    @GetMapping("/players/{id}")
    public Collection<ConnectedPlayer> getPlayers(HttpServletRequest request, @PathVariable("id") Long id) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.view(id).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        try {
            return this.playerList.getPlayers(id);
        } catch (UnknownServerIdException ex) {
            throw new NotFoundException();
        }
    }

    @GetMapping("/archive/players/{id}")
    public Collection<ConnectedPlayer> getArchive(HttpServletRequest request, @PathVariable("id") Long id) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.view(id).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        try {
            return this.playerList.getPlayerArchive(id);
        } catch (UnknownServerIdException ex) {
            throw new NotFoundException();
        }
    }

    @PostMapping("/players/refresh/{id}")
    public void refreshPlayerList(HttpServletRequest request, @PathVariable("id") Long id) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.refreshPlayers(id).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        this.playerList.refreshList(id);
    }
}
