package com.jsoc.overwatch.controller;

public enum Pages {
    PLAYER_LIST,
    CHAT_LOG,
    SERVER_STATUS,
    SERVER_DASHBOARD,
    RAW_RCON,
    PLAYER_LOOKUP,
    MULTI_BAN,
    SERVER_MANAGEMENT,
    LOGIN,
    REGISTER,
    LOGOUT;
}
