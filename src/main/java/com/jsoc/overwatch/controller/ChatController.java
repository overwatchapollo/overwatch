/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jsoc.overwatch.controller;

import com.jsoc.overwatch.Security.SecurityHelper;
import com.jsoc.overwatch.Security.permissions.ServerPermissionGenerator;
import com.jsoc.overwatch.controller.dto.ForbiddenException;
import com.jsoc.overwatch.controller.dto.NotFoundException;
import com.jsoc.overwatch.controller.dto.ServerError;
import com.jsoc.overwatch.controller.dto.UnauthorisedException;
import com.jsoc.overwatch.services.chatService.ChatMessage;
import com.jsoc.overwatch.services.chatService.ChatCommandProxy;
import com.jsoc.overwatch.services.chatService.ChatMessageCache;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;
import java.util.OptionalLong;
import java.util.concurrent.ExecutionException;

/**
 * REST controller for querying chat.
 */
@RestController
@Slf4j
@RequestMapping("/api/chat")
@PreAuthorize("isAuthenticated()")
public class ChatController {

    @Autowired
    private ChatMessageCache messageMonitor;

    @Autowired
    private ChatCommandProxy proxy;

    private ServerPermissionGenerator generator = new ServerPermissionGenerator();

    @GetMapping("/log/{serverMrid}")
    public List<ChatMessage> getChatLog(@PathVariable("serverMrid") Long serverMrid, @RequestParam(value = "limit", required = false) Integer limit, @RequestParam(value = "lastSeen", required = false) Long lastSeenId) {

        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.view(serverMrid).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        List<ChatMessage> globalList = this.messageMonitor.getChatLog(serverMrid).orElseThrow(NotFoundException::new);

        int lastIndexId = 0;
        if (lastSeenId == null) {
            lastIndexId = Math.max(globalList.size() - 1, 0);
        } else {
            OptionalLong lastSeen = StreamEx.of(globalList).indexOf(x -> Objects.equals(x.getMessageId(), lastSeenId));
            if (lastSeen.isPresent()) {
                lastIndexId = (int)lastSeen.getAsLong();
            }
        }

        if (limit == null) {
            limit = 100;
        } else if (limit == 0) {
            return globalList;
        }

        int startIndex = Math.max(lastIndexId - limit, 0);
        return globalList.subList(startIndex, lastIndexId);
    }
    
    @PostMapping("/send/{serverMrid}/{id}")
    public void sendMessage(
        HttpServletRequest request,
        @PathVariable("id") String playerId,
        @PathVariable("serverMrid") Long serverMrid,
        @RequestBody String message) 
    {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.whisper(serverMrid).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        log.info("Whisper message received: {} : {}", serverMrid, playerId);

        try {
            this.proxy.whisper(serverMrid, Integer.parseInt(playerId), message).get();
        } catch (InterruptedException | ExecutionException e) {
            log.error("Could not whisper: {}", e.getMessage());
            throw new ServerError();
        }
    }
    
    @PostMapping("/sendAll/{serverMrid}")
    public void broadcastMessage(
        HttpServletRequest request,
        @PathVariable("serverMrid") Long serverMrid,
        @RequestBody String message)
    {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!this.generator.broadcast(serverMrid).hasPermission(maybeUser)) {
            throw new ForbiddenException();
        }

        try {
            this.proxy.broadcast(serverMrid, message).get();
        } catch (InterruptedException | ExecutionException e) {
            log.error("Could not broadcast: {}", e.getMessage());
            throw new ServerError();
        }
    }
}
