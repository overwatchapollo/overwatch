package com.jsoc.overwatch;

import com.jsoc.overwatch.services.userManagement.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@ComponentScan(basePackages = "com.jsoc.overwatch")
@EnableAutoConfiguration
public class OverwatchApplication extends SpringBootServletInitializer {

	@Autowired
	private CustomUserDetailsService userDetailsService;

	@Autowired
	private PasswordEncoder encoder;

	public static void main(String[] args) {
		SpringApplication.run(OverwatchApplication.class, args);
	}

	/**
	 * Set AuthenticationManager his UserDetailsService and PasswordEncoder so he can authenticate an user with a given username/password
	 * */
	@Autowired
	public void authenticationManager(AuthenticationManagerBuilder builder) throws Exception{
		builder.userDetailsService(this.userDetailsService).passwordEncoder(this.encoder);
	}
}
