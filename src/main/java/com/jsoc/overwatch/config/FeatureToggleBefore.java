package com.jsoc.overwatch.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static com.jsoc.overwatch.config.Features.ALWAYS_OFF;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FeatureToggleBefore {
    Features value() default ALWAYS_OFF;
}
