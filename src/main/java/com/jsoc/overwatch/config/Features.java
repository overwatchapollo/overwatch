package com.jsoc.overwatch.config;

import org.togglz.core.Feature;
import org.togglz.core.annotation.EnabledByDefault;
import org.togglz.core.context.FeatureContext;

public enum Features implements Feature {
    ALWAYS_OFF,
    NEW_REPORT_ROUTE,
    CHAT_ADMIN_HANDLER,
    CHAT_HELP_REQUEST;
    public boolean isActive() {
        return FeatureContext.getFeatureManager().isActive(this);
    }
}
