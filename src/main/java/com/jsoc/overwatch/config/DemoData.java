package com.jsoc.overwatch.config;

import com.jsoc.overwatch.services.userManagement.*;
import com.jsoc.overwatch.services.userManagement.model.RoleModel;
import com.jsoc.overwatch.services.userManagement.model.UserModel;
import com.jsoc.overwatch.services.userManagement.repository.RoleRepository;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Component
@Slf4j
@Profile("Prepopulate")
public class DemoData {
    @Autowired
    private CustomUserDetailsService detailsService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleAssignmentService roleAssignmentService;

    @Autowired
    private UserService userService;

    @EventListener
    @Transactional
    public void appReady(ApplicationReadyEvent event) throws Exception {
        this.addUsersAndRoles();
    }

    private void addUsersAndRoles() throws DuplicateUsernameException {
        log.info("Application started, injecting root user.");

        UserModel rootUser = this.createUser("root", "test@email.com", "mK?88s");
        UserModel overwatchUser = this.createUser("overwatch", "test@email.com", "E/7mFk");
        UserModel prUser = this.createUser("publicRelations", "test@email.com", "tr$2jM");
        UserModel memberUser = this.createUser("member", "test@email.com", "k3Fh;M");
        UserModel guestUser = this.createUser("guest", "test@email.com", "password");

        RoleModel rootRole = this.createRootRole();
        RoleModel overwatchRole = this.createOverwatchRole();
        RoleModel prRole = this.createPrRole();
        RoleModel memberRole = this.createMemberRole();
        RoleModel guestRole = this.createRole("guest");

        this.giveUserRoles(rootUser, rootRole);
        this.giveUserRoles(overwatchUser, overwatchRole);
        this.giveUserRoles(prUser, prRole, memberRole);
        this.giveUserRoles(memberUser, memberRole);
        this.giveUserRoles(guestUser, guestRole);

        UserDetails details = this.detailsService.loadUserByUsername("root");
        log.info("root auth: {}", details.getAuthorities());
    }

    private RoleModel createRootRole() {
        return this.createRole("Root Administrator", "testAdmin", "experimental", "serverAdmin", "assignRoles", "view:1", "view:2", "kick:1", "kick:2", "refresh:1", "refresh:2", "ban:1", "ban:2", "chat:1", "chat:2", "registerServer", "unregisterServer", "multiban");
    }

    private RoleModel createOverwatchRole() {
        return this.createRole("Overwatch", "assignRoles", "experimental", "siteAdmin", "investigate", "registerServer", "userManager", "serverAdmin", "view:1", "view:2", "kick:1", "kick:2", "refresh:1", "refresh:2", "ban:1", "ban:2", "chat:1", "chat:2");
    }

    private RoleModel createPrRole() {
        return this.createRole("Public Relations Team", "kick:1", "kick:2", "investigate");
    }

    private RoleModel createMemberRole() {
        return this.createRole("Member", "view:1", "view:2");
    }

    private RoleModel createRole(String roleName, String... authorities) {
        Set<GrantedAuthority> memberAuthority = new HashSet<>();
        for(String auth : authorities) {
            memberAuthority.add(new SimpleGrantedAuthority(auth));
        }

        return StreamEx.of(this.roleRepository.findAll().iterator())
            .findAny(x -> Objects.equals(x.getName(), roleName))
            .orElse(this.roleService.createRole(roleName, memberAuthority));
    }

    private UserModel createUser(String username, String email, String password) throws DuplicateUsernameException {
        try {
            return this.userService.getModelByUsername(username);
        } catch (UsernameNotFoundException ex) {
            return this.userService.createUser(username, email, password);
        }
    }

    private void giveUserRoles(UserModel userModel, RoleModel... roles) {
        User user = this.detailsService.loadUserById(userModel.getId());
        for(RoleModel role : roles) {
            this.roleAssignmentService.mapUserToRole(user, role.getId());
        }
    }
}
