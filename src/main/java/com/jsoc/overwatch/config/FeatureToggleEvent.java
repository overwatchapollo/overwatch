package com.jsoc.overwatch.config;

import lombok.Data;
import org.springframework.context.ApplicationEvent;
import org.springframework.security.core.userdetails.User;

@Data
public class FeatureToggleEvent extends ApplicationEvent {

    private final User caller;
    private final String feature;
    private final boolean value;

    public FeatureToggleEvent(Object source, User caller, String feature, boolean value) {
        super(source);
        this.caller = caller;
        this.feature = feature;
        this.value = value;
    }
}
