package com.jsoc.overwatch.config;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;

@Aspect
public class FeatureToggleBeforeAspect {

    @Around("@annotation(com.jsoc.overwatch.config.FeatureToggleBefore)")
    public void Join(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature)joinPoint.getSignature();
        Method method = signature.getMethod();
        FeatureToggleBefore annotation = method.getAnnotation(FeatureToggleBefore.class);
        if (annotation.value().isActive()) {
            joinPoint.proceed();
        }
    }
}
