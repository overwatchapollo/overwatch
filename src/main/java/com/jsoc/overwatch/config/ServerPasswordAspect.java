package com.jsoc.overwatch.config;

import com.jsoc.overwatch.services.serverRegister.models.ServerConnection;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

import java.util.Set;

@Configuration
@Aspect
@Slf4j
public class ServerPasswordAspect {
    @AfterReturning(pointcut = "execution(* com.jsoc.overwatch.services.connection.RconConnectionPool.getServers())", returning = "retVal")
    public void removeServerPassword(Set<ServerConnection> retVal) {
        retVal.forEach(x -> x.setPassword(null));
    }
}
