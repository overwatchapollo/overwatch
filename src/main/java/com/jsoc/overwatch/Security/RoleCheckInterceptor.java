package com.jsoc.overwatch.Security;

import com.jsoc.overwatch.services.userManagement.CustomUserDetailsService;
import com.jsoc.overwatch.services.userManagement.events.StaleUserSessionEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

@Slf4j
public class RoleCheckInterceptor implements HandlerInterceptor {

    private CustomUserDetailsService service;

    private Set<String> staleSessions = new CopyOnWriteArraySet<>();

    public RoleCheckInterceptor(CustomUserDetailsService service) {
        this.service = service;
    }

    @EventListener
    public void StaleUserListener(StaleUserSessionEvent event) {
        if(!this.staleSessions.contains(event.getUsername())) {
            log.info("adding stale session {}", event.getUsername());
            this.staleSessions.add(event.getUsername());
        }
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        Optional<User> maybeUser = SecurityHelper.getUserFromRequest();
        maybeUser.ifPresent(x -> {
            if (this.staleSessions.contains(x.getUsername())) {
                this.staleSessions.remove(x.getUsername());
                this.updateRoles(SecurityContextHolder.getContext().getAuthentication(), x);
            }
        });
        return true;
    }

    private void updateRoles(Authentication auth, User current) {
        log.info("Updating roles for user {}", current.getUsername());
        UserDetails details = this.service.loadUserByUsername((current.getUsername()));
        Authentication newAuth = new UsernamePasswordAuthenticationToken(auth.getPrincipal(), auth.getCredentials(),
                details.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(newAuth);
    }
}
