package com.jsoc.overwatch.Security.actions;

import lombok.Getter;

/**
 * Enum containing all actions which can be taken on a role.
 */
@Getter
public enum RoleActions {
    ALL("*", "All Role Actions for role ", "All role actions on role."),
    ADD_USER("add-user", "Add user for role ", "Add user to role."),
    REMOVE_USER("remove-user", "Remove user for role ", "Remove user from role."),
    ADD_ACTION("add-action", "Add action to role ", "Add action to role."),
    REMOVE_ACTION("remove-action", "Remove action from role ", "Remove action from role."),
    DELETE_ROLE("delete", "Delete role ", "Delete role."),
    VIEW("view", "View role ", "View role.");

    private final String permission;

    private final String readableName;

    private final String description;

    RoleActions(String permission, String readableName, String description) {
        this.permission = permission;
        this.readableName = readableName;
        this.description = description;
    }

    @Override
    public String toString() {
        return this.permission;
    }

    public String getName() {
        return this.readableName;
    }

    public String getDescription() {
        return this.description;
    }
}
