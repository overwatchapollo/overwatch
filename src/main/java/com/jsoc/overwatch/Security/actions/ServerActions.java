package com.jsoc.overwatch.Security.actions;

import lombok.Getter;

/**
 * Class containing all actions which can be taken on a server.
 */
@Getter
public enum ServerActions {
    /**
     * Wildcard, can do everything on this server.
     */
    ALL("*", "All actions on ", "Access to all actions "),

    /**
     * Build a role from this server.
     */
    BUILD_ROLE("build-role", "Build role on ", "Can build a role from server."),

    /**
     * Whisper private messages to users.
     */
    WHISPER("whisper", "Whisper on ", "Whisper to a player on the server."),

    /**
     * Broadcast messages to whole server.
     */
    BROADCAST("broadcast", "Broadcast to ", "Broadcast a message to all players on the server."),

    /**
     * Authority to act as owner of a server.
     */
    OWNER("owner", "Server owner of ", "Meant to flag player as owner of server."),

    /**
     * Issue permanent bans.
     */
    BAN("ban", "Permanent ban on ", "Permanent ban player on server."),

    /**
     * Issue bans of a week.
     */
    BAN_1W("ban-1w", "Ban for one week on ", "Ban player for one week."),

    /**
     * Issue bans of two weeks.
     */
    BAN_2W("ban-2w", "Ban for two weeks on ", "Ban player for two weeks."),

    /**
     * Remove ban from a player.
     */
    BAN_REMOVE("ban-remove", "Remove ban on ", "Remove ban from player on server."),

    /**
     * Refresh the ban list on a server.
     */
    BAN_REFRESH("ban-refresh", "Ban refresh on ", "Refresh the ban list from the server."),

    /**
     * Ban privileged member.
     */
    BAN_PRIVILEGED("ban-privileged", "Ban privileged players on ", "Allows the ban of privileged players."),

    /**
     * Edit ban on server.
     */
    BAN_EDIT("ban-edit", "Ban edit on ", "Edit bans on server."),

    /**
     * Kick player from server.
     */
    KICK("kick", "Kick on ", "Kick player from server."),

    /**
     * Lookup player data collected from server.
     */
    PLAYER_LOOKUP("player-lookup", "Lookup on ", "Use data from server for player lookup."),

    /**
     * Lookup protected players on server.
     */
    PRIVILEGED_LOOKUP("privileged-lookup", "Privileged lookup on ", ""),

    /**
     * Can unregister server.
     */
    UNREGISTER("unregister", "Delete server ", "Delete the server configuration."),

    /**
     * Can update server.
     */
    UPDATE("update", "Update configuration for ", "Update the server configuration."),

    /**
     * Refresh player list.
     */
    REFRESH_PLAYERS("refresh-players", "Refresh player list on server ", "Force a refresh of the player list on the server."),

    /**
     * Restart server.
     */
    RESTART("restart", "Restart server ", "Restart the server."),

    /**
     * Assign permissions on the server.
     */
    ASSIGN_PERMISSIONS("assign-permissions", "Assign permissions from server ", "Allows the assignment of permissions from this server to roles."),

    /**
     * Do player lookups on server.
     */
    INVESTIGATE("investigate", "Lookup from server ", "Use the data from the listed server for lookup."),

    /**
     * Raw RCON access.
     */
    RCON("rcon", "Rcon viewer ", "Access to the RCON console."),

    /**
     * Can view server status.
     */
    VIEW("view", "View server ", "View server");

    private final String permission;

    private final String name;

    private final String description;

    ServerActions(String permission, String name, String description) {
        this.permission = permission;
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return this.permission;
    }
}
