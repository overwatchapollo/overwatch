package com.jsoc.overwatch.Security.actions;

public enum OrganisationActions {
    VIEW("view"),
    DELETE("delete"),
    ADD_USER("user-add"),
    REMOVE_USER("user-remove"),
    ADD_NOTE("note-add"),
    DELETE_NOTE("note-delete"),
    VIEW_NOTE("note-view"),
    CREATE_ROLE("create-role"),
    DELETE_ROLE("delete-role");

    private String action;
    OrganisationActions(String action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return this.action;
    }
}
