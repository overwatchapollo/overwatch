package com.jsoc.overwatch.Security.actions;

public enum SystemActions {
    REGISTER("register"),
    FEATURE_TOGGLE("feature-toggle"),
    SYSTEM_ADMINISTRATOR("system-administrator"),
    EXPERIMENTAL("experimental");
    private final String permission;
    SystemActions(String permission) {
        this.permission = permission;
    }

    @Override
    public String toString() {
        return this.permission;
    }
}
