package com.jsoc.overwatch.Security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

@Component
@Slf4j
public class UserLoginListener implements HttpSessionBindingListener {
    @Override
    public void valueBound(HttpSessionBindingEvent event) {
        log.info("bound {}", event);
    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent event) {
        log.info("unbound {}", event);
    }
}
