package com.jsoc.overwatch.Security;

import com.jsoc.overwatch.controller.dto.ForbiddenException;
import com.jsoc.overwatch.controller.dto.UnauthorisedException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Objects;
import java.util.Optional;

public class SecurityHelper {
    public static HttpSession getSessionFromRequest(HttpServletRequest request) {
        return request.getSession(false);
    }

    public static Authentication getAuthenticationFromRequest() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public static Optional<User> getUserFromRequest() {
        Authentication auth = getAuthenticationFromRequest();
        if (auth ==  null || Objects.equals(auth.getClass(), AnonymousAuthenticationToken.class)) {
            return Optional.empty();
        }

        return Optional.ofNullable((User)auth.getPrincipal());
    }

    public static boolean userHasPermission(String permission) {
        Optional<User> maybeUser = SecurityHelper.getUserFromRequest();
        return maybeUser.map(x -> userHasPermission(x, permission))
            .orElse(false);
    }

    public static boolean userHasPermission(User user, String permission) {
        return user.getAuthorities().contains(new SimpleGrantedAuthority(permission));
    }

    public static User authoriseUserFromSession(String permission) {
        User maybeUser = SecurityHelper.getUserFromRequest().orElseThrow(UnauthorisedException::new);
        if (!maybeUser.getAuthorities().contains(new SimpleGrantedAuthority(permission))) {
            throw new ForbiddenException();
        }
        return maybeUser;
    }
}
