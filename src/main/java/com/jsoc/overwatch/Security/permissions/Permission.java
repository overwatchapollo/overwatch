package com.jsoc.overwatch.Security.permissions;

import lombok.Getter;
import one.util.streamex.StreamEx;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.regex.Pattern;

/**
 * A permission is meant to be a lock. The permission must be matched to unlock a resource.
 * The key is the {@link org.springframework.security.core.GrantedAuthority} which may have
 * wildcards in them.
 */
@Getter
public class Permission {
    /**
     * Permission string.
     */
    private String permission;

    private String name;

    private String description;

    /**
     * Initializes a new instance of the {@link Permission} class.
     * @param permission Permission string to use.
     */
    public Permission(String permission, String name, String description) {
        this.permission = permission;
        this.name = name;
        this.description = description;
    }

    /**
     * Checks if the user has an authority which matches this permission.
     * @param user user to check.
     * @return True if an authority matches this permission.
     */
    public boolean hasPermission(User user) {
        return StreamEx.of(user.getAuthorities())
            .findFirst(x -> Pattern.compile(x.getAuthority())
                .matcher(this.permission).matches())
            .isPresent();
    }

    /**
     * Checks if the user has an authority which matches this permission.
     * @param user user to check.
     * @return True if an authority matches this permission.
     */
    public boolean hasPermission(UserDetails user) {
        return StreamEx.of(user.getAuthorities())
                .findFirst(x -> Pattern.compile(x.getAuthority())
                        .matcher(this.permission).matches())
                .isPresent();
    }
}
