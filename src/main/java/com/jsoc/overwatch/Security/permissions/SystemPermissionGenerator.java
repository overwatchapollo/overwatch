package com.jsoc.overwatch.Security.permissions;

import com.jsoc.overwatch.Security.actions.SystemActions;

public class SystemPermissionGenerator extends PermissionGenerator {
    public SystemPermissionGenerator() {
        super(Domains.SYSTEM);
    }

    public Permission registerServer() {
        return this.build(SystemActions.REGISTER);
    }

    public Permission experimental() {
        return this.build(SystemActions.EXPERIMENTAL);
    }

    public Permission featureToggle() {
        return this.build(SystemActions.FEATURE_TOGGLE);
    }

    public Permission systemAdministrator() {
        return this.build(SystemActions.SYSTEM_ADMINISTRATOR);
    }

    private Permission build(SystemActions action) {
        String sb = this.domain + ":" + action;
        return new Permission(sb, "", "");
    }
}
