package com.jsoc.overwatch.Security.permissions;

public abstract class PermissionGenerator {
    protected Domains domain;
    public PermissionGenerator(Domains domain) {
        this.domain = domain;
    }

    protected Permission build(Long id, String action, String name, String description) {
        String sb = this.domain + ":" + id + ":" + action;
        return new Permission(sb, name, description);
    }

    protected Permission build(String id, String action, String name, String description) {
        String sb = this.domain + ":" + id + ":" + action;
        return new Permission(sb, name, description);
    }

    protected Permission buildWildcard( String action, String name, String description) {
        String sb = this.domain + ":" + "(\\d+)+" + ":" + action;
        return new Permission(sb, name, description);
    }
}
