package com.jsoc.overwatch.Security.permissions;

import com.jsoc.overwatch.Security.actions.OrganisationActions;

public class OrganisationPermissionGenerator extends PermissionGenerator {

    public OrganisationPermissionGenerator() {
        super(Domains.ORGANISATION);
    }

    public Permission addUser(Long orgId) {
        return this.build(orgId, OrganisationActions.ADD_USER);
    }

    public Permission removeUser(Long orgId) {
        return this.build(orgId, OrganisationActions.REMOVE_USER);
    }

    public Permission view(Long orgId) {
        return this.build(orgId, OrganisationActions.VIEW);
    }

    public Permission delete(Long orgId) {
        return this.build(orgId, OrganisationActions.DELETE);
    }

    public Permission createRole(Long orgID) {
        return this.build(orgID, OrganisationActions.CREATE_ROLE);
    }

    public Permission deleteRole(Long orgId) {
        return this.build(orgId, OrganisationActions.DELETE_ROLE);
    }

    private Permission build(Long id, OrganisationActions action) {
        String sb = this.domain + ":" + id + ":" + action;
        return new Permission(sb, "", "");
    }
}
