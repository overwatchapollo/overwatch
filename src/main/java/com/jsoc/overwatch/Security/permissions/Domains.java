package com.jsoc.overwatch.Security.permissions;

public enum Domains {
    ORGANISATION("organisation"),
    SERVER("server"),
    SYSTEM("system"),
    ROLE("role");
    private String domain;
    Domains(String domain) {
        this.domain = domain;
    }

    @Override
    public String toString() {
        return this.domain;
    }
}
