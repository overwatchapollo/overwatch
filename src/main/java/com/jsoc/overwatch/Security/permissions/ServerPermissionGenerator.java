package com.jsoc.overwatch.Security.permissions;

import com.jsoc.overwatch.Security.actions.ServerActions;

import java.util.regex.Pattern;

public class ServerPermissionGenerator extends PermissionGenerator {

    public ServerPermissionGenerator() {
        super(Domains.SERVER);
    }

    public Permission buildRole(Long serverId) {
        return this.build(serverId, ServerActions.BUILD_ROLE);
    }

    public Permission view() {
        return this.buildWildcard(ServerActions.VIEW);
    }

    public Permission view(Long serverId) {
        return this.build(serverId, ServerActions.VIEW);
    }

    public Permission view(String serverId) {
        return this.build(serverId, ServerActions.VIEW);
    }

    public Permission whisper(Long serverId) {
        return this.build(serverId, ServerActions.WHISPER);
    }

    public Permission ban(Long serverId) {
        return this.build(serverId, ServerActions.BAN);
    }

    public Permission ban1w(Long serverId) {
        return this.build(serverId, ServerActions.BAN_1W);
    }

    public Permission ban2w(Long serverId) {
        return this.build(serverId, ServerActions.BAN_2W);
    }

    public Permission banEdit(Long serverId) {
        return this.build(serverId, ServerActions.BAN_EDIT);
    }

    public Permission banPrivileged(Long serverId) {
        return this.build(serverId, ServerActions.BAN_PRIVILEGED);
    }

    public Permission banRemove(Long serverId) {
        return this.build(serverId, ServerActions.BAN_REMOVE);
    }

    public Permission banRefresh(Long serverId) {
        return this.build(serverId, ServerActions.BAN_REFRESH);
    }

    public Permission playerLookup(Long serverId) {
        return this.build(serverId, ServerActions.PLAYER_LOOKUP);
    }

    public Permission kick(Long serverId) {
        return this.build(serverId, ServerActions.KICK);
    }

    public Permission broadcast(Long serverId) {
        return this.build(serverId, ServerActions.BROADCAST);
    }

    public Permission owner(Long serverId) {
        return this.build(serverId, ServerActions.OWNER);
    }

    public Permission assignPermissions(Long serverId) {
        return this.build(serverId, ServerActions.ASSIGN_PERMISSIONS);
    }

    public Permission rcon() {
        return this.buildWildcard(ServerActions.RCON);
    }

    public Permission rcon(Long serverId) {
        return this.build(serverId, ServerActions.RCON);
    }

    public Permission investigate() {
        return this.buildWildcard(ServerActions.INVESTIGATE);
    }

    public Permission investigate(Long serverId) {
        return this.build(serverId, ServerActions.INVESTIGATE);
    }

    public Permission update(Long serverId) {
        return this.build(serverId, ServerActions.UPDATE);
    }

    public Permission unregister(Long serverId) {
        return this.build(serverId, ServerActions.UNREGISTER);
    }

    public Permission restart(Long serverId) {
        return this.build(serverId, ServerActions.RESTART);
    }

    public Permission refreshPlayers(Long serverId) {
        return this.build(serverId, ServerActions.REFRESH_PLAYERS);
    }

    private static final Pattern WILDCARD = Pattern.compile("server:(\\d+):\\*");
    private static final Pattern VIEW = Pattern.compile("server:(\\d+):view");
    private static final Pattern WHISPER = Pattern.compile("server:(\\d+):whisper");
    private static final Pattern BAN_PERM = Pattern.compile("server:(\\d+):ban$");
    private static final Pattern BAN_1W = Pattern.compile("server:(\\d+):ban-1w$");
    private static final Pattern BAN_2W = Pattern.compile("server:(\\d+):ban-2w$");
    private static final Pattern BAN_EDIT = Pattern.compile("server:(\\d+):ban-edit");
    private static final Pattern BAN_PRIV = Pattern.compile("server:(\\d+):ban-privileged");

    private static final Pattern BAN_REM = Pattern.compile("server:(\\d+):ban-remove");
    private static final Pattern BAN_REF = Pattern.compile("server:(\\d+):ban-refresh");

    private static final Pattern PLAYER_LOOKUP = Pattern.compile("server:(\\d+):player-lookup");

    private static final Pattern KICK = Pattern.compile("server:(\\d+):kick");
    private static final Pattern BROADCAST = Pattern.compile("server:(\\d+):broadcast");

    private static final Pattern OWNER = Pattern.compile("server:(\\d+):owner");

    private static final Pattern ASSIGN_PERMISSIONS = Pattern.compile("server:(\\d+):assign-permissions");

    private static final Pattern RCON = Pattern.compile("server:(\\d+):rcon");
    private static final Pattern INVESTIGATE = Pattern.compile("server:(\\d+):investigate");
    private static final Pattern UPDATE = Pattern.compile("server:(\\d+):update");
    private static final Pattern UNREGISTER = Pattern.compile("server:(\\d+):unregister");
    private static final Pattern RESTART = Pattern.compile("server:(\\d+):restart");
    private static final Pattern REFRESH_PLAYER = Pattern.compile("server:(\\d+):refresh-players");

    public static Permission from(String permission) {
        if (WILDCARD.matcher(permission).matches()) {
            return from(ServerActions.ALL, permission);
        }

        if (VIEW.matcher(permission).matches()) {
            return from(ServerActions.VIEW, permission);
        }

        if (WHISPER.matcher(permission).matches()) {
            return from(ServerActions.WHISPER, permission);
        }

        if (BAN_PERM.matcher(permission).matches()) {
            return from(ServerActions.BAN, permission);
        }

        if (BAN_1W.matcher(permission).matches()) {
            return from(ServerActions.BAN_1W, permission);
        }

        if (BAN_2W.matcher(permission).matches()) {
            return from(ServerActions.BAN_2W, permission);
        }

        if (BAN_EDIT.matcher(permission).matches()) {
            return from(ServerActions.BAN_EDIT, permission);
        }

        if (BAN_PRIV.matcher(permission).matches()) {
            return from(ServerActions.BAN_PRIVILEGED, permission);
        }

        if (BAN_REM.matcher(permission).matches()) {
            return from(ServerActions.BAN_REMOVE, permission);
        }

        if (BAN_REF.matcher(permission).matches()) {
            return from(ServerActions.BAN_REFRESH, permission);
        }

        if (PLAYER_LOOKUP.matcher(permission).matches()) {
            return from(ServerActions.PLAYER_LOOKUP, permission);
        }

        if (KICK.matcher(permission).matches()) {
            return from(ServerActions.KICK, permission);
        }

        if (BROADCAST.matcher(permission).matches()) {
            return from(ServerActions.BROADCAST, permission);
        }

        if (OWNER.matcher(permission).matches()) {
            return from(ServerActions.OWNER, permission);
        }

        if (ASSIGN_PERMISSIONS.matcher(permission).matches()) {
            return from(ServerActions.ASSIGN_PERMISSIONS, permission);
        }

        if (RCON.matcher(permission).matches()) {
            return from(ServerActions.RCON, permission);
        }

        if (INVESTIGATE.matcher(permission).matches()) {
            return from(ServerActions.INVESTIGATE, permission);
        }

        if (UPDATE.matcher(permission).matches()) {
            return from(ServerActions.UPDATE, permission);
        }

        if (UNREGISTER.matcher(permission).matches()) {
            return from(ServerActions.UNREGISTER, permission);
        }

        if (RESTART.matcher(permission).matches()) {
            return from(ServerActions.RESTART, permission);
        }

        if (REFRESH_PLAYER.matcher(permission).matches()) {
            return from(ServerActions.REFRESH_PLAYERS, permission);
        }

        throw new RuntimeException("");
    }

    private static Permission from(ServerActions action, String permission) {
        return new Permission(permission, action.getName(), action.getDescription());
    }

    private Permission build(String roleId, ServerActions action) {
        return this.build(roleId, action.getPermission(), action.getName(), action.getDescription());
    }

    private Permission build(Long roleId, ServerActions action) {
        return this.build(roleId, action.getPermission(), action.getName(), action.getDescription());
    }

    private Permission buildWildcard(ServerActions action) {
        return this.buildWildcard(action.getPermission(), action.getName(), action.getDescription());
    }
}
