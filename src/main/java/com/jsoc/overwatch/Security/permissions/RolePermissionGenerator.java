package com.jsoc.overwatch.Security.permissions;

import com.jsoc.overwatch.Security.actions.RoleActions;

import java.util.regex.Pattern;

public class RolePermissionGenerator extends PermissionGenerator {
    private static final Pattern WILDCARD = Pattern.compile("role:\\d+:\\*");
    private static final Pattern ADD_USER = Pattern.compile("role:\\d+:add-user");
    private static final Pattern REMOVE_USER = Pattern.compile("role:\\d+:remove-user");

    private static final Pattern VIEW = Pattern.compile("role:\\d+:view");

    private static final Pattern DELETE = Pattern.compile("role:\\d+:delete");

    private static final Pattern ADD_ACTION = Pattern.compile("role:\\d+:add-action");

    private static final Pattern REMOVE_ACTION = Pattern.compile("role:\\d+:remove-action");

    public RolePermissionGenerator() {
        super(Domains.ROLE);
    }

    public Permission all(Long roleId) {
        return this.build(roleId, RoleActions.ALL);
    }

    public Permission addUser(Long roleId) {
        return this.build(roleId, RoleActions.ADD_USER);
    }

    public Permission removeUser(Long roleId) {
        return this.build(roleId, RoleActions.REMOVE_USER);
    }

    public Permission view() {
        return this.buildWildcard(RoleActions.VIEW);
    }

    public Permission view(Long roleId) {
        return this.build(roleId, RoleActions.VIEW);
    }

    public Permission delete(Long roleId) {
        return this.build(roleId, RoleActions.DELETE_ROLE);
    }

    public Permission addAction(Long roleId) {
        return this.build(roleId, RoleActions.ADD_ACTION);
    }

    public Permission removeAction(Long roleId) {
        return this.build(roleId, RoleActions.REMOVE_ACTION);
    }

    private Permission build(Long roleId, RoleActions action) {
        return this.build(roleId, action.getPermission(), action.getName(), action.getDescription());
    }

    private Permission buildWildcard(RoleActions action) {
        return this.buildWildcard(action.getPermission(), action.getName(), action.getDescription());
    }

    public static Permission from(String permission) {
        if (WILDCARD.matcher(permission).matches()) {
            return from(RoleActions.ALL, permission);
        }

        if (ADD_USER.matcher(permission).matches()) {
            return from(RoleActions.ADD_USER, permission);
        }

        if (REMOVE_USER.matcher(permission).matches()) {
            return from(RoleActions.REMOVE_USER, permission);
        }

        if (VIEW.matcher(permission).matches()) {
            return from(RoleActions.VIEW, permission);
        }

        if (DELETE.matcher(permission).matches()) {
            return from(RoleActions.DELETE_ROLE, permission);
        }

        if (ADD_ACTION.matcher(permission).matches()) {
            return from(RoleActions.ADD_ACTION, permission);
        }

        if (REMOVE_ACTION.matcher(permission).matches()) {
            return from(RoleActions.REMOVE_ACTION, permission);
        }

        throw new RuntimeException("");
    }

    private static Permission from(RoleActions action, String permission) {
        return new Permission(permission, action.getName(), action.getDescription());
    }
}
